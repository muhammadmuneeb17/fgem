<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Membertypes Model
 *
 * @property \App\Model\Table\RegisterationsTable|\Cake\ORM\Association\HasMany $Registerations
 *
 * @method \App\Model\Entity\Membertype get($primaryKey, $options = [])
 * @method \App\Model\Entity\Membertype newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Membertype[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Membertype|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Membertype patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Membertype[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Membertype findOrCreate($search, callable $callback = null, $options = [])
 */
class MembertypesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('membertypes');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('Registerations', [
            'foreignKey' => 'membertype_id'
        ]);
        $this->hasMany('MemberTypeAmounts', [
            'foreignKey' => 'membertype_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('name')
                ->requirePresence('name', 'create')
                ->notEmpty('name');

        return $validator;
    }

}
