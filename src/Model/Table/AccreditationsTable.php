<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Accreditations Model
 *
 * @property \App\Model\Table\OmbudsmanaccredsTable|\Cake\ORM\Association\HasMany $Ombudsmanaccreds
 *
 * @method \App\Model\Entity\Accreditation get($primaryKey, $options = [])
 * @method \App\Model\Entity\Accreditation newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Accreditation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Accreditation|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Accreditation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Accreditation[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Accreditation findOrCreate($search, callable $callback = null, $options = [])
 */
class AccreditationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('accreditations');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('Ombudsmanaccreds', [
            'foreignKey' => 'accreditation_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        return $validator;
    }
}
