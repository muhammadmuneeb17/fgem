<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Ombudsmanswears Model
 *
 * @method \App\Model\Entity\Ombudsmanswear get($primaryKey, $options = [])
 * @method \App\Model\Entity\Ombudsmanswear newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Ombudsmanswear[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Ombudsmanswear|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Ombudsmanswear patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Ombudsmanswear[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Ombudsmanswear findOrCreate($search, callable $callback = null, $options = [])
 */
class OmbudsmanswearsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('ombudsmanswears');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');


        $this->belongsTo('Ombudsmen', [
            'foreignKey' => 'ombudsman_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('place')
                ->requirePresence('place', 'create')
                ->notEmpty('place');
        return $validator;
    }

}
