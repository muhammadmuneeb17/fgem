<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Medtypes Model
 *
 * @method \App\Model\Entity\Medtype get($primaryKey, $options = [])
 * @method \App\Model\Entity\Medtype newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Medtype[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Medtype|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Medtype patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Medtype[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Medtype findOrCreate($search, callable $callback = null, $options = [])
 */
class MedtypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('medtypes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('names')
            ->requirePresence('names', 'create')
            ->notEmpty('names');

        return $validator;
    }
}
