<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Ombudsmanlevels Model
 *
 * @property \App\Model\Table\OmbudsmenTable|\Cake\ORM\Association\BelongsTo $Ombudsmen
 * @property \App\Model\Table\LanguagesTable|\Cake\ORM\Association\BelongsTo $Languages
 *
 * @method \App\Model\Entity\Ombudsmanlevel get($primaryKey, $options = [])
 * @method \App\Model\Entity\Ombudsmanlevel newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Ombudsmanlevel[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Ombudsmanlevel|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Ombudsmanlevel patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Ombudsmanlevel[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Ombudsmanlevel findOrCreate($search, callable $callback = null, $options = [])
 */
class OmbudsmanlevelsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('ombudsmanlevels');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Ombudsmen', [
            'foreignKey' => 'ombudsman_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Languages', [
            'foreignKey' => 'language_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        
        return $rules;
    }
}
