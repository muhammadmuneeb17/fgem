<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Ombudsmans Model
 *
 * @property |\Cake\ORM\Association\BelongsTo $Registerations
 * @property \App\Model\Table\OmbudsmanaccredsTable|\Cake\ORM\Association\HasMany $Ombudsmanaccreds
 * @property \App\Model\Table\OmbudsmanformationsTable|\Cake\ORM\Association\HasMany $Ombudsmanformations
 * @property \App\Model\Table\OmbudsmanlevelsTable|\Cake\ORM\Association\HasMany $Ombudsmanlevels
 *
 * @method \App\Model\Entity\Ombudsman get($primaryKey, $options = [])
 * @method \App\Model\Entity\Ombudsman newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Ombudsman[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Ombudsman|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Ombudsman patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Ombudsman[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Ombudsman findOrCreate($search, callable $callback = null, $options = [])
 */
class OmbudsmansTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('ombudsmans');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Registerations', [
            'foreignKey' => 'registeration_id',
            'joinType' => 'INNER'
        ]);
        
        $this->hasMany('Ombudsmanaccreds', [
            'foreignKey' => 'ombudsman_id'
            , 'dependent' => true
        ]);
        $this->hasMany('Ombudsmanformations', [
            'foreignKey' => 'ombudsman_id'
            , 'dependent' => true
        ]);
        $this->hasMany('Ombudsmanlevels', [
            'foreignKey' => 'ombudsman_id'
            , 'dependent' => true
        ]);
        
        $this->hasMany('Ombudsmanswears', [
            'foreignKey' => 'ombudsman_id'
            , 'dependent' => true
        ]);
        $this->hasMany('Ombudsmanmeds', [
            'foreignKey' => 'ombudsman_id'
            , 'dependent' => true
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('mediator_question')
            //->requirePresence('mediator_question', 'create')
            ->allowEmpty('mediator_question', 'create');

        $validator
            ->scalar('presentation_text')
            //->requirePresence('presentation_text', 'create')
            ->allowEmpty('presentation_text', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['registeration_id'], 'Registerations'));

        return $rules;
    }
}
