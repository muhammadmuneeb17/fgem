<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * NextRenewalDate Model
 *
 * @method \App\Model\Entity\NextRenewalDate get($primaryKey, $options = [])
 * @method \App\Model\Entity\NextRenewalDate newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\NextRenewalDate[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\NextRenewalDate|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\NextRenewalDate patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\NextRenewalDate[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\NextRenewalDate findOrCreate($search, callable $callback = null, $options = [])
 */
class NextRenewalDateTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('next_renewal_date');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('year')
            ->allowEmpty('year');

        $validator
            ->integer('month')
            ->allowEmpty('month');

        $validator
            ->integer('day')
            ->allowEmpty('day');

        $validator
            ->datetime('created')
            ->allowEmpty('created');

        return $validator;
    }
}
