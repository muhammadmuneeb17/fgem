<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Registerations Model
 *
 * @property \App\Model\Table\MembertypesTable|\Cake\ORM\Association\BelongsTo $Membertypes
 *
 * @method \App\Model\Entity\Registeration get($primaryKey, $options = [])
 * @method \App\Model\Entity\Registeration newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Registeration[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Registeration|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Registeration patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Registeration[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Registeration findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RegisterationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('registerations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        
         $this->hasOne('Ombudsmans', [
            'foreignKey' => 'registeration_id'
             , 'dependent' => true
             //'joinType' => 'INNER'
             
        ]);
         
        $this->hasMany('Accounts', [
            'foreignKey' => 'registeration_id'
            , 'dependent' => true            
            //'joinType' => 'INNER'
        ]);
        $this->belongsTo('Membertypes', [
            'foreignKey' => 'membertype_id',
            'joinType' => 'INNER'
        ]);
        
         $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('profession')
            ->requirePresence('profession', 'create')
            ->notEmpty('profession');

        $validator
            ->scalar('username')
            ->requirePresence('username', 'create')
            ->notEmpty('username');

        $validator
            ->scalar('image')
            ->requirePresence('image', 'create')
            ->notEmpty('image');

        $validator
            ->scalar('password')
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        $validator
            ->scalar('address1')
            ->requirePresence('address1', 'create')
            ->notEmpty('address1');

        $validator
            ->scalar('address2')
            ->allowEmpty('address2');

        $validator
            ->integer('postalcode')
            ->requirePresence('postalcode', 'create')
            ->notEmpty('postalcode');

        $validator
            ->scalar('city')
            ->requirePresence('city', 'create')
            ->notEmpty('city');

        $validator
            ->scalar('pay')
            ->requirePresence('pay', 'create')
            ->notEmpty('pay');

        //        $validator
//            ->scalar('enterprise')
//            ->requirePresence('enterprise', 'create')
//            ->notEmpty('enterprise');

        $validator
            ->scalar('tel1')
            ->requirePresence('tel1', 'create')
            ->notEmpty('tel1');

        $validator
            ->scalar('tel2')
            ->allowEmpty('tel2');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->boolean('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->scalar('site_web')
            ->allowEmpty('site_web');

        return $validator;
    }
    
    public function validationRegCheck(Validator $validator) {
        $validator = $this->validationDefault($validator);
        $validator->remove('username');
        $validator->remove('image');
        $validator->remove('password');
        $validator->remove('status');
       
        return $validator;
    }
     public function validationRegremoveCheck(Validator $validator) {
        $validator = $this->validationDefault($validator);
        $validator->remove('profession');
        $validator->remove('address1');
        $validator->remove('postalcode');
        $validator->remove('city');
        $validator->remove('pay');
        $validator->remove('enterprise');
        $validator->remove('tel1');
        $validator->remove('username');
        $validator->remove('image');
        $validator->remove('password');
        $validator->remove('email');
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['membertype_id'], 'Membertypes'));

        return $rules;
    }
}
