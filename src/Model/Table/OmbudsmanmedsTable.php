<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Ombudsmanmeds Model
 *
 * @property \App\Model\Table\OmbudsmenTable|\Cake\ORM\Association\BelongsTo $Ombudsmen
 * @property \App\Model\Table\MedtypesTable|\Cake\ORM\Association\BelongsTo $Medtypes
 *
 * @method \App\Model\Entity\Ombudsmanmed get($primaryKey, $options = [])
 * @method \App\Model\Entity\Ombudsmanmed newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Ombudsmanmed[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Ombudsmanmed|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Ombudsmanmed patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Ombudsmanmed[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Ombudsmanmed findOrCreate($search, callable $callback = null, $options = [])
 */
class OmbudsmanmedsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('ombudsmanmeds');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Ombudsmen', [
            'foreignKey' => 'ombudsman_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Medtypes', [
            'foreignKey' => 'medtype_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
//        $rules->add($rules->existsIn(['ombudsman_id'], 'Ombudsmen'));
//        $rules->add($rules->existsIn(['medtype_id'], 'Medtypes'));

        return $rules;
    }
}
