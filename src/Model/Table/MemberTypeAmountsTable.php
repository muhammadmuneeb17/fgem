<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MemberTypeAmounts Model
 *
 * @property \App\Model\Table\MembertypesTable|\Cake\ORM\Association\BelongsTo $Membertypes
 *
 * @method \App\Model\Entity\MemberTypeAmount get($primaryKey, $options = [])
 * @method \App\Model\Entity\MemberTypeAmount newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MemberTypeAmount[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MemberTypeAmount|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MemberTypeAmount patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MemberTypeAmount[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MemberTypeAmount findOrCreate($search, callable $callback = null, $options = [])
 */
class MemberTypeAmountsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('member_type_amounts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Membertypes', [
            'foreignKey' => 'membertype_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('year')
            ->allowEmpty('year');

        $validator
            ->integer('amount')
            ->allowEmpty('amount');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['membertype_id'], 'Membertypes'));

        return $rules;
    }
}
