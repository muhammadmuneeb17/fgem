<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \App\Model\Table\MessagesTable|\Cake\ORM\Association\HasMany $Messages
 * @property \App\Model\Table\PaymentsTable|\Cake\ORM\Association\HasMany $Payments
 * @property \App\Model\Table\UploadsTable|\Cake\ORM\Association\HasMany $Uploads
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Contents', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('ContentTypes', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Sliders', [
            'foreignKey' => 'user_id'
        ]);
        
        $this->belongsTo('Roles', [
            'foreignKey' => 'role_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('MemberRoles', [
            'foreignKey' => 'user_id',
            'dependent' => true
        ]);
        $this->hasMany('Tickets', [
            'foreignKey' => 'user_id',
            'dependent' => true
        ]);
        $this->hasMany('TicketCalendars', [
            'foreignKey' => 'user_id',
            'dependent' => true
        ]);
        
        $this->hasOne('Registerations', [
            'foreignKey' => 'user_id',
            'dependent' => true
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

//        $validator
//            ->scalar('first_name')
//            ->requirePresence('first_name', 'create')
//            ->notEmpty('first_name');
//
//        $validator
//            ->scalar('last_name')
//            ->requirePresence('last_name', 'create')
//            ->notEmpty('last_name');

        $validator
            ->scalar('user_name')
            ->requirePresence('user_name', 'create')
            ->notEmpty('user_name');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->scalar('password')
            ->requirePresence('password', 'create')
            ->notEmpty('password');

//        $validator
//            ->scalar('phone')
//            ->requirePresence('phone', 'create')
//            ->notEmpty('phone');

//        $validator
//            ->scalar('address1')
//            ->requirePresence('address1', 'create')
//            ->notEmpty('address1');


//        $validator
//            ->scalar('state')
//            ->requirePresence('state', 'create')
//            ->notEmpty('state');

//        $validator
//            ->scalar('zip')
//            ->requirePresence('zip', 'create')
//            ->notEmpty('zip');

//        $validator
//            ->scalar('city')
//            ->requirePresence('city', 'create')
//            ->notEmpty('city');

        $validator
            ->integer('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');
        
        $validator
            ->scalar('role_id')
            ->requirePresence('role_id', 'create')
            ->notEmpty('role_id');
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    
    public function validationUserCheck(Validator $validator) {
        $validator = $this->validationDefault($validator);
        $validator->remove('user_name');
        
       
        return $validator;
    }
    public function validationUserCheck2(Validator $validator) {
        $validator = $this->validationDefault($validator);
        $validator->remove('first_name');
        $validator->remove('last_name');
        $validator->remove('email');
        $validator->remove('password');
        $validator->remove('user_name');
        
       
        return $validator;
    }
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['user_name'],
            "Nom d'utilisateur déjà utilisé"));
        $rules->add($rules->isUnique(['email'],
            "Adresse email déjà utilisée"));
        
        return $rules;
    }
    public function validationPassword(Validator $validator)
    {
        $validator
                ->add('new_password',[
                    'length' => [
                        'rule' => ['minLength',8],
                        'message' => 'Veuillez entrer 8 caractères au minimum '
                    ]
                ])
                ->add('new_password',[
                    'match' => [
                        'rule' => ['compareWith','confirm_password'],
                        'message' => 'Sorry! Password dose not match. Please try again!'
                    ]
                ])
                ->notEmpty('new_password');
        
        $validator
                ->add('confirm_password',[
                    'length' => [
                        'rule' => ['minLength',8],
                        'message' => 'Veuillez entrer 8 caractères au minimum '
                    ]
                ])
                ->add('confirm_password',[
                    'match' => [
                        'rule' => ['compareWith','new_password'],
                        'message' => 'Sorry! Password dose not match. Please try again!'
                    ]
                ])
                ->notEmpty('confirm_password');
        
        return $validator;
    }
}
