<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Emailtypes Model
 *
 * @property \App\Model\Table\EmailsTable|\Cake\ORM\Association\HasMany $Emails
 *
 * @method \App\Model\Entity\Emailtype get($primaryKey, $options = [])
 * @method \App\Model\Entity\Emailtype newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Emailtype[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Emailtype|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Emailtype patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Emailtype[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Emailtype findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class EmailtypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('emailtypes');
        $this->setDisplayField('email_type');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Emails', [
            'foreignKey' => 'emailtype_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('email_type')
            ->requirePresence('email_type', 'create')
            ->notEmpty('email_type');

        return $validator;
    }
}
