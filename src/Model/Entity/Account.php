<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Account Entity
 *
 * @property int $id
 * @property int $registeration_id
 * @property int $paymenttype_id
 * @property string $amount_paid
 * @property string $amount_due
 * @property \Cake\I18n\FrozenTime $amout_date
 * @property \Cake\I18n\FrozenTime $due_date
 * @property int $user_id
 * @property int $status
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Registeration $registeration
 * @property \App\Model\Entity\Paymenttype $paymenttype
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Comment[] $comments
 */
class Account extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
