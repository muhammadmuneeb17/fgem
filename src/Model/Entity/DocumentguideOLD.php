<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Documentguide Entity
 *
 * @property int $id
 * @property string $title
 * @property string $image
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $status
 * @property int $corporate_id
 * @property int $patient_id
 * @property int $physician_id
 * @property int $user_id
 * @property int $ordinal
 *
 * @property \App\Model\Entity\Corporate $corporate
 * @property \App\Model\Entity\Patient $patient
 * @property \App\Model\Entity\Physician $physician
 * @property \App\Model\Entity\User $user
 */
class Documentguide extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
