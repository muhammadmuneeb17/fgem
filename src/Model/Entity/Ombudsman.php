<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Ombudsman Entity
 *
 * @property int $id
 * @property int $registeration_id
 * @property string $mediator_question
 * @property string $presentation_text
 *
 * @property \App\Model\Entity\Ombudsmanaccred[] $ombudsmanaccreds
 * @property \App\Model\Entity\Ombudsmanformation[] $ombudsmanformations
 * @property \App\Model\Entity\Ombudsmanlevel[] $ombudsmanlevels
 */
class Ombudsman extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
