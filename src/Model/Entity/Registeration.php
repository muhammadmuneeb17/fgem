<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Registeration Entity
 *
 * @property int $id
 * @property int $membertype_id
 * @property string $profession
 * @property string $username
 * @property string $image
 * @property string $password
 * @property string $address1
 * @property string $address2
 * @property int $postalcode
 * @property string $city
 * @property int $pay
 * @property string $enterprise
 * @property string $tel1
 * @property string $tel2
 * @property string $email
 * @property bool $status
 * @property string $site_web
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Membertype $membertype
 */
class Registeration extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
}
