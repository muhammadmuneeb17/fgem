<div class="container" style="background-color: white">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-responsive table-striped table-bordered">
                <thead>
                    <tr>
                        <th scope="col">Sujet du message</th>
                        <th scope="col">Messages</th>
                        <th scope="col">Créé</th>
                        <th scope="col">Statut</th>
                        <!--<th scope="col">Action</th>-->
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($tickets as $ticket): ?>
                        <tr>
                            <!--<td><?= ($ticket->assigned_to) ? $users[$ticket->assigned_to] : '' ?></td>-->
                            <td><?= h($ticket->subject) ?></td>
                            <td><?= h($ticket->message) ?></td>

                            <td><?= date('Y-m-d', strtotime($ticket->created)) ?></td>
                            <?php
                            $status = '';
                            if ($ticket->status == 1) {
                                $status = '<b class="text-danger">Nouveau</b>';
                            }
                            if ($ticket->status == 2) {
                                $status = '<b class="text-warning">en attendant</b>';
                            }
                            if ($ticket->status == 3) {
                                $status = '<b class="text-success">fermé</b>';
                            }
                            ?>
                            <td><?= $status; ?></td>
<!--                            <td class="actions">
                                <a onclick="viewMessage(<?= $ticket->id; ?>)" class="btn btn-primary">Conversation</a>&nbsp; 
                                <a onclick="addMessage(<?= $ticket->id; ?>)" class="btn btn-primary">Add Message</a> 
                            </td>-->
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} de {{pages}}, affiche {{current}} entrée(s) sur un de total {{count}}')]) ?></p>
            </div>
        </div>
    </div>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New Message</h4>
                </div>
                <form action="" method="POST" id="addMessage">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" name="ticketId" value="">
                                <textarea name="message" class="form-control" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Send</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <div id="viewMessages" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Conversation</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12" id="injectConversation">

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    function addMessage(id) {
        $('#addMessage input[name=ticketId]').val(id);
        $('#myModal').modal('show');
    }
    function viewMessage(id) {
        $.ajax({
            url: "<?php echo $this->request->webroot . 'Tickets/viewMessages' ?>",
            type: "GET",
            data: 'ticketId=' + id,
            success: function (data)
            {
                $('#injectConversation').html(data);
                $('#viewMessages').modal('show');
            }

        });
    }

</script>
