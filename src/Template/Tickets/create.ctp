<style>
    .customButtons{
        border: 1px solid #147172;
        background: none;
        padding: 25px 90px;
        font-size: 20px;
        color: #147172;
        font-weight: bold;
    }
    .customButtons:hover{
        background: #147172;
        color: white;
        transition: all .4s ease;
        -webkit-transition: all .4s ease;
    }
    .customButtons:focus{
        background: #147172;
        color: white;
    }
    .activeMember{
        background: #147172;
        color: white;
    }
    .MyRadio{
        margin-right:10px !important;
    }

</style>
<div class="container bootstrap snippet" style="min-height: 502px;">
    <div class="row">
        <div class="col-sm-1">
        </div>
        <div class="col-sm-10">
            <div class="tab-content">
                <div class="tab-pane active" id="home">
                    <!--<hr>-->
                    <h3 style="text-align:center;margin: 30px 0;font-weight: bold;">Créer un nouveau ticket</h3>

                    <!--                  <form class="form" action="##" method="post" id="registrationForm">-->
                    <?= $this->Form->create($ticket) ?>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label for="sender_email"><h4>Email <small class="text-danger">*</small></h4></label>
                            <?php
                            echo $this->Form->control('sender_email', ['type' => 'email',
                                'class' => 'form-control', 'style' => 'height:40px;background-color: white;border: 1px solid #ccc;', 'label' => false, 'required' => 'required']);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label for="first-name"><h4>Sujet du message <small class="text-danger">*</small></h4></label>

                            <?php
                            echo $this->Form->control('subject', [
                                'class' => 'form-control', 'style' => 'height:40px;background-color: white;border: 1px solid #ccc;', 'label' => false, 'required' => 'required']);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label for="message"><h4>Message <small class="text-danger">*</small></h4></label>

                            <?php
                            echo $this->Form->control('message', ['type' => 'textarea',
                                'class' => 'form-control', 'style' => 'height:80px;', 'label' => false, 'required' => 'required']);
                            ?>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <button class="btn btn-lg btn-success btn-block" type="submit" id='register' style="border-radius: 0;border-color: #147172;background:#147172;  margin-top:43px;height:40px;"> Soumettre</button>
                        <br/>
                        <br/>
                    </div>

                    <?php echo $this->Form->end(); ?>

                </div><!--/tab-pane-->

            </div><!--/tab-content-->

        </div><!--/col-9-->
        <div class="col-sm-1">
        </div>
    </div>
</div>



