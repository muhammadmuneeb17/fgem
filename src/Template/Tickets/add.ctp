<div class="container" style="background-color: white;">
    <div class="row" style="margin: 10px 0;">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <br/>
            <?= $this->Form->create($ticket) ?>
            <?php
            echo $this->Form->control('subject', ['label'=>'Sujet du message','class' => 'form-control', 'required']);
            echo $this->Form->control('message', ['class' => 'form-control', 'required']);
            ?>
            <br/>
            <?=
            $this->Form->button(__('soumettre'), [
                'class' => 'btn btn-default'
            ])
            ?>
            <?= $this->Form->end() ?>
        </div>
        <div class="col-md-1"></div>

    </div>
</div>
