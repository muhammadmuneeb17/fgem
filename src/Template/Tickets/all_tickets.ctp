<div class="container" style="background-color: white">
    <?php
    if ($superviser) {
        echo $this->Form->create('');
        ?>
        <h4>Les filtres</h4>
        <div class="row" style="margin: 10px 0;">
            <div class="col-md-3">
                <?php echo $this->Form->control('user', ['type' => 'text', 'class' => 'form-control', 'label' => false, 'placeholder' => 'Recherche par nom']); ?>
            </div>
            <div class="col-md-3">
                <?php echo $this->Form->control('from_date', ['class' => 'form-control datepicker', 'label' => false, 'placeholder' => 'Partir de la date']); ?>
            </div>
            <div class="col-md-3">
                <?php echo $this->Form->control('to_date', ['class' => 'form-control datepicker', 'label' => false, 'placeholder' => 'à ce jour']); ?>
            </div>
            <div class="col-md-3">
                <?php echo $this->Form->control('type', ['class' => 'form-control', 'label' => false, 'options' => array('' => 'Sélectionner le statut', '1' => 'Nouveau', '2' => 'En attendant', '3' => 'Fermé')]); ?>
            </div>
            <br/>
            <br/>
            <div class="col-md-3">
                <?php echo $this->Form->control('type_of_message', ['class' => 'form-control', 'label' => false, 'options' => array('' => 'Sélectionner le type de message','email' => 'Email', 'vm' => 'Messagerie vocale')]); ?>
            </div>
            <div class="col-md-9">
                <?php echo $this->Form->submit('Chercher', ['class' => 'btn btn-primary']); ?>
            </div>
        </div>
        <?php
        echo $this->Form->end();
    }
    ?>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-responsive table-striped table-bordered" style="margin-top: 20px;">
                <thead>
                    <tr>
                        <th scope="col">Assigné à</th>
                        <th scope="col">Sujet du message</th>
                        <!--<th scope="col">Messages</th>-->
                        <th scope="col">Message Type</th>
                        <th><?= $this->Paginator->sort('status', 'Statut') ?></th>
                        <th><?= $this->Paginator->sort('created', 'créé') ?></th>
                        <th scope="col">Action</th>
                        <!--<th scope="col" class="actions">Actions</th>-->
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($tickets as $ticket):
                        ?>
                        <tr>
                            <td>
                                <?php
                                if ($superviser) {
                                    echo $this->Form->control('user_id', ['class' => 'changeUser form-control', 'ticketId' => $ticket->id, 'label' => false, 'empty' => 'Select User', 'options' => $users, 'value' => $ticket->assigned_to]);
                                } else {
                                    echo ($ticket->assigned_to) ? $users[$ticket->assigned_to] : '';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
//                                echo $ticket->wav_url;
                                $subject = (strlen($ticket->subject) > 40) ? substr($ticket->subject, 0, 40) . '...' : $ticket->subject;
                                if (!empty($ticket->wav_url)) {
                                    $explodesubject = explode(' ', $ticket->subject);
                                    $subject= end($explodesubject);
                                }
                                echo $subject;
                                ?>
                            </td>
                            <!--<td><a onclick="getMessage(<?= $ticket->id; ?>)" class="btn btn-info btn-sm">vue</a></td>-->
                            <td>
                                <?php
                                if ($ticket->type_of_message == 'web') {
                                    $typeOfMessage = 'Web';
                                }
//                                debug($ticket->type_of_message);
                                if ($ticket->type_of_message == 'email') {
                                    $typeOfMessage = 'Email';
                                }
                                if ($ticket->type_of_message == 'vm') {
                                    $typeOfMessage = 'Répondeur';
                                }
                                echo $typeOfMessage;
                                ?>
                            </td>
                            <?php
                            $status = '';
                            if ($ticket->status == 1) {
                                $status = '<b class="text-danger">Nouveau</b>';
                            }
                            if ($ticket->status == 2) {
                                $status = '<b class="text-warning">En attendant</b>';
                            }
                            if ($ticket->status == 3) {
                                $status = '<b class="text-success">Fermé</b>';
                            }
                            ?>
                            <td><?= $status; ?></td>
                            <td><?= date('Y-m-d h:i a', strtotime($ticket->created)) ?></td>
                            <td class="actions">
                                <!--<a href="<?php // echo $this->request->webroot . 'Tickets/viewTicket/' . $ticket->id;                                                       ?>" class="btn btn-primary">View</a>&nbsp;--> 
                                <?php if ($superviser) { ?>
                                    <a href="<?php echo $this->request->webroot . 'Tickets/deleteTicket/' . $ticket->id; ?>" class="btn btn-danger" onclick='return confirm("Êtes-vous sûr de vouloir supprimer le ticket?")'>Effacer</a>&nbsp; 
                                <?php } ?>
                                <a href="<?php echo $this->request->webroot . 'Tickets/editTicket/' . $ticket->id; ?>" class="btn btn-primary">vue</a>&nbsp; 
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} de {{pages}}, affiche {{current}} entrée(s) sur un de total {{count}}')]) ?></p>
            </div>
        </div>
    </div>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New Message</h4>
                </div>
                <form action="" method="POST" id="addMessage">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" name="ticketId" value="">
                                <textarea name="message" class="form-control" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Send</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <div id="viewMessages" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Conversation</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12" id="injectConversation">

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <div id="loadMessage" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Message complet</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12" id="injectmessage">

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    function addMessage(id) {
        $('#addMessage input[name=ticketId]').val(id);
        $('#myModal').modal('show');
    }
    function getMessage(id) {
        $.ajax({
            url: "<?php echo $this->request->webroot . 'Tickets/getMessage' ?>",
            type: "GET",
            data: 'ticketId=' + id,
            success: function (data)
            {
                $('#injectmessage').html(data);
                $('#loadMessage').modal('show');
            }

        });
    }
    function viewMessage(id) {
        $.ajax({
            url: "<?php echo $this->request->webroot . 'Tickets/viewMessages' ?>",
            type: "GET",
            data: 'ticketId=' + id,
            success: function (data)
            {
                $('#injectConversation').html(data);
                $('#viewMessages').modal('show');
            }

        });
    }
    $(function () {
        $('.changeUser').change(function () {
            var input = $(this);
            if (confirm("Êtes-vous sûr de vouloir changer le gestionnaire de tickets")) {
                $.ajax({
                    url: "<?php echo $this->request->webroot . 'Tickets/changeUser' ?>",
                    type: "GET",
                    data: 'ticketId=' + input.attr('ticketId') + '&value=' + input.val(),
                    success: function (data)
                    {
                        if (data == 1) {

                            input.parent('div').append('<p style="color:green">Ticket attribué avec succès</p>').fadeIn('slow');
                            input.siblings('p').fadeOut(3000);

                        }

                    }

                });
            }
        });

    });

</script>
