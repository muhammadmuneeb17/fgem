<div class="container" style="background-color: white;padding: 10px;">
    <a id="mybutton" class="btn btn-primary pull-right">retourner</a>
    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
        <li class="nav-item active">
            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Message reçu</a>
        </li>
        <li class="nav-item">
            <a onclick="" class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Traitement de la demande</a>
        </li>
    </ul>
    <?php echo $this->Form->create($ticket, ['id' => 'ticketEditForm']); ?>
    <div class="col-md-12" style="margin-top: 10px;">
        <?php echo $this->Form->submit('Enregistrer', ['class' => 'btn btn-success pull-right']); ?>

    </div>
    <?php if ($ticket->wav_url) { ?>
        <h4>Enregistrement Audio</h4>
        <audio controls>
            <source src="<?php echo $this->request->webroot . 'email_attachments/' . $ticket->wav_url; ?>" type="audio/mpeg">
            Your browser does not support the audio element.
        </audio>
    <?php } ?>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
            <div class="row">
                <div class="col-md-6">
                    <?php echo $this->Form->control('status', ['label' => 'Statut', 'class' => 'form-control', 'options' => array('2' => 'En attendant', '3' => 'Fermé')]); ?>
                </div>
                <div class="col-md-6">
                    <br/>
                    <?php echo $this->Form->control('notify_supervisor', ['type' => 'checkbox', 'label' => 'Aviser le superviseur']); ?>
                </div>
            </div>
            <hr style="border-color: lightblue !important;" />
            <div class="row">
                <div class="col-md-6">
                    <?php echo $this->Form->control('sender_email', ['type' => 'email', 'label' => "Email de l'expéditeur", 'class' => 'form-control']); ?>

                </div>
                <div class="col-md-6">
                    <?php echo $this->Form->control('contact', ['label' => "Numéro de contact", 'class' => 'form-control']); ?>

                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?php echo $this->Form->control('request_type', ['label' => 'Type de demande', 'class' => 'form-control', 'options' => $Medtypes]); ?>
                    <div style="display: none;padding: 0" id="other_request_type_div">
                        <?php echo $this->Form->control('other_request_type', ['label' => 'Autre type de demande', 'class' => 'form-control']); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <?php echo $this->Form->control('action_taken', ['label' => 'Action prise', 'class' => 'form-control', 'options' => array('' => "Sélectionnez l'action", 'transfered' => "Transféré à d'autres plus qualifiés", 'call' => 'Appel', 'meeting' => 'Réunion', 'others' => 'Autres')]); ?>
                    <!--            <div style="display: none;padding: 0" id="transfered_to">
                    <?php // echo $this->Form->control('assigned_to', ['label' => 'Transférer à', 'class' => 'form-control', 'empty' => 'sélectionner les utilisateurs', 'options' => $users, 'value' => '']); ?>
                                </div>-->
                    <div style="display: none;padding: 0" id="other_action_taken_div">
                        <?php echo $this->Form->control('other_action_taken', ['label' => 'Autre action entreprise', 'class' => 'form-control']); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <?php echo $this->Form->control('type_of_message', ['class' => 'form-control', 'label' => 'Type de message', 'options' => array('' => 'Sélectionner le type de message', 'email' => 'Email', 'vm' => 'Messagerie vocale'),'disabled']); ?>

                </div>
                <div class="col-md-6">
                    <?php echo $this->Form->control('subject', ['label' => 'Sujet du message', 'class' => 'form-control', 'disabled']); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php echo $this->Form->control('description', ['label' => 'la description', 'class' => 'form-control']); ?>

                </div>
            </div>
        </div>
        <div class="tab-pane fade active in" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
            <div class="row">
                <div class="col-md-12">
                    <iframe style="width: 100%;margin-top: 10px;" src="<?php echo $this->request->webroot . 'Tickets/getMessage?ticketId=' . $ticket->id ?>"></iframe>
                </div>    
            </div>    
        </div>
    </div>

    <div class="row">
        <div class="col-md-12" style="margin-top: 10px;">
            <?php echo $this->Form->submit('Enregistrer', ['class' => 'btn btn-success pull-right']); ?>

        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<script>
    $(function () {
        var form_original_data = $("#ticketEditForm").serialize();

        $("#mybutton").click(function () {
            if ($("#ticketEditForm").serialize() != form_original_data) {
                if (confirm('Etes-vous sûr de vouloir quitter sans sauvegarder?')) {
                    window.history.back();
                }
            } else {
                window.history.back();
            }
        });

        $('select[name=request_type]').change(function () {
            if ($(this).val() == 'others') {
                $('#other_request_type_div').show();
                $('input[name=other_request_type]').prop('required', true);
            } else {
                $('#other_request_type_div').hide();
                $('input[name=other_request_type]').prop('required', false);


            }
        });
        $('select[name=action_taken]').change(function () {
            if ($(this).val() == 'others') {
                $('#other_action_taken_div').show();
                $('input[name=other_action_taken]').prop('required', true);
            } else {
                $('#other_action_taken_div').hide();
                $('input[name=other_action_taken]').prop('required', false);

            }
//            if ($(this).val() == 'transfered') {
//                $('#transfered_to').show();
//                $('select[name=assigned_to]').prop('required', false);
//
//            } else {
//                $('#transfered_to').hide();
//                $('select[name=assigned_to]').prop('required', false);
//
//            }
        });
        $('select[name=status]').change(function () {
            if ($(this).val() == 3) {
                $('select[name=request_type]').prop('required', true);
                $('select[name=action_taken]').prop('required', true);
                $('input[name=subject]').prop('required', true);
                $('input[name=sender_email]').prop('required', true);
                $('input[name=contact]').prop('required', true);
                $('textarea[name=message]').prop('required', true);
                $('textarea[name=description]').prop('required', true);
            } else {
                $('select[name=request_type]').prop('required', true);
                $('select[name=action_taken]').prop('required', true);
                $('input[name=subject]').prop('required', true);
                $('input[name=sender_email]').prop('required', false);
                $('input[name=contact]').prop('required', false);
                $('textarea[name=message]').prop('required', true);
                $('textarea[name=description]').prop('required', true);
            }
        });
    });
</script>