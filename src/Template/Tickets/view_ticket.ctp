
<div class="container" style="background-color: white;padding: 10px;">
    <div class="row">
        <div class="col-md-12" style="text-align: right;">
            <a onclick="addMessage(<?= $ticket->id; ?>)" class="btn btn-primary">Add Reply</a> 

        </div>
        <div class="col-md-12">
            <h4><b>Subject: </b><?= $ticket->subject; ?></h4>
            <table class="table table-bordered table-srtiped table-responsive">
                <tr>
                    <th>Type</th>
                    <th>User</th>
                    <th>Message</th>
                    <th>Created</th>
                </tr>
                <?php
                foreach ($ticket->ticket_conversations as $conversation):
                    $class = 'adminResponse';
                    $role = "Customer";

                    if ($conversation->created_by == $this->request->session()->read('Auth.User.id')) {
                        $role = "Admin";
                        $class = 'customerResponse';
                    }
                    ?>
                    <tr class="<?= $class; ?>">
                        <td><?= $role; ?></td>
                        <td><?= ucwords($users[$ticket->user_id]); ?></td>
                        <td><?= $conversation->message; ?></td>
                        <td><?= $conversation->created->format('Y-m-d'); ?></td>
                    </tr>
                <?php endforeach; ?>
                <tr class="adminResponse">
                    <td>Customer</td>
                    <td><?= ucwords($users[$ticket->user_id]); ?></td>
                    <td><?= $ticket->message; ?></td>
                    <td><?= $ticket->created->format('Y-m-d'); ?></td>
                </tr>

            </table>

        </div>
    </div>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New Message</h4>
                </div>
                <form action="" method="POST" id="addMessage">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" name="ticketId" value="">
                                <textarea name="message" class="form-control" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Send</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<script>
    function addMessage(id) {
//        alert(id);
        $('#addMessage input[name=ticketId]').val(id);
        $('#myModal').modal('show');
    }
</script>
