<style>

    .buttons-html5 {
        border-radius: 0px; 
        margin-left: 10px;
        width: 80px;

        color:white;
        background: #3c8dbc;
        border-color: #367fa9;
    }

    .buttons-print{
        border-radius:3px; 
        margin-left: 10px;
        width: 40px;

        color:white;
        background: #3c8dbc;
        border-color: #367fa9;
    }
</style>
<!-- JQuery DataTable Css -->
<link href="<?php echo $this->request->webroot; ?>jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<div class="container-fluid content-container" style="">
    <div class="row">
        <?php ?>
        <div id="no-more-tables" class="table-responsive">
            <p class="pull-left info-text">Exportation </p>

            <table class="table table-bordered table-striped table-hover dataTable js-exportable">

                <thead>
                    <tr>
                        <th scope="col">Nom complet</th>
                        <th scope="col">Permanent</th>
                        <th scope="col">Suppléant</th>
                        <th>Disponible depuis</th>
                        <th>Calendrier récent</th>
                        <th>Prochain horaire</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($tickets as $ticket):
//                        debug($tickets);
                        ?>
                        <tr>
                            <td>
                                <?php
                                echo ucwords($ticket['name']);
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $ticket['as_primary'];
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $ticket['as_secondary'];
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $ticket['available_from'];
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $ticket['last_schedule'];
                                ?>
                            </td>
                            <td>
                                <?php
                                echo ($ticket['next_schedule'])?$ticket['next_schedule']:"<span style='color:red;'>Pas d'horaire</span>";
                                ?>
                            </td>

                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            </form>
        </div>
    </div>

    <br />

</div>

<script type="text/javascript">
    $(".toggle-state").on("change", function () {
        if ($(this).is(":checked")) {
            window.location.assign("<?php echo $this->request->webroot ?>Accounts/report?showdetails=1");
        } else {
            window.location.assign("<?php echo $this->request->webroot ?>Accounts/report");
        }
    });
</script>

<!-- Jquery DataTable Plugin Js -->
<script src="<?php echo $this->request->webroot; ?>jquery-datatable/jquery.dataTables.js"></script>
<script src="<?php echo $this->request->webroot; ?>jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?php echo $this->request->webroot; ?>jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="<?php echo $this->request->webroot; ?>jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="<?php echo $this->request->webroot; ?>jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="<?php echo $this->request->webroot; ?>jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="<?php echo $this->request->webroot; ?>jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="<?php echo $this->request->webroot; ?>jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="<?php echo $this->request->webroot; ?>jquery-datatable/extensions/export/buttons.print.min.js"></script>
<script src="<?php echo $this->request->webroot; ?>jquery-datatable/jquery-datatable.js"></script>