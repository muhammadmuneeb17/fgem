<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Account[]|\Cake\Collection\CollectionInterface $accounts
  */

$array = explode('/', $_SERVER['REQUEST_URI']);
$accId = end($array);
?>
<div class="panel-head-info">
    <p class="pull-left info-text">Paiement - <strong><?php echo $user['user']['first_name'];echo " " ;?><?php echo $user['user']['last_name']; ?></strong></p>
    <!--<a href="<?php // echo $this->request->webroot ?>pages" style="font-size:14px;" class="btn btn-primary btn-blue-custom pull-right text-info"><i class="fa fa-angle-left"></i> Go Back</a>-->
    <div class="clearfix"></div>
    <?php 
    //if($user['user']['status'] == 2){
    ?>

    <a href="<?php  echo $this->request->webroot ?>accounts/add/<?php echo $accId;?>" class="btn btn-success"><i class="fa fa-plus"></i> Ajouter le Paiement reçu</a>
    <?php //} 
    ?>

    <a href="<?php echo $this->request->webroot ?>registerations/index/<?php echo $accId;?>" style="font-size:14px;" class="btn btn-primary btn-blue-custom pull-right text-info"><i class="fa fa-angle-left"></i> Précédent</a>

    <!--     <a href="<?php echo $this->request->webroot ?>accounts/export/<?php echo $accId;?>" style="font-size:14px; margin-right: 10px; margin-top: 10px;" class="btn btn-primary btn-green-custom pull-right text-info"> Export</a>
-->
    <?php
    if($user['user']['status'] == 2){
        if ($user['status']) {
            echo $this->Html->link(__('Active'), array('controller' => 'registerations', 'action' => 'change_status', $user['id'], 0), array('class' => 'btn btn-success', 'onclick' => "return confirm('Ce compte est actif voulez-vous l'inactiver')", 'escape' => false));
        } else {
            echo $this->Html->link(__('Inactif'), array('controller' => 'registerations', 'action' => 'change_status', $user['id'], 1), array('class' => 'btn btn-danger', 'onclick' => "return confirm('Ce compte est inactif voulez-vous l'activer')", 'escape' => false));
        }
    } else{
        echo '<div class="clearfix">&nbsp;</div>';
        echo '<p class="text-danger">Vous ne pouvez pas activer cet utilisateur car le profil est incomplet..</p>';
    } 
    ?>
</div>
<br />

<div class="container-fluid content-container" style="">

    <div class="row">
        <div id="no-more-tables">
            <table class="col-md-12 table-bordered table-striped table-condensed cf" >
                <thead class="cf">
                    <tr class="text-nowrap">

                        <!-- <th scope="col"><? //$this->Paginator->sort('id','Id') ?></th> -->
                        <th scope="col"><?= $this->Paginator->sort('paymenttype_id','Type de paiement') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('amount_paid','Montant payé') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('amount_due','Montant dû') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('amout_date','Ate de règlement') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('due_date','Prochaine date de paiement') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('user_id','Ajouté par') ?></th>

                        <th scope="col"><?= $this->Paginator->sort('modified','modifie') ?></th>
                        <th scope="col" class="actions"><?= __('Action') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($accounts as $account): ?>
                    <tr>
                        <!-- <td><? //h($account->id) ?></td> -->

                        <td data-title="Type de Paiement"><?= h($account->paymenttype->payment_name)  ?></td>
                        <td data-title="Montant Payé"> <?= h($account->amount_paid) ?></td>
                        <td data-title="Montant Possédé"><?= h($account->amount_due) ?></td>
                        <td data-title="Ate de Règlement"><?= h(date('d.m.y',strtotime($account->amout_date))) ?></td>
                        <td data-title="Prochaine date de Paiement"><?= h(date('d.m.y',strtotime($account->due_date))) ?></td>
                        <td data-title="Ajouté par"><?= $account->has('user') ? $this->Html->link($account->user->user_name, ['controller' => 'Users', 'action' => 'view', $account->user->id]) : '' ?></td>

                        <td data-title="modifie"><?php echo h(date('d.m.y',strtotime($account->modified))); ?></td>
                        <td data-title="Action" class="actions">
                            <?php echo $this->Html->link(__('<i class="fa fa-comment"></i>'), array('controller' => 'Comments', 'action' => 'commentlist','id' =>$account->id,'accont_id'=>$accId),array('class'=>'btn btn-primary','escape'=>false,'data-toggle'=>"tooltip", 'title'=>"Commentaire")); ?>

                            <?php 
                            // if($permission == 2){                                                                                              
                                   if($us['id']==$account->id){
                            echo $this->Html->link(__('<i class="fa fa-edit"></i>'), array('controller' => 'accounts', 'action' => 'edit','id' =>$account->id,'accont_id'=>$accId),array('class'=>'btn btn-success','escape'=>false,'data-toggle'=>"tooltip", 'title'=>"Modifier")); 

                            ?>
                            <?php
                            echo $this->Form->postLink(__('<i class="fa fa-trash-o"></i>'), ['action' => 'delete' ,'id' =>$account->id,'accont_id'=>$accId], ['escape'=>false,'class'=>'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $account->id),'data-toggle'=>"tooltip", 'title'=>"Effacer"]);
                            // }
                            //echo $this->Html->link(__('<i class="fa fa-trash"></i> Delete'), ['controller'=>'users','action' => 'delete', $user->id], ['escape'=>false,'class'=>'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $user->username)]);

                            //echo $this->Form->postLink(__('<i class="fa fa-trash"></i> Delete'), ['action' => 'delete', $user->id], ['escape'=>false,'class'=>'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $user->id)]); 
                                   } // } }?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            </form>
    </div>
</div>

<br />
<div class="paginator">
    <ul class="pagination">
        <?php
        echo $this->Paginator->prev(__('Précédent'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
        echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
        echo $this->Paginator->next(__('Suivante'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
        ?>
    </ul>
    <p class="small"><?= $this->Paginator->counter(['format' => __('Page {{page}} de {{pages}}, affiche {{current}} entrée(s) sur un de total {{count}}')]) ?></p>
</div>
</div>
