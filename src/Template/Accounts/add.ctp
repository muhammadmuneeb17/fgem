<?php
/**
  * @var \App\View\AppView $this
  */
$array = explode('/', $_SERVER['REQUEST_URI']);
$userId = end($array);
?>
<div class="panel-head-info">
    <p class="pull-left info-text">Ajouter le Paiement reçu - <strong><?php echo $user->user->first_name . ' '.$user->user->last_name ; ?></strong></p>
    <a href="<?php echo $this->request->webroot ?>accounts/index/<?php echo $userId;?>" style="font-size:14px;" class="btn btn-primary btn-blue-custom pull-right text-info"><i class="fa fa-angle-left"></i> Précédent</a>
    <div class="clearfix"></div>

</div>
<div class="users form">
    <?php echo $this->Form->create($account,array('type'=>'file','class'=>'form-horizontal','id'=>'UserAddForm')); ?>
    <fieldset>
        <div class="form-group required">
            <label for="title" class="col-md-2 control-label"> Type de Paiement</label>
            <div class="col-md-2" style="ma">

                <?php echo $this->Form->control('paymenttype_id',array('div'=>false,'label'=>false,'class'=>'form-control','required','options' => $paymenttypes,'default'=> '2')); ?>
                <?php echo $this->Form->control('registeration_id',array('type'=>'hidden','value' => $userId,'div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
            </div> 
            <label for="title" class="col-md-2 control-label">Montant Payé</label>
            <div class="col-md-2">

                <?php echo $this->Form->control('amount_paid',array('div'=>false,'label'=>false,'class'=>'form-control','required','value'=>$amount));
                $userid = $this->request->session()->read('Auth.User.id');
                echo $this->Form->control('user_id', ['type' => 'hidden','value'=>$userid]);
                ?>
            </div> 
            <label for="title" class="col-md-2 control-label">Votre prochaine échéance</label>
            <div class="col-md-2">
                <?php  $args = array();
                echo $this->Form->input('due_date',array('options'=>$args,'default'=>'','div'=>false,'label'=>false,'class'=>'form-control','required', 'id' => 'year')); ?>
                <?php //echo $this->Form->control('due_date',array('div'=>false,'label'=>false,'class'=>'form-control datepicker','required','type'=>'text','value'=>$due_date)); ?>
            </div> 
        </div>  



        <div class="form-group required">
            <!--            <label for="title" class="col-md-3 control-label">Current Date</label>-->
            <div class="col-md-10">

                <?php  echo $this->Form->control('amount_date',array('div'=>false,'label'=>false,'class'=>'form-control','required','type'=>'hidden','value'=>date("Y-m-d"),'disabled')); ?>
            </div> 
        </div>

        <!--        <div class="form-group required">
<label for="title" class="col-md-3 control-label">Amount Due</label>
<div class="col-md-10">

<?php //echo $this->Form->control('amount_due',array('div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
</div> 
</div>-->


    </fieldset>
    <fieldset>
        <div class="form-group required">
            <label for="title" class="col-md-3 control-label ">Commentaire</label>
            <div class="col-md-10">

                <?php echo $this->Form->control('comment',array('div'=>false,'label'=>false,'class'=>'form-control','type'=>'textarea','required')); ?>
                <?php $userid = $this->request->session()->read('Auth.User.id');
                echo $this->Form->control('user_id', ['type' => 'hidden','value'=>$userid]);
                ?>

            </div> 
        </div>  



    </fieldset>


    <div class="form-group">
        <div class="col-md-12 text-center">
            <?php 
            echo $this->Form->control('Ajouter le Paiement reçu',array('type'=>'submit','div'=>false,'label'=>false,'class'=>'btn btn-default btn-blue-custom btn-lg'));
            echo $this->Form->end(); ?>
        </div>
    </div>

</div>




</div>
<script>
    $("#UserAddForm").submit(function () {

    });
    $(document).ready(function(){
        var date = new Date();
        var year = date.getFullYear() - 1;
        var selectedDate = year + 2;
        var options = '<option value="">Select Year</option>';
        for(var i = 0; i < 3; i++){
            year = year + 1;
            options = options + '<option value="'+year+'">'+year+'</option>';

        }

        $('#year').html(options);
        $('#year').val(selectedDate);
    });
</script>

