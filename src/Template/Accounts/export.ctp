<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Account[]|\Cake\Collection\CollectionInterface $accounts
  */
//debug($accounts);exit;
$array = explode('/', $_SERVER['REQUEST_URI']);
$accId = end($array);

$filename = "accounts.csv";
$fp = fopen('php://output', 'w');

//$query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='test2' AND TABLE_NAME='toy'";
//$result = mysqli_query($conn,$query);
//while ($row = mysqli_fetch_row($result)) {
//	$header[] = $row[0];
//}	

header('Content-type: application/csv');
header('Content-Disposition: attachment; filename='.$filename);
$heading='<b style="font-size:70px;"> this is some  csv stuff you know.</b>';

fputcsv($fp, array($heading));
fputcsv($fp, array('id','name', 'fist', 'you know.'));

foreach ($accounts as $account):
    
    fputcsv($fp, $account);
    
endforeach;
exit;
//$query = "SELECT * FROM toy";
//$result = mysqli_query($conn, $query);
while($row = mysqli_fetch_row($accounts)) {
	fputcsv($fp, $row);
}
exit;
?>
<div class="panel-head-info">
    <p class="pull-left info-text">Accounts - <strong><?php echo $user['user']['first_name'];echo " " ;?><?php echo $user['user']['last_name']; ?></strong></p>
            <!--<a href="<?php // echo $this->request->webroot ?>pages" style="font-size:14px;" class="btn btn-primary btn-blue-custom pull-right text-info"><i class="fa fa-angle-left"></i> Go Back</a>-->
    <div class="clearfix"></div>
    <?php 
//    if($permission == 2){
//    ?>
    
    <a href="<?php  echo $this->request->webroot ?>accounts/add/<?php echo $accId;?>" class="btn btn-success"><i class="fa fa-plus"></i> Add Accounts</a>
     <a href="<?php echo $this->request->webroot ?>registerations/index/<?php echo $accId;?>" style="font-size:14px;" class="btn btn-primary btn-blue-custom pull-right text-info"><i class="fa fa-angle-left"></i> Go Back</a>
     <a href="<?php echo $this->request->webroot ?>registerations/export/<?php echo $accId;?>" style="font-size:14px; margin-right: 10px; margin-top: 10px;" class="btn btn-primary btn-green-custom pull-right text-info"> Export</a>

</div>
     </br>

<div class="container-fluid content-container" style="">

    <div class="row">
        <div id="no-more-tables">
            <table class="col-md-12 table-bordered table-striped table-condensed cf" >
                <thead class="cf">
                    <tr>
                        
                       <th scope="col"><?= $this->Paginator->sort('id','Account id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('paymenttype_id','Payment Type') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('amount_paid','Amount Paid') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('amount_due','Amount Due') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('amout_date','Amount Date') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('due_date','Due Date') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('user_id','Added By') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('status','Status') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('modified','Modified') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                   </tr>
                </thead>
                <tbody>
                       <?php foreach ($accounts as $account): ?>
            <tr>
                <td><?= h($account->id) ?></td>

                <td><?= h($account->paymenttype->payment_name)  ?></td>
                <td><?= h($account->amount_paid) ?></td>
                <td><?= h($account->amount_due) ?></td>
                <td><?= h(date('d.m.y',strtotime($account->amout_date))) ?></td>
                <td><?= h(date('d.m.y',strtotime($account->due_date))) ?></td>
                <td><?= $account->has('user') ? $this->Html->link($account->user->user_name, ['controller' => 'Users', 'action' => 'view', $account->user->id]) : '' ?></td>
                <td><?= $this->Number->format($account->status) ?></td>
                <td><?php echo h(date('d.m.y',strtotime($account->modified))); ?></td>
                <td class="actions">
                     <?php echo $this->Html->link(__('Comments'), array('controller' => 'Comments', 'action' => 'index','id' =>$account->id,'accont_id'=>$accId),array('class'=>'btn btn-primary','escape'=>false)); ?>

                    <?php 
                      // if($permission == 2){                                                                                              
                                
				echo $this->Html->link(__('<i class="fa fa-edit"></i> Edit'), array('controller' => 'accounts', 'action' => 'edit','id' =>$account->id,'accont_id'=>$accId),array('class'=>'btn btn-success','escape'=>false)); 
			
			
								echo $this->Form->postLink(__('<i class="fa fa-trash-o"></i> Delete'), ['action' => 'delete' ,'id' =>$account->id,'accont_id'=>$accId], ['escape'=>false,'class'=>'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $account->id)]);
                                                               // }
								//echo $this->Html->link(__('<i class="fa fa-trash"></i> Delete'), ['controller'=>'users','action' => 'delete', $user->id], ['escape'=>false,'class'=>'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $user->username)]);
                            
							//echo $this->Form->postLink(__('<i class="fa fa-trash"></i> Delete'), ['action' => 'delete', $user->id], ['escape'=>false,'class'=>'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $user->id)]); 
                                                       // } ?></td>
            </tr>
            <?php endforeach; ?>
                </tbody>
            </table>
            </form>
        </div>
    </div>

    <br />
    <div class="paginator">
        <ul class="pagination">
            <?php
                echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
            ?>
        </ul>
        <p class="small"><?= $this->Paginator->counter(['format' => __('Page {{page}} de {{pages}}, affiche {{current}} entrée(s) sur un de total {{count}}')]) ?></p>
    </div>
</div>
