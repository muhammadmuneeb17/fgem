<?php
/**
  * @var \App\View\AppView $this
  */
//debug($total=$account['amount_paid']+$account['amount_due']);exit;
$total=$account['amount_paid']+$account['amount_due'];
$year = date('Y', strtotime($account->due_date));

?>
<div class="panel-head-info">
    <p class="pull-left info-text">Modifier le Paiement - <strong><?php echo $account['registeration']['user']['first_name'] . ' '. $account['registeration']['user']['last_name'] ; ?></strong></p>
    <a href="<?php echo $this->request->webroot ?>accounts/index/<?php echo $accont_id;?>" style="font-size:14px;" class="btn btn-primary btn-blue-custom pull-right text-info"><i class="fa fa-angle-left"></i> Précédent</a>
    <div class="clearfix"></div>

</div>
<div class="users form">
    <?php echo $this->Form->create($account,array('type'=>'file','class'=>'form-horizontal','id'=>'SliderAddForm')); ?>
    <fieldset>
        <div class="form-group required">
            <label for="title" class="col-md-2 control-label">Type de Paiement</label>
            <div class="col-md-2">

                <?php echo $this->Form->control('paymenttype_id',array('div'=>false,'label'=>false,'class'=>'form-control','required','options' => $paymenttypes)); ?>
            </div> 
            <label for="title" class="col-md-2 control-label">Montant</label>
            <div class="col-md-2">

                <?php echo $this->Form->control('amount_paid',array('div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
            </div> 
            <label for="title" class="col-md-2 control-label">Date d'échéance</label>
            <div class="col-md-2">
                <?php  $args = array();
                echo $this->Form->input('due_date',array('options'=>$args,'default'=>'','div'=>false,'label'=>false,'class'=>'form-control','required', 'id' => 'year')); ?>
                <?php 

                //echo $this->Form->control('due_date',array('div'=>false,'value'=>date('Y',strtotime($account['due_date'])),'label'=>false,'class'=>'form-control datepicker','required','type'=>'text')); ?>
            </div> 
        </div>  



        <!--        <div class="form-group required">
<label for="title" class="col-md-3 control-label">Amount Date</label>
<div class="col-md-10">

<?php //echo $this->Form->control('amout_date',array('div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
</div> 
</div>

<div class="form-group required">
<label for="title" class="col-md-3 control-label">Amount Due</label>
<div class="col-md-10">

<?php //echo $this->Form->control('amount_due',array('div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
</div> 
</div>-->


        <div class="form-group required">

        </div>
    </fieldset>

    <fieldset>
        <div class="form-group required">
            <label for="title" class="col-md-3 control-label">Commentaire</label>
            <div class="col-md-10">

                <?php echo $this->Form->control('comment',array('div'=>false,'label'=>false,'class'=>'form-control','','type'=>'textarea','required')); ?>
                <?php $userid = $this->request->session()->read('Auth.User.id');
                echo $this->Form->control('user_id', ['type' => 'hidden','value'=>$userid]);
                ?>

            </div> 
        </div>  



    </fieldset>
    <div class="form-group">
        <div class="col-md-12 text-center">
            <?php 
            echo $this->Form->control('Modifier le Compte',array('type'=>'submit','div'=>false,'label'=>false,'class'=>'btn btn-default btn-blue-custom btn-lg'));
            echo $this->Form->end(); ?>
        </div>
    </div>

</div>




</div>

<script>
    $(document).ready(function(){
        var date = new Date();
        var year = date.getFullYear() - 1;
        var selectedDate = '<?php echo $year;?>';
        var options = '<option value="">Select Year</option>';
        for(var i = 0; i < 3; i++){
            year = year + 1;
            options = options + '<option value="'+year+'">'+year+'</option>';

        }

        $('#year').html(options);
        $('#year').val(selectedDate);
    });
</script>
