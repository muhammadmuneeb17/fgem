<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Account[]|\Cake\Collection\CollectionInterface $results
 */
$array = explode('/', $_SERVER['REQUEST_URI']);
$accId = end($array);
?>

<style>

    .buttons-html5 {
        border-radius: 0px; 
        margin-left: 10px;
        width: 80px;

        color:white;
        background: #3c8dbc;
        border-color: #367fa9;
    }

    .buttons-print{
        border-radius:3px; 
        margin-left: 10px;
        width: 40px;

        color:white;
        background: #3c8dbc;
        border-color: #367fa9;
    }
</style>
<!-- JQuery DataTable Css -->
<link href="<?php echo $this->request->webroot; ?>jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<div class="panel-head-info">
    <p class="pull-left info-text">Rapports </p>
    <div class="clearfix"></div>

</div>
</br>

<div class="container-fluid content-container" style="">
    </br>
    <?= $this->Form->create('search', ['id' => 'myForm']) ?>
    <div class="row" >
        <p class="pull-left info-text"> <strong>Filtre</strong></p>
        <div class="col-md-12">

            <div class="form-line col-lg-4">
                <br/>
                <?php
//                                debug($results[0]['f_id']);
                echo $this->Form->control('pay', ['empty' => 'Sélectionnez le Type de Paiement', 'options' => $paymenttypes, 'value' => '', 'class' => 'form-control show-tick', 'label' => false]);
                ?>
            </div>
            <div class="form-line col-lg-4">
                <br/>
                <?php
//                                debug($results[0]['f_id']);
                echo $this->Form->control('membertypes', ['empty' => 'Sélectionnez le Type de Membre', 'options' => $membertypes, 'value' => '', 'class' => 'form-control show-tick', 'label' => false]);
                ?>
            </div>
            <div class="form-line col-md-4">
                <br/>
                <?php echo $this->Form->control('search', ['placeholder' => 'Rechercher par nom', 'class' => 'form-control show-tick show sub_categ', 'label' => false]); ?>

            </div>
            <div class="form-line col-md-4">
                <br/>
                <?php echo $this->Form->control('amount_paid', ['placeholder' => 'Recherche par montant payé', 'class' => 'form-control show-tick show sub_categ', 'label' => false]); ?>

            </div>
            <div class="form-line col-lg-4">
                <br/>
                <?php echo $this->Form->control('amount_due', ['placeholder' => 'Recherche par montant dû', 'class' => 'form-control show-tick show sub_categ', 'label' => false]); ?>

            </div>

            <div class="form-line col-lg-4">
                <br/>
                <?php echo $this->Form->control('from_amount_date', ['placeholder' => 'Recherche par date', 'class' => 'form-control datepicker show-tick show sub_categ', 'label' => false]); ?>
            </div>
            <div class="form-line col-lg-4">
                <br/>
                <?php echo $this->Form->control('to_amount_date', ['placeholder' => 'Paiement du', 'class' => 'form-control datepicker  show-tick show ', 'label' => false]); ?>
            </div>
            <div class="form-line col-lg-4">
                <br/>
                <?php echo $this->Form->control('from_due_date', ['placeholder' => 'Rechercher par montant Quantité Date suivante', 'class' => 'form-control datepicker show-tick show sub_categ', 'label' => false]); ?>
            </div>
            <div class="form-line col-lg-4">
                <br/>
                <?php echo $this->Form->control('to_due_date', ['placeholder' => 'Rechercher par montant montant Date suivante', 'class' => 'form-control datepicker  show-tick show ', 'label' => false]); ?>
            </div>
            <div class="form-line col-lg-4">
                <br/>
                <?php echo $this->Form->control('status', ['placeholder' => 'Statut', 'class' => 'form-control', 'label' => false, 'options' => ['' => 'Sélectionnez le statut', '0' => 'Pending', '1' => 'Approved', '2' => 'New']]); ?>
            </div>
            <div class="col-lg-2" style="float:right; margin-right: -55px;">
                <br/>
                <button class="btn btn-primary waves-effect" type="submit" onclick="myFunction()" id="submit">Chercher</button>
            </div>
        </div>

    </div>
    <?php
    echo $this->Form->end();
    ?>
    </br>
    <div class="row">
        <?php ?>
        <label class="label pull-right">
            <div class="toggle">
                <input class="toggle-state" type="checkbox" name="check" value="check" <?php
                if (isset($this->request->params['?']['showdetails']) && $this->request->params['?']['showdetails'] == 1) {
                    echo "checked";
                }
                ?>/>
                <div class="toggle-inner">
                    <div class="indicator"></div>
                </div>
                <div class="active-bg"></div>
            </div>
            <h5 class="text-success">&nbsp;&nbsp;Afficher les Détails</h5>
        </label>
        <div class="clearfix"></div>
        <div id="no-more-tables" class="table-responsive">
            <p class="pull-left info-text">Exportation </p>

            <table class="table table-bordered table-striped table-hover dataTable js-exportable">

                <thead>

                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('name', 'Nom Complet') ?></th>
                       <!-- <th scope="col"><? //$this->Paginator->sort('lastname', 'Nom de famille') ?></th> -->
                        <th scope="col"><?= $this->Paginator->sort('membertype_id', 'Type d\'adhésion') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('paymenttype_id', 'Type de Paiement') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('amount_paid', 'Montant Payé') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('amount_due', 'Montant dû') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('amout_date', 'Date paiement') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('due_date', 'Prochaine échéance') ?></th>
                        <th class="actions noExport"><?php echo __('Action'); ?></th>
                    </tr>

                </thead>
                <tbody>
                    <?php
                    foreach ($results as $result):
                        //debug($result['name']);exit;
                        ?>
                        <tr>
                            <!-- <td data-title="Prénom"><? //h($result['first_name']) ?></td> -->
                            <td data-title="Nom Complet"><?= h($result['first_name']) ?> <?= h($result['last_name']) ?></td>
                            <td data-title="Type d\'adhésion"><?= h($result['description'] . ' (' . $result['amount'] . ')') ?></td>
                            <td data-title="Type de Paiement"><?= h($result['payment_name']) ?></td>
                            <td data-title="Montant Payé"><?= h($result['amount_paid']) ?></td>
                            <td data-title="Montant dû"><?= h($result['amount_due']) ?></td>
                            <td data-title="Date paiement"><?= h(date('d.m.y', strtotime($result['amout_date']))) ?></td>
                            <td data-title="Prochaine échéance"><?= h(date('d.m.y', strtotime($result['due_date']))) ?></td>
                            <td  data-title="Action" class="actions">
                                <?php //echo $this->Html->link(__('View'), array('controller' => 'users', 'action' => 'view',$user->id),array('class'=>'btn btn-primary'));   ?>
                                <?php echo $this->Html->link(__('<span class="fa fa-money"></span>'), array('controller' => 'Accounts', 'action' => 'index', $result['registeration_id']), array('class' => 'btn btn-primary', 'escape' => false,'data-toggle'=>"tooltip",'title'=>'Paiement')); ?>

                                <?php
                                if ($permission == 2) {

                                    echo "<br /><br />" . $this->Html->link(__('<i class="fa fa-edit"></i>'), array('controller' => 'registerations', 'action' => 'edit', $result['registeration_id']), array('class' => 'btn btn-success', 'escape' => false,'data-toggle'=>"tooltip",'title'=>'Modifier'));
                                }
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                </tbody>
            </table>
            </form>
        </div>
    </div>

    <br />

</div>

<script type="text/javascript">
    $(".toggle-state").on("change", function () {
        if ($(this).is(":checked")) {
            window.location.assign("<?php echo $this->request->webroot ?>Accounts/report?showdetails=1");
        } else {
            window.location.assign("<?php echo $this->request->webroot ?>Accounts/report");
        }
    });
</script>

<!-- Jquery DataTable Plugin Js -->
<script src="<?php echo $this->request->webroot; ?>jquery-datatable/jquery.dataTables.js"></script>
<script src="<?php echo $this->request->webroot; ?>jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?php echo $this->request->webroot; ?>jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="<?php echo $this->request->webroot; ?>jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="<?php echo $this->request->webroot; ?>jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="<?php echo $this->request->webroot; ?>jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="<?php echo $this->request->webroot; ?>jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="<?php echo $this->request->webroot; ?>jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="<?php echo $this->request->webroot; ?>jquery-datatable/extensions/export/buttons.print.min.js"></script>
<script src="<?php echo $this->request->webroot; ?>jquery-datatable/jquery-datatable.js"></script>