<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Membertype[]|\Cake\Collection\CollectionInterface $membertypes
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Membertype'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Registerations'), ['controller' => 'Registerations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Registeration'), ['controller' => 'Registerations', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="membertypes index large-9 medium-8 columns content">
    <h3><?= __('Membertypes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($membertypes as $membertype): ?>
            <tr>
                <td><?= $this->Number->format($membertype->id) ?></td>
                <td><?= h($membertype->name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $membertype->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $membertype->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $membertype->id], ['confirm' => __('Are you sure you want to delete # {0}?', $membertype->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} de {{pages}}, affiche {{current}} entrée(s) sur un de total {{count}}')]) ?></p>
    </div>
</div>
