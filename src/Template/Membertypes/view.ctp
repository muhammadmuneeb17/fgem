<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Membertype $membertype
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Membertype'), ['action' => 'edit', $membertype->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Membertype'), ['action' => 'delete', $membertype->id], ['confirm' => __('Are you sure you want to delete # {0}?', $membertype->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Membertypes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Membertype'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Registerations'), ['controller' => 'Registerations', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Registeration'), ['controller' => 'Registerations', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="membertypes view large-9 medium-8 columns content">
    <h3><?= h($membertype->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($membertype->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($membertype->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Registerations') ?></h4>
        <?php if (!empty($membertype->registerations)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Membertype Id') ?></th>
                <th scope="col"><?= __('Profession') ?></th>
                <th scope="col"><?= __('Username') ?></th>
                <th scope="col"><?= __('Image') ?></th>
                <th scope="col"><?= __('Password') ?></th>
                <th scope="col"><?= __('Address1') ?></th>
                <th scope="col"><?= __('Address2') ?></th>
                <th scope="col"><?= __('Postalcode') ?></th>
                <th scope="col"><?= __('City') ?></th>
                <th scope="col"><?= __('Pay') ?></th>
                <th scope="col"><?= __('Enterprise') ?></th>
                <th scope="col"><?= __('Tel1') ?></th>
                <th scope="col"><?= __('Tel2') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('Site Web') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($membertype->registerations as $registerations): ?>
            <tr>
                <td><?= h($registerations->id) ?></td>
                <td><?= h($registerations->membertype_id) ?></td>
                <td><?= h($registerations->profession) ?></td>
                <td><?= h($registerations->username) ?></td>
                <td><?= h($registerations->image) ?></td>
                <td><?= h($registerations->password) ?></td>
                <td><?= h($registerations->address1) ?></td>
                <td><?= h($registerations->address2) ?></td>
                <td><?= h($registerations->postalcode) ?></td>
                <td><?= h($registerations->city) ?></td>
                <td><?= h($registerations->pay) ?></td>
                <td><?= h($registerations->enterprise) ?></td>
                <td><?= h($registerations->tel1) ?></td>
                <td><?= h($registerations->tel2) ?></td>
                <td><?= h($registerations->email) ?></td>
                <td><?= h($registerations->status) ?></td>
                <td><?= h($registerations->site_web) ?></td>
                <td><?= h($registerations->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Registerations', 'action' => 'view', $registerations->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Registerations', 'action' => 'edit', $registerations->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Registerations', 'action' => 'delete', $registerations->id], ['confirm' => __('Are you sure you want to delete # {0}?', $registerations->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
