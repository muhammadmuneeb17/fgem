<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Membertypes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Registerations'), ['controller' => 'Registerations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Registeration'), ['controller' => 'Registerations', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="membertypes form large-9 medium-8 columns content">
    <?= $this->Form->create($membertype) ?>
    <fieldset>
        <legend><?= __('Add Membertype') ?></legend>
        <?php
            echo $this->Form->control('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
