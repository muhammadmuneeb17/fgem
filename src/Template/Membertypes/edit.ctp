<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $membertype->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $membertype->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Membertypes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Registerations'), ['controller' => 'Registerations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Registeration'), ['controller' => 'Registerations', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="membertypes form large-9 medium-8 columns content">
    <?= $this->Form->create($membertype) ?>
    <fieldset>
        <legend><?= __('Edit Membertype') ?></legend>
        <?php
            echo $this->Form->control('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
