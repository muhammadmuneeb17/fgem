<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\MemberTypeAmount[]|\Cake\Collection\CollectionInterface $memberTypeAmounts
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Member Type Amount'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Membertypes'), ['controller' => 'Membertypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Membertype'), ['controller' => 'Membertypes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="memberTypeAmounts index large-9 medium-8 columns content">
    <h3><?= __('Member Type Amounts') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('membertype_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('year') ?></th>
                <th scope="col"><?= $this->Paginator->sort('amount') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($memberTypeAmounts as $memberTypeAmount): ?>
            <tr>
                <td><?= $this->Number->format($memberTypeAmount->id) ?></td>
                <td><?= $memberTypeAmount->has('membertype') ? $this->Html->link($memberTypeAmount->membertype->name, ['controller' => 'Membertypes', 'action' => 'view', $memberTypeAmount->membertype->id]) : '' ?></td>
                <td><?= h($memberTypeAmount->year) ?></td>
                <td><?= $this->Number->format($memberTypeAmount->amount) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $memberTypeAmount->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $memberTypeAmount->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $memberTypeAmount->id], ['confirm' => __('Are you sure you want to delete # {0}?', $memberTypeAmount->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
