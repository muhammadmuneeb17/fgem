<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\MemberTypeAmount $memberTypeAmount
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Member Type Amount'), ['action' => 'edit', $memberTypeAmount->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Member Type Amount'), ['action' => 'delete', $memberTypeAmount->id], ['confirm' => __('Are you sure you want to delete # {0}?', $memberTypeAmount->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Member Type Amounts'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Member Type Amount'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Membertypes'), ['controller' => 'Membertypes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Membertype'), ['controller' => 'Membertypes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="memberTypeAmounts view large-9 medium-8 columns content">
    <h3><?= h($memberTypeAmount->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Membertype') ?></th>
            <td><?= $memberTypeAmount->has('membertype') ? $this->Html->link($memberTypeAmount->membertype->name, ['controller' => 'Membertypes', 'action' => 'view', $memberTypeAmount->membertype->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Year') ?></th>
            <td><?= h($memberTypeAmount->year) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($memberTypeAmount->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Amount') ?></th>
            <td><?= $this->Number->format($memberTypeAmount->amount) ?></td>
        </tr>
    </table>
</div>
