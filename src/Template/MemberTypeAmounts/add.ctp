<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Member Type Amounts'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Membertypes'), ['controller' => 'Membertypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Membertype'), ['controller' => 'Membertypes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="memberTypeAmounts form large-9 medium-8 columns content">
    <?= $this->Form->create($memberTypeAmount) ?>
    <fieldset>
        <legend><?= __('Add Member Type Amount') ?></legend>
        <?php
            echo $this->Form->control('membertype_id', ['options' => $membertypes, 'empty' => true]);
            echo $this->Form->control('year');
            echo $this->Form->control('amount');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
