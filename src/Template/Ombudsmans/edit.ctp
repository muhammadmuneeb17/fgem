<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $ombudsman->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $ombudsman->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Ombudsmans'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Ombudsmanaccreds'), ['controller' => 'Ombudsmanaccreds', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ombudsmanaccred'), ['controller' => 'Ombudsmanaccreds', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Ombudsmanformations'), ['controller' => 'Ombudsmanformations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ombudsmanformation'), ['controller' => 'Ombudsmanformations', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Ombudsmanlevels'), ['controller' => 'Ombudsmanlevels', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ombudsmanlevel'), ['controller' => 'Ombudsmanlevels', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ombudsmans form large-9 medium-8 columns content">
    <?= $this->Form->create($ombudsman) ?>
    <fieldset>
        <legend><?= __('Edit Ombudsman') ?></legend>
        <?php
            echo $this->Form->control('registeration_id');
            echo $this->Form->control('mediator_question');
            echo $this->Form->control('presentation_text');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
