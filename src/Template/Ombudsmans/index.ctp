<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Ombudsman[]|\Cake\Collection\CollectionInterface $ombudsmans
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Ombudsman'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Ombudsmanaccreds'), ['controller' => 'Ombudsmanaccreds', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ombudsmanaccred'), ['controller' => 'Ombudsmanaccreds', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Ombudsmanformations'), ['controller' => 'Ombudsmanformations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ombudsmanformation'), ['controller' => 'Ombudsmanformations', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Ombudsmanlevels'), ['controller' => 'Ombudsmanlevels', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ombudsmanlevel'), ['controller' => 'Ombudsmanlevels', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ombudsmans index large-9 medium-8 columns content">
    <h3><?= __('Ombudsmans') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('registeration_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('mediator_question') ?></th>
                <th scope="col"><?= $this->Paginator->sort('presentation_text') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ombudsmans as $ombudsman): ?>
            <tr>
                <td><?= $this->Number->format($ombudsman->id) ?></td>
                <td><?= $this->Number->format($ombudsman->registeration_id) ?></td>
                <td><?= h($ombudsman->mediator_question) ?></td>
                <td><?= h($ombudsman->presentation_text) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $ombudsman->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ombudsman->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $ombudsman->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ombudsman->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} de {{pages}}, affiche {{current}} entrée(s) sur un de total {{count}}')]) ?></p>
    </div>
</div>
