<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Ombudsman $ombudsman
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Ombudsman'), ['action' => 'edit', $ombudsman->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Ombudsman'), ['action' => 'delete', $ombudsman->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ombudsman->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Ombudsmans'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ombudsman'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Ombudsmanaccreds'), ['controller' => 'Ombudsmanaccreds', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ombudsmanaccred'), ['controller' => 'Ombudsmanaccreds', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Ombudsmanformations'), ['controller' => 'Ombudsmanformations', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ombudsmanformation'), ['controller' => 'Ombudsmanformations', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Ombudsmanlevels'), ['controller' => 'Ombudsmanlevels', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ombudsmanlevel'), ['controller' => 'Ombudsmanlevels', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="ombudsmans view large-9 medium-8 columns content">
    <h3><?= h($ombudsman->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Mediator Question') ?></th>
            <td><?= h($ombudsman->mediator_question) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Presentation Text') ?></th>
            <td><?= h($ombudsman->presentation_text) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($ombudsman->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Registeration Id') ?></th>
            <td><?= $this->Number->format($ombudsman->registeration_id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Ombudsmanaccreds') ?></h4>
        <?php if (!empty($ombudsman->ombudsmanaccreds)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Ombudsman Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($ombudsman->ombudsmanaccreds as $ombudsmanaccreds): ?>
            <tr>
                <td><?= h($ombudsmanaccreds->id) ?></td>
                <td><?= h($ombudsmanaccreds->ombudsman_id) ?></td>
                <td><?= h($ombudsmanaccreds->name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Ombudsmanaccreds', 'action' => 'view', $ombudsmanaccreds->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Ombudsmanaccreds', 'action' => 'edit', $ombudsmanaccreds->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Ombudsmanaccreds', 'action' => 'delete', $ombudsmanaccreds->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ombudsmanaccreds->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Ombudsmanformations') ?></h4>
        <?php if (!empty($ombudsman->ombudsmanformations)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Ombudsman Id') ?></th>
                <th scope="col"><?= __('Formation') ?></th>
                <th scope="col"><?= __('Certificate Date') ?></th>
                <th scope="col"><?= __('Copy Upload') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($ombudsman->ombudsmanformations as $ombudsmanformations): ?>
            <tr>
                <td><?= h($ombudsmanformations->id) ?></td>
                <td><?= h($ombudsmanformations->ombudsman_id) ?></td>
                <td><?= h($ombudsmanformations->formation) ?></td>
                <td><?= h($ombudsmanformations->certificate_date) ?></td>
                <td><?= h($ombudsmanformations->copy_upload) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Ombudsmanformations', 'action' => 'view', $ombudsmanformations->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Ombudsmanformations', 'action' => 'edit', $ombudsmanformations->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Ombudsmanformations', 'action' => 'delete', $ombudsmanformations->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ombudsmanformations->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Ombudsmanlevels') ?></h4>
        <?php if (!empty($ombudsman->ombudsmanlevels)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Ombudsman Id') ?></th>
                <th scope="col"><?= __('Language Id') ?></th>
                <th scope="col"><?= __('Language Level') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($ombudsman->ombudsmanlevels as $ombudsmanlevels): ?>
            <tr>
                <td><?= h($ombudsmanlevels->id) ?></td>
                <td><?= h($ombudsmanlevels->ombudsman_id) ?></td>
                <td><?= h($ombudsmanlevels->language_id) ?></td>
                <td><?= h($ombudsmanlevels->language_level) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Ombudsmanlevels', 'action' => 'view', $ombudsmanlevels->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Ombudsmanlevels', 'action' => 'edit', $ombudsmanlevels->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Ombudsmanlevels', 'action' => 'delete', $ombudsmanlevels->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ombudsmanlevels->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
