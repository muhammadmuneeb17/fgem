<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Language $language
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Language'), ['action' => 'edit', $language->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Language'), ['action' => 'delete', $language->id], ['confirm' => __('Are you sure you want to delete # {0}?', $language->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Languages'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Language'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Ombudsmanlevels'), ['controller' => 'Ombudsmanlevels', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ombudsmanlevel'), ['controller' => 'Ombudsmanlevels', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="languages view large-9 medium-8 columns content">
    <h3><?= h($language->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($language->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($language->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($language->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Ombudsmanlevels') ?></h4>
        <?php if (!empty($language->ombudsmanlevels)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Ombudsman Id') ?></th>
                <th scope="col"><?= __('Language Id') ?></th>
                <th scope="col"><?= __('Language Level') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($language->ombudsmanlevels as $ombudsmanlevels): ?>
            <tr>
                <td><?= h($ombudsmanlevels->id) ?></td>
                <td><?= h($ombudsmanlevels->ombudsman_id) ?></td>
                <td><?= h($ombudsmanlevels->language_id) ?></td>
                <td><?= h($ombudsmanlevels->language_level) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Ombudsmanlevels', 'action' => 'view', $ombudsmanlevels->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Ombudsmanlevels', 'action' => 'edit', $ombudsmanlevels->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Ombudsmanlevels', 'action' => 'delete', $ombudsmanlevels->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ombudsmanlevels->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
