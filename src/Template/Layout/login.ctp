<!DOCTYPE html>
<html>
    <head>
         <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<?php echo $this->Html->charset(); ?>
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
        <title>
		<?php //echo $cakeDescription ?>
		 FGeM
        </title>
        <link rel="icon" href="<?php echo $this->request->webroot ?>images/logo/icon-logo.png" />
        
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

	<?php
		echo $this->Html->css('bootstrap');
		echo $this->Html->css('AdminLTE.min');
		//echo $this->Html->css('style');
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>

  <!-- bootstrap wysihtml5 - text editor -->
  <!--<link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">-->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            
            .browser-text {
                
                padding:40px;
                color:#fff;
                max-width: 800px;
                margin:auto;
                margin-top:20px;   
            }
            .browser-text a {
                color:#fff;   
            }
            .browser-message {
                display: none;
            }
        </style>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
      <div class="browser-message" style="position:fixed;z-index:9999999999;left:0px;top:0px;background: rgba(0,0,0,0.98);width:100%;height:100%;color:#fff;">
        
    </div>
<div class="wrapper" style="background:#ECF0F5;">
    
  <header class="main-header" style="background:#ECF0F5;max-height:260px;">
           <img src="<?php echo $this->request->webroot; ?>img/logo.png" class="backend-logo" width="120" style="margin:auto;display:block;margin-top:20px"/>
      <h3 align="center" class="login-headtitle">Administrative Section</h3>
    <!-- Logo -->
    <!-- Header Navbar: style can be found in header.less -->
    
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  
  
  <div class="content-wrapper" style="width: 100%;margin-left: 0px;min-height:450px;">
    <!-- Content Header (Page header) -->
    <div class="container" style="max-width:600px !important; position: relative;height: 22px;">
        <?= $this->Flash->render() ?>
    </div>    
    <?php echo $this->fetch('content'); ?>
  </div>  
    
        
   <footer class="main-footer" style="margin-left:0px;">
    <div class="pull-right hidden-xs">
      <small> Developed By: <a href="https://www.cyberclouds.com">Cyber Clouds</a></small> | <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; FGeM.</strong> Toute reproduction strictement interdite sans autorisation préalable écrite de la <a href="mailto:info@fgem.ch">FGEM</a>
  </footer>
  </div>

	<?php //echo $this->element('sql_dump'); ?>
    <?php echo $this->Html->script('jquery-2.2.3.min'); ?>   
        
<script type="text/javascript">
    $(document).ready(function(){


            if (navigator.appName == 'Microsoft Internet Explorer' ||  !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv:11/)) || (typeof $.browser !== "undefined" && $.browser.msie == 1))
                {
                  $(".browser-message").show().html("<div class='browser-text'><p><img src='<?php echo $this->request->webroot; ?>front/img/logo-light.png' style='margin:auto;display:block;'/></p><p>Nous vous remercions de votre visite et de l’intérêt que vous portez à la Fédération Genevoise MédiationS. </p><p>Nous avons le regret de devoir vous informer que votre navigateur n’est malheureusement plus supporté par son développeur. Malgré tout nos efforts, nous n’avons pas été en mesure de concevoir notre site, qui utilise les dernières technologies, de manière à le rendre compatible avec la version du navigateur que vous utilisez.</p><p>Nous vous invitons à télécharger l’un des navigateurs suivants qui vous permettra d’accéder et d’utiliser notre site dans les meilleures conditions.</p><p>En cas de difficultés, nous vous invitons à prendre contact avec nous en nous envoyant un message à <a href='mailto:communication@fgem.ch'>communication@fgem.ch</a></p><p>Nous vous remercions par avance et nous vous prions de bien vouloir nous excuser de ce désagrément.</p> <p><a href='https://www.mozilla.org/en-US/firefox/new/?redirect_source=firefox-com' title='Mozilla Firefox'><img width='100' src='https://cdn.iconscout.com/icon/free/png-256/firefox-20-569406.png'></a><a href='https://www.google.com/chrome/' title='Google Chrome'><img width='90' src='https://www.google.com/chrome/static/images/chrome-logo.svg'></a><a href='https://www.apple.com/safari/'><img width='85' src='https://www.apple.com/v/safari/i/images/overview/safari_icon_large.png'></a></p></div>");
                }



                });
        </script>
    </body>
</html>
