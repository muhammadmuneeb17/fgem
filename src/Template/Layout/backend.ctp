<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <?php echo $this->Html->charset(); ?>
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
        <title>
            FGeM
        </title>
        <link rel="icon" href="<?php echo $this->request->webroot ?>images/logo/icon-logo.png" />

        <!-- Font Awesome -->
        <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">-->
        <!-- Ionicons -->
        <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">-->

        <?php
        echo $this->Html->css('font-awesome');

        echo $this->Html->css('_all-skins.min');
        echo $this->Html->css('blue');

        echo $this->Html->css('morris');
        echo $this->Html->css('jquery-jvectormap-1.2.2');
        echo $this->Html->css('datepicker3');
        echo $this->Html->css('timepicker');
        echo $this->Html->css('daterangepicker');
        echo $this->Html->css('bootstrap3-wysihtml5.min');
        //echo $this->Html->css('style');
        echo $this->Html->css('bootstrap.min');
        echo $this->Html->css('AdminLTE.min');
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        ?>

        <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
        <?php echo $this->Html->script('jquery-2.2.3.min'); ?>  
        <style>
            input[name=city] {    
                text-transform: capitalize !important;
            }
        </style>
        <style>
            .dot {
                height: 15px;
                width: 15px;
                background-color: #bbb;
                border-radius: 50%;
                display: inline-block;
                margin-right: 10px;
            }
            .dot-primary {
                background-color: #99CCCC;
            }
            .dot-secondary {
                background-color: #FF5D4C;
            }
            .dot-offday {
                background-color: #f40035;
            }
            input[name=pay] {    
                text-transform: capitalize !important;
            }
        </style>
         <style>
            
            .browser-text {
                
                padding:40px;
                color:#fff;
                max-width: 800px;
                margin:auto;
                margin-top:20px;   
            }
            .browser-text a {
                color:#fff;   
            }
            .browser-message {
                display: none;
            }
        </style>

    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="browser-message" style="position:fixed;z-index:9999999999;left:0px;top:0px;background: rgba(0,0,0,0.98);width:100%;height:100%;color:#fff;">
        
    </div>
        <div class="wrapper">

            <header class="main-header" style="padding: 5px 0px;">
                <!-- Logo -->

                <a href="<?php echo $this->request->webroot; ?>" class="logo show-mb">
                    <img src="<?php echo $this->request->webroot; ?>img/logo.png" class="backend-logo" width="50"/>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">

                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">

                            <!-- Messages: style can be found in dropdown.less-->
                            <!--          <li class="dropdown messages-menu">
<a href="#" class="dropdown-toggle" data-toggle="dropdown">
<i class="fa fa-envelope-o"></i>
<span class="label label-success">4</span>
</a>
<ul class="dropdown-menu">
<li class="header">You have 4 messages</li>
<li>
inner menu: contains the actual data 
<ul class="menu">
<li> start message 
<a href="#">
<div class="pull-left">
<img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
</div>
<h4>
Support Team
<small><i class="fa fa-clock-o"></i> 5 mins</small>
</h4>
<p>Why not buy a new awesome theme?</p>
</a>
</li>
end message 
<li>
<a href="#">
<div class="pull-left">
<img src="dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
</div>
<h4>
AdminLTE Design Team
<small><i class="fa fa-clock-o"></i> 2 hours</small>
</h4>
<p>Why not buy a new awesome theme?</p>
</a>
</li>
<li>
<a href="#">
<div class="pull-left">
<img src="dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
</div>
<h4>
Developers
<small><i class="fa fa-clock-o"></i> Today</small>
</h4>
<p>Why not buy a new awesome theme?</p>
</a>
</li>
<li>
<a href="#">
<div class="pull-left">
<img src="dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
</div>
<h4>
Sales Department
<small><i class="fa fa-clock-o"></i> Yesterday</small>
</h4>
<p>Why not buy a new awesome theme?</p>
</a>
</li>
<li>
<a href="#">
<div class="pull-left">
<img src="dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
</div>
<h4>
Reviewers
<small><i class="fa fa-clock-o"></i> 2 days</small>
</h4>
<p>Why not buy a new awesome theme?</p>
</a>
</li>
</ul>
</li>
<li class="footer"><a href="#">See All Messages</a></li>
</ul>
</li>-->
                            <!-- Notifications: style can be found in dropdown.less -->
                            <!--          <li class="dropdown notifications-menu">
<a href="#" class="dropdown-toggle" data-toggle="dropdown">
<i class="fa fa-bell-o"></i>
<span class="label label-warning">10</span>
</a>
<ul class="dropdown-menu">
<li class="header">You have 10 notifications</li>
<li>
inner menu: contains the actual data 
<ul class="menu">
<li>
<a href="#">
<i class="fa fa-users text-aqua"></i> 5 new members joined today
</a>
</li>
<li>
<a href="#">
<i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
page and may cause design problems
</a>
</li>
<li>
<a href="#">
<i class="fa fa-users text-red"></i> 5 new members joined
</a>
</li>
<li>
<a href="#">
<i class="fa fa-shopping-cart text-green"></i> 25 sales made
</a>
</li>
<li>
<a href="#">
<i class="fa fa-user text-red"></i> You changed your username
</a>
</li>
</ul>
</li>
<li class="footer"><a href="#">View all</a></li>
</ul>
</li>-->
                            <!--           Tasks: style can be found in dropdown.less 
<li class="dropdown tasks-menu">
<a href="#" class="dropdown-toggle" data-toggle="dropdown">
<i class="fa fa-flag-o"></i>
<span class="label label-danger">9</span>
</a>
<ul class="dropdown-menu">
<li class="header">You have 9 tasks</li>
<li>
inner menu: contains the actual data 
<ul class="menu">
<li> Task item 
<a href="#">
<h3>
Design some buttons
<small class="pull-right">20%</small>
</h3>
<div class="progress xs">
<div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
<span class="sr-only">20% Complete</span>
</div>
</div>
</a>
</li>
end task item 
<li> Task item 
<a href="#">
<h3>
Create a nice theme
<small class="pull-right">40%</small>
</h3>
<div class="progress xs">
<div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
<span class="sr-only">40% Complete</span>
</div>
</div>
</a>
</li>
end task item 
<li> Task item 
<a href="#">
<h3>
Some task I need to do
<small class="pull-right">60%</small>
</h3>
<div class="progress xs">
<div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
<span class="sr-only">60% Complete</span>
</div>
</div>
</a>
</li>
end task item 
<li> Task item 
<a href="#">
<h3>
Make beautiful transitions
<small class="pull-right">80%</small>
</h3>
<div class="progress xs">
<div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
<span class="sr-only">80% Complete</span>
</div>
</div>
</a>
</li>
end task item 
</ul>
</li>
<li class="footer">
<a href="#">View all tasks</a>
</li>
</ul>
</li>-->
                            <!-- User Account: style can be found in dropdown.less -->

                            <li class="dropdown user user-menu">
                                <?php
                                $uid = $this->request->session()->read('Auth.User.id');
                                ?>
                                <?php
                                // THESE TWO LINES ARE COMMON FOR PERMISSION, FIRST LINE APPCONTROLLER OBJ
                                $appController = new \App\Controller\AppController;
                                $roleId = $this->request->session()->read('Auth.User.role_id');
                                ?>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo $this->request->webroot; ?>img/Users/admin.png"  class="user-image" alt="<?php echo ucwords($this->request->session()->read('Auth.User.user_name')); ?>">
                                    <span class="hidden-xs"><?php echo ucwords($this->request->session()->read('Auth.User.user_name')); ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">

                                        <img src="<?php echo $this->request->webroot; ?>img/Users/admin.png" class="img-circle" alt="<?php echo $this->request->session()->read('Auth.User.user_name'); ?>">

                                        <p>
                                            <?php echo ucwords($this->request->session()->read('Auth.User.first_name')) . " " . ucwords($this->request->session()->read('Auth.User.last_name')); ?>
                                            <small>Membre depuis <?php echo date('m,Y', strtotime($this->request->session()->read('Auth.User.modified'))); ?></small>
                                        </p>
                                    </li>
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="<?php echo $this->request->webroot ?>Users/edit/<?php echo $this->request->session()->read('Auth.User.id'); ?>" class="btn btn-default btn-flat">Profil</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="<?php echo $this->request->webroot ?>Users/logout" class="btn btn-default btn-flat">Se déconnecter</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">

                            <img src="<?php echo $this->request->webroot; ?>img/Users/admin.png" class="img-circle" alt="<?php
                            echo ucwords($this->request->session()->read('Auth.User.user_name'));
                            ?>">
                        </div>
                        <div class="pull-left info">
                            <p><?php echo ucwords($this->request->session()->read('Auth.User.user_name')); ?></p>
                            <a href="#"><i class="fa fa-circle text-success"></i> En ligne</a>
                        </div>
                    </div>

                    <ul class="sidebar-menu">
                        <li class="header">NAVIGATION PRINCIPALE</li>

                        <?php
                        // ADD PERMISSION
                        $actionPermission = 0;
                        $controller = '';
                        $controller = 'PAGES';
                        $actionPermission = $appController->permissionsList($roleId, $controller);

                        if ($actionPermission == 1 || $actionPermission == 2) {
                            ?>
                            <li class="<?php if ($this->request->params['controller'] == 'Pages' && ($this->request->params['action'] == "dashboard" )) echo 'active'; ?> treeview">
                                <a href="<?php echo $this->request->webroot ?>/Pages/dashboard">
                                    <i class="fa fa-system"></i> <span>Tableau de bord</span>
                                </a>
                            </li>
                        <?php } ?>



                        <?php
                        // ADD PERMISSION
                        $actionPermission = 0;
                        $controller = '';
                        $controller = 'REGISTERATIONS';
                        $actionPermission = $appController->permissionsList($roleId, $controller);
                        if ($actionPermission == 1 || $actionPermission == 2) {
                            ?>
                            <li class="<?php if ($this->request->params['controller'] == 'Registerations' && ($this->request->params['action'] == "index" || $this->request->params['action'] == "add" || $this->request->params['action'] == "edit")) echo 'active'; ?> treeview">
                                <a href="#">
                                    <i class="fa fa-users"></i> <span>Membres</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <?php
                                    if ($actionPermission == 1 || $actionPermission == 2) {
                                        ?>
                                        <li class="<?php if ($this->request->params['action'] == 'index') echo 'active'; ?>"><a href="<?php echo $this->request->webroot ?>Registerations/index"><i class="fa fa-circle-o"></i> Liste</a></li>
                                        <?php
                                    }
                                    ?>
                                    <li class="<?php if ($this->request->params['action'] == 'index') echo 'active'; ?>"><a href="<?php echo $this->request->webroot ?>Accounts/report"><i class="fa fa-circle-o"></i> Rapports</a></li>
                                    <li class="<?php if ($this->request->params['action'] == 'index') echo 'active'; ?>"><a href="<?php echo $this->request->webroot ?>Registerations/membertypes"><i class="fa fa-circle-o"></i> Member types</a></li>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php
                        if (!empty($ticketConditions) || !empty($superviser)) {

                            $ticketsArray = array('ticketCalendar', 'allTickets', 'mySchedule');
                            ?>
                            <li class="<?php echo in_array($this->request->params['action'], $ticketsArray) ? 'active' : ''; ?> treeview">
                                <a href="#">
                                    <i class="fa fa-file-text-o"></i> <span>PIM</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <?php if ($superviser) { ?>
                                        <li class="<?php if ($this->request->params['action'] == 'ticketHandlers') echo 'active'; ?>">
                                            <a href="<?php echo $this->request->webroot; ?>Tickets/ticketHandlers">
                                                <i class="fa fa-circle-o"></i> <span>PIM handlers</span>
                                            </a>
                                        </li>
                                    <?php } ?>
                                    <li class="<?php if ($this->request->params['action'] == 'ticketCalendar') echo 'active'; ?>">
                                        <a href="<?php echo $this->request->webroot; ?>Pages/ticket_calendar">
                                            <i class="fa fa-circle-o"></i> <span>Agenda</span>
                                        </a>
                                    </li>
                                    <li class="<?php if ($this->request->params['action'] == 'allTickets') echo 'active'; ?>">
                                        <a href="<?php echo $this->request->webroot; ?>Tickets/allTickets">
                                            <i class="fa fa-circle-o"></i> <span>Demandes Reçues</span>

                                        </a>
                                    </li>
                                    <li class="<?php if ($this->request->params['action'] == 'mySchedule') echo 'active'; ?>">
                                        <a href="<?= $this->request->webroot; ?>Pages/my_schedule">
                                            <i class="fa fa-circle-o"></i> <span>Mon Planning</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php
// ADD PERMISSION
                        $actionPermission = 0;
                        $controller = '';
                        $controller = 'NEXTRENEWALDATES';
                        $actionPermission = $appController->permissionsList($roleId, $controller);

                        if ($actionPermission == 1 || $actionPermission == 2) {
                            ?>
                            <li class="<?php if ($this->request->params['controller'] == 'NextRenewalDate' && ($this->request->params['action'] == "index" || $this->request->params['action'] == "add" || $this->request->params['action'] == "edit")) echo 'active'; ?> treeview">
                                <a href="#">
                                    <i class="fa fa-cogs"></i> <span>Réglages</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <?php if ($actionPermission == 1 || $actionPermission == 2) { ?>
                                        <li class="<?php if ($this->request->params['action'] == 'index') echo 'active'; ?>"><a href="<?php echo $this->request->webroot ?>NextRenewalDate/index"><i class="fa fa-circle-o"></i> Liste</a></li>
                                    <?php } if ($actionPermission == 2) { ?>

                                        <li class="<?php if ($this->request->params['action'] == 'index') echo 'active'; ?>"><a href="<?php echo $this->request->webroot ?>NextRenewalDate/add"><i class="fa fa-circle-o"></i> Date de renouvellement</a></li>
                                    <?php } ?>
                                    <?php
                                    // ADD PERMISSION
                                    $actionPermission = 0;
                                    $controller = '';
                                    $controller = "USERS";
                                    $actionPermission = $appController->permissionsList($roleId, $controller);
                                    if ($actionPermission == 1 || $actionPermission == 2) {
                                        ?>
                                        <li class="<?php if ($this->request->params['controller'] == 'Users' && ($this->request->params['action'] == "index" || $this->request->params['action'] == "add" || $this->request->params['action'] == "edit")) echo 'active'; ?> treeview">
                                            <a href="#">
                                                <i class="fa fa-user"></i> <span>Administrateurs</span>
                                                <span class="pull-right-container">
                                                    <i class="fa fa-angle-left pull-right"></i>
                                                </span>
                                            </a>
                                            <ul class="treeview-menu">
                                                <?php
                                                if ($actionPermission == 1 || $actionPermission == 2) {
                                                    ?>
                                                    <li class="<?php if ($this->request->params['action'] == 'index') echo 'active'; ?>"><a href="<?php echo $this->request->webroot ?>Users/"><i class="fa fa-circle-o"></i> Liste</a></li>
                                                    <!--<li class="<?php // if($this->request->params['action'] == 'add') echo 'active';                     ?>"><a href="<?php // echo $this->request->webroot                     ?>Users/add"><i class="fa fa-circle-o"></i> Add New</a></li>-->
                                                <?php } ?>
                                            </ul>
                                        </li>
                                    <?php } ?>
                                    <?php
                                    // ADD PERMISSION
                                    $actionPermission = 0;
                                    $controller = '';
                                    $controller = 'ROLES';
                                    $actionPermission = $appController->permissionsList($roleId, $controller);
                                    if ($actionPermission == 1 || $actionPermission == 2) {
                                        ?>
                                        <li class="<?php if ($this->request->params['controller'] == 'Roles' && ($this->request->params['action'] == "index" || $this->request->params['action'] == "add" || $this->request->params['action'] == "edit")) echo 'active'; ?> treeview">
                                            <a href="#">
                                                <i class="fa fa-user"></i> <span>Rôles</span>
                                                <span class="pull-right-container">
                                                    <i class="fa fa-angle-left pull-right"></i>
                                                </span>
                                            </a>
                                            <ul class="treeview-menu">
                                                <?php
                                                if ($actionPermission == 1 || $actionPermission == 2) {
                                                    ?>
                                                    <li class="<?php if ($this->request->params['action'] == 'index') echo 'active'; ?>"><a href="<?php echo $this->request->webroot ?>Roles/"><i class="fa fa-circle-o"></i> Liste</a></li>
                                                    <?php
                                                }
                                                if ($actionPermission == 2) {
                                                    ?>

                                                    <li class="<?php if ($this->request->params['action'] == 'add') echo 'active'; ?>"><a href="<?php echo $this->request->webroot ?>Roles/add"><i class="fa fa-circle-o"></i> Ajouter un Rôle</a></li>
                                                    <?php
                                                }
                                                ?>
                                            </ul>
                                        </li>
                                    <?php } ?>
                                    <?php
                                    // ADD PERMISSION
                                    $actionPermission = 0;
                                    $controller = '';
                                    $controller = 'EMAILS';
                                    $actionPermission = $appController->permissionsList($roleId, $controller);
                                    if ($actionPermission == 1 || $actionPermission == 2) {
                                        ?>

                                        <li class="<?php if ($this->request->params['controller'] == 'Emails' && ($this->request->params['action'] == "index" || $this->request->params['action'] == "add" || $this->request->params['action'] == "edit")) echo 'active'; ?> treeview">
                                            <a href="#">
                                                <i class="fa fa-envelope"></i> <span>Modèles d'Email</span>
                                                <span class="pull-right-container">
                                                    <i class="fa fa-angle-left pull-right"></i>
                                                </span>
                                            </a>
                                            <ul class="treeview-menu">
                                                <?php
                                                if ($actionPermission == 1 || $actionPermission == 2) {
                                                    ?>
                                                    <li class="<?php if ($this->request->params['action'] == 'index') echo 'active'; ?>"><a href="<?php echo $this->request->webroot ?>Emails/"><i class="fa fa-circle-o"></i> Liste</a></li>
                                                    <?php
                                                }
                                                if ($actionPermission == 2) {
                                                    ?>

                                                                                                                    <!-- <li class="<?php if ($this->request->params['action'] == 'add') echo 'active'; ?>"><a href="#"><i class="fa fa-circle-o"></i> Ajouter un nouveau</a></li> -->
                                                    <?php
                                                }
                                                ?>
                                            </ul>
                                        </li>
                                    <?php } ?>

                                </ul>
                            </li>
                        <?php } ?>
                        <?php
                        // ADD PERMISSION
                        $actionPermission = 0;
                        $controller = '';
                        $controller = 'CONTENTS';
                        $actionPermission = $appController->permissionsList($roleId, $controller);
                        if ($actionPermission == 1 || $actionPermission == 2) {
                            ?>

                            <li  class="<?php if ($this->request->params['controller'] == "ContentTypes" || $this->request->params['controller'] == "Contents") echo 'active'; ?> treeview">
                                <a href="#">
                                    <i class="fa fa-file-text"></i> <span>Site Web FGeM</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <?php
                                    if ($actionPermission == 1 || $actionPermission == 2) {
                                        ?>
                                        <li class="<?php if ($this->request->params['action'] == 'listPages') echo 'active'; ?>"><a href="<?php echo $this->request->webroot ?>ContentTypes/listPages"><i class="fa fa-circle-o"></i> Contenu</a></li>
                                        <?php
                                    }
                                    if ($actionPermission == 2) {
                                        ?>
                                        <li class="<?php if ($this->request->params['action'] == 'addPage') echo 'active'; ?>"><a href="<?php echo $this->request->webroot ?>ContentTypes/addPage"><i class="fa fa-circle-o"></i> Ajouter une Page</a></li>  
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php
                        // ADD PERMISSION
                        $actionPermission = 0;
                        $controller = '';
                        $controller = 'SLIDERS';
                        $actionPermission = $appController->permissionsList($roleId, $controller);
                        if ($actionPermission == 1 || $actionPermission == 2) {
                            ?>
                            <li class="<?php if ($this->request->params['controller'] == "Sliders") echo 'active'; ?> treeview">
                                <a href="#">
                                    <i class="fa fa-picture-o"></i> <span>Carrousel</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <?php
                                    if ($actionPermission == 1 || $actionPermission == 2) {
                                        ?>
                                        <li class="<?php if ($this->request->params['action'] == 'sliderlist') echo 'active'; ?>"><a href="<?php echo $this->request->webroot ?>Sliders/sliderlist"><i class="fa fa-circle-o"></i> Liste</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php
                        // ADD PERMISSION
                        $actionPermission = 0;
                        $controller = '';
                        $controller = 'Documentguides';
                        $actionPermission = $appController->permissionsList($roleId, $controller);
                        if ($actionPermission == 1 || $actionPermission == 2) {
                            ?>
                            <li class="<?php if ($this->request->params['controller'] == "Documentguides") echo 'active'; ?> treeview">
                                <a href="#">
                                    <i class="fa fa-file-o"></i> <span>Réservé aux Membres</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <?php
                                    if ($actionPermission == 2) {
                                        ?>
                                        <li class="<?php if ($this->request->params['action'] == 'guide_content') echo 'active'; ?>"><a href="<?php echo $this->request->webroot ?>Documentguides/guide_content"><i class="fa fa-circle-o"></i> Contenu de la page</a></li>
                                    <?php } ?>
                                    <?php
                                    if ($actionPermission == 1 || $actionPermission == 2) {
                                        ?>
                                        <!-- <li class="<?php if ($this->request->params['action'] == 'doclist') echo 'active'; ?>"><a href="<?php echo $this->request->webroot ?>Documentguides/doclist"><i class="fa fa-circle-o"></i> Document List</a></li> -->
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                        <?php
                        // ADD PERMISSION
                        $actionPermission = 0;
                        $controller = '';
                        $controller = 'CONTACTS';
                        $actionPermission = $appController->permissionsList($roleId, $controller);
                        if ($actionPermission == 1 || $actionPermission == 2) {
                            ?>
                            <li style="display:none;" class="<?php if ($this->request->params['controller'] == "Contacts") echo 'active'; ?> treeview">
                                <a href="#">
                                    <i class="fa fa-envelope"></i> <span>Contacts</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    <?php
                                    if ($actionPermission == 1 || $actionPermission == 2) {
                                        ?>
                                        <li class="<?php if ($this->request->params['action'] == 'contactlist') echo 'active'; ?>"><a href="<?php echo $this->request->webroot ?>Contacts/contactlist"><i class="fa fa-circle-o"></i> Liste</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->

                <section class="content-header">
                    <a href="<?php echo $this->request->webroot; ?>" class="logo hide-mb">
                        <img src="<?php echo $this->request->webroot; ?>img/logo.png" class="backend-logo" width="150"/>
                    </a>
                    <h1>
                        <?php //echo ucwords($this->request->params['controller']);     ?>
                        Section Administrative <?= isset($contentTitle) ? ' - ' . $contentTitle : ' - Espace Membre'; ?>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?php echo $this->request->webroot; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
                        <?php if ($this->request->params['controller'] != "" && $this->request->params['action'] != "") { ?>
                            <li><a href="<?php echo $this->request->webroot; ?><?php echo $this->request->params['controller']; ?>"><?php echo $this->request->params['controller']; ?></a></li>
                            <li class="active"><?php
                                if ($this->request->params['action'] == 'index') {
                                    echo 'Listing';
                                } else {
                                    echo $this->request->params['action'];
                                }
                                ?></li>
                        <?php } ?>

                    </ol>
                </section>
                <div class="container" style="margin-bottom: 20px;max-width:650px;max-height: 22px;">

                    <?= $this->Flash->render() ?>
                </div> 
                <?php echo $this->fetch('content'); ?>
            </div>  


            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <small> Développé par: <a href="https://www.cyberclouds.com">Cyber Clouds</a></small> | <b>Version</b> 1.0
                </div>
                <strong>Droits d'auteur &copy; <?php echo date('Y') ?> <a href="#">FGEM</a>.</strong> Tous les droits sont réservés. 
            </footer>
        </div>

        <?php //echo $this->element('sql_dump');     ?>

        <?php echo $this->Html->script('ckeditor/ckeditor'); ?>       
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
            $.widget.bridge('uibutton', $.ui.button);
        </script>
        <?php echo $this->Html->script('bootstrap.min'); ?>   
        <!-- Morris.js charts -->
        <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>-->

        <?php // echo $this->Html->script('morris.min');  ?> 
        <?php // echo $this->Html->script('jquery.sparkline'); ?> 
        <?php // echo $this->Html->script('jquery-jvectormap-1.2.2.min');   ?> 
        <?php // echo $this->Html->script('jquery-jvectormap-world-mill-en');   ?> 
        <?php // echo $this->Html->script('jquery.knob');    ?> 

        <!-- daterangepicker -->
        <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>-->
        <?php // echo $this->Html->script('daterangepicker');  ?> 
        <?php echo $this->Html->script('timepicker'); ?> 
        <?php echo $this->Html->script('bootstrap-datepicker'); ?> 
        <?php // echo $this->Html->script('bootstrap3-wysihtml5.all.min'); ?> 
        <?php echo $this->Html->script('jquery.slimscroll.min'); ?> 
        <?php // echo $this->Html->script('fastclick'); ?> 
        <?php echo $this->Html->script('app.min'); ?> 
        <?php // echo $this->Html->script('dashboard');   ?> 
        <?php // echo $this->Html->script('demo');    ?> 

        <script>
            jQuery(function () {
                jQuery('.datepicker').datepicker({
                    'format': 'yyyy-mm-dd'
                }).on('changeDate', function (e) {
                    jQuery(this).datepicker('hide');
                });

            });
            function readURL(input, showId = null) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    //alert(input.files[0]);   
                    reader.onload = function (e) {

                        if (showId)
                        {
                            if (input.id == "sliderImage") {
                                $('#show_uploaded_image' + showId)
                                        .attr('src', e.target.result);
                            }
                            if (input.id == "sectionImage") {
                                $('#show_section_uploaded_image' + showId)
                                        .attr('src', e.target.result);
                            }
                        } else {
                            if (input.id == "sliderImage") {
                                $('#show_uploaded_image')
                                        .attr('src', e.target.result);
                            }
                            if (input.id == "sectionImage") {
                                $('#show_section_uploaded_image')
                                        .attr('src', e.target.result);
                            }
                        }
                    };

                    reader.readAsDataURL(input.files[0]);
            }
            }
        </script>
        <script type="text/javascript">
    $(document).ready(function(){


            if (navigator.appName == 'Microsoft Internet Explorer' ||  !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv:11/)) || (typeof $.browser !== "undefined" && $.browser.msie == 1))
                {
                  $(".browser-message").show().html("<div class='browser-text'><p><img src='<?php echo $this->request->webroot; ?>front/img/logo-light.png' style='margin:auto;display:block;'/></p><p>Nous vous remercions de votre visite et de l’intérêt que vous portez à la Fédération Genevoise MédiationS. </p><p>Nous avons le regret de devoir vous informer que votre navigateur n’est malheureusement plus supporté par son développeur. Malgré tout nos efforts, nous n’avons pas été en mesure de concevoir notre site, qui utilise les dernières technologies, de manière à le rendre compatible avec la version du navigateur que vous utilisez.</p><p>Nous vous invitons à télécharger l’un des navigateurs suivants qui vous permettra d’accéder et d’utiliser notre site dans les meilleures conditions.</p><p>En cas de difficultés, nous vous invitons à prendre contact avec nous en nous envoyant un message à <a href='mailto:communication@fgem.ch'>communication@fgem.ch</a></p><p>Nous vous remercions par avance et nous vous prions de bien vouloir nous excuser de ce désagrément.</p> <p><a href='https://www.mozilla.org/en-US/firefox/new/?redirect_source=firefox-com' title='Mozilla Firefox'><img width='100' src='https://cdn.iconscout.com/icon/free/png-256/firefox-20-569406.png'></a><a href='https://www.google.com/chrome/' title='Google Chrome'><img width='90' src='https://www.google.com/chrome/static/images/chrome-logo.svg'></a><a href='https://www.apple.com/safari/'><img width='85' src='https://www.apple.com/v/safari/i/images/overview/safari_icon_large.png'></a></p></div>");
                }



                });
        </script>
    </body>
</html>
