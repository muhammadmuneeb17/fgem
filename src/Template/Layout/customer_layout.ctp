<!doctype html>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>FGeM</title>
        <style id='dashicons-inline-css' type='text/css'>
            .vc_custom_1501409171141{padding-top:10px !important;padding-right:2% !important;padding-left:2% !important;background-color:#151c28 !important}.vc_custom_1501324681366{padding-top:0px !important;padding-right:0px !important;padding-left:0px !important}.vc_custom_1501324688350{padding-top:0px !important;padding-right:0px !important;padding-left:0px !important}.vc_custom_1501391060831{background-color:#18202e !important}.vc_custom_1501335087266{border-top-width:1px !important;background-color:#1a2332 !important;border-top-color:#28364d !important;border-top-style:solid !important}.vc_custom_1501400150790{padding-top:7px !important}.vc_custom_1501400154823{padding-top:7px !important}.vc_custom_1501400158285{padding-top:7px !important}.vc_custom_1501398234611{margin-bottom:21px !important}.vc_custom_1501397786962{margin-bottom:16px !important}.vc_custom_1501335220553{padding-top:12px !important}.vc_custom_1501335264161{margin-bottom:15px !important}
            [data-font="Dashicons"]:before {font-family: 'Dashicons' !important;content: attr(data-icon) !important;speak: none !important;font-weight: normal !important;font-variant: normal !important;text-transform: none !important;line-height: 1 !important;font-style: normal !important;-webkit-font-smoothing: antialiased !important;-moz-osx-font-smoothing: grayscale !important;}
            .vc_custom_1502811638541{margin-bottom:5px !important}.vc_custom_1502811644854{margin-bottom:5px !important}.vc_custom_1502811650547{margin-bottom:5px !important}.vc_custom_1502811656403{margin-bottom:5px !important}
        </style>
         <style>
            
            .browser-text {
                
                padding:40px;
                color:#fff;
                max-width: 800px;
                margin:auto;
                margin-top:20px;   
            }
            .browser-text a {
                color:#fff;   
            }
            .browser-message {
                display: none;
            }
        </style>

        <link rel='stylesheet' id='motor-less-css'  href='<?php echo $this->request->webroot; ?>frontend/wp-content/themes/motor/wp-less-cache/motor-less.css' type='text/css' media='all' />
        <link rel='stylesheet' id='js_composer_front-css'  href='<?php echo $this->request->webroot; ?>frontend/wp-content/plugins/js_composer/assets/css/js_composer.min.css' type='text/css' media='all' />
        <link rel='stylesheet' id='clever-mega-menu-motor-css'  href='<?php echo $this->request->webroot; ?>frontend/wp-content/uploads/clever-mega-menu/clever-mega-menu-theme-motor.css' type='text/css' media='all' />
        <link rel='stylesheet' id='dgwt-wcas-style-css'  href='<?php echo $this->request->webroot; ?>frontend/bootstrap/bootstrap.min.css' type='text/css' media='all' />

        <script type='text/javascript' src='<?php echo $this->request->webroot; ?>frontend/wp-includes/js/jquery/jquery.js'></script>
        <script type='text/javascript' src='<?php echo $this->request->webroot; ?>frontend/wp-includes/js/jquery/jquery-migrate.min.js'></script>

        <link rel='stylesheet' id='font-awesome-css'  href='wp-content/plugins/js_composer/assets/lib/bower/font-awesome/css/font-awesome.min.css' type='text/css' media='all' />
        <style id='font-awesome-inline-css' type='text/css'>
            [data-font="FontAwesome"]:before {font-family: 'FontAwesome' !important;content: attr(data-icon) !important;speak: none !important;font-weight: normal !important;font-variant: normal !important;text-transform: none !important;line-height: 1 !important;font-style: normal !important;-webkit-font-smoothing: antialiased !important;-moz-osx-font-smoothing: grayscale !important;}
        </style>

        <link rel='stylesheet' href='<?php echo $this->request->webroot; ?>frontend/bootstrap/custom.css' type='text/css' media='all' />
        <link rel='stylesheet' href='<?php echo $this->request->webroot; ?>js/tags/jquery.tagsinput.css' type='text/css' media='all' />
        <link rel='stylesheet' href='https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css' type='text/css' media='all' />

        <?php echo $this->Html->script('jquery-2.2.3.min'); ?>  
        <?php
        echo $this->Html->css('font-awesome');

        echo $this->Html->css('_all-skins.min');
        echo $this->Html->css('blue');
        echo $this->Html->css('morris');
        echo $this->Html->css('jquery-jvectormap-1.2.2');
        echo $this->Html->css('datepicker3');
        echo $this->Html->css('timepicker');
        echo $this->Html->css('daterangepicker');
        echo $this->Html->css('bootstrap3-wysihtml5.min');
        //echo $this->Html->css('style');
        echo $this->Html->css('bootstrap.min');
        echo $this->Html->css('AdminLTE.min');
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        ?>

        <style>
            .member-btn , .member-btn:hover {
                background: #147172 !important;
                color:#fff !important;
                padding: 5px 10px !important;
            }
            .logo{
                max-width: 155px !important;
            }
            .header .header-logo img{
                top: 40% !important;
            }
            .member_profile{
                padding-left: 15px;
            }
            .member_profile img{
                width: 45px;
                height: 45px;
                border-radius: 50%;
                float: left;
                margin-right: 10px;
            }

        </style>
    </head>
    <body style="background-color: #18202E !important;" class="home page-template-default page page-id-543 wpb-js-composer js-comp-ver-5.4.7 vc_responsive">

        <div class="browser-message" style="position:fixed;z-index:9999999999;left:0px;top:0px;background: rgba(0,0,0,0.98);width:100%;height:100%;color:#fff;">
        
    </div>

        <div id="page" class="site">


            <div id="masthead" class="header">

                <a href="#" class="header-menutoggle" id="header-menutoggle">Menu</a>

                <p class="header-logo">
                    <a href="<?php echo $this->request->webroot; ?>pages/memberdashboard"><img class="logo" src="<?php echo $this->request->webroot ?>front/img/logo-light.png" alt="FGEM"></a> </p>

                <div id="cmm-rw-top-menu" class="cmm-container" style="width:80%">
                    <ul id="menu-top-menu" class="cmm-theme-motor cmm cmm-horizontal cmm-horizontal-align-left cmm-menu-fade" data-options='{&quot;menuStyle&quot;:&quot;horizontal&quot;,&quot;parentSelector&quot;:&quot;.cmm&quot;,&quot;breakPoint&quot;:&quot;768&quot;}' data-mobile='{&quot;toggleDisable&quot;:&quot;0&quot;,&quot;toggleWrapper&quot;:&quot;.cmm-container&quot;,&quot;ariaControls&quot;:&quot;&quot;,&quot;toggleIconOpen&quot;:&quot;dashicons dashicons-menu&quot;,&quot;toggleIconClose&quot;:&quot;dashicons dashicons-no-alt&quot;,&quot;toggleMenuText&quot;:&quot;Menu&quot;}'>

                        <?php
                        //    debug($this->request->session()->read('Auth.User.first_name'));
                        foreach ($menus as $key => $menu):
                            $slug = $menu['slug'];
                            if (@$this->request->params['pass'][0] == "") {
                                $this->request->params['pass'][0] = "home";
                            }
                            ?>
                            <li <?php
                            if ($this->request->params['pass'][0] == $slug) {
                                echo "class='active'";
                            }
                            ?>><a href="<?php echo $this->request->webroot; ?>contents/page/<?php echo $slug ?>"><?php echo h(ucwords($menu['title'])); ?></a>
                                    <?php //if($menu['id'] == 19) { ?>
                                <!--  <ul style="margin-top: 10px;">
                                     <li ><a style="color: red;" href="<?php echo $this->request->webroot; ?>registerations/view_members">Annuaire des Médiateurs</a></li>
                                 </ul> -->
                                <?php //} ?>
                            </li>
                        <?php endforeach; ?>
                        <li ><a  href="<?php echo $this->request->webroot; ?>registerations/view_members">Trouver des Médiateurs</a>
                        </li>

                    </ul>

                </div>

            </div>
            <aside class="main-sidebar" style="position: absolute !important; top:98px !important;">
                <!-- sidebar: style can be found in sidebar.less -->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button" style="position: absolute; right: -30px; background-color: #18202E; padding: 5px 10px;">
                    <i class="fa fa-bars"></i>
                </a>
                <section class="sidebar">

                    <ul class="sidebar-menu">
                        <li class="treeview">
                            <a href="<?php echo $this->request->webroot; ?>Tickets">
                                <i class="fa fa-file-text-o"></i> <span>Des billets</span>
                            </a>

                        </li>
                        <li class="treeview">
                            <a href="<?php echo $this->request->webroot; ?>Tickets/add">
                                <i class="fa fa-plus"></i> <span>Nouveau billet</span>
                            </a>

                        </li>
                        <li class="treeview">
                            <a href="<?php echo $this->request->webroot; ?>users/logout">
                                <i class="fa fa-sign-out"></i> <span>Se déconnecter</span>

                            </a>

                        </li>
                    </ul>
                </section>

                <!-- /.sidebar -->
            </aside>
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">

                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12" id="sote_name">

                                <h1>
                                    <?= isset($contentTitle) ? $contentTitle : '' ?>
                                </h1>
                            </div>
                        </div>
                    </div>

                </section>
                <div class="container mymsg" style="max-width:650px; position: relative;height: 22px;">

                    <?= $this->Flash->render() ?>
                </div> 
                <?php echo $this->fetch('content'); ?>
            </div>




            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <small> Developed By: <a href="https://www.cyberclouds.com">Cyber Clouds</a></small>
                </div>
                <strong>Copyright &copy; FGeM.</strong> Toute reproduction strictement interdite sans autorisation préalable écrite de la <a href="mailto:info@fgem.ch">FGEM</a>
            </footer>

        </div>


        <!--        <script type='text/javascript' src='https://use.fontawesome.com/30858dc40a.js'></script>-->
        <script type='text/javascript' src='<?php echo $this->request->webroot; ?>frontend/wp-content/plugins/clever-mega-menu/assets/frontend/js/clever-mega-menu.min.js'></script>
        <script type='text/javascript' src='<?php echo $this->request->webroot; ?>frontend/bootstrap/bootstrap.min.js'></script>
        <?php echo $this->Html->script('ckeditor/ckeditor'); ?>   

<!--        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>-->
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

        <?php echo $this->Html->script('timepicker'); ?> 
        <?php echo $this->Html->script('jquery-ui'); ?> 
        <?php //echo $this->Html->script('bootstrap-datepicker'); ?>
        <?php echo $this->Html->script('jquery.slimscroll.min'); ?> 
        <?php echo $this->Html->script('app.min'); ?> 

    </script>
    <script>
        jQuery(function () {
            $(document).on('focus', '.datepicker', function () {
                jQuery(this).datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'dd.mm.yy'

                });
            });

        });

        function readURL(input, showId = null) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                //alert(input.files[0]);   
                reader.onload = function (e) {

                    if (showId)
                    {
                        if (input.id == "sliderImage") {
                            jQuery('#show_uploaded_image' + showId)
                                    .attr('src', e.target.result);
                        }
                        if (input.id == "sectionImage") {
                            jQuery('#show_section_uploaded_image' + showId)
                                    .attr('src', e.target.result);
                        }
                    } else {
                        if (input.id == "sliderImage") {
                            jQuery('#show_uploaded_image')
                                    .attr('src', e.target.result);
                        }
                        if (input.id == "sectionImage") {
                            jQuery('#show_section_uploaded_image')
                                    .attr('src', e.target.result);
                        }
                    }
                };

                reader.readAsDataURL(input.files[0]);
        }
        }
   
    </script>
    <script type="text/javascript">
    $(document).ready(function(){


            if (navigator.appName == 'Microsoft Internet Explorer' ||  !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv:11/)) || (typeof $.browser !== "undefined" && $.browser.msie == 1))
                {
                  $(".browser-message").show().html("<div class='browser-text'><p><img src='<?php echo $this->request->webroot; ?>front/img/logo-light.png' style='margin:auto;display:block;'/></p><p>Nous vous remercions de votre visite et de l’intérêt que vous portez à la Fédération Genevoise MédiationS. </p><p>Nous avons le regret de devoir vous informer que votre navigateur n’est malheureusement plus supporté par son développeur. Malgré tout nos efforts, nous n’avons pas été en mesure de concevoir notre site, qui utilise les dernières technologies, de manière à le rendre compatible avec la version du navigateur que vous utilisez.</p><p>Nous vous invitons à télécharger l’un des navigateurs suivants qui vous permettra d’accéder et d’utiliser notre site dans les meilleures conditions.</p><p>En cas de difficultés, nous vous invitons à prendre contact avec nous en nous envoyant un message à <a href='mailto:communication@fgem.ch'>communication@fgem.ch</a></p><p>Nous vous remercions par avance et nous vous prions de bien vouloir nous excuser de ce désagrément.</p> <p><a href='https://www.mozilla.org/en-US/firefox/new/?redirect_source=firefox-com' title='Mozilla Firefox'><img width='100' src='https://cdn.iconscout.com/icon/free/png-256/firefox-20-569406.png'></a><a href='https://www.google.com/chrome/' title='Google Chrome'><img width='90' src='https://www.google.com/chrome/static/images/chrome-logo.svg'></a><a href='https://www.apple.com/safari/'><img width='85' src='https://www.apple.com/v/safari/i/images/overview/safari_icon_large.png'></a></p></div>");
                }



                });
        </script>
</body>
</html>
