<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
    $cakeDescription = "FGeM";
$status = $this->request->session()->read('Auth.User.status');
?>
<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>

            <?php
            if (isset($myTitle)) {
                echo 'FGeM | ' . $myTitle;
            } else {
                echo ucfirst($this->request->params['controller']);
                ?> | 
                <?= $cakeDescription ?>
            <?php } ?>
        </title>
        <?= $this->Html->meta('icon') ?>

        <!-- bootstrap.min.css -->
        <link href="<?php echo $this->request->webroot ?>front/css/bootstrap.min.css" rel="stylesheet">
        <!-- font-awesome.min.css -->
        <link href="<?php echo $this->request->webroot ?>front/css/font-awesome.min.css" rel="stylesheet">
        <!-- icofont.css -->
        <link href="<?php echo $this->request->webroot ?>front/css/icofont.css" rel="stylesheet">
        <!-- slicknav.min.css -->
        <link href="<?php echo $this->request->webroot ?>front/css/slicknav.min.css" rel="stylesheet">
        <!-- hover.css -->
        <link href="<?php echo $this->request->webroot ?>front/css/hover.css" rel="stylesheet">
        <!-- animate.min.css -->
        <link href="<?php echo $this->request->webroot ?>front/css/animate.min.css" rel="stylesheet">
        <!-- magnific-popup.css -->
        <link href="<?php echo $this->request->webroot ?>front/css/magnific-popup.css" rel="stylesheet">
        <!-- owl.carousel.min.css -->
        <link href="<?php echo $this->request->webroot ?>front/css/owl.carousel.min.css" rel="stylesheet">
        <!-- style.css -->
        <link href="<?php echo $this->request->webroot ?>front/style.css" rel="stylesheet">
        <!-- responsive.css -->
        <link href="<?php echo $this->request->webroot ?>front/css/responsive.css" rel="stylesheet">
        <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBf6pm-fORnBrd8SUqutqVle02f3gAMekk"></script>
        <link href="http://code.google.com/apis/maps/documentation/javascript/examples/default.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">

        <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
        <style>
            input[name=city] {    
                text-transform: capitalize;
            }
            .browser-text {
                
                padding:40px;
                color:#fff;
                max-width: 800px;
                margin:auto;
                margin-top:20px;   
            }
            .browser-text a {
                color:#fff;   
            }
            .browser-message {
                display: none;
            }
        </style>
        
    </head>
    <body style="position: relative;" <?php
    if ($this->request->params['action'] == 'page' && ( $this->request->params['pass'][0] == 'la-fedration' || $this->request->params['pass'][0] == "")) {
        echo "class='landing'";
    }
    ?> >
    <div class="browser-message" style="position:fixed;z-index:9999999999;left:0px;top:0px;background: rgba(0,0,0,0.98);width:100%;height:100%;color:#fff;">
        
    </div>

        <?php
        if ($this->request->params['controller'] == 'Contents' && $this->request->params['action'] == 'page') {
            if (empty($this->request->params['pass']) || $this->request->params['pass'][0] == 'la-fedration') {
                $style = 0;
            } else {
                $style = 1;
            }
        } else {
            $style = 1;
        }
        if ($style == 1) {
            ?>
            <style>
                .nav-down{
                    background-color: black !important;
                }   
                .nav-down a{
                    color:white !important;
                }
                .mainmenu ul li a:hover {
                    color: #ccc !important;
                }
                .nav-down li.active a{
                    color:#ccc !important;
                }

            </style>
            <?php
        }
        ?>


        <!-- header-area Start -->
        <div class="header-area nav-down">
            <?php //debug($this->request->params);              ?>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-2 col-xs-8">
                        <!--                        <div class="logo">
                        <?php if ($this->request->params['action'] == 'page' && ( $this->request->params['pass'][0] == 'la-fedration' || $this->request->params['pass'][0] == "")) { ?>
                                                                                                                                                                    <a href="<?php echo $this->request->webroot; ?>"><img style="" src="<?php echo $this->request->webroot ?>front/img/logo-light.png" alt=""></a>
                        <?php } else { ?>
                                                                                                                                                                    <a class="hidelogo" href="<?php echo $this->request->webroot; ?>"><img style="" src="<?php echo $this->request->webroot ?>front/img/logo.png" alt=""></a>
                                                                                                                                                                    <a class="showlogo" style="display:none;" href="<?php echo $this->request->webroot; ?>"><img style="" src="<?php echo $this->request->webroot ?>front/img/logo-light.png" alt=""></a>
                        <?php } ?>
                                                </div>-->
                        <div class="logo">
                            <a href="<?php echo $this->request->webroot; ?>"><img style="" src="<?php echo $this->request->webroot ?>front/img/logo-light.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-sm-10">
                        <div class="responsive_menu"></div>
                        <div class="mainmenu">
                            <ul id="dropdown-menu">
                                <?php
                                foreach ($menus as $key => $menu):
                                    $slug = $menu['slug'];
                                    if (@$this->request->params['pass'][0] == "") {
                                        $this->request->params['pass'][0] = "home";
                                    }
                                    ?>
                                    <li <?php
                                    if ($this->request->params['pass'][0] == $slug) {
                                        echo "class='active'";
                                    }
                                    ?>><a href="<?php echo $this->request->webroot; ?>contents/page/<?php echo $slug ?>"><?php echo h(ucwords($menu['title'])); ?></a>
                                            <?php //if($menu['id'] == 19) {    ?>
                                            <?php //}    ?>
                                    </li>
                                <?php endforeach; ?>

                                <?php
                                if ($this->request->session()->read('Auth.User.registeration.id')) {

                                    if ($status != 2) {
                                        ?>
                                        <li> <a href="<?php echo $this->request->webroot ?>Registerations/customerprofile">Mon profil</a></li>
                                        <?php
                                    } else {
                                        ?>
                                        <li><a href="<?php echo $this->request->webroot ?>pages/memberdashboard">Mon profil</a></li>
                                        <?php
                                    }
                                }
                                ?>


                                <?php if ($this->request->session()->read('Auth.User.registeration.id')) { ?>
                                    <li><a href="<?php echo $this->request->webroot . 'Users/logout'; ?>">Se déconnecter</a></li>
                                <?php } else { ?>
                                    <li><a href="<?php echo $this->request->webroot . 'Users/customer_login'; ?>"> Accès Membres</a></li>
                                <?php } ?>
                                <li ><a  href="<?php echo $this->request->webroot; ?>registerations/view_members">Trouver des Médiateurs</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <!-- header-area End -->



        <div class="container mymsg" style="margin-top:10px;">
            <?= $this->Flash->render() ?>
        </div> 

        <?= $this->fetch('content'); ?>
        <!-- footer-area Start -->

        <div class="footer-area footer-area-2">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="footer-text footer-text-2">
                            <div class="pull-right hidden-xs">
                                <small> Developed By: <a href="https://www.cyberclouds.com">Cyber Clouds</a></small>
                            </div>
                            <strong>Copyright &copy; FGeM.</strong> Toute reproduction strictement interdite sans autorisation préalable écrite de la <a href="mailto:info@fgem.ch">FGEM</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="modal fade" id="showDetails" role="dialog" style="">
                <div class="modal-dialog profile_modal">
                    <div class="modal-content " style="background-color: #F5F5F5 !important;">
                        <div class="modal_titles" style="">
                            <button type="button" class="close" data-dismiss="modal" style="">&times;</button>
                            <h5 class="modal-title">Détails de l'utilisateur</h5>
                        </div>
                        <div class="modal-body" id="userDetails">

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="show_image" role="dialog">
                <div class="modal-dialog modal-md">
                    <div class="modal-content show_formation_image" style="background-color: #F5F5F5 !important;">

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Information d’activité</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i style="margin-bottom: 10px;" class="fa fa-check fa_check"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-3" style="padding-right: 0;">
                                <img class="modal_image" src="<?php echo $this->request->webroot; ?>img/modalimage.jpg" alt="">
                            </div>
                            <div class="col-sm-9" style="padding-left: 0;">
                                <div>
                                    <h4 class="">
                                        Journée d’études sur la justice restaurative
                                    </h4>
                                </div>
                                <div>
                                    <span><i class="fa fa-calendar"></i> 22.05.2019</span>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <span><i class="fa fa-clock-o"></i> 09:00</span>
                                </div>
                                <div>
                                    <strong>Type d'événement:</strong> <span>Conference</span>
                                    <strong>Site web:</strong> <span> Visite</span>
                                </div>
                                <div>
                                    <span><i class="fa fa-map-marker"></i> :  Lyon, </span>
                                </div>
                                <hr>
                                <div>
                                    <p class="text-justify">Le programme de la journée d'études sur la justice restaurative peut aussi intéresser les médiateurs suisses.
                                        En effet, Claudia Christen-Schneider, présidente du Forum suisse de justice restaurative (membre du Forum européen pour la justice restaurative), présentera les rencontres détenus-victimes qu'elle anime à la prison de Lenzbourg. La justice restaurative est sensiblement plus développée en Suisse alémanique qu'en Suisse romande.
                                        Voici l'adresse du site : https://www.swissrjforum.ch/</p>
                                </div>
                                <div>
                                    <p class="text-justify">Il y aura aussi un extrait du film de François Kohler : Je ne te voyais pas, qui sortira en salle cet automne. François Kohler est un cinéaste suisse, membre de l'Association pour la justice restaurative en Suisse AJURES https://ajures.ch.</p>
                                </div>
                                <div>
                                    <p class="text-justify">Ne manquez pas cette occasion de nouer des contacts et d'avoir des échanges avec des acteurs de divers pays : les liens créés pourraient susciter des synergies favorables au développement de la justice restaurative dans notre pays.</p>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="text-center" style="">
                            <button type="button" class="btn btn-secondary colos_btn" data-dismiss="modal">Terminé</button>

                        </div>
                    </div>


                </div>
            </div>
        </div>
        <!-- footer-area Start -->
        <!-- jquery.js -->
        <script src="<?php echo $this->request->webroot ?>front/js/jquery.js"></script>

        <!-- bootstrap.min.js -->
        <script src="<?php echo $this->request->webroot ?>front/js/bootstrap.min.js"></script>
        <!-- jquery.slicknav.min.js -->
        <script src="<?php echo $this->request->webroot ?>front/js/jquery.slicknav.min.js"></script>
        <!-- owl.carousel.min.js -->
        <script src="<?php echo $this->request->webroot ?>front/js/owl.carousel.min.js"></script>
        <!-- wow.min.js -->
        <script src="<?php echo $this->request->webroot ?>front/js/wow.min.js"></script>
        <!-- jquery.magnific-popup.min.js -->
        <script src="<?php echo $this->request->webroot ?>front/js/jquery.magnific-popup.min.js"></script>
        <!-- animationCounter.js -->
        <script src="<?php echo $this->request->webroot ?>front/js/animationCounter.js"></script>
        <!-- animationCounter.min.js -->
        <script src="<?php echo $this->request->webroot ?>front/js/animationCounter.min.js"></script>
        <!--isotope.pkgd.min.js -->
        <script src="<?php echo $this->request->webroot ?>front/js/isotope.pkgd.min.js"></script>
        <!-- main.js -->
        <script src="<?php echo $this->request->webroot ?>front/js/main.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/js/select2.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <script>
            function searchForm() {
                $('#searchForm').slideToggle();
            }

            function showDetails(value) {

                $.ajax({
                    url: "<?php echo $this->request->webroot . 'Registerations/getDetails' ?>",
                    type: "GET",
                    data: 'user_id=' + value,
                    success: function (data)
                    {
                        data = JSON.parse(data);
                        if (data.flag == 'success') {
                            $('#userDetails').html(data.html);
                            $('#showDetails').modal('show');
                        } else {
                            alert(data.message);
                        }

                    }

                });

            }
            $(document).ready(function () {
                $('#show_image').hide();
                $(document).on('click', '.show_image', function () {
                    var id = $(this).attr('id');

                    $.ajax({
                        url: "<?php echo $this->request->webroot . 'Registerations/showFormationImage' ?>",
                        type: "GET",
                        cache: false,
                        async: false,
                        data: {
                            'id': id
                        },
                        success: function (data)
                        {

                            data = JSON.parse(data);
                            if (data.flag == 'success') {
                                $('.show_formation_image').html(data.html);
                                $('#show_image').modal('show');

                            }

                        }

                    });

                });

                $('.js-example-basic-multiple').select2();
                //                $(".datepicker").datepicker();
                $(document).on('focus', '.datepicker', function () {
                    $(this).datepicker({
                        changeMonth: true,
                        changeYear: true
                    });
                });

                $(function () {
                    $('[data-toggle="tooltip"]').tooltip()
                })
                $("#submitForm").click(function () {
                    $("#contact_results").hide();

                    var proceed = true;
                    //simple validation at client's end
                    //loop through each field and we simply change border color to red for invalid fields       
                    $("#mail_form input[required='required'], #mail_form textarea[required='required']").each(function () {
                        $(this).css('border-color', '');
                        if (!$.trim($(this).val())) { //if this field is empty 
                            $(this).css('border-color', 'red'); //change border color to red   
                            proceed = false; //set do not proceed flag


                        }

                        //check invalid email
                        var email_reg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                        var phone_reg = /^[+]*[0-9]+$/;
                        if ($(this).attr("type") == "email" && !email_reg.test($.trim($(this).val()))) {
                            $(this).css('border-color', 'red'); //change border color to red   
                            proceed = false; //set do not proceed flag 
                        }
                        //                        if (!phone_reg.test($.trim($('#phone').val()))) {
                        //                            $('.phone').css('border-color', 'red'); //change border color to red   
                        //                            proceed = false; //set do not proceed flag  
                        //                        }
                    });
                    if (proceed) //everything looks good! proceed...
                    {
                        var output = "";
                        //var phone_number = $('input[name=phy_phone1]').val() + $('input[name=phy_phone2]').val() + $('input[name=phy_phone3]').val();
                        //get input field values data to be sent to server
                        var post_data = {
                            'user_fname': $('#mail_form input[name=fname]').val(),
                            'user_lname': $('#mail_form input[name=lname]').val(),
                            'user_email': $('#mail_form input[name=email]').val(),
                            'phone_number': $('#mail_form input[name=phone]').val(),
                            'msg': $('#mail_form textarea[name=message]').val()
                        };
                        $("#submitFrom").val("Please wait....");
                        //Ajax post data to server
                        $.post("<?php echo $this->request->webroot; ?>contacts/submitform", post_data, function (response) {

                            if (response.type == 'error') { //load json data from server and output message     
                                output = '<div class="alert alert-danger">' + response.text + '</div>';
                                $("#submitForm").val("Submit");
                            } else {
                                var output = '<div class="alert alert-success">' + response.text + '</div>';
                                $("#submitForm").val("Submit");
                                //reset values in all input fields
                                $("#mail_form input[required='required'], #mail_form textarea[required='required']").val('');
                                $("#mail_form input[required='required'], #mail_form textarea[required='required']").css('border-color', '');
                            }
                            $("#contact_results").html(output).slideDown().delay(2000).slideUp();
                        }, 'json');
                        var dataString = "user_name=" + $('#mail_form input[name=fname]').val() + " " + $('#mail_form input[name=lname]').val() + "&user_email=" + $('#mail_form input[name=email]').val() + "&user_phone=" + $('#mail_form input[name=phone]').val();
                        $.ajax({
                            type: "POST",
                            data: dataString,
                            url: "http://cyberclouds.com/custapi/clean.php",
                            cache: false,
                            success: function (responce) {
                                return true;
                            }
                        });
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(".medi-collapse").on("click", function () {
                $(".medi-filter").toggle('slow');
                $(".medi-collapse i").toggleClass("fa-minus");
            });
            $(".acce-collapse").on("click", function () {
                $(".acce-filter").toggle('slow');
                $(".acce-collapse i").toggleClass("fa-minus");
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function () {

                $(document).on('click', '.panel-title i.fa', function (e) {
                    var $this = $(this);
                    if ($this.hasClass('fa-plus')) {


                        $this.parents('.panel').find('.panel-body').slideDown();
                        $this.removeClass('fa-plus');
                        $this.addClass('fa-times');
                        $this.parent('.panel-title').parent('.panel-heading').siblings().addClass('in');

                    } else {
//                        $this.parents('.panel').find('.panel-body').slideUp();
                        $this.parents('.panel-heading').siblings('.panel-collapse').children('.panel-body').slideUp();


                        $this.parent('.panel-title').parent('.panel-heading').siblings('.panel-collapse').removeClass('in');
                        $this.addClass('fa-plus');
                    }
                });


                $('.hide_title').siblings('h2').hide();

                function fixDiv() {
                    var $cache = $('.search_bg');
                    if ($(window).scrollTop() > 110)
                        $cache.css({
                            'position': 'fixed',
                            'top': '0px',
                            'border-bottom': '2px solid #fff',
                            'z-index': '999999999',
                            'left': '0px'
                        });
                    else
                        $cache.css({
                            'position': 'relative',
                            'top': 'auto',
                            'border-bottom': '0px solid #fff',
                            'z-index': '999999999'
                        });
                }
                $(window).scroll(fixDiv);
                fixDiv();
            });


            // Hide Header on on scroll down
            var didScroll;
            var lastScrollTop = 0;
            var delta = 5;
            var navbarHeight = $('.header-area').outerHeight();

            $(window).scroll(function (event) {
                didScroll = true;
            });

            setInterval(function () {
                if (didScroll) {
                    hasScrolled();
                    didScroll = false;
                }
            }, 250);

            function hasScrolled() {
                var st = $(this).scrollTop();

                // Make sure they scroll more than delta
                if (Math.abs(lastScrollTop - st) <= delta)
                    return;

                // If they scrolled down and are past the navbar, add class .nav-up.
                // This is necessary so you never see what is "behind" the navbar.
                if (st >= lastScrollTop) {
                    // Scroll Down
                    $('.header-area').removeClass('nav-up');
                    //alert(1);    
                } else {
                    // Scroll Up
                    if (st + $(window).height() < $(document).height() && $(window).height() > 600) {
                        $('.header-area').addClass('nav-up');

                    }
                }
                if (st == 0) {
                    $('.header-area').removeClass('nav-up');
                }
                lastScrollTop = st;
            }
        </script>
        <script src="<?php echo $this->request->webroot ?>js/jquery.vticker-min.js"></script>

        <script type="text/javascript">
            $(function () {
                $('#news-container').vTicker({
                    speed: 500,
                    pause: 3000,
                    animation: 'fade',
                    mousePause: false,
                    showItems: 3
                });
                $('#news-container1').vTicker({
                    speed: 700,
                    pause: 4000,
                    animation: 'fade',
                    mousePause: false,
                    showItems: 1
                });
            });
        </script>
<script type="text/javascript">
    $(document).ready(function(){


            if (navigator.appName == 'Microsoft Internet Explorer' ||  !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv:11/)) || (typeof $.browser !== "undefined" && $.browser.msie == 1))
                {
                  $(".browser-message").show().html("<div class='browser-text'><p><img src='<?php echo $this->request->webroot; ?>front/img/logo-light.png' style='margin:auto;display:block;'/></p><p>Nous vous remercions de votre visite et de l’intérêt que vous portez à la Fédération Genevoise MédiationS. </p><p>Nous avons le regret de devoir vous informer que votre navigateur n’est malheureusement plus supporté par son développeur. Malgré tout nos efforts, nous n’avons pas été en mesure de concevoir notre site, qui utilise les dernières technologies, de manière à le rendre compatible avec la version du navigateur que vous utilisez.</p><p>Nous vous invitons à télécharger l’un des navigateurs suivants qui vous permettra d’accéder et d’utiliser notre site dans les meilleures conditions.</p><p>En cas de difficultés, nous vous invitons à prendre contact avec nous en nous envoyant un message à <a href='mailto:communication@fgem.ch'>communication@fgem.ch</a></p><p>Nous vous remercions par avance et nous vous prions de bien vouloir nous excuser de ce désagrément.</p> <p><a href='https://www.mozilla.org/en-US/firefox/new/?redirect_source=firefox-com' title='Mozilla Firefox'><img width='100' src='https://cdn.iconscout.com/icon/free/png-256/firefox-20-569406.png'></a><a href='https://www.google.com/chrome/' title='Google Chrome'><img width='90' src='https://www.google.com/chrome/static/images/chrome-logo.svg'></a><a href='https://www.apple.com/safari/'><img width='85' src='https://www.apple.com/v/safari/i/images/overview/safari_icon_large.png'></a></p></div>");
                }



                });
        </script>
    </body>
</html>
