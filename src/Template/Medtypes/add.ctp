<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Medtypes'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="medtypes form large-9 medium-8 columns content">
    <?= $this->Form->create($medtype) ?>
    <fieldset>
        <legend><?= __('Add Medtype') ?></legend>
        <?php
            echo $this->Form->control('names');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
