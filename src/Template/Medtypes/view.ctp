<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Medtype $medtype
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Medtype'), ['action' => 'edit', $medtype->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Medtype'), ['action' => 'delete', $medtype->id], ['confirm' => __('Are you sure you want to delete # {0}?', $medtype->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Medtypes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Medtype'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="medtypes view large-9 medium-8 columns content">
    <h3><?= h($medtype->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Names') ?></th>
            <td><?= h($medtype->names) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($medtype->id) ?></td>
        </tr>
    </table>
</div>
