<div class="users form">
<?php //echo $this->Flash->render('auth'); ?>
<?php echo $this->Form->create('User',array('class'=>'form-horizontal')); ?>
    <fieldset>
        <legend style="text-align:center">
            <?php echo __('Réinitialiser le mot de passe'); ?>
        </legend>
        <div class="form-group">
        <label for="newpassword" class="col-md-3 control-label">Nouveau mot de passe</label>
        <div class="col-md-7">
            <?php echo $this->Form->input('new_password',array('type'=>'password','div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
         </div>   
        </div> 
        <div class="form-group">
        <label for="confirmpassword" class="col-md-3 control-label">Confirmez le mot de passe</label>
        <div class="col-md-7">
            <?php echo $this->Form->input('confirm_password',array('type'=>'password','div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
         </div>   
        </div> 
    </fieldset>
      <div class="form-group">
    <div class="col-md-10">
        <button class="btn btn-blue-custom pull-right"  style="margin: auto;width: 250px;background: #242424;color: #fff;"><i class="fa fa-lock"></i> Réinitialiser le mot de passe</button>
        <?php 
        //echo $this->Form->input("<i class='fa fa-edit'></i> Login",array('type'=>'submit','div'=>false,'label'=>false,'class'=>'btn btn-blue-custom btn-default pull-right'),array('escape'=>false));
        echo $this->Form->end(); ?>
    </div>
    </div>
</div>