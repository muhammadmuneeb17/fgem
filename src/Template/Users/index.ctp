<div class="panel-head-info">
    <p class="pull-left info-text">Les Utilisateurs</p>
            <!--<a href="<?php // echo $this->request->webroot ?>pages" style="font-size:14px;" class="btn btn-primary btn-blue-custom pull-right text-info"><i class="fa fa-angle-left"></i> Go Back</a>-->
    <div class="clearfix"></div>
    <?php 
    if($permission == 2){
    ?>
    
    <a href="<?php echo $this->request->webroot ?>users/add" class="btn btn-success"><i class="fa fa-plus"></i> Ajouter un utilisateur</a>
     <?php } ?>
</div>
<div class="container content-container" style="">

    <div class="row">
        <div id="no-more-tables">
            <table class="col-md-12 table-bordered table-striped table-condensed cf">
                <thead class="cf">
                    <tr>
                        <th><?= $this->Paginator->sort('role','Rôle') ?></th>
                        <th><?= $this->Paginator->sort('Nom d\'utilisateur') ?></th>
                        <th><?= $this->Paginator->sort('adresse email') ?></th>
                        <th><?= $this->Paginator->sort('Statut') ?></th>
                        <th><?= $this->Paginator->sort('Modifié') ?></th>
<!--			<th><?php // echo $this->Paginator->sort('modified'); ?></th>-->
                        <th class="actions"><?php echo __('Action'); ?></th>
                    </tr>
                </thead>
                <tbody>
                            <?php foreach ($users as $user): 
                               
if($user->user_name != "cyberclouds" && $user->role->id!=6)  {
?>
                    <tr>
                        <td data-title="Rôle"><?php echo h($user->role->name); ?></td>
                        <td data-title="Nom d\'utilisateur"><?php echo h($user->user_name); ?>&nbsp;</td>
                        <td data-title="adresse email"><?php echo h($user->email); ?>&nbsp;</td>

                        
                        <td data-title="Statut"><?php
			if($user->status == TRUE) {
				$status = "Active";
			}else {
				$status = "Inactive";
			
			}

                        echo h($status); ?>&nbsp;</td>
                        <td data-title="Modifié"><?php echo h(date('d.m.y',strtotime($user->modified))); ?>&nbsp;</td>
                        <td  data-title="Action" class="actions">
			<?php //echo $this->Html->link(__('View'), array('controller' => 'users', 'action' => 'view',$user->id),array('class'=>'btn btn-primary')); ?>
			 <?php 
                       if($permission == 2){
                             
				echo $this->Html->link(__('<i class="fa fa-edit"></i>'), array('controller' => 'users', 'action' => 'edit',$user->id),array('class'=>'btn btn-success','escape'=>false,'data-toggle'=>"tooltip",'title'=>'Modifer')); 
			
			?>
            <?php
								echo $this->Form->postLink(__('<i class="fa fa-trash-o"></i>'), ['action' => 'delete', $user->id], ['escape'=>false,'class'=>'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $user->user_name),'data-toggle'=>"tooltip",'title'=>"Effacer"]);
                                                                }
								//echo $this->Html->link(__('<i class="fa fa-trash"></i> Delete'), ['controller'=>'users','action' => 'delete', $user->id], ['escape'=>false,'class'=>'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $user->username)]);
                            
							//echo $this->Form->postLink(__('<i class="fa fa-trash"></i> Delete'), ['action' => 'delete', $user->id], ['escape'=>false,'class'=>'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $user->id)]); } ?>
                        </td>
                    </tr>
<?php }
endforeach; ?>
                </tbody>
            </table>
            </form>
        </div>
    </div>

    <br />
    <div class="paginator">
        <ul class="pagination">
            <?php
                echo $this->Paginator->prev(__('Précédent'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                echo $this->Paginator->next(__('Suivant'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
            ?>
        </ul>
        <p class="small"><?= $this->Paginator->counter(['format' => __('Page {{page}} de {{pages}}, affiche {{current}} entrée(s) sur un de total {{count}}')]) ?></p>
    </div>
</div>