<div class="container" style="min-height: 525px">
    <div class="row">
        <?php
        if (!empty($isSuccess)) {
            if ($isSuccess['success'] == 1) {
                ?>
                <div class="col-md-12 alert alert-success alert-dismissible" role="alert" style="margin-top: 10px">
                <?php } else { ?>
                    <div class="col-md-12 alert alert-danger alert-dismissible" role="alert" style="margin-top: 10px">
                    <?php } ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>     
                    <?php echo $isSuccess['message']; ?>

                </div>
            <?php } ?>
            <div class="col-md-2">

            </div>
            <div class="col-md-8">
                <?php //echo $this->Flash->render('auth');  ?>
                <?php echo $this->Form->create('users', array('class' => 'form-horizontal')); ?>
                <fieldset>
                    <legend style="text-align: center">
                        Mot de passe oublié
                    </legend>
                    <div class="form-group" style="text-align: center;">
                        <label>Veuillez entrer votre adresse email enregistrée</label>
                    </div>
                    <div style="padding: 30px 0;border-radius: 5px;background: #F8F8F8;box-shadow: 2px 2px  5px #ccc;">
                        <div class="form-group">
                            <label for="email" class="col-md-3 control-label">Votre identifiant (adresse mail valide)</label>
                            <div class="col-md-7">
                                <?php echo $this->Form->input('email', array('div' => false, 'label' => false, 'class' => 'form-control', 'required', "oninput" => "setCustomValidity('')", "oninvalid" => "this.setCustomValidity('Veuillez entrer un email valide')", 'style' => 'height:40px;background-color:#fff;border: 1px solid #ccc;')); ?>
                            </div>   
                        </div> 
                        <div class="form-group">
                            <div class="col-md-10">
                                <a href="javascript:" onclick="return window.history.back();" style="margin-left: 10px;" class="text-info">Retour connexion</a>
                                <button class="btn btn-blue-custom pull-right"><i class="fa fa-lock"></i> réinitialiser le mot de passe</button>
                            </div>
                        </div>
                    </div>

                </fieldset>
                <?php
                echo $this->Form->end();
                ?>


            </div>
            <div class="col-md-2">

            </div>
        </div>
    </div>