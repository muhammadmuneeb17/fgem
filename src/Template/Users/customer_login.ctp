<div class="container" style="min-height: 525px">

    <div class="row">
        <?php if (!empty($message)) { ?>
            <div class="col-md-12 alert alert-success alert-dismissible" role="alert" style="margin-top: 10px">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>     
                <?php echo $message; ?>
            </div>
        <?php } ?>
        <div class="col-md-2">

        </div>
        <div class="col-md-8">
            <?php //echo $this->Flash->render('auth'); ?>
            <?php echo $this->Form->create('users', array('class' => 'form-horizontal')); ?>
            <fieldset>
                <legend style="text-align: center">
                    Identifiant
                </legend>
                <div class="form-group" style="text-align: center;">
                    <label>Veuillez entrer votre identifiant et votre mot de passe <br />
                        Vous n'êtes pas membre et vous voulez rejoindre la FGeM? <a href="<?php echo $this->request->webroot ?>Registerations/registerationform">Cliquez ici!</a>
                    </label>
                </div>
                <div style="padding: 30px 0;border-radius: 5px;background: #F8F8F8;box-shadow: 2px 2px  5px #ccc;">
                    <div class="form-group">
                        <label for="username" class="col-md-3 control-label">Votre identifiant (adresse mail valide) <small class="text-danger">*</small></label>
                        <div class="col-md-7">
                            <?php echo $this->Form->input('email', array('div' => false, 'label' => false, 'class' => 'form-control', 'required', "oninput" => "setCustomValidity('')", "oninvalid" => "this.setCustomValidity('Veuillez entrer un email valide')", 'style' => 'height:40px;background-color:#fff;border: 1px solid #ccc;')); ?>
                        </div>   
                    </div> 
                    <div class="form-group">
                        <label for="password" class="col-md-3 control-label">Mot de passe <small class="text-danger">*</small></label>
                        <div class="col-md-7">
                            <?php
                            echo $this->Form->input('password', array('div' => false, 'label' => false, 'class' => 'form-control', 'required'));
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-10">
                            <a style="margin-left: 10px;" href="<?php echo $this->request->webroot ?>users/forgot_password" class="text-info">Mot de passe oublié?</a>
                            <button class="btn btn-blue-custom pull-right" style="border: 1px solid #ccc;width:auto;"><i class="fa fa-lock"></i> s'identifier</button>
                            <?php
                            //echo $this->Form->input("<i class='fa fa-edit'></i> Login",array('type'=>'submit','div'=>false,'label'=>false,'class'=>'btn btn-blue-custom btn-default pull-right'),array('escape'=>false));
                            echo $this->Form->end();
                            ?>
                        </div>
                    </div>
                </div>

            </fieldset>

        </div>
        <div class="col-md-2">

        </div>
    </div>
</div>