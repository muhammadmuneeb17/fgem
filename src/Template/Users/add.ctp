<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="panel-head-info">
    <p class="pull-left info-text">Ajouter un Utilisateur</p>
    <a href="<?php echo $this->request->webroot ?>users" style="font-size:14px;" class="btn btn-primary btn-blue-custom pull-right text-info"><i class="fa fa-angle-left"></i> Précédent</a>
    <div class="clearfix"></div>

</div>
<div class="users form">
    <?php echo $this->Form->create('User',array('type'=>'file','class'=>'form-horizontal')); ?>
    <fieldset>
        <div class="form-group required">
            <label for="first_name" class="col-md-2 control-label">Prénom</label>
            <div class="col-md-4">
                <?php echo $this->Form->control('first_name',array('div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
            </div>
            <label for="last_name" class="col-md-2 control-label">Nom de famille</label>	
            <div class="col-md-4">
                <?php echo $this->Form->control('last_name',array('div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
            </div>  
        </div>   


        <div class="form-group required">
            <label for="username" class="col-md-2 control-label">Nom d'utilisateur</label>	
            <div class="col-md-4">
                <?php echo $this->Form->control('user_name',array('div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
            </div>
            <label for="email" class="col-md-2 control-label">eMail</label>	
            <div class="col-md-4">    
                <?php
                echo $this->Form->control('email',array('div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
            </div> 
        </div>

        <div class="form-group required">
            <label for="password" class="col-md-2 control-label">Mot de passe</label>	
            <div class="col-md-4">  
                <?php	echo $this->Form->control('password',array('type'=>'password','div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
            </div>
            <label for="role" class="col-md-2 control-label">RÔle</label>	
            <div class="col-md-4"> 
                <?php 

                echo $this->Form->control('role_id',array('div'=>false,'label'=>false,'class'=>'form-control','options' => $roleList,'empty'=>'SELECT ROLE','required'));

                ?>

                <?php	echo $this->Form->control('status',array('type'=>'hidden','div'=>false,'label'=>false,'class'=>'form-control','value'=>'1')); ?>
            </div>
        </div>  

    </fieldset>
    <div class="form-group">
        <div class="col-md-12 text-center">
            <?php 
            echo $this->Form->control('Ajouter un Utilisateur',array('type'=>'submit','div'=>false,'label'=>false,'class'=>'btn btn-default btn-blue-custom btn-lg'));
            echo $this->Form->end(); ?>
        </div>
    </div>
</div>
<script>
    $('#UserAddForm').submit(function (e) {
        var $container = $("html,body");
        var $scrollTo = $('.mymsg');
        var pwd_len = $("#password").val().length;

        if (pwd_len < 8) {
            $('.mymsg').html('');
            $('.mymsg').append("<div class='error'>Password too short, provide at least 8 characters</div>");
            $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top, scrollLeft: 0}, 300);
            return false;
        }
    });
</script>


