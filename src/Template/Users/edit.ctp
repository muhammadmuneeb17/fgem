<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="panel-head-info">
    <p class="pull-left info-text">Modifier l'utilisateur</p>
    <a href="<?php echo $this->request->webroot ?>users" style="font-size:14px;" class="btn btn-primary btn-blue-custom pull-right text-info"><i class="fa fa-angle-left"></i> Précédent</a>
    <div class="clearfix"></div>

</div>
<div class="users form">
    <?php echo $this->Form->create($user,array('type'=>'file','class'=>'form-horizontal','id'=>'UserEditForm')); ?>
    <fieldset>
        <div class="form-group required">
            <label for="first_name" class="col-md-2 control-label">Pr&eacute;nom</label>
            <div class="col-md-4">
                <?php echo $this->Form->control('first_name',array('div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
            </div>
            <label for="last_name" class="col-md-2 control-label">Nom de famille</label>	
            <div class="col-md-4">
                <?php echo $this->Form->control('last_name',array('div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
            </div>  
        </div>   


        <div class="form-group required">
            <label for="username" class="col-md-2 control-label">Nom d'utilisateur</label>	
            <div class="col-md-4">
                <?php echo $this->Form->control('user_name',array('div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
            </div>
            <label for="email" class="col-md-2 control-label">eMail</label>	
            <div class="col-md-4">    
                <?php
                echo $this->Form->control('email',array('div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
            </div> 
        </div>

        <div class="form-group required">
            <label for="password" class="col-md-2 control-label notrequired">Mot de passe</label>	
            <div class="col-md-4">  
                <?php	echo $this->Form->control('password',array('type'=>'password','div'=>false,'label'=>false,'class'=>'form-control','style' => 'display:none','required'=>false)); ?>
            </div>
            <?php //if($this->request->session()->read('Auth.User.role') == "ADMIN" ){ ?>
            <label for="role" class="col-md-2 control-label notrequired">R&Ocirc;le</label>	
            <div class="col-md-4"> 
                <?php  
                //             $role = [
                //                    'ADMIN' =>'Admin'
                //                ];
                echo $this->Form->control('role_id', ['label'=>false,'class'=>'form-control','required','options' => $roleList,'empty'=>'SELECT ROLE']); ?>
            </div>
            <?php //} ?>
        </div>
        <?php if($this->request->session()->read('Auth.User.id') != $user['id'] && $this->request->session()->read('Auth.User.role') == "ADMIN" ){ ?>
        <div class="form-group required">
            <label for="status" class="col-md-2 control-label">Status</label>	
            <div class="col-md-4"> 
                <?php	 

    $status = [
        1 =>'Active',
        0 => 'Deactive'
    ];
    echo $this->Form->control('status',array('div'=>false,'label'=>false,'class'=>'form-control','options' => $status,'empty'=>'SELECT STATUS')); ?>

            </div>


        </div>  
        <?php } ?>
    </fieldset>
    <div class="form-group">
        <div class="col-md-12 text-center">
            <?php 
            
            echo $this->Form->control('Enregistrer', array('type'=>'submit','div'=>false,'label'=>false,'class'=>'btn btn-default btn-blue-custom btn-lg'));
            echo $this->Form->end(); ?>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $("#password").hide();
        $("#password").val('');
    });
    $(window).load(function () {
        $("#password").val('');
        $("#password").show();
    });
    $('#UserEditForm').submit(function (e) {
        var $container = $("html,body");
        var $scrollTo = $('.mymsg');
        var pwd_len = $("#password").val().length;

        if($("#password").val() != "") {
            if (pwd_len < 8) {
                $('.mymsg').html('');
                $('.mymsg').append("<div class='error'>Password too short, provide at least 8 characters</div>");
                $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top, scrollLeft: 0}, 300);
                return false;
            }
        }
    });
</script>