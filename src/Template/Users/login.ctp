<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <?php echo $this->Html->charset(); ?>
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
        <title>
            <?php //echo $cakeDescription ?>
            Users | FGeM
        </title>
        <link rel="icon" href="<?php echo $this->request->webroot ?>favicon.ico" />

        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

        <?php
        echo $this->Html->css('bootstrap');
        echo $this->Html->css('AdminLTE.min');
        //echo $this->Html->css('style');
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        ?>

        <!-- bootstrap wysihtml5 - text editor -->
        <!--<link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">-->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            .main-footer{
                position: absolute;
                width: 100%;
                bottom: 0;
            }
            label.control-label{
                font-weight: lighter;

            }
            .users.form{
                max-width: 500px;
            }
            @media (min-width:320px) and (max-width:767px) {
                .main-header{
                    background: transparent;
                }
            }
        </style>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper" style="background:#fff;">

            <header class="main-header" style="max-height:260px;">
                <a href="<?php echo $this->request->webroot; ?>">
                    <img src="<?php echo $this->request->webroot; ?>img/logo.png" class="backend-logo" width="120" style="margin:auto;display:block;margin-top:20px"/>
                </a>
                <!--<h3 align="center" class="login-headtitle" style="color:#eee;">Administrative Section</h3>-->
                <!-- Logo -->
                <!-- Header Navbar: style can be found in header.less -->
            </header>
            <!-- Left side column. contains the logo and sidebar -->


            <div class="content-wrapper" style="width: 100%;margin-left: 0px;min-height:300px;background-color: #fff;">
                <!-- Content Header (Page header) -->
                <div class="container" style="max-width:600px !important; position: relative;height: 22px;">
                    <?= $this->Flash->render() ?>
                </div>    
                <div class="users form" style="box-shadow: unset;">
                    <?php //echo $this->Flash->render('auth'); ?>
                    <?php echo $this->Form->create('users', array('class' => 'form-horizontal')); ?>
                    <fieldset>
                        <legend style="text-align: center;font-size: 24px;">
                            Section administrative

                            <?php // echo __('Please enter your username and password'); ?>
                        </legend>
                        <div class="form-group">
                            <!--<div class="col-md-1"></div>-->
                            <label for="username" class="col-md-4 control-label" style="font-size: 15px !important;">Nom d'utilisateur</label>
                            <div class="col-md-8">
                                <?php echo $this->Form->input('user_name', array('div' => false, 'label' => false, 'class' => 'form-control', 'required')); ?>
                            </div>   


                        </div> 
                        <div class="form-group">
                            <!--<div class="col-md-1"></div>-->

                            <label for="password" class="col-md-4 control-label" style="font-size: 15px !important;">Mot de passe</label>
                            <div class="col-md-8">
                                <?php
                                echo $this->Form->input('password', array('div' => false, 'label' => false, 'class' => 'form-control', 'required'));
                                ?>
                            </div>
                            <!--<div class="col-md-1"></div>-->

                        </div>
                    </fieldset>
                    <div class="form-group">

                        <div class="col-md-12">
                            <a href="<?php echo $this->request->webroot ?>users/forgot_password" class="text-info">Mot de passe oublié?</a>
                            <button class="btn btn-blue-custom pull-right"><i class="fa fa-lock"></i> s'identifier</button>
                            <?php
                            //echo $this->Form->input("<i class='fa fa-edit'></i> Login",array('type'=>'submit','div'=>false,'label'=>false,'class'=>'btn btn-blue-custom btn-default pull-right'),array('escape'=>false));
                            echo $this->Form->end();
                            ?>
                        </div>

                    </div>
                </div>

            </div>  

            <footer class="main-footer" style="margin-left:0px;">
                <div class="pull-right hidden-xs">
                    <small> Developed By: <a href="http://www.cyberclouds.com">Cyber Clouds</a></small> | <b>Version</b> 1.0
                </div>
                <strong>Copyright &copy; <?php echo date('Y') ?> <a href="#">FGeM</a>.</strong> All rights
                reserved.
            </footer>
        </div>

        <?php //echo $this->element('sql_dump'); ?>
        <?php echo $this->Html->script('jquery-2.2.3.min'); ?>   


    </body>
</html>
