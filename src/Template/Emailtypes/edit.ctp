<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $emailtype->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $emailtype->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Emailtypes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Emails'), ['controller' => 'Emails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Email'), ['controller' => 'Emails', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="emailtypes form large-9 medium-8 columns content">
    <?= $this->Form->create($emailtype) ?>
    <fieldset>
        <legend><?= __('Edit Emailtype') ?></legend>
        <?php
            echo $this->Form->control('email_type');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
