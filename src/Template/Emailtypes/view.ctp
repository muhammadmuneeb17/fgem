<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Emailtype $emailtype
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Emailtype'), ['action' => 'edit', $emailtype->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Emailtype'), ['action' => 'delete', $emailtype->id], ['confirm' => __('Are you sure you want to delete # {0}?', $emailtype->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Emailtypes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Emailtype'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Emails'), ['controller' => 'Emails', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Email'), ['controller' => 'Emails', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="emailtypes view large-9 medium-8 columns content">
    <h3><?= h($emailtype->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Email Type') ?></th>
            <td><?= h($emailtype->email_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($emailtype->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($emailtype->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Emails') ?></h4>
        <?php if (!empty($emailtype->emails)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Emailtype Id') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('From Person') ?></th>
                <th scope="col"><?= __('To Person') ?></th>
                <th scope="col"><?= __('Subject') ?></th>
                <th scope="col"><?= __('Message') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($emailtype->emails as $emails): ?>
            <tr>
                <td><?= h($emails->id) ?></td>
                <td><?= h($emails->emailtype_id) ?></td>
                <td><?= h($emails->title) ?></td>
                <td><?= h($emails->from_person) ?></td>
                <td><?= h($emails->to_person) ?></td>
                <td><?= h($emails->subject) ?></td>
                <td><?= h($emails->message) ?></td>
                <td><?= h($emails->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Emails', 'action' => 'view', $emails->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Emails', 'action' => 'edit', $emails->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Emails', 'action' => 'delete', $emails->id], ['confirm' => __('Are you sure you want to delete # {0}?', $emails->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
