<?php ?>
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }


</style>
<!--test code start-->

<!--test code end-->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/css/select2.min.css" rel="stylesheet" />


<div class="container-fluid" style="min-height:530px;">


    <div class="col-md-12 search_bg">

        <div style="padding-top: 10px;">
            <h3 style="" class="col-md-7 col-sm-12 f8_labels">Annuaire des Médiateurs</h3>
            <!-- <div class="">
                <form method="get" action="<?php echo $this->request->webroot . 'Pages/search'; ?>">
                    <div class="col-md-4 col-sm-12 col-xs-12">
            <?php
            /* echo $this->Form->control('keyword', [
              'class' => 'form-control searchbox', 'label' => false, 'placeholder' => 'recherché par nom, téléphone, Votre identifiant (adresse mail valide)', 'style' => 'height:40px;color:#fff;','id' => 'first_name','required']); */
            ?>
                    </div>
                    <div class="col-md-1 col-sm-12 col-xs-12" style="padding-left:0px;"> 
                        <button  class="btn btn-blue-custom pull-right" style="background:none;border:none;color:#fff"><i class="fa fa-search"></i> Chercher</button>
                    </div>

                </form>
            </div> -->
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="col-md-3 search-aside">
        <h3 class="filter_heading">Filter</h3>
        <form method="get" action="<?php echo $this->request->webroot . 'Pages/search'; ?>">
            <div class="">
                <div class="col-md-12">
                    <label class="filter_label" for="key_words">Chercher mots-clés</label>
                    <input type="text" style="height:35px;" name="key_word" placeholder="Rechercher un mot clé ou un nom ..." class="form-control searchbox" />
                    <input type="hidden" name="advance_search" value="1">
                    <div class="clearfix"></div>
                    <hr class="hr_margin">
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-12">


                    <label class="filter_label" for="mediation">Types de médiation <span class="pull-right"><a href="javascript:" class="medi-collapse"><i class="fa fa-plus"></i></a></span></label>
                    <div class="advance_search_box medi-filter" style="display: none;">
                        <?php
                        foreach ($mediations as $mediation) {
                            ?>
                            <div class="single-col">
                                <div class="styled-input-container styled-input--diamond">
                                    <div class="styled-input-single">
                                        <input value="<?php echo $mediation->id; ?>" type="checkbox" name="mediation[]" id="mediation<?php echo $mediation->id; ?>" />
                                        <label for="mediation<?php echo $mediation->id; ?>"><?php echo $mediation->names; ?></label>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="clearfix"></div>
                    <hr class="hr_margin">
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-12">
                    <label class="filter_label" for="accreditation">Accréditation <span class="pull-right"><a href="javascript:" class="acce-collapse"><i class="fa fa-plus"></i></a></span></label>
                    <div class="advance_search_box acce-filter" style="display: none;">
                        <?php
                        foreach ($accreds as $accred) {
                            ?>
                            <div class="single-col">
                                <div class="styled-input-container styled-input--diamond">
                                    <div class="styled-input-single">
                                        <input value="<?php echo $accred->id; ?>" id="accreditation<?php echo $accred->id; ?>" type="checkbox" name="accreditation[]"  />
                                        <label for="accreditation<?php echo $accred->id; ?>"><?php echo $accred->name; ?></label>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="clearfix"></div>
                    <hr class="hr_margin">
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12">
                    <label class="filter_label" for="languages">Langues</label><br>
                    <div class="">
                        <select class="js-example-basic-multiple" id="multipleLanguages" name="languages[]" multiple="multiple" style="width:100%;">
                            <?php foreach ($languages as $k => $l) {
                                ?>
                                <option value="<?= $k ?>"><?= $l ?></option>    
                                <?php
                            }
                            ?>
                        </select>

                    </div>
                    <div class="clearfix"></div>
                    <hr class="hr_margin">
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12">
                    <label for=""></label><br>
                    <button  class="btn advance_search_members" style="width:100%;"><i class="fa fa-search"></i> Chercher</button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-9">

        <?php
        foreach ($members as $members) {

//            debug($members->accounts);
            if ($members->accounts != null) {
                ?>
                <div class="col-md-6 col-lg-4">
                    <div class="custom_box" onclick="showDetails(<?php echo $members['user_id'] ?>)">
                        <?php
                        if ($members['image'] && file_exists(WWW_ROOT . 'img' . DS . 'slider' . DS . $members['image'])) {
                            $p = $this->request->webroot . 'img/slider/' . $members['image'];
                            ?>

                            <?php
                        } else {
                            $p = $this->request->webroot . 'img/Users/cyber1550500683.png';
                        }
                        ?>
                        <div class="profile_image" style="background-image: url('<?php echo $p; ?>')">

                        </div>
                        <div class="member_profile_info">
                            <h3 class="profile_name"><?php echo ucwords($members->user['first_name'] . ' ' . $members->user['last_name']); ?></h3>
                            <p class='member-profession'>
                                <?php
                                if ($members['ombudsman']['mediator_question'] == "Oui") {
                                    echo "Médiateur";
                                } else {
                                    echo '';
                                }
                                if ($members['ombudsman']['mediator_question'] == "Oui" && $members['ombudsman']['accept_info'] == 1) {
                                    echo " ";
                                }
                                if ($members['ombudsman']['accept_info'] == 1) {
                                    echo "Assermenté";
                                } else {
                                    echo '';
                                }
                                ?>
                            </p>
                                <?php
                                if ($members['emailpublic'] == 1) {
                                    echo "<p class='member-email'>" . $members['email'] . "</p>";
                                } else {
                                    echo '';
                                }
                                ?>


                        </div>
                    </div>
                </div>
                <?php
            }
        }
        ?>
    </div>
</div>
<div class="clearfix">&nbsp;</div>
