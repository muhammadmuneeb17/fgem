<div class="panel-head-info">
    <!--<a href="<?php // echo $this->request->webroot         ?>pages" style="font-size:14px;" class="btn btn-primary btn-blue-custom pull-right text-info"><i class="fa fa-angle-left"></i> Go Back</a>-->
    <div class="clearfix"></div>
    <p class="pull-left info-text">Types de membres</p>
    <div class="clearfix"></div>

    <a href="<?php echo $this->request->webroot ?>NextRenewalDate/addMemberType" class="btn btn-success"><i class="fa fa-plus"></i> Types de membres</a>
    <?php //} ?>
</div>
<div class="container content-container" style="">

    <div class="row">
        <div id="no-more-tables">
            <table class="col-md-12 table-bordered table-striped table-condensed cf">
                <thead class="cf">
                    <tr>
                        <th><?= $this->Paginator->sort('name', 'Nom') ?></th>
                        <th><?= $this->Paginator->sort('type', 'type') ?></th>
                        <!--<th><?= $this->Paginator->sort('year', 'Année') ?></th>-->
                        <th>Montant</th>
                        <th class="actions"><?php echo __('Action'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($membertypes as $nextDate):
                        ?>
                        <tr>
                            <td data-title="Nom"><?php echo $nextDate->name; ?></td>
                            <td data-title="Type">
                                <?php
                                echo ($nextDate->type == 1) ? 'Individuel' : 'collectif';
                                ?>&nbsp;</td>
                            <!--<td data-title="Année"><?php echo h($nextDate->year); ?>&nbsp;</td>-->
                            <td data-title="Montant">

                                <a class="btn btn-default btn-sm" href="<?= $this->request->webroot . 'Registerations/annualAmount/' . $nextDate->id; ?>">montant annuel</a>
                            </td>

                            <td  data-title="Action" class="actions">
                                <?php
                                echo $this->Html->link(__('<i class="fa fa-edit"></i>'), array('controller' => 'NextRenewalDate', 'action' => 'editMemberType', $nextDate->id), array('class' => 'btn btn-success', 'escape' => false,'data-toggle'=>"tooltip",'title'=>'Modifier'));
                                ?>
                                <?php
                                echo $this->Form->postLink(__('<i class="fa fa-trash-o"></i>'), ['controller' => 'NextRenewalDate', 'action' => 'deleteMemberType', $nextDate->id], ['escape' => false, 'class' => 'btn btn-danger', 'confirm' => __('Are you sure you want to delete # {0}?', $nextDate->day . '.' . $nextDate->month . '.' . $nextDate->year),'data-toggle'=>"tooltip",'title'=>'Effacer']);
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

    <br />
    <div class="paginator">
        <ul class="pagination">
            <?php
            echo $this->Paginator->prev(__('retour'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
            echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1));
            echo $this->Paginator->next(__('suivant'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
            ?>
        </ul>
        <p class="small"><?= $this->Paginator->counter(['format' => __('Page {{page}} de {{pages}}, affiche {{current}} entrée(s) sur un de total {{count}}')]) ?></p>
    </div>
</div>