<?php
/**
 * @var \App\View\AppView $this
 */
//$status = $this->request->session()->read('Auth.User.status');
$array = explode('/', $_SERVER['REQUEST_URI']);
$tabId = end($array);
//debug($user);
$fName = $this->request->session()->read('Auth.User.first_name');
$lName = $this->request->session()->read('Auth.User.last_name');
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/css/select2.min.css"rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/js/select2.min.js"></script> -->

<link href="<?php echo $this->request->webroot ?>front/css/multiselect.css" rel="stylesheet" />

<script src="<?php echo $this->request->webroot ?>front/js/multiselect.js"></script>

<style>
    div.avatar{
        height: 200px;
        width: 200px;
        background-position: center;
        background-size: cover;
        border-radius:50%;
    }
    .formHeading{
        padding: 10px 10px;
        border-bottom: 1px solid #ccc;
    }
    .groupFields{
        display: inline-block;
        width: 100%;
        padding: 0 15px;
        margin-bottom: 10px;
    }
    .groupFields .col-xs-12{
        background-color: #f8f8f8;
    }
    .groupFields input{
        background-color: #fff;
    }
    .crossButton{
        margin: 5px 0px;
        font-size: 18px;
        cursor: pointer;
        color: gray;
    }
    /*CHECKBOXEXS DESIGN START*/
    .styled-input--diamond .styled-input-single {
        padding-left: 45px;
    }
    .styled-input--diamond label:before, .styled-input--diamond label:after {
        border-radius: 0;
    }
    .styled-input--diamond label:before {
        transform: rotate(45deg);
    }
    .styled-input--diamond input[type="radio"]:checked + label:after, .styled-input--diamond input[type="checkbox"]:checked + label:after {
        transform: rotate(45deg);
        opacity: 1;
    }
    .styled-input-single {
        position: relative;
        padding: 0px 0 5px 40px;
        text-align: left;
    }
    .styled-input-single label {
        cursor: pointer;
        font-size: 14px;
    }
    .styled-input-single label:before, .styled-input-single label:after {
        content: '';
        position: absolute;
        top: 50%;
        border-radius: 50%;
    }
    .styled-input-single label:before {
        left: 15px;
        width: 15px;
        height: 15px;
        margin: -15px 0 0;
        //background: #f7f7f7;
        //box-shadow: 0 0 1px grey;
        border: 1px solid #37b2b2 !important;
    }
    .styled-input-single label:after {
        left: 19px;
        width: 7px;
        height: 7px;
        margin: -11px 0 0;
        opacity: 0;
        background: #37b2b2;
        transform: translate3d(-40px, 0, 0) scale(0.5);
        transition: opacity 0.25s ease-in-out, transform 0.25s ease-in-out;
    }
    .styled-input-single input[type="radio"], .styled-input-single input[type="checkbox"] {
        position: absolute;
        top: 0;
        left: -9999px;
        visibility: hidden;
    }
    .styled-input-single input[type="radio"]:checked + label:after, .styled-input-single input[type="checkbox"]:checked + label:after {
        transform: translate3d(0, 0, 0);
        opacity: 1;
    }
    .styled-input--square label:before, .styled-input--square label:after {
        border-radius: 0;
    }
    .styled-input--rounded label:before {
        border-radius: 10px;
    }
    .styled-input--rounded label:after {
        border-radius: 6px;
    }
    .styled-input--diamond .styled-input-single {
        padding-left: 45px;
    }
    .styled-input--diamond label:before, .styled-input--diamond label:after {
        border-radius: 0;
    }
    .styled-input--diamond label:before {
        transform: rotate(45deg);
    }
    .styled-input--diamond input[type="radio"]:checked + label:after, .styled-input--diamond input[type="checkbox"]:checked + label:after {
        transform: rotate(45deg);
        opacity: 1;
    }
    /*CHECKBOXEXS DESIGN END*/
    .dynamic_cities{
        padding: 0;
        width:100%;
        margin-bottom: 0; 
    }
    .dynamic_cities li{
        padding:10px;
        border-bottom: 1px solid #eee;
    }
    .dynamic_cities li:hover{
        background-color: #eeeeee4d;
    }
    .search-result{
        background-color: white;
        min-height: 0;
        max-height: 200px;
        overflow: auto;
        /*border: none;*/
        border-top: none;
        position: absolute;
        z-index: 1 !important;
        width: 96%;
    }
    .searched_user p{
        margin:0;
    }
    .searched_user{
        cursor: pointer;
    }
    .search-result ul{
        margin: 0;
    }
    .ms-has-selections button {
        padding: 10px 15px;
    }
    .TermsAndConditionButon{
        border-color: #147172 !important;
        background: #147172 !important;
    }
    .accept_info:hover{
        text-decoration: underline;
        color:#147172;
    }
    .ms-search input{
        width: 95% !important;
    }
</style>

<hr>
<div class="container bootstrap snippet">
    <?= $this->Form->create('', array('id' => 'businessForm', 'type' => 'file')) ?>

    <div class="row">

        <div class="col-sm-12" style="min-height: 300px;">

            <div class="form-group">
                <h4 class="formHeading" style="font-weight:bold">INFORMATIONS SUR LE MÉDIATEUR</h4>
            </div><!--/tab-pane-->

            <div class="clearfix">&nbsp;</div>
            <p style="padding:0 10px;color:red;">
                <span style="font-weight:bold;">ATTENTION !!!</span> Vous vous engagez formellement en remplissant votre espace membre à refléter l'exacte vérité quant aux dates et aux qualifications que vous indiquerez. La FGeM ne saurait être tenue responsable d'indications erronées publiées sur son site. 
            </p>
            <div class="form-group">

                <div class="col-xs-12">
                    <label for="mediator"><h4>Êtes-vous un médiateur<small class="text-danger">*</small></h4></label>
                    <?php
                    $mediator_questionVal = '';
                    $presentation_textVal = '';
                    $accredsArray = array();
                    $medArray = array();
                    $lanArray = array();
                    $asVal = 0;
                    $accept_info = 0;

                    if ($registrations['ombudsman']) {
                        $presentation_textVal = $registrations['ombudsman']['presentation_text'];
                        $mediator_questionVal = $registrations['ombudsman']['mediator_question'];
                        $mediator_questionVal = $registrations['ombudsman']['mediator_question'];
                        $asVal = ($registrations['ombudsman']['assermentation'] != null) ? $registrations['ombudsman']['assermentation'] : 0;
                        $accept_info = ($registrations['ombudsman']['accept_info']) ? 1 : 0;


                        if ($registrations['ombudsman']['ombudsmanaccreds']) {
                            foreach ($registrations['ombudsman']['ombudsmanaccreds'] as $acreed) {
                                array_push($accredsArray, $acreed['accreditation_id']);
                            }
                        }
                        if ($registrations['ombudsman']['ombudsmanlevels']) {
                            foreach ($registrations['ombudsman']['ombudsmanlevels'] as $lang) {
                                array_push($lanArray, $lang['language_id']);
                            }
                        }
                        if ($registrations['ombudsman']['ombudsmanmeds']) {
                            foreach ($registrations['ombudsman']['ombudsmanmeds'] as $meds) {
                                array_push($medArray, $meds['medtype_id']);
                            }
                        }
                    }
                    $args = array('Oui' => 'Oui', 'Non' => 'Non');
                    echo $this->Form->control('mediator_question', [
                        'class' => 'form-control', 'options' => $args, 'value' => $mediator_questionVal, 'default' => '', 'style' => 'height:40px;', 'label' => false, 'div' => false, 'required']);
                    ?>

                </div>

            </div>

            <div class="form-group mediationTypes">

                <div class="col-xs-12">
                    <label for=""><h4>Types de médiation<small class="text-danger">*</small></h4></label>
                    <?php foreach ($medtype as $medtype): ?>
                        <?php
                        $medCheck = '';
                        if (in_array($medtype['id'], $medArray)) {
                            $medCheck = 'checked';
                        }
                        //echo $this->Form->checkbox('ombudsmanmeds.mediat[]', ['value' => $medtype['id'], $medCheck,
                        // 'label' => false, 'div' => false, 'hiddenField' => false]);
                        //echo " " . $medtype['names'];
                        ?>
                        <div class="single-col">
                            <div class="styled-input-container styled-input--diamond" style="float:left">
                                <div class="styled-input-single">
                                    <input value="<?php echo $medtype['id']; ?>" type="checkbox" name="ombudsmanmeds[mediat][]" id="mediation<?php echo $medtype['id']; ?>" <?php echo $medCheck; ?>/>
                                    <label for="mediation<?php echo $medtype['id']; ?>"><?php echo $medtype['names']; ?></label>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?> 
                    <div class="clearfix"></div>
                </div>

                <div class="form-group">

                    <div class="col-xs-12">
                        <label for="mediation"><h4>Langues<small class="text-danger">*</small></h4></label>
                        <select class="form-control" id="multipleLanguages" name="ombudsmanlevels.mediation[]" multiple="multiple" style="width:100%" required>
                            <?php foreach ($languages as $k => $l) {
                                ?>
                                <option value="<?= $k ?>" <?= in_array($k, $lanArray) ? 'selected' : ''; ?> ><?= $l ?></option>    
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div id="myLanguages">
                    <?php
                    if (isset($registrations['ombudsman']['ombudsmanlevels'])) {

                        foreach ($registrations['ombudsman']['ombudsmanlevels'] as $k => $l) {
                            ?>
                            <div class="form-group">
                                <div class="col-xs-12 col-md-4" style="padding:0 50px;">
                                    <label for=""><h4>Niveau <?= $languages[$l['language_id']] ?><small class="text-danger">*</small></h4></label>
                                    <select name="ombudsmanlevels.language_level[]" class="form-control">
                                        <option value="basic" <?= ($l['language_level'] == 'basic') ? 'selected' : '' ?> >Basique</option>
                                        <option value="conversational" <?= ($l['language_level'] == 'conversational') ? 'selected' : '' ?>>Moyen</option>
                                        <option value="fluent" <?= ($l['language_level'] == 'fluent') ? 'selected' : '' ?>>Courant</option>
                                    </select>

                                </div>
                            </div>

                            <?php
                        }
                    }
                    ?>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label for=""><h4>Assermentation<small class="text-danger">*</small></h4></label>
                        <?php
//                    debug($asVal);
                        $args = array('1' => 'Oui', '0' => 'Non');
                        echo $this->Form->control('assermentation', [
                            'class' => 'form-control', 'options' => $args, 'default' => '', 'style' => 'height:40px;', 'label' => false, 'div' => false, 'value' => $asVal, 'required']);
                        ?>
                        <br/>
                    </div>
                </div>

                <?php
                $staric = '<small class="text-danger">*</small>';
                $isRequired = 'required';
                if ($asVal == 0) {
                    $staric = '';
                    $isRequired = '';
                }
                if (isset($registrations['ombudsman']['ombudsmanswears']) && !empty($registrations['ombudsman']['ombudsmanswears'])) {
                    foreach ($registrations['ombudsman']['ombudsmanswears'] as $oK => $owears):
                        ?>
                        <div class="groupFields assermentation-wrapper" id="fieldPlace<?php echo $oK; ?>">
                            <div class="col-xs-12">
                                <span onclick="closeDiv(<?php echo $oK; ?>)" class="pull-right crossButton">&times;</span>
                                <label for="first_name"><h4>Lieu d'Assermentation <?php echo $staric; ?></h4></label>
                                <?php
                                echo $this->Form->control('place[]', [
                                    'class' => 'form-control', 'default' => '', 'style' => 'height:40px;', 'label' => false, 'div' => false, 'value' => $owears['place'], $isRequired]);
                                ?>
                            </div>
                            <div class="col-xs-12">
                                <label for=""><h4>Date d'Assermentation (jj.mm.aaaa - 28.02.1999)</h4></label>
                                <?php
                                $args = array('1' => 'Oui', '0' => 'Non');
                                echo $this->Form->control('date_of_auth[]', [
                                    'class' => 'form-control datepicker', 'style' => 'height:40px;', 'label' => false, 'type' => 'text', 'div' => false, 'value' => ($owears['date_of_auth']) ? date('m/d/Y', strtotime($owears['date_of_auth'])) : '']);
                                ?>
                                <br/>
                            </div>
                        </div>
                        <?php
                    endforeach;
                }else {
                    ?>
                    <div class="groupFields assermentation-wrapper" id="fieldPlace0" style="display:none;">
                        <div class="col-xs-12">
                            <label for="first_name"><h4>Lieu d'Assermentation</h4></label>
                            <?php
                            echo $this->Form->control('place[]', [
                                'class' => 'form-control', 'default' => '', 'style' => 'height:40px;', 'label' => false, 'div' => false]);
                            ?>
                        </div>
                        <div class="col-xs-12">
                            <label for=""><h4>Date d'Assermentation (jj.mm.aaaa - 28.02.1999)</h4></label>
                            <?php
                            $args = array('1' => 'Oui', '0' => 'Non');
                            echo $this->Form->control('date_of_auth[]', [
                                'class' => 'form-control datepicker', 'default' => '', 'style' => 'height:40px;', 'label' => false, 'type' => 'text', 'div' => false]);
                            ?>
                            <br/>
                        </div>
                    </div>
                <?php } ?>
                <div class="col-xs-12" id="addMoreAssermentation" style="text-align:right;display:none;">
                    <span class="btn btn-success btn-xs btn-asser" onclick='addField()'><i class="fa fa-plus" aria-hidden="true"></i> Autre assermentation</span>
                </div>

                <div class="col-xs-12" style="padding-top:15px;display:none;" id="acceptInfoDiv">
                    <div class="single-col">
                        <div class="styled-input-container styled-input--diamond" style="float:left">
                            <div class="styled-input-single">
                                <input name="accept_info" type="checkbox" value="1" type="checkbox" id="accept_info" <?= ($accept_info == 1) ? 'checked' : ''; ?> />
                                <label for="accept_info" style="width: 95%;">
                                    <!--                                <label for="accept_info" style="width: 95%;" data-toggle="modal" data-target="#termsAndConditions" class="accept_info">-->
                                    <!--Je suis disponible pour assurer des permanences dans le cadre de la Permanence Info-Médiation, PIM. (Seules les membres à jour de leur cotisation et ayant publié leur profile dans l'Annuaire de la FGeM seront pris en compte pour la PIM)-->

                                    Je suis disponible pour assurer des permanences et je m'inscris au tableau des médiateurs permanents de la Permanence Info Médiation (PIM).
                                    Je demande à ce que le règlement de la PIM me parvienne pour lecture, puis signature et retour à la FGeM.
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">

                <div class="col-xs-12">
                    <label for=""><h4>Texte de présentation<small class="text-danger">*</small></h4></label>
                    <?php
                    echo $this->Form->control('presentation_text', [
                        'label' => false, 'type' => 'textarea', 'div' => false, 'value' => $presentation_textVal, 'required', 'class' => 'form-control']);
                    ?>

                </div>
            </div>
            <div class="mediationTypes">
                <?php
                if (isset($registrations['ombudsman']['ombudsmanformations']) && !empty($registrations['ombudsman']['ombudsmanformations'])) {
                    foreach ($registrations['ombudsman']['ombudsmanformations'] as $oF => $oFormation):
                        ?>
                        <div class="groupFields" id="fieldFormation<?= $oF; ?>" style="margin-top:10px">
                            <div class="col-xs-12">
                                <span onclick="deleteFormation(<?= $oF; ?>,<?= $oFormation['id']; ?>)" class="pull-right crossButton"><i class="fa fa-trash-o"></i></span>
                                <label><h4>Formation</h4></label>
                                <input type="text" id="formationId<?= 'edit' . $oFormation['id']; ?>" dynamic_val="<?= 'edit' . $oFormation['id']; ?>" name="ombudsmanformations.formation[]" placeholder="Formation" value="<?php echo $oFormation['formation']; ?>" class="form-control name_list searching"  style="height:40px;" autocomplete="off">
                            </div>
                            <div class="col-xs-12">
                                <div class="search-result" id="search-result<?= 'edit' . $oFormation['id']; ?>">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <label for="">
                                    <h4>Date de certification (jj.mm.aaaa - 28.02.1999)</h4>
                                </label>
                                <input type="text" name="ombudsmanformations.date[]" placeholder="Date de certification " class="datepicker form-control" value="<?php echo date('m/d/Y', strtotime($oFormation['certificate_date'])); ?>" style="height: 40px;">
                            </div>                
                            <div class="col-xs-12">
                                <label for="image" class="col-md-12" style="padding:0">Copie de la certification</label>
                                <input type="file" name="image[]" class="section-image" id="sectionImage" onchange="readURL(this)">
                                <br/>
                            </div>
                            <div class="col-xs-12">
                                <img width="100px" src="<?php echo $this->request->webroot . 'img/formations/' . $oFormation['copy_upload'] ?>" alt="Image Not Available">
                                <input type="hidden" name="ombudsmanformations.image_id[]" value="<?= $oFormation['id']; ?>">
                                <input type="hidden" name="ombudsmanformations.image_name[]" value="<?= $oFormation['copy_upload']; ?>">
                                <br/>
                                <br/>
                            </div>
                        </div>
                        <?php
                    endforeach;
                }else {
                    ?>
                    <div class="groupFields" id="fieldFormation0" style="margin-top:10px">
                        <div class="col-xs-12">
                            <label><h4>Formation</h4></label>
                            <input type="text" id="formationId" name="ombudsmanformations.formation[]" placeholder="Formation" class="form-control name_list searching"  style="height:40px;" autocomplete="off">
                        </div>
                        <div class="col-xs-12">
                            <div class="search-result" id="search-result">
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <label for="">
                                <h4>Date de certification (jj.mm.aaaa - 28.02.1999)</h4>
                            </label>
                            <input type="text" name="ombudsmanformations.date[]" placeholder="Date de certification " class="datepicker form-control"   style="height: 40px;">
                        </div>                
                        <div class="col-xs-12">
                            <label for="image" class="col-md-12" style="padding:0">Copie de la certification</label>
                            <input type="file" name="image[]" class="section-image" id="sectionImage" onchange="readURL(this)">
                            <br/>
                        </div>
                    </div>

                <?php } ?>
                <br />
                <div class="col-xs-12" style="text-align:right;">
                    <span class="btn btn-success btn-xs" id="addFormationButton" onclick='addFormation()'><i class="fa fa-plus" aria-hidden="true"></i> Autres Formations</span>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <label for=""><h4>ACCRÉDITATION<small class="text-danger">*</small></h4></label>
                        <?php foreach ($acc as $key => $acc): ?>
                            <?php
                            $accreCheck = '';
                            if (in_array($acc['id'], $accredsArray)) {
                                $accreCheck = 'checked';
                            }
                            //echo $this->Form->checkbox('accre[]', ['value' => $acc['id'], 'label' => false, 'id' => '//a11y-issue-' . $key, 'div' => false, 'hiddenField' => false, $accreCheck]);
                            //echo '&nbsp;' . $acc['name'] . '&nbsp;';
                            ?>
                            <div class="single-col">
                                <div class="styled-input-container styled-input--diamond" style="float:left">
                                    <div class="styled-input-single">
                                        <input value="<?php echo $acc['id']; ?>" type="checkbox" name="accre[]" id="acc<?php echo $acc['id']; ?>" <?php echo $accreCheck; ?> />
                                        <label for="acc<?php echo $acc['id']; ?>"><?php echo $acc['name']; ?></label>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?> 
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12 text-center">
                    <?php
                    if ($status != 2) {
                        ?>

                        <a href="<?php echo $this->request->webroot; ?>Registerations/customerprofile" class="btn btn-lg btn-success pull-left" type="submit" id='register' style="border-radius: 0;border-color: #18202E;background:#18202E;  margin-top:43px;">&laquo; Retour</a>
                        <?php
                    }
                    ?>
                    <button class="btn btn-lg btn-success pull-right" type="submit" id='register' style="border-radius: 0;border-color: #18202E;background:#18202E;  margin-top:43px;"> Enregistrer</button>

                </div>

            </div>
            <!--            <div class="form-group">
<div class="col-xs-12">
<button class="btn btn-lg btn-success btn-block" type="submit" id='register' style="border-radius: 0;border-color: #147172;background:#147172;  margin-top:43px;height:50px;"> Enregistrer</button>
<br>
<br>
<br>
</div>
</div>-->
        </div>
    </div><!--/row-->


</div>
<?= $this->Form->end(); ?>
<!-- Modal -->
<div class="modal fade" id="termsAndConditions" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div style="padding:10px;color:white;background-color:#147172;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 style="width: 90%;text-align: justify;">
                    <?= $termsAndConditions->title; ?>
                </h4>
            </div>
            <div class="modal-body" style="height:600px;overflow: auto;">
                <!--                <div class="elm_wp">
                                    <div class="elementor elementor-12283 elementor-bc-flex-widget" data-elementor-id="12283" data-elementor-settings="[]" data-elementor-type="page">
                                        <div class="elementor-inner">
                                            <div class="elementor-section-wrap">
                                                <section class="elementor-element elementor-element-cca88a5 elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-element_type="section" data-id="cca88a5" data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}" style="width: 100%; left: 0px;">
                                                    <div class="elementor-container elementor-column-gap-default">
                                                        <div class="elementor-row">
                                                            <div class="elementor-element elementor-element-2e50c08 elementor-column elementor-col-100 elementor-top-column" data-element_type="column" data-id="2e50c08">
                                                                <div class="elementor-column-wrap  elementor-element-populated">
                                                                    <div class="elementor-widget-wrap">
                                                                        <div class="elementor-element elementor-element-4b2c474 elementor-widget elementor-widget-text-editor" data-element_type="widget" data-id="4b2c474" data-widget_type="text-editor.default">
                                                                            <div class="elementor-widget-container">
                                                                                <div class="elementor-text-editor elementor-clearfix">
                                                                                    <p>Conditions de l&rsquo;utilisation des outils interactifs du site de la FGeM (N.B: L&rsquo;emploi du pluriel regroupe la forme masculine et f&eacute;minine) L&rsquo;association FGeM dite : F&eacute;d&eacute;ration)</p>
                
                                                                                    <p> La F&eacute;d&eacute;ration Genevoise M&eacute;diations f&eacute;d&egrave;re les structures genevoises de m&eacute;diation en regroupant les associations de m&eacute;diation et l&rsquo;ensemble des m&eacute;diateurs. Ses statuts d&eacute;finissent ses objectifs. Tous ses membres alors utilisateurs des outils interactifs mis &agrave; leur disposition, s&rsquo;engagent &agrave; respecter tant ses statuts que les pr&eacute;sentes conditions d&rsquo;utilisation.</p>
                
                                                                                    <p> &nbsp; &nbsp; &nbsp; 1.Outils Interactifs Les outils interactifs mis &agrave; disposition des membres de la FGeM sont :<br />
                                                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; a.&nbsp;&nbsp; &nbsp;Une plateforme Internet de diffusion d&rsquo;informations diverses relatives &agrave; la m&eacute;diation et &agrave; l&rsquo;organisation d&rsquo;&eacute;v&eacute;nements li&eacute;s &agrave; la m&eacute;diation.<br />
                                                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; b.&nbsp;&nbsp; &nbsp;Un annuaire &eacute;lectronique de m&eacute;diateurs permettant aux membres de publier leur profil en mentionnant les formations qu&rsquo;ils ont suivies et en d&eacute;crivant leur pratique ainsi que leurs comp&eacute;tences.<br />
                                                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; c.&nbsp;&nbsp; &nbsp;Des &eacute;l&eacute;ments de codes permettant de r&eacute;pliquer et diss&eacute;miner des informations relatives &agrave; des activit&eacute;s de m&eacute;diation publi&eacute;s sur le site de la FGeM par ses membres.</p>
                
                                                                                    <p> &nbsp; &nbsp; &nbsp; 2.&nbsp;&nbsp; &nbsp;Annuaire &eacute;lectronique &ndash; Profil des membres<br />
                                                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  a.&nbsp;&nbsp; &nbsp;Tous les membres doivent compl&eacute;ter avec exactitude et tenir &agrave; jour les informations contenues dans le r&eacute;pertoire qui d&eacute;finit leur profil.&nbsp;<br />
                                                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  b.&nbsp;&nbsp; &nbsp;La candidature d&rsquo;un nouveau membre ne pourra pas &ecirc;tre prise en consid&eacute;ration tant que le profil n&rsquo;aura pas &eacute;t&eacute; compl&eacute;t&eacute;.<br />
                                                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  c.&nbsp;&nbsp; &nbsp;Les informations contenues dans le profil peuvent &ecirc;tre consult&eacute;es en tout temps par les membres du comit&eacute;.&nbsp;<br />
                                                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  d.&nbsp;&nbsp; &nbsp;La publication ne pourra &ecirc;tre effective qu&rsquo;apr&egrave;s validation des cotisations re&ccedil;ues.&nbsp;</p>
                
                                                                                    <p> &nbsp; &nbsp; &nbsp; 3.&nbsp;&nbsp; &nbsp;Administrateur du Site Internet<br />
                                                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  a.&nbsp;&nbsp; &nbsp;La t&acirc;che d&rsquo;administrateur du site peut &ecirc;tre d&eacute;l&eacute;gu&eacute;e par le comit&eacute; &agrave; un tiers.&nbsp;<br />
                                                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  b.&nbsp;&nbsp; &nbsp;Tout administrateur du site est tenu par un devoir de strict confidentialit&eacute; et ne peuvent en aucun cas communiquer &agrave; des tiers une quelconque information figurant dans le profil d&rsquo;un utilisateur sans son consentement explicite.</p>
                
                                                                                    <p>&nbsp; &nbsp; &nbsp; 4.&nbsp;&nbsp; &nbsp;Contenu du Profil<br />
                                                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;a.&nbsp;&nbsp; &nbsp;Les membres choisissent eux-m&ecirc;mes les informations qu&rsquo;ils souhaitent rendre publique.&nbsp;<br />
                                                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; b.&nbsp;&nbsp; &nbsp;La FGeM ne pourra pas &ecirc;tre tenue pour responsable si des informations publi&eacute;es par un membre sont utilis&eacute;es contre lui.<br />
                                                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; c.&nbsp;&nbsp; &nbsp;Les membres sont seuls responsable des informations contenues dans leur profil.&nbsp;</p>
                
                                                                                    <p>&nbsp; &nbsp; &nbsp;  5.&nbsp;&nbsp; &nbsp;V&eacute;rification des donn&eacute;es des membres et commission ad hoc<br />
                                                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  a.&nbsp;&nbsp; &nbsp;Un administrateur du site ou une personne mandat&eacute;e par le comit&eacute; de la FGeM peut &ecirc;tre amen&eacute; &agrave; v&eacute;rifier les informations &eacute;dit&eacute;es par un membre dans son profil.&nbsp;<br />
                                                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  b.&nbsp;&nbsp; &nbsp;Si un membre fait &eacute;tat de qualifications qu&rsquo;il ne poss&egrave;de pas ou a abus&eacute; des moyens mis &agrave; sa disposition pour promouvoir des activit&eacute;s qui ne sont pas en accord avec les objectifs de la F&eacute;d&eacute;ration, son profil pourra &ecirc;tre suspendu et le membre en question pourra &ecirc;tre exclu de la F&eacute;d&eacute;ration.<br />
                                                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; c.&nbsp;&nbsp; &nbsp;Dans le cas o&ugrave; une telle proc&eacute;dure devait &ecirc;tre engag&eacute;e, le membre sera inform&eacute; dans un d&eacute;lai de 30 jours par simple Courrier &eacute;lectronique avec une invitation &agrave; s&rsquo;expliquer.&nbsp;<br />
                                                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  d.&nbsp;&nbsp; &nbsp;Le cas &eacute;ch&eacute;ant, un membre aura la possibilit&eacute; de recourir contre la d&eacute;cision prise dans un d&eacute;lai de 30 jours en demandant l&rsquo;organisation d&rsquo;une m&eacute;diation &agrave; ses frais.</p>
                
                                                                                    <p>&nbsp; &nbsp; &nbsp; 6.&nbsp;&nbsp; &nbsp;Type d&rsquo;informations<br />
                                                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; a.&nbsp;&nbsp; &nbsp;Les informations publi&eacute;es par les membres par l&rsquo;interm&eacute;diaire de la plateforme de diffusion d&rsquo;information de la F&eacute;d&eacute;ration doivent imp&eacute;rativement &ecirc;tre en ad&eacute;quation avec la m&eacute;diation ou &ecirc;tre associ&eacute;es &agrave; une activit&eacute; en lien avec des comp&eacute;tences pouvant &ecirc;tre associ&eacute;s &agrave; la m&eacute;diation.<br />
                                                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; b.&nbsp;&nbsp; &nbsp;Le comit&eacute; de la FGeM, peut d&eacute;cider en tout temps de supprimer les informations ne r&eacute;pondant pas aux objectifs de la F&eacute;d&eacute;ration.<br />
                                                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; c.&nbsp;&nbsp; &nbsp;Les informations doivent entrer dans le cadre des buts de la F&eacute;d&eacute;ration.</p>
                
                                                                                    <p>&nbsp; &nbsp; &nbsp; 7.&nbsp;&nbsp; &nbsp;Droits d&rsquo;utilisation<br />
                                                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; a.&nbsp;&nbsp; &nbsp;La publication du profil des membres ainsi que l&rsquo;utilisation de la plateforme de diffusion d&rsquo;informations est r&eacute;serv&eacute;e aux seuls membres qui sont &agrave; jour de leurs cotisations.&nbsp;<br />
                                                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; b.&nbsp;&nbsp; &nbsp;Les droits d&rsquo;utilisation des outils interactifs seront automatiquement suspendus pass&eacute; le d&eacute;lai de paiement des cotisations annuelles dues ou de tout autre solde d&ucirc;.<br />
                                                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; c.&nbsp;&nbsp; &nbsp;Les droits pourront &ecirc;tre r&eacute;tablis par le tr&eacute;sorier apr&egrave;s r&eacute;ception de l&rsquo;int&eacute;gralit&eacute; des cotisations et sommes dues.</p>
                
                                                                                    <p>&nbsp; &nbsp; &nbsp; 8.&nbsp;&nbsp; &nbsp;R&eacute;solution des diff&eacute;rends<br />
                                                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; a.&nbsp;&nbsp; &nbsp;Les membres, utilisateurs des outils mis &agrave; leur disposition par la FGeM, s&rsquo;engagent en cas de litige &agrave; avoir pr&eacute;alablement recours &agrave; une m&eacute;diation pour r&eacute;soudre tout conflit pouvant &eacute;merger de l&rsquo;utilisation des outils et/ou de la plateforme de la F&eacute;d&eacute;ration.&nbsp;<br />
                                                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;b.&nbsp;&nbsp; &nbsp;Les pr&eacute;sentes conditions sont r&eacute;gies par le droit suisse. En cas d&rsquo;&eacute;chec de la m&eacute;diation pr&eacute;alable, tout litige en r&eacute;sultant sera exclusivement jug&eacute; par les tribunaux comp&eacute;tents du Canton de Gen&egrave;ve, sous r&eacute;serve de recours au tribunal f&eacute;d&eacute;ral.<br />
                                                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; c.&nbsp;&nbsp; &nbsp;La F&eacute;d&eacute;ration conserve toutefois la facult&eacute; de faire valoir ses droits au domicile du d&eacute;posant ou devant tout autre tribunal comp&eacute;tent.&nbsp;</p>
                
                                                                                    <p>En cliquant sur le bouton lu et accept&eacute;, vous vous engagez &agrave; accepter et &agrave; respecter les conditions d&rsquo;utilisation d&eacute;finis ci-dessus.<br />
                                                                                        &nbsp; &nbsp; &nbsp;
                                                                                    </p>
                                                                                    <br/>
                                                                                    <br/>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->
                <?= $termsAndConditions->description; ?>
                <div style="text-align:right;margin-bottom: 20px;">
                    <span class="button-checkbox">
                        <button type="button" class="btn btn-lg" data-color="success  TermsAndConditionButon">Lu et Accept&eacute;</button>
                        <input value="1" type="checkbox" name="isAgree" id="isAgree" class="hidden" <?= ($accept_info == 1) ? 'checked' : ''; ?> >
                    </span>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
            </div>
        </div>

    </div>
</div>
</div>
<script>
    $(function () {

        $('<a id="removeLanguageList" style="margin:5px;cursor:pointer;color:red;float:right;font-size:20px;" class="fa fa-times"></a>').insertBefore($('#ms-list-1 .ms-search'));
        $('#removeLanguageList').click(function () {
            $('#ms-list-1').removeClass('ms-active');
        });
        $('.button-checkbox').each(function () {

            // Settings
            var $widget = $(this),
                    $button = $widget.find('button'),
                    $checkbox = $widget.find('input:checkbox'),
                    color = $button.data('color'),
                    settings = {
                        on: {
                            icon: 'glyphicon glyphicon-check'
                        },
                        off: {
                            icon: 'glyphicon glyphicon-unchecked'
                        }
                    };

            // Event Handlers
            $button.on('click', function () {
                $checkbox.prop('checked', !$checkbox.is(':checked'));
                $checkbox.triggerHandler('change');
                updateDisplay();
            });
            $checkbox.on('change', function () {
                updateDisplay();
            });

            // Actions
            function updateDisplay() {
                var isChecked = $checkbox.is(':checked');

                // Set the button's state
                $button.data('state', (isChecked) ? "on" : "off");

                // Set the button's icon
                $button.find('.state-icon')
                        .removeClass()
                        .addClass('state-icon ' + settings[$button.data('state')].icon);

                // Update the button's color
                if (isChecked) {
                    $button
                            .removeClass('btn-default')
                            .addClass('btn-' + color + ' active');
                } else {
                    $button
                            .removeClass('btn-' + color + ' active')
                            .addClass('btn-default');
                }
            }

            // Initialization
            function init() {

                updateDisplay();

                // Inject the icon if applicable
                if ($button.find('.state-icon').length == 0) {
                    $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
                }
            }
            init();
        });
    });
    $(function () {
        if ($('select[name=mediator_question]').val() == 'Non') {
            $('input[name="ombudsmanmeds[mediat][]"]').prop('checked', false);
            $('#multipleLanguages').attr('required', false);
            $('.mediationTypes').hide();
        } else {
            $('#multipleLanguages').attr('required', true);
            $('.mediationTypes').show();
        }
        $('select[name=mediator_question]').change(function () {
            if ($('select[name=mediator_question]').val() == 'Non') {

                $('input[name="ombudsmanmeds[mediat][]"]').prop('checked', false);
                $('#multipleLanguages').attr('required', false);
                $('.mediationTypes').hide();
            } else {
                $('#multipleLanguages').attr('required', true);
                $('.mediationTypes').show();

            }
        });

//        addMoreAssermentation
        //        fieldPlace0
        if ($('select[name=assermentation]').val() == 0) {
            $('#addMoreAssermentation').hide();
            $('#fieldPlace0').hide();
        } else {
            $('#addMoreAssermentation').show();
            $('#fieldPlace0').show();
        }
        $('select[name=assermentation]').change(function () {
            if ($('select[name=assermentation]').val() == 0) {
                $('#addMoreAssermentation').hide();
                $('#fieldPlace0').hide();
            } else {
                $('#addMoreAssermentation').show();
                $('#fieldPlace0').show();
            }

        });
    });
    function put_value(value, dynamic_val) {
        //        alert(dynamic_val);
        if (typeof dynamic_val == 'undefined') {
            dynamic_val = '';
        }
        $('#formationId' + dynamic_val).val(value);
        $('#search-result' + dynamic_val).hide();
        $('#search-result' + dynamic_val).css({'border': 'none'});

    }
    function closeDiv(id) {
        $('#fieldPlace' + id).remove();
    }
    function closeFormation(id) {
        $('#fieldFormation' + id).remove();
    }
    function deleteFormation(row_id, id) {
        if (confirm('Are you sure you want to delete')) {
            row_id = row_id;
            $.ajax({
                url: "<?php echo $this->request->webroot . 'Registerations/deleteFormation' ?>",
                type: "GET",
                data: 'id=' + id,
                success: function (data)
                {
                    data = JSON.parse(data);
                    if (data.flag == 1) {
                        $('#fieldFormation' + row_id).remove();
                        alert(data.message);
                    } else {
                        alert(data.message);
                    }
                }
            });
        }
    }
    function addField() {
        var dynamicVal = $('div[id^=fieldPlace]').length;

        var insertAfterId = dynamicVal - 1;
        var html = '<div class="groupFields assermentation-wrapper" id="fieldPlace' + dynamicVal + '">\n\
<div class="col-xs-12">\n\
<span onclick="closeDiv(' + dynamicVal + ')" class="pull-right crossButton">&times;</span>\n\
<label for="first_name"><h4>Lieu d\'Assermentation<small class="text-danger">*</small></h4></label>\n\
<div class="input text"><input name="place[]" class="form-control" style="height:40px;" id="place" value="" type="text" required></div></div>\n\
<div class="col-xs-12">\n\
<label for=""><h4>Date d\'Assermentation (jj.mm.aaaa - 28.02.1999)<small class="text-danger">*</small></h4></label>\n\
<div class="input text"><input name="date_of_auth[]" class="form-control datepicker" style="height:40px;" value="" type="text" required><br/></div></div>\n\
                        </div>';
        $(html).insertAfter($('#fieldPlace' + insertAfterId));

    }
    function addFormation() {
        var dynamicVal = $('div[id^=fieldFormation]').length;
        var insertAfterId = dynamicVal - 1;
        var html = '<div class="groupFields" id="fieldFormation' + dynamicVal + '" style="margin-top:10px">\n\
<div class="col-xs-12">\n\
<span onclick="closeFormation(' + dynamicVal + ')" class="pull-right crossButton">&times;</span>\n\
<label><h4>Formation <small class="text-danger">*</small></h4></label>\n\
<input name="ombudsmanformations.formation[]" id="formationId' + dynamicVal + '" placeholder="Formation" dynamic_val="' + dynamicVal + '" class="form-control name_list searching" style="height:40px;" type="text" required autocomplete="off">\n\
    </div><div class="col-xs-12">\n\
                            <div class="search-result" id="search-result' + dynamicVal + '">\n\
                            </div>\n\
                        </div>\n\
<div class="col-xs-12">\n\
<label for="">\n\
<h4>Date de certification (jj.mm.aaaa - 28.02.1999)<small class="text-danger">*</small></h4>\n\
    </label>\n\
<input name="ombudsmanformations.date[]" placeholder="Date de certification" class="datepicker form-control" style="height: 40px;" type="text" required>\n\
    </div><div class="col-xs-12">\n\
<label for="image" class="col-md-12" style="padding:0">Copie de la certification <small class="text-danger">*</small></label>\n\
<input name="image[]" class="section-image" id="sectionImage" onchange="readURL(this)" type="file" required>\n\
<br>\n\
    </div>\n\
                        </div>';
        $(html).insertAfter($('#fieldFormation' + insertAfterId));

    }
    var formation = $('#fieldFormation0 input[name="ombudsmanformations.formation[]"]').val();
    if (formation != '') {

        $('#fieldFormation0 input[name="ombudsmanformations.date[]"]').prop('required', true);
        $('#fieldFormation0 input[name="ombudsmanformations.date[]"]').siblings('label').children('h4').append('<small class="text-danger">*</small>');
        //        $('#fieldFormation0 input[name="image[]"]').prop('required', true);
        $('#fieldFormation0 input[name="image[]"]').siblings('label').append('<small class="text-danger">*</small>');

    } else {
        $('#fieldFormation0 input[name="ombudsmanformations.date[]"]').prop('required', false);
        $('#fieldFormation0 input[name="ombudsmanformations.date[]"]').siblings('label').children('h4').children('small').remove();
        //        $('#fieldFormation0 input[name="image[]"]').prop('required', false);
        $('#fieldFormation0 input[name="image[]"]').siblings('label').children('small').remove();

    }
    $(document).ready(function () {

        $('input[name=accept_info]').change(function () {
            $('input[name=isAgree]').attr('checked', false);
            $('.button-checkbox button').removeClass('btn-success TermsAndConditionButon active').addClass('btn-default');
            $('.button-checkbox button i').removeClass('glyphicon-check').addClass('glyphicon-unchecked');
            if (typeof $('input[name=accept_info]:checked').val() != 'undefined') {
                $('#termsAndConditions').modal('show');
            }
        });
        $(document).on('focusout', '.searching', function () {
            var searching = $(this);
            var dynamic_val = searching.attr('dynamic_val');
            if (typeof searching.attr('dynamic_val') == 'undefined') {
                dynamic_val = '';
            }
            setTimeout(function () {
                $('#search-result' + dynamic_val).hide();

            }, 300);

        });
        $(document).on('keyup', '.searching', function () {
            var value = $(this).val();
            var dynamic_val = '';
            if (typeof $(this).attr('dynamic_val') != 'undefined') {
                dynamic_val = $(this).attr('dynamic_val');
            }

            $('#search-result' + dynamic_val).show();
            $('#search-result' + dynamic_val).css({'border': '1px solid #ccc'});
            if (value != '')
            {
                $.ajax({
                    type: 'GET',
                    url: '<?php echo $this->request->webroot ?>Users/formation',
                    data: 'field=' + value,
                    contentType: 'json',
                    success: function (data)
                    {
                        var data = JSON.parse(data);
                        if ($.isEmptyObject(data))
                        {
                            var divsearch = $('#search-result' + dynamic_val).empty();
                            var app = "";
                            $('#search-result' + dynamic_val).append(app);
                        } else
                        {
                            $('#search-result' + dynamic_val).empty();
                            var icon;
                            var loginWithSocial;
                            var path = '';
                            var app = "<ul class='dynamic_cities'>";
                            $.each(data, function (key, value) {
                                icon = value.profile_image;
                                loginWithSocial = value.loginwithsocial;

                                $('#search-result' + dynamic_val).empty();
                                app += "<li class='searched_user'>"
                                        + "<p onclick='put_value(" + '"' + value + '"' + "," + '"' + dynamic_val + '"' + ")'>" + value + "</p>"
                                        + "</li>";
                            });
                            app += "</ul>";

                            $('#search-result' + dynamic_val).append(app);
                        }
                    }
                });
                //end
            } else {
                $('#search-result' + dynamic_val).hide();
            }
        });





        $('#fieldFormation0 input[name="ombudsmanformations.formation[]"]').change(function () {
            var formation = $('#fieldFormation0 input[name="ombudsmanformations.formation[]"]').val();
            if (formation != '') {
                var check = $('#fieldFormation0 input[name="ombudsmanformations.date[]"]').siblings('label').children('h4').children('small').length;
                if (check == 0) {
                    $('#fieldFormation0 input[name="ombudsmanformations.date[]"]').prop('required', true);
                    $('#fieldFormation0 input[name="ombudsmanformations.date[]"]').siblings('label').children('h4').append('<small class="text-danger">*</small>');
                    //                $('#fieldFormation0 input[name="image[]"]').prop('required', true);
                    $('#fieldFormation0 input[name="image[]"]').siblings('label').append('<small class="text-danger">*</small>');
                    $('#fieldFormation0 input[name="image[]"]').prop('required', true);
                }
            } else {
                $('#fieldFormation0 input[name="ombudsmanformations.date[]"]').prop('required', false);
                $('#fieldFormation0 input[name="ombudsmanformations.date[]"]').siblings('label').children('h4').children('small').remove();
                //                $('#fieldFormation0 input[name="image[]"]').prop('required', false);
                $('#fieldFormation0 input[name="image[]"]').siblings('label').children('small').remove();
                $('#fieldFormation0 input[name="image[]"]').prop('required', false);

            }
        });



        $('#businessForm').submit(function () {
            if (typeof $('input[name=accept_info]:checked').val() != 'undefined') {
                if (typeof $('input[name=isAgree]:checked').val() == "undefined") {
                    $('#termsAndConditions').modal('show');
                    return false;
                }
            }
            var mediation = $('input[name="ombudsmanmeds[mediat][]"]:checked').val();

            if ($('select[name=mediator_question]').val() == 'Oui') {
                if (typeof mediation == 'undefined') {
                    alert('MÃ©diation is required');
                    $('input[name="ombudsmanmeds[mediat][]"]').focus();
                    return false;
                }
                var accre = $('input[name="accre[]"]:checked').val();
                if (typeof accre == 'undefined') {
                    alert('ACCRÃ‰DITATION is required');
                    $('input[name="accre[]"]').focus();
                    return false;
                }
                var formation = $('#fieldFormation0 input[name="ombudsmanformations.formation[]"]').val();
                if (formation != '') {
                    if ($('#fieldFormation0 input[name="ombudsmanformations.date[]"]').val() == '') {
                        alert('Date de certification is required');
                        $('#fieldFormation0 input[name="ombudsmanformations.date[]"]').focus();
                        return false;

                    }

                }
            }


        });
        /*jQuery('.js-example-basic-multiple').select2();*/

        $(document).on('change', '#multipleLanguages', function () {
            var values = $(this).val();
            if (values == null) {
                $('#myLanguages').html('');
                return false;
            }
            $.ajax({
                url: "<?php echo $this->request->webroot . 'Registerations/languagae_experience' ?>",
                type: "GET",
                data: 'values=' + values,
                success: function (data)
                {
                    data = JSON.parse(data);
                    $('#myLanguages').html(data);
                }
            });
        });
        if ($('#fieldPlace0 input[name="place[]"]').val() == '') {
            $('#acceptInfoDiv').hide();
            $('#acceptInfoDiv input[type=checkbox]').prop('checked', false);
        } else {
            $('#acceptInfoDiv').show();

        }

        $('#fieldPlace0 input[name="place[]"]').change(function () {
            //        alert();
            if ($('#fieldPlace0 input[name="place[]"]').val() == '') {
                $('#acceptInfoDiv').hide();
                $('#acceptInfoDiv input[type=checkbox]').prop('checked', false);

            } else {
                $('#acceptInfoDiv').show();

            }
        });
        $('#assermentation').change(function () {
            if ($(this).val() == 1) {
                //                $('#fieldPlace0 label h4').append('<small class="text-danger">*</small>');
                $('#fieldPlace0 input[name="place[]"]').parent().siblings('label').children('h4').append('<small class="text-danger">*</small>');
                $('#fieldPlace0 input[name="place[]"]').prop('required', true);

                $('#fieldPlace0 input[name="date_of_auth[]"]').parent().siblings('label').children('h4').append('<small class="text-danger">*</small>');
                $('#fieldPlace0 input[name="date_of_auth[]"]').prop('required', true);
            } else {
                $('#fieldPlace0 label h4').children('small').remove();
                $('#fieldPlace0 input[name="place[]"]').prop('required', false);
                
                $('#fieldPlace0 label h4').children('small').remove();
                $('#fieldPlace0 input[name="date_of_auth[]"]').prop('required', false);

            }
        });

        $(document).on('click', '.btn_remove', function () {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
        });

        setTimeout(function () {
            $('input[name=pwd]').val('');

        }, 100);

        $('input[name=pwd]').blur(function () {
            if ($(this).val()) {
                if ($(this).val().length < 6) {
                    if ($(this).siblings('small').length > 0) {
                        $(this).siblings('small').text('At least 6 characters');
                    } else {
                        $('<small style="color:red">At least 6 characters</small>').insertAfter($(this));
                    }
                    $('input[name=repassword]').prop('disabled', true);
                    return false;
                } else {
                    $('input[name=repassword]').prop('disabled', false);
                    $(this).siblings('small').text('');
                }
                $('input[name=repassword]').val('');
            }
        });
        $('input[name=repassword]').blur(function () {
            if ($(this).val()) {
                if ($('input[name="pwd"]').val() != $(this).val()) {
                    if ($(this).siblings('small').length > 0) {
                        $(this).siblings('small').text('Confirm Password does not match');
                    } else {
                        $('<small style="color:red">Confirm Password does not match</small>').insertAfter($(this));
                    }
                    return false;

                } else {
                    $(this).siblings('small').text('');
                }
            }
        });
        var readURL = function (input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    //                        console.log(e.target);
                    //                        $('.avatar').attr('style', 'background-image:url("' + e.target.result + '")');
                    $('.avatar').css("background-image", "url('" + e.target.result + "')");
                    ;
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
        $(".file-upload").on('change', function () {
            readURL(this);
        });
    });

    $("#UserAddForm").submit(function () {
        if ($('input[name="pwd"]').val()) {
            if ($('input[name="pwd"]').val().length < 6) {
                if ($('input[name="pwd"]').siblings('small').length > 0) {
                    $('input[name="pwd"]').siblings('small').text('At least 6 characters');
                } else {
                    $('<small style="color:red">At least 6 characters</small>').insertAfter($('input[name="pwd"]'));
                }
                return false;
            } else {
                if ($('input[name="repassword"]').val()) {
                    if ($('input[name="pwd"]').val() != $('input[name="repassword"]').val()) {
                        if ($('input[name="repassword"]').siblings('small').length > 0) {
                            $('input[name="repassword"]').siblings('small').text('Confirm Password does not match');
                        } else {
                            $('<small style="color:red">Confirm Password does not match</small>').insertAfter($('input[name="repassword"]'));
                        }
                        return false;

                    } else {
                        $('input[name="pwd"]').siblings('small').text('');
                    }
                    $('input[name="pwd"]').siblings('small').text('');
                } else {
                    if ($('input[name="repassword"]').siblings('small').length > 0) {
                        $('input[name="repassword"]').siblings('small').text('Please enter confirm password');
                    } else {
                        $('<small style="color:red">Please enter confirm password</small>').insertAfter($('input[name="repassword"]'));
                    }
                    return false;
                }
            }
        }
        /*
         
         var phone_pat = /^\(?(\+\d{3})\)?[-\. ]?(\d{3})[-\. ]?(\d{2})[-\. ]?(\d{2})$/;
         var phone = $("#tel1").val();
         //alert(phone);
         
         if ($('input[name=tel1]').val()) {
         //code for further validation
         if (!phone_pat.test(phone)) {
         
         $('input[name=tel1]').focus();
         $('label[for=tel1] i').addClass('fa-frown-o field_error').removeClass('fa-smile-o field_ok');
         alert("S'il vous plaÃ®t entrer le format de tÃ©lÃ©phone valide");
         return false;
         
         alert("S'il vous plaÃ®t entrer le format de tÃ©lÃ©phone valide");
         
         
         
         } else {
         
         $('label[for=tel1] i').addClass('fa-smile-o field_ok').removeClass('fa-frown-o field_error');
         }
         
         } else {
         $('input[name=tel1]').focus();
         $('label[for=tel1] i').addClass('fa-frown-o field_error').removeClass('fa-smile-o field_ok');
         alert("S'il vous plaÃ®t entrer le format de tÃ©lÃ©phone valide");
         return false;
         
         alert("S'il vous plaÃ®t entrer le format de tÃ©lÃ©phone valide");
         
         
         
         }
         */
        if (!$('input[name=first_name]').val()) {
            $('input[name=firstname]').focus();
            return false;
        }

        if (!$('input[name=lastname]').val()) {
            $('input[name=lastname]').focus();
            return false;
        }


        if ($('input[name=postalcode]').length) {
            if ($('input[name=postalcode]').val()) {
                //             alert($('input[name=zipcode]').val().length);
                if ($('input[name=postalcode]').val().length > 5) {
                    $('input[name=postalcode]').focus();
                    alert("S'il vous plaÃ®t entrer le code postal valide");
                    return false;

                    alert("S'il vous plaÃ®t entrer le code postal valide");



                } else {
                    return true;
                }
            } else {
                $('input[name=postalcode]').focus();
                alert("S'il vous plaÃ®t entrer le code postal valide");
                return false;

                alert("S'il vous plaÃ®t entrer le code postal valide");



            }
        }
    });
    $('#multipleLanguages').multiselect({
        columns: 1,
        placeholder: '',
        search: true
    });

    $("#assermentation").on("change", function () {
        if ($(this).val() == 0) {
            $('.assermentation-wrapper input').attr("value", "");
            $('.assermentation-wrapper').hide();
            $('.btn-asser').hide();
            $('#acceptInfoDiv input[type=checkbox]').prop('checked', false);
            $('#acceptInfoDiv').hide();

        } else {
            $('.assermentation-wrapper').show();
            $('.btn-asser').show();
            $('#acceptInfoDiv input[type=checkbox]').prop('checked', false);
            $('#acceptInfoDiv').show();
        }
    });

</script>