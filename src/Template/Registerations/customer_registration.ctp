<style>
    .customButtons{
        border: 1px solid #147172;
        background: none;
        padding: 25px 90px;
        font-size: 20px;
        color: #147172;
        font-weight: bold;
    }
    .customButtons:hover{
        background: #147172;
        color: white;
        transition: all .4s ease;
        -webkit-transition: all .4s ease;
    }
    .customButtons:focus{
        background: #147172;
        color: white;
    }
    .activeMember{
        background: #147172;
        color: white;
    }
    .MyRadio{
        margin-right:10px !important;
    }

</style>
<div class="container bootstrap snippet" style="min-height: 502px;">
    <div class="row">
        <div class="col-sm-1">
        </div>
        <div class="col-sm-10">
            <div class="tab-content">
                <div class="tab-pane active" id="home">
                    <!--<hr>-->
                    <h3 style="text-align:center;margin: 30px 0;font-weight: bold;">Customer Login</h3>

                    <!--                  <form class="form" action="##" method="post" id="registrationForm">-->
                    <?= $this->Form->create($user, array('type' => 'file', 'id' => 'UserAdd')) ?>
                    <div class="form-group">

                        <div class="col-xs-6">
                            <label for="first-name"><h4>Prénom <small class="text-danger">*</small></h4></label>

                            <?php
                            echo $this->Form->control('first_name', [
                                'class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'required' => 'required']);
                            ?></div>
                    </div>

                    <div class="form-group">

                        <div class="col-xs-6">
                            <label for="lastname"><h4>Nom de famille <small class="text-danger">*</small></h4></label>
                            <?php
                            echo $this->Form->control('lastname', [
                                'class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'required' => 'required']);
                            ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-6">
                            <label for="pass"><h4>Mot de passe <small class="text-danger">*</small></h4></label>
                            <?php
                            echo $this->Form->control('password', [
                                'class' => 'form-control', 'type' => 'password', 'style' => 'height:40px;', 'id' => 'pass', 'label' => false, 'required' => 'required']);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">

                        <div class="col-xs-6">
                            <label for="repass"><h4>Confirmez le mot de passe <small class="text-danger">*</small></h4></label>
                            <?php
                            echo $this->Form->control('repassword', [
                                'class' => 'form-control', 'type' => 'password', 'style' => 'height:40px;', 'label' => false, 'id' => 'repass', 'required' => 'required']);
                            ?> </div>
                    </div>
                    <div class="form-group">

                        <div class="col-xs-6">
                            <label for="email"><h4>Votre identifiant (adresse mail valide) <small class="text-danger">*</small></h4></label>
                            <?php
                            echo $this->Form->control('email', [
                                'class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'required' => 'required']);
                            ?>

                        </div>

                        <div class="col-xs-6">
                            <label for="confirmemail"><h4>Confirmez Votre identifiant (adresse mail valide) <small class="text-danger">*</small></h4></label>
                            <?php
                            echo $this->Form->control('confirmemail', [
                                'class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'required' => 'required']);
                            ?>

                        </div>


                    </div>
                    <div class="col-xs-12">
                        <button class="btn btn-lg btn-success btn-block" type="submit" id='register' style="border-radius: 0;border-color: #147172;background:#147172;  margin-top:43px;height:40px;"> S'INCRIRE</button>
                        <br/>
                        <br/>
                    </div>

                    <?php echo $this->Form->end(); ?>

                </div><!--/tab-pane-->
               
            </div><!--/tab-content-->

        </div><!--/col-9-->
        <div class="col-sm-1">
        </div>
    </div>
</div><!--/row-->
<script>
    $("#UserAdd").submit(function () {
        if (!$('input[name=first_name]').val()) {
            $('input[name=firstname]').focus();
            return false;
        }

        if (!$('input[name=lastname]').val()) {
            $('input[name=lastname]').focus();
            return false;
        }
        if ($('input[name=password]').val().length < 6) {
            if ($('input[name=password]').siblings('small').length > 0) {
                $('input[name=password]').siblings('small').text('Veuillez entrer 6 caractères au minimum');
            } else {
                $('<small style="color:red">Veuillez entrer 6 caractères au minimum</small>').insertAfter($('input[name=password]'));
            }
            return false;
        }
        if ($('input[name=password]').val() != $('input[name=repassword]').val()) {
            if ($('input[name=repassword]').siblings('small').length > 0) {
                $('input[name=repassword]').siblings('small').text("les deux mots de passe ne sont pas pareil.");
            } else {
                $("<small style='color:red'>les deux mots de passe ne sont pas pareil.</small>").insertAfter($('input[name=repassword]'));
            }
            return false;
        }
        if ($('input[name=confirmemail]').val() != $('input[name=email]').val()) {
            if ($('input[name=confirmemail]').siblings('small').length > 0) {
                $('input[name=confirmemail]').siblings('small').text("L'adresse de messagerie ne correspond pas");
            } else {
                $("<small style='color:red'>L'adresse de messagerie ne correspond pas.</small>").insertAfter($('input[name=confirmemail]'));
            }
            return false;
        }
    });
    $('input[name=password]').blur(function () {
        if ($(this).val()) {
            if ($(this).val().length < 6) {
                $('label[for=password] i').addClass('fa-frown-o field_error').removeClass('fa-smile-o field_ok');
                if ($(this).siblings('small').length > 0) {
                    $(this).siblings('small').text('Veuillez entrer 6 caractères au minimum');
                } else {
                    $('<small style="color:red">Veuillez entrer 6 caractères au minimum</small>').insertAfter($(this));
                }
                return false;
            }
        } else {
            $('label[for=password] i').addClass('fa-frown-o field_error').removeClass('fa-smile-o field_ok');
            return false;
        }
    });
    $('input[name=repassword]').blur(function () {
        if ($(this).val()) {
            if ($('input[name="password"]').val() != $(this).val()) {
                if ($(this).siblings('small').length > 0) {
                    $(this).siblings('small').text('les deux mots de passe ne sont pas pareil.');
                } else {
                    $('<small style="color:red">les deux mots de passe ne sont pas pareil.</small>').insertAfter($(this));
                }
                $('label[for=repassword] i').addClass('fa-frown-o field_error').removeClass('fa-smile-o field_ok');
                return false;

            } else {
                $(this).siblings('small').text('');
                $('label[for=repassword] i').addClass('fa-smile-o field_ok').removeClass('fa-frown-o field_error');
            }
        } else {
            $('label[for=repassword] i').addClass('fa-frown-o field_error').removeClass('fa-smile-o field_ok');
            return false;
        }
    });


    $("#ind").hide();
    $("#col").hide();
    $("#mem").click(function () {
        $("#ind").show();
        $("#col").hide();
    });
    $("#mem2").click(function () {
        $("#col").show();
        $("#ind").hide();
    });
    $(document).ready(function () {
        $('.customButtons').click(function () {
            $("input[type=radio]").attr('checked', false);
            var type = $(this).attr('memberType');
            $('button').removeClass('activeMember');
            $('button[memberType=' + type + ']').addClass('activeMember');
            if (type == 'individualOptions') {
                $('#labelMembership , .individualOptions').show('slow');

                $('.collectiveOptions').hide();
            } else {
                $('.individualOptions').hide();
                $('#labelMembership, .collectiveOptions').show('slow');
            }

        });
    });


</script>



