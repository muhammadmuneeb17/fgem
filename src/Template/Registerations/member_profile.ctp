<?php
/**
 * @var \App\View\AppView $this
 */
$array = explode('/', $_SERVER['REQUEST_URI']);
$tabId = end($array);
?>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style>
    div.avatar{
        height: 200px;
        width: 200px;
        background-position: center;
        background-size: cover;
        border-radius:50%;
    }
    .formHeading{
        padding: 10px 10px;
        border-bottom: 1px solid #ccc;
    }
</style>

<div class="container bootstrap snippet">
    <?= $this->Form->create($registeration, array('type' => 'file', 'id' => 'UserAddForm')) ?>
    <?php //debug($registeration['user']['email']);exit;?>

    <div class="row">
        <div class="col-sm-12"><!--left col-->

            <div class="tab-content">
                <div class="tab-pane <?php if($tabId==0){echo 'active';}?>" id="home">

                    <div class="form-group">

                        <div class="col-md-3">
                            <div class="form-group">
                                <?php
                                if (empty($registeration->image)) {
                                    $path = 'http://ssl.gstatic.com/accounts/ui/avatar_2x.png';
                                } else {
                                    $path = $this->request->webroot . 'img/slider/thumbnail/' . $registeration->image;
                                }
                                ?>
                                <div class="avatar" style="background-image: url(<?php echo $path; ?>);">

                                </div> 
                                <div class="clearfix"></div>
                                <h6 class="text-primary">Mettre une photo augmente vos chances d'être choisi</h6>
                                <input type="file"  name='image' class="file-upload"  id="sectionImage" onchange="readURL(this)">


                            </div>
                        </div>
                        <div class="col-md-9" style="padding-top:60px;">

                            <label for="is_public" style="width:100%;">
                                <input type="radio" name="publicprofile" id="is_public" checked value="public" class="col-sm-1" <?php
                                       if ($registeration['publicprofile'] == 1 || $registeration['publicprofile'] == '') {
                                           echo 'checked';
                                       }
                                       ?> >
                                <h4 class="text-success col-sm-11" style="padding:0px;margin:0px">Publier mon Profil dans l'Annuaire des médiateurs de la FGeM</h4>
                            </label>
                            <label for="is_private" style="width:100%;">
                                <input type="radio" name="publicprofile" <?php
                                       if ($registeration['publicprofile'] == '0') {
                                           echo 'checked';
                                       }
                                       ?> value="private" id="is_private" class="col-sm-1">
                                <h4 class="text-danger col-sm-11" style="padding:0px;margin:0px"> Ne pas publier mon profil dans l'Annuaire des médiateurs de la FGeM</h4>
                            </label>
                        </div>
                        <div class="clearfix"></div>
                        <h4 class="formHeading" style="font-weight:bold;margin-top:10px !important;">Informations personnelles</h4>
                        <div class="form-group">

                            <div class="col-md-6">
                                <label for="firstname"><h4>Prénom <small class="text-danger">*</small></h4></label>
                                <?php
                                echo $this->Form->control('first_name', [
                                    'class' => 'form-control', 'value' => $registeration['user']['first_name'], 'style' => 'height:40px;', 'label' => false, 'div' => false, 'required' => 'required']);
                                ?></div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-6">
                                <label for="lastname"><h4>Nom de famille <small class="text-danger">*</small></h4></label>
                                <?php
                                echo $this->Form->control('lastname', [
                                    'class' => 'form-control', 'value' => $registeration['user']['last_name'], 'style' => 'height:40px;', 'label' => false, 'div' => false, 'required' => 'required']);
                                ?>
                                <?php
                                echo $this->Form->control('header', [
                                    'class' => 'form-control', 'value' => 0,'type' => 'hidden', 'style' => 'height:40px;', 'label' => false, 'div' => false, 'required' => 'required']);
                                ?>
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-6">
                                <label for="telephone1" style="display:block">
                                    <h4 style="float:left;">Téléphone 1 <small class="text-danger">*</small></h4>
                                    <?php
                                    //if($registeration['status'] == 1){
                                    ?>
                                    <label title="Vous pouvez préciser quelles sont vos données que vous nous permettez de publier." data-toggle="tooltip" data-placement="bottom" style="margin: 10px 0;float: right;" >Publique
                                        <input type="checkbox" name="tel1public"  <?php
                                               if ($registeration['tel1public'] == 1) {
                                                   echo 'checked';
                                               }
                                               ?> />
                                        <span style="color: #4285F4">&#63;</span>
                                    </label>
                                    <?php //} ?>
                                </label>
                                <?php
                                echo $this->Form->control('tel1', [
                                    'class' => 'form-control','placeholder'=>'+1234567895' ,'style' => 'height:40px;', 'label' => false, 'id' => 'tel1', 'div' => false, 'required' => 'required']);
                                ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <div class="col-md-6">
                                <label for="telephone2" style="display:block">
                                    <h4 style="float:left;">Téléphone 2</h4>
                                    <?php
                                    //if($registeration['status'] == 1){
                                    ?>
                                    <label title="Vous pouvez préciser quelles sont vos données que vous nous permettez de publier." data-toggle="tooltip" data-placement="bottom" style="margin: 10px 0;float: right" >Publique
                                        <input type="checkbox" name="tel2public"  <?php
                                               if ($registeration['tel2public'] == 1) {
                                                   echo 'checked';
                                               }
                                               ?> />
                                        <span style="color: #4285F4">&#63;</span>
                                    </label>
                                    <?php //} ?>
                                </label>
                                <?php
                                echo $this->Form->control('tel2', [
                                    'placeholder'=>'+1234567895','class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'div' => false]);
                                ?>
                            </div> 
                        </div>
                        <div class="form-group">

                            <div class="col-md-6">
                                <label for="email" style="display:block">
                                    <h4 style="float:left;">Votre identifiant (adresse mail valide) <small class="text-danger">*</small></h4>
                                    <?php
                                    //if($registeration['status'] == 1){
                                    ?>
                                    <label title="Vous pouvez préciser quelles sont vos données que vous nous permettez de publier." data-toggle="tooltip" data-placement="bottom" style="margin: 10px 0;float: right;" >Publique
                                        <input type="checkbox" name="emailpublic"  <?php
                                               if ($registeration['emailpublic'] == 1) {
                                                   echo 'checked';
                                               }
                                               ?> />
                                        <span style="color: #4285F4">&#63;</span>
                                    </label>
                                    <?php //} ?>
                                </label>
                                <?php
                                echo $this->Form->control('email', [
                                    'class' => 'form-control', 'value' => $registeration['user']['email'], 'style' => 'height:40px;', 'label' => false, 'div' => false, 'readonly']);
                                ?>
                            </div>

                        </div>
                        <div class="form-group">

                            <div class="col-md-6">
                                <label for="site_web"><h4>URL du site</h4></label>
                                <?php
                                echo $this->Form->control('site_web', [
                                    'class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'div' => false]);
                                ?>

                            </div>
                        </div>

                        <div class="form-group">

                            <div class="col-md-6">
                                <label for="pwd"><h4>Mot de passe</h4></label>
                                <?php
                                echo $this->Form->control('pwd', [
                                    'type' => 'password', 'value' => '', 'autocomplete' => 'off', 'class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'div' => false]);
                                ?>

                            </div>
                        </div>

                        <div class="form-group">

                            <div class="col-md-6">
                                <label for="repass"><h4>Confirmez le mot de passe</h4></label>
                                <?php
                                echo $this->Form->control('repassword', [
                                    'class' => 'form-control', 'autocomplete' => 'off', 'type' => 'password', 'style' => 'height:40px;', 'label' => false, 'id' => 'repass', 'required' => 'required', 'disabled']);
                                ?> 

                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-xs-12">
                                <h4 class="formHeading" style="font-weight:bold">Détails de l'adresse</h4>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <label for="address1"><h4>Adresse 1 <small class="text-danger">*</small></h4></label>
                                <?php
                                echo $this->Form->control('address1', [
                                    'class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'div' => false, 'required' => 'required']);
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <label for="address2"><h4>Adresse 2</h4></label>
                                <?php
                                echo $this->Form->control('address2', [
                                    'class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'div' => false]);
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">
                                <label for="postalcode"><h4>Code Postal <small class="text-danger">*</small></h4></label>
                                <?php
                                echo $this->Form->control('postalcode', [
                                    'class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'div' => false, 'required' => 'required']);
                                ?> </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-4">
                                <label for="city"><h4>Ville <small class="text-danger">*</small></h4></label>
                                <?php
                                echo $this->Form->control('city', [
                                    'class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'div' => false, 'required' => 'required']);
                                ?>

                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-4">
                                <label for="country"><h4>
                                    Pays <small class="text-danger">*</small></h4></label>
                                <?php
                                echo $this->Form->control('pay',[
                                    'class'=>'form-control','style'=>'height:40px;','label'=>false,'div'=>false,'required' => 'required']);
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <h4 class="formHeading" style="font-weight:bold">Profession</h4>
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-6">
                                <label for="Profession"><h4>Profession <small class="text-danger">*</small></h4></label>
                                <?php
                                echo $this->Form->control('profession', [
                                    'class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'div' => false, 'required' => 'required', 'id'=>'Profession']);
                                ?></div>
                        </div>

                        <div class="form-group">

                            <div class="col-md-6">
                                <label for="enterprise"><h4>Entreprise</h4></label>
                                <?php
                                echo $this->Form->control('enterprise', [
                                    'class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'div' => false]);
                                ?>
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-xs-12 text-center">
                                <button class="btn btn-lg btn-success" type="submit" id='register' style="border-radius: 0;border-color: #147172;background:#147172;  margin-top:43px;height:50px;">  Enregistrer</button>


                            </div>
                        </div>
                        <div class="clearfix">&nbsp;</div>
                        </form>



                </div><!--/tab-pane-->

            </div><!--/tab-pane-->

        </div><!--/tab-content-->

    </div><!--/col-9-->
</div><!--/row-->

</div>
<script>
    $(document).ready(function () {
        $('#addmore').click(function () {
            var j = $('#add_radius input[name="name[]"]').length;
            $('#dynamic_field2').append('<tr id="rowRadius' + j + '"><td style="width:250px;background-color: white !important;"><input type="text" name="name[]"   placeholder="city"  class="custom_field"  required  style="height: 35px; width:200px;" data-original-title="This Field is required" title=""><span id="' + j + '" onclick="btn_remove1(' + j + ')" class="fa fa-trash-o"></span></td></tr>');


        });
        $('.btn_remove1').on('click', function () {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
        });

        setTimeout(function () {
            $('input[name=pwd]').val('');

        }, 100);

        $('input[name=pwd]').blur(function () {
            if ($(this).val()) {
                if ($(this).val().length < 6) {
                    if ($(this).siblings('small').length > 0) {
                        $(this).siblings('small').text('At least 6 characters');
                    } else {
                        $('<small style="color:red">At least 6 characters</small>').insertAfter($(this));
                    }
                    $('input[name=repassword]').prop('disabled', true);
                    return false;
                } else {
                    $('input[name=repassword]').prop('disabled', false);
                    $(this).siblings('small').text('');
                }
                $('input[name=repassword]').val('');
            }
        });
        $('input[name=repassword]').blur(function () {
            if ($(this).val()) {
                if ($('input[name="pwd"]').val() != $(this).val()) {
                    if ($(this).siblings('small').length > 0) {
                        $(this).siblings('small').text('Confirm Password does not match');
                    } else {
                        $('<small style="color:red">Confirm Password does not match</small>').insertAfter($(this));
                    }
                    return false;

                } else {
                    $(this).siblings('small').text('');
                }
            }
        });
        var readURL = function (input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    //                        console.log(e.target);
                    //                        $('.avatar').attr('style', 'background-image:url("' + e.target.result + '")');
                    $('.avatar').css("background-image", "url('" + e.target.result + "')");
                    ;
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
        $(".file-upload").on('change', function () {
            readURL(this);
        });
    });

    $("#UserAddForm").submit(function () {
        if ($('input[name="pwd"]').val()) {
            if ($('input[name="pwd"]').val().length < 6) {
                if ($('input[name="pwd"]').siblings('small').length > 0) {
                    $('input[name="pwd"]').siblings('small').text('At least 6 characters');
                } else {
                    $('<small style="color:red">At least 6 characters</small>').insertAfter($('input[name="pwd"]'));
                }
                return false;
            } else {
                if ($('input[name="repassword"]').val()) {
                    if ($('input[name="pwd"]').val() != $('input[name="repassword"]').val()) {
                        if ($('input[name="repassword"]').siblings('small').length > 0) {
                            $('input[name="repassword"]').siblings('small').text('Confirm Password does not match');
                        } else {
                            $('<small style="color:red">Confirm Password does not match</small>').insertAfter($('input[name="repassword"]'));
                        }
                        return false;

                    } else {
                        $('input[name="pwd"]').siblings('small').text('');
                    }
                    $('input[name="pwd"]').siblings('small').text('');
                } else {
                    if ($('input[name="repassword"]').siblings('small').length > 0) {
                        $('input[name="repassword"]').siblings('small').text('Please enter confirm password');
                    } else {
                        $('<small style="color:red">Please enter confirm password</small>').insertAfter($('input[name="repassword"]'));
                    }
                    return false;
                }
            }
        }


        /* var phone_pat = /^\(?(\+\d{3})\)?[-\. ]?(\d{3})[-\. ]?(\d{2})[-\. ]?(\d{2})$/;
        var phone = $("#tel1").val();
        //alert(phone);

        if ($('input[name=tel1]').val()) {
            //code for further validation
            if (!phone_pat.test(phone)) {

                $('input[name=tel1]').focus();
                $('label[for=tel1] i').addClass('fa-frown-o field_error').removeClass('fa-smile-o field_ok');
                alert("S'il vous plaît entrer le format de téléphone valide");
                return false;

                alert("S'il vous plaît entrer le format de téléphone valide");



            } else {

                $('label[for=tel1] i').addClass('fa-smile-o field_ok').removeClass('fa-frown-o field_error');
            }

        } else {
            $('input[name=tel1]').focus();
            $('label[for=tel1] i').addClass('fa-frown-o field_error').removeClass('fa-smile-o field_ok');
            alert("S'il vous plaît entrer le format de téléphone valide");
            return false;

            alert("S'il vous plaît entrer le format de téléphone valide");



        } */

        if (!$('input[name=first_name]').val()) {
            $('input[name=firstname]').focus();
            return false;
        }

        if (!$('input[name=lastname]').val()) {
            $('input[name=lastname]').focus();
            return false;
        }


        if ($('input[name=postalcode]').length) {
            if ($('input[name=postalcode]').val()) {
                //             alert($('input[name=zipcode]').val().length);
                if ($('input[name=postalcode]').val().length > 5) {
                    $('input[name=postalcode]').focus();
                    alert("S'il vous plaît entrer le code postal valide");
                    return false;

                    alert("S'il vous plaît entrer le code postal valide");



                } else {
                    return true;
                }
            } else {
                $('input[name=postalcode]').focus();
                alert("S'il vous plaît entrer le code postal valide");
                return false;

                alert("S'il vous plaît entrer le code postal valide");



            }
        }
    });
</script>