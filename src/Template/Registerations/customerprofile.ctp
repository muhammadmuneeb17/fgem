<?php
/**
 * @var \App\View\AppView $this
 */
//$status = $this->request->session()->read('Auth.User.status');

$array = explode('/', $_SERVER['REQUEST_URI']);
$tabId = end($array);
?>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="<?php echo $this->request->webroot ?>front/intlTelInput.css"  />
<style>
    div.avatar{
        height: 200px;
        width: 200px;
        background-position: center;
        background-size: cover;
        border-radius:50%;
    }
    .formHeading{
        padding: 10px 10px;
        border-bottom: 1px solid #ccc;
    }
</style>

<div class="container bootstrap snippet">
    <?= $this->Form->create($registeration, array('type' => 'file', 'id' => 'UserAddFormCollective')) ?>
    <?php if ($registeration->membertype->type == 2) { ?>
        <div class="row">
            <div class="col-sm-12"><!--left col-->

                <div class="tab-content">
                    <div class="tab-pane <?php
                    if ($tabId == 0) {
                        echo 'active';
                    }
                    ?>" id="home">

                        <div class="form-group">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <?php
                                    if (empty($registeration->image)) {
                                        $path = 'http://ssl.gstatic.com/accounts/ui/avatar_2x.png';
                                    } else {
                                        $path = $this->request->webroot . 'img/slider/thumbnail/' . $registeration->image;
                                    }
                                    ?>
                                    <div class="avatar" style="background-image: url(<?php echo $path; ?>);">

                                    </div> 
                                    <div class="clearfix"></div>
                                    <h6 class="text-primary">Mettre une photo augmente vos chances d'être choisi</h6>
                                    <label for="sectionImage" style="border: 1px solid #ccc;padding: 5px 7px;font-weight: normal;background-color: #E1E1E1;">Télécharger une image</label>
                                    <input type="file"  name='image' class="file-upload"  id="sectionImage" onchange="readURL(this)" style="display:none;cursor:pointer;">


                                </div>
                            </div>
                            <div class="col-md-9" style="padding-top:60px;">

                                <label for="is_public" style="width:100%;">
                                    <input type="radio" name="publicprofile" id="is_public" checked value="public" class="col-sm-1" <?php
                                    if ($registeration['publicprofile'] == 1 || $registeration['publicprofile'] == '') {
                                        echo 'checked';
                                    }
                                    ?> >
                                    <h4 class="text-success col-sm-11" style="padding:0px;margin:0px">Publier mon Profil dans l'Annuaire des médiateurs de la FGeM</h4>
                                </label>
                                <label for="is_private" style="width:100%;">
                                    <input type="radio" name="publicprofile" <?php
                                    if ($registeration['publicprofile'] == '0') {
                                        echo 'checked';
                                    }
                                    ?> value="private" id="is_private" class="col-sm-1">
                                    <h4 class="text-danger col-sm-11" style="padding:0px;margin:0px"> Ne pas publier mon profil dans l'Annuaire des médiateurs de la FGeM</h4>
                                </label>
                                <div class="clearfix">&nbsp;</div>
                                <div class="col-lg-12">
                                    <a href="javascript:" class="showpass-wrapper"><i class="fa fa-lock"></i> Changer le mot de passe</a>
                                </div>
                                <div class="password-wrapper" style="display:none;">
                                    <div class="col-md-6">
                                        <label for="pwd"><h4>Mot de passe</h4></label>
                                        <?php
                                        echo $this->Form->control('pwd', [
                                            'type' => 'password', 'value' => '', 'autocomplete' => 'off', 'class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'div' => false]);
                                        ?>

                                    </div>
                                    <div class="col-md-6">
                                        <label for="repass"><h4>Confirmez le mot de passe</h4></label>
                                        <?php
                                        echo $this->Form->control('repassword', [
                                            'class' => 'form-control', 'autocomplete' => 'off', 'type' => 'password', 'style' => 'height:40px;', 'label' => false, 'id' => 'repass', 'required' => 'required', 'disabled']);
                                        ?> 

                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <h4 class="formHeading" style="font-weight:bold;margin-top:10px !important;">Informations personnelles</h4>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label for="org_name"><h4>Nom de l'organisation<small class="text-danger">*</small></h4></label>
                                    <?php
                                    echo $this->Form->control('org_name', [
                                        'class' => 'form-control', 'id' => 'org_name', 'style' => 'height:40px;', 'label' => false, 'div' => false, 'required' => 'required']);
                                    ?>
                                </div>
                                <div class="col-md-6">
                                    <label for="persons_in_company"><h4>Nombre de membres ou d'employés<small class="text-danger">*</small></h4></label>
                                    <?php
                                    echo $this->Form->control('persons_in_company', [
                                        'class' => 'form-control', 'id' => 'persons_in_company', 'style' => 'height:40px;', 'label' => false, 'div' => false, 'required' => 'required']);
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <p style="font-size: 16px;margin-top:15px;font-weight: bold;">Prénom et Nom de la personne de contact </p>

                                </div>
                            </div>


                            <div class="form-group">

                                <div class="col-md-6">
                                    <label for="firstname"><h4>Prénom <small class="text-danger">*</small></h4></label>
                                    <?php
                                    echo $this->Form->control('first_name', [
                                        'class' => 'form-control', 'value' => $registeration['user']['first_name'], 'style' => 'height:40px;', 'label' => false, 'div' => false, 'required' => 'required']);
                                    ?></div>
                            </div>
                            <div class="form-group">

                                <div class="col-md-6">
                                    <label for="lastname"><h4>Nom de famille <small class="text-danger">*</small></h4></label>
                                    <?php
                                    echo $this->Form->control('lastname', [
                                        'class' => 'form-control', 'value' => $registeration['user']['last_name'], 'style' => 'height:40px;', 'label' => false, 'div' => false, 'required' => 'required']);
                                    ?>
                                    <?php
                                    echo $this->Form->control('header', [
                                        'class' => 'form-control', 'value' => 0, 'type' => 'hidden', 'style' => 'height:40px;', 'label' => false, 'div' => false, 'required' => 'required']);
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">

                                <div class="col-md-6">
                                    <label for="telephone1" style="display:block">
                                        <h4 style="float:left;">Téléphone 1 <small class="text-danger">*</small></h4>
                                        <?php
                                        //if($registeration['status'] == 1){
                                        ?>
                                        <label class="showPublicFields" title="Si vous désirez ne pas publier ce n°, vous devez décocher la case." data-toggle="tooltip" data-placement="bottom" style="margin: 10px 0;float: right;" >Publier
                                            <input type="checkbox" name="tel1public"  <?php
                                            if ($registeration['tel1public'] == 1) {
                                                echo 'checked';
                                            }
                                            ?>  />
                                            <span style="color: #4285F4">&#63;</span>
                                        </label>
                                        <?php //}   ?>
                                    </label>
                                    <?php
                                    echo $this->Form->control('tel1', [
                                        'class' => 'form-control tel', 'style' => 'height:40px;', 'label' => false, 'id' => 'tel1', 'div' => false, 'required' => 'required']);
                                    ?>

                                </div>

                            </div>

                            <div class="form-group">

                                <div class="col-md-6">
                                    <label for="telephone2" style="display:block">
                                        <h4 style="float:left;">Téléphone 2</h4>
                                        <?php
                                        //if($registeration['status'] == 1){
                                        ?>
                                        <label class="showPublicFields" title="Si vous désirez ne pas publier ce n°, vous devez décocher la case." data-toggle="tooltip" data-placement="bottom" style="margin: 10px 0;float: right" >Publier
                                            <input type="checkbox" name="tel2public"  <?php
                                            if ($registeration['tel2public'] == 1) {
                                                echo 'checked';
                                            }
                                            ?> />
                                            <span style="color: #4285F4">&#63;</span>
                                        </label>
                                        <?php //}   ?>
                                    </label>
                                    <?php
                                    echo $this->Form->control('tel2', [
                                        'class' => 'form-control tel', 'style' => 'height:40px;', 'label' => false, 'div' => false]);
                                    ?>
                                </div> 
                            </div>
                            <div class="form-group">

                                <div class="col-md-6">
                                    <label for="email" style="display:block">
                                        <h4 style="float:left;">Votre identifiant (adresse mail valide) <small class="text-danger">*</small></h4>
                                        <?php
                                        //if($registeration['status'] == 1){
                                        ?>
                                        <label class="showPublicFields" title="Si vous désirez ne pas publier ce n°, vous devez décocher la case." data-toggle="tooltip" data-placement="bottom" style="margin: 10px 0;float: right;" >Publier
                                            <input type="checkbox" name="emailpublic"  <?php
                                            if ($registeration['emailpublic'] == 1) {
                                                echo 'checked';
                                            }
                                            ?> />
                                            <span style="color: #4285F4">&#63;</span>
                                        </label>
                                        <?php //}   ?>
                                    </label>
                                    <?php
                                    echo $this->Form->control('email', [
                                        'class' => 'form-control', 'value' => $registeration['user']['email'], 'style' => 'height:40px;', 'label' => false, 'div' => false, 'readonly']);
                                    ?>
                                </div>
                                <div class="form-group">

                                    <div class="col-md-6">
                                        <label for="site_web" style="margin-bottom:0;"><h4>URL du site</h4></label>
                                        <?php
                                        echo $this->Form->control('site_web', [
                                            'class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'div' => false]);
                                        ?>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-xs-12">
                                    <h4 class="formHeading" style="font-weight:bold">Détails de l'adresse</h4>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label for="address1"><h4>Adresse 1 <small class="text-danger">*</small></h4></label>
                                    <?php
                                    echo $this->Form->control('address1', [
                                        'class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'div' => false, 'required' => 'required']);
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label for="address2"><h4>Adresse 2</h4></label>
                                    <?php
                                    echo $this->Form->control('address2', [
                                        'class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'div' => false]);
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                    <label for="postalcode"><h4>Code postal <small class="text-danger">*</small></h4></label>
                                    <?php
                                    echo $this->Form->control('postalcode', [
                                        'class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'div' => false, 'required' => 'required']);
                                    ?> </div>
                            </div>
                            <div class="form-group">

                                <div class="col-md-4">
                                    <label for="city"><h4>Ville <small class="text-danger">*</small></h4></label>
                                    <?php
                                    echo $this->Form->control('city', [
                                        'class' => 'form-control', 'style' => 'height:40px;text-transform: lowercase', 'label' => false, 'div' => false, 'required' => 'required']);
                                    ?>

                                </div>
                            </div>
                            <div class="form-group">

                                <div class="col-md-4">
                                    <label for="country"><h4>
                                            Pays <small class="text-danger">*</small></h4></label>
                                    <?php
                                    echo $this->Form->control('pay', [
                                        'class' => 'form-control', 'style' => 'height:40px;text-transform: lowercase', 'label' => false, 'div' => false, 'required', 'value' => strtolower($registeration->pay)]);
                                    ?>
                                </div>
                            </div>

                            <!--                            <div class="form-group">
                            
                                                            <div class="col-md-12">
                                                                <label for="contact_person"><h4>Personne de contact <small class="text-danger">*</small></h4></label>
                            <?php
//                                    echo $this->Form->control('contact_person', [
//                                        'class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'div' => false, 'required' => 'required', 'type' => 'email']);
                            ?></div>
                                                        </div>-->

                            <div class="form-group">

                                <div class="col-md-12">
                                    <label for="person_description"><h4>Description</h4></label>
                                    <?php
                                    echo $this->Form->control('person_description', [
                                        'class' => 'form-control', 'type' => 'textarea', 'label' => false, 'div' => false]);
                                    ?>
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="col-xs-12 text-center">

                                    <button class="btn btn-lg btn-success" type="submit" id='register' style="border-radius: 0;border-color: #18202E;background:#18202E;  margin-top:43px;">Enregistrer &raquo;</button>
                                </div>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            </form>



                        </div><!--/tab-pane-->

                    </div><!--/tab-pane-->

                </div><!--/tab-content-->

            </div><!--/col-9-->
        </div><!--/row-->

    <?php } else { ?>
        <div class="row">
            <div class="col-sm-12"><!--left col-->

                <div class="tab-content">
                    <div class="tab-pane <?php
                    if ($tabId == 0) {
                        echo 'active';
                    }
                    ?>" id="home">

                        <div class="form-group">

                            <div class="col-md-3">
                                <div class="form-group">
                                    <?php
                                    if (empty($registeration->image)) {
                                        $path = 'http://ssl.gstatic.com/accounts/ui/avatar_2x.png';
                                    } else {
                                        $path = $this->request->webroot . 'img/slider/thumbnail/' . $registeration->image;
                                    }
                                    ?>
                                    <div class="avatar" style="background-image: url(<?php echo $path; ?>);">

                                    </div> 
                                    <div class="clearfix"></div>
                                    <h6 class="text-primary">Mettre une photo augmente vos chances d'être choisi</h6>
                                    <label for="sectionImage" style="border: 1px solid #ccc;padding: 5px 7px;font-weight: normal;background-color: #E1E1E1;">Télécharger une image</label>
                                    <input type="file"  name='image' class="file-upload"  id="sectionImage" onchange="readURL(this)" style="display:none;cursor:pointer;">


                                </div>
                            </div>
                            <div class="col-md-9" style="padding-top:60px;">

                                <label for="is_public" style="width:100%;">
                                    <input type="radio" name="publicprofile" id="is_public" checked value="public" class="col-sm-1" <?php
                                    if ($registeration['publicprofile'] == 1 || $registeration['publicprofile'] == '') {
                                        echo 'checked';
                                    }
                                    ?> >
                                    <h4 class="text-success col-sm-11" style="padding:0px;margin:0px">Publier mon Profil dans l'Annuaire des médiateurs de la FGeM</h4>
                                </label>
                                <label for="is_private" style="width:100%;">
                                    <input type="radio" name="publicprofile" <?php
                                    if ($registeration['publicprofile'] == '0') {
                                        echo 'checked';
                                    }
                                    ?> value="private" id="is_private" class="col-sm-1">
                                    <h4 class="text-danger col-sm-11" style="padding:0px;margin:0px"> Ne pas publier mon profil dans l'Annuaire des médiateurs de la FGeM</h4>
                                </label>
                                <div class="clearfix">&nbsp;</div>
                                <div class="col-lg-12">
                                    <a href="javascript:" class="showpass-wrapper"><i class="fa fa-lock"></i> Changer le mot de passe</a>
                                </div>
                                <div class="password-wrapper" style="display:none;">
                                    <div class="col-md-6">
                                        <label for="pwd"><h4>Mot de passe</h4></label>
                                        <?php
                                        echo $this->Form->control('pwd', [
                                            'type' => 'password', 'value' => '', 'autocomplete' => 'off', 'class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'div' => false]);
                                        ?>

                                    </div>
                                    <div class="col-md-6">
                                        <label for="repass"><h4>Confirmez le mot de passe</h4></label>
                                        <?php
                                        echo $this->Form->control('repassword', [
                                            'class' => 'form-control', 'autocomplete' => 'off', 'type' => 'password', 'style' => 'height:40px;', 'label' => false, 'id' => 'repass', 'required' => 'required', 'disabled']);
                                        ?> 

                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <h4 class="formHeading" style="font-weight:bold;margin-top:10px !important;">Informations personnelles</h4>
                            <div class="form-group">

                                <div class="col-md-6">
                                    <label for="firstname"><h4>Prénom <small class="text-danger">*</small></h4></label>
                                    <?php
                                    echo $this->Form->control('first_name', [
                                        'class' => 'form-control', 'value' => $registeration['user']['first_name'], 'style' => 'height:40px;', 'label' => false, 'div' => false, 'required' => 'required']);
                                    ?></div>
                            </div>
                            <div class="form-group">

                                <div class="col-md-6">
                                    <label for="lastname"><h4>Nom de famille <small class="text-danger">*</small></h4></label>
                                    <?php
                                    echo $this->Form->control('lastname', [
                                        'class' => 'form-control', 'value' => $registeration['user']['last_name'], 'style' => 'height:40px;', 'label' => false, 'div' => false, 'required' => 'required']);
                                    ?>
                                    <?php
                                    echo $this->Form->control('header', [
                                        'class' => 'form-control', 'value' => 0, 'type' => 'hidden', 'style' => 'height:40px;', 'label' => false, 'div' => false, 'required' => 'required']);
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">

                                <div class="col-md-6">
                                    <label for="telephone1" style="display:block">
                                        <h4 style="float:left;">Téléphone 1 <small class="text-danger">*</small></h4>
                                        <?php
                                        //if($registeration['status'] == 1){
                                        ?>
                                        <label class="showPublicFields" title="Si vous désirez ne pas publier ce n°, vous devez décocher la case." data-toggle="tooltip" data-placement="bottom" style="margin: 10px 0;float: right;" >Publier
                                            <input type="checkbox" name="tel1public"  <?php
                                            if ($registeration['tel1public'] == 1) {
                                                echo 'checked';
                                            }
                                            ?>  />
                                            <span style="color: #4285F4">&#63;</span>
                                        </label>
                                        <?php //}     ?>
                                    </label>
                                    <?php
                                    echo $this->Form->control('tel1', [
                                        'class' => 'form-control tel', 'style' => 'height:40px;', 'label' => false, 'id' => 'tel1', 'div' => false, 'required' => 'required']);
                                    ?>

                                </div>

                            </div>

                            <div class="form-group">

                                <div class="col-md-6">
                                    <label for="telephone2" style="display:block">
                                        <h4 style="float:left;">Téléphone 2</h4>
                                        <?php
                                        //if($registeration['status'] == 1){
                                        ?>
                                        <label class="showPublicFields" title="Si vous désirez ne pas publier ce n°, vous devez décocher la case." data-toggle="tooltip" data-placement="bottom" style="margin: 10px 0;float: right" >Publier
                                            <input type="checkbox" name="tel2public"  <?php
                                            if ($registeration['tel2public'] == 1) {
                                                echo 'checked';
                                            }
                                            ?> />
                                            <span style="color: #4285F4">&#63;</span>
                                        </label>
                                        <?php //}     ?>
                                    </label>
                                    <?php
                                    echo $this->Form->control('tel2', [
                                        'class' => 'form-control tel', 'style' => 'height:40px;', 'label' => false, 'div' => false]);
                                    ?>
                                </div> 
                            </div>
                            <div class="form-group">

                                <div class="col-md-6">
                                    <label for="email" style="display:block">
                                        <h4 style="float:left;">Votre identifiant (adresse mail valide) <small class="text-danger">*</small></h4>
                                        <?php
                                        //if($registeration['status'] == 1){
                                        ?>
                                        <label class="showPublicFields" title="Si vous désirez ne pas publier ce n°, vous devez décocher la case." data-toggle="tooltip" data-placement="bottom" style="margin: 10px 0;float: right;" >Publier
                                            <input type="checkbox" name="emailpublic"  <?php
                                            if ($registeration['emailpublic'] == 1) {
                                                echo 'checked';
                                            }
                                            ?> />
                                            <span style="color: #4285F4">&#63;</span>
                                        </label>
                                        <?php //}     ?>
                                    </label>
                                    <?php
                                    echo $this->Form->control('email', [
                                        'class' => 'form-control', 'value' => $registeration['user']['email'], 'style' => 'height:40px;', 'label' => false, 'div' => false, 'readonly']);
                                    ?>
                                </div>

                            </div>
                            <div class="form-group">

                                <div class="col-md-6">
                                    <label for="site_web"><h4>URL du site</h4></label>
                                    <?php
                                    echo $this->Form->control('site_web', [
                                        'class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'div' => false]);
                                    ?>

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-xs-12">
                                    <h4 class="formHeading" style="font-weight:bold">Détails de l'adresse</h4>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label for="address1"><h4>Adresse 1 <small class="text-danger">*</small></h4></label>
                                    <?php
                                    echo $this->Form->control('address1', [
                                        'class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'div' => false, 'required' => 'required']);
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label for="address2"><h4>Adresse 2</h4></label>
                                    <?php
                                    echo $this->Form->control('address2', [
                                        'class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'div' => false]);
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                    <label for="postalcode"><h4>Code postal <small class="text-danger">*</small></h4></label>
                                    <?php
                                    echo $this->Form->control('postalcode', [
                                        'class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'div' => false, 'required' => 'required']);
                                    ?> </div>
                            </div>
                            <div class="form-group">

                                <div class="col-md-4">
                                    <label for="city"><h4>Ville <small class="text-danger">*</small></h4></label>
                                    <?php
                                    echo $this->Form->control('city', [
                                        'class' => 'form-control', 'style' => 'height:40px;text-transform: lowercase', 'label' => false, 'div' => false, 'required' => 'required']);
                                    ?>

                                </div>
                            </div>
                            <div class="form-group">

                                <div class="col-md-4">
                                    <label for="country"><h4>
                                            Pays <small class="text-danger">*</small></h4></label>
                                    <?php
                                    echo $this->Form->control('pay', [
                                        'class' => 'form-control', 'style' => 'height:40px;text-transform: lowercase', 'label' => false, 'div' => false, 'required' => 'required', 'value' => strtolower($registeration->pay)]);
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <h4 class="formHeading" style="font-weight:bold">Profession</h4>
                                </div>
                            </div>
                            <div class="form-group">

                                <div class="col-md-6">
                                    <label for="Profession"><h4>Profession <small class="text-danger">*</small></h4></label>
                                    <?php
                                    echo $this->Form->control('profession', [
                                        'class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'div' => false, 'required' => 'required', 'id' => 'Profession']);
                                    ?></div>
                            </div>

                            <div class="form-group">

                                <div class="col-md-6">
                                    <label for="enterprise"><h4>Entreprise</h4></label>
                                    <?php
                                    echo $this->Form->control('enterprise', [
                                        'class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'div' => false]);
                                    ?>
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="col-xs-12 text-center">
                                    <?php
                                    if ($status == 2) {
                                        ?>
                                        <button class="btn btn-lg btn-success" type="submit" id='register' style="border-radius: 0;border-color: #18202E;background:#18202E;  margin-top:43px;">Enregistrer</button>
                                        <?php
                                    } else {
                                        ?>
                                        <button class="btn btn-lg btn-success pull-right" type="submit" id='register' style="border-radius: 0;border-color: #18202E;background:#18202E;  margin-top:43px;">Suivant &raquo;</button>
                                        <?php
                                    }
                                    ?>



                                </div>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            </form>



                        </div><!--/tab-pane-->

                    </div><!--/tab-pane-->

                </div><!--/tab-content-->

            </div><!--/col-9-->
        </div><!--/row-->
    <?php } ?>

</div>
<script>
    $(document).ready(function () {
        $('input[name=publicprofile]').click(function () {
            if ($('input[name=publicprofile]:checked').val() == 'private') {
                $('.showPublicFields').hide();
            } else {
                $('.showPublicFields').show();
            }
        });

        $('#addmore').click(function () {
            var j = $('#add_radius input[name="name[]"]').length;
            $('#dynamic_field2').append('<tr id="rowRadius' + j + '"><td style="width:250px;background-color: white !important;"><input type="text" name="name[]"   placeholder="city"  class="custom_field"  required  style="height: 35px; width:200px;" data-original-title="This Field is required" title=""><span id="' + j + '" onclick="btn_remove1(' + j + ')" class="fa fa-trash-o"></span></td></tr>');


        });
        $('.btn_remove1').on('click', function () {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
        });

        setTimeout(function () {
            $('input[name=pwd]').val('');

        }, 100);

        $('input[name=pwd]').blur(function () {
            if ($(this).val()) {
                if ($(this).val().length < 6) {
                    if ($(this).siblings('small').length > 0) {
                        $(this).siblings('small').text('At least 6 characters');
                    } else {
                        $('<small style="color:red">At least 6 characters</small>').insertAfter($(this));
                    }
                    $('input[name=repassword]').prop('disabled', true);
                    return false;
                } else {
                    $('input[name=repassword]').prop('disabled', false);
                    $(this).siblings('small').text('');
                }
                $('input[name=repassword]').val('');
            }
        });
        $('input[name=repassword]').blur(function () {
            if ($(this).val()) {
                if ($('input[name="pwd"]').val() != $(this).val()) {
                    if ($(this).siblings('small').length > 0) {
                        $(this).siblings('small').text('Le mot de passe ne correspond pas');
                    } else {
                        $('<small style="color:red">Le mot de passe ne correspond pas</small>').insertAfter($(this));
                    }
                    return false;

                } else {
                    $(this).siblings('small').text('');
                }
            }
        });
        var readURL = function (input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    //                        console.log(e.target);
                    //                        $('.avatar').attr('style', 'background-image:url("' + e.target.result + '")');
                    $('.avatar').css("background-image", "url('" + e.target.result + "')");
                    ;
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
        $(".file-upload").on('change', function () {
            readURL(this);
        });
    });

    $("#UserAddForm").submit(function () {
        if ($('input[name="pwd"]').val()) {
            if ($('input[name="pwd"]').val().length < 6) {
                if ($('input[name="pwd"]').siblings('small').length > 0) {
                    $('input[name="pwd"]').siblings('small').text('At least 6 characters');
                } else {
                    $('<small style="color:red">At least 6 characters</small>').insertAfter($('input[name="pwd"]'));
                }
                return false;
            } else {
                if ($('input[name="repassword"]').val()) {
                    if ($('input[name="pwd"]').val() != $('input[name="repassword"]').val()) {
                        if ($('input[name="repassword"]').siblings('small').length > 0) {
                            $('input[name="repassword"]').siblings('small').text('Le mot de passe ne correspond pas');
                        } else {
                            $('<small style="color:red">Le mot de passe ne correspond pas</small>').insertAfter($('input[name="repassword"]'));
                        }
                        $('html, body').animate({
                            scrollTop: $("#repass").offset().top
                        }, 500);
                        return false;

                    } else {
                        $('input[name="pwd"]').siblings('small').text('');
                    }
                    $('input[name="pwd"]').siblings('small').text('');
                } else {
                    if ($('input[name="repassword"]').siblings('small').length > 0) {
                        $('input[name="repassword"]').siblings('small').text('Please enter confirm password');
                    } else {
                        $('<small style="color:red">Please enter confirm password</small>').insertAfter($('input[name="repassword"]'));
                    }
                    return false;
                }
            }
        }



        /*if(validate_phone1("#tel1") == "INVALID") {
         $('input[name=tel1]').focus();
         $('label[for=tel1] i').addClass('fa-frown-o field_error').removeClass('fa-smile-o field_ok');
         alert("S'il vous plaît entrer le format de téléphone valide");
         return false;
         } else {
         var phoneno = $('input[name=tel1]').val();
         var countrycode = validate_phone1("#tel1");
         $('input[name=tel1]').val("+"+countrycode+ " "+phoneno);
         }*/



        /* var phone_pat = /^\(?(\+\d{3})\)?[-\. ]?(\d{3})[-\. ]?(\d{2})[-\. ]?(\d{2})$/;
         var phone = $("#tel1").val();
         //alert(phone);
         
         if ($('input[name=tel1]').val()) {
         //code for further validation
         if (!phone_pat.test(phone)) {
         
         $('input[name=tel1]').focus();
         $('label[for=tel1] i').addClass('fa-frown-o field_error').removeClass('fa-smile-o field_ok');
         alert("S'il vous plaît entrer le format de téléphone valide");
         return false;
         
         alert("S'il vous plaît entrer le format de téléphone valide");
         
         
         
         } else {
         
         $('label[for=tel1] i').addClass('fa-smile-o field_ok').removeClass('fa-frown-o field_error');
         }
         
         } else {
         $('input[name=tel1]').focus();
         $('label[for=tel1] i').addClass('fa-frown-o field_error').removeClass('fa-smile-o field_ok');
         alert("S'il vous plaît entrer le format de téléphone valide");
         return false;
         
         alert("S'il vous plaît entrer le format de téléphone valide");
         
         
         
         } */

        if (!$('input[name=first_name]').val()) {
            $('input[name=firstname]').focus();
            return false;
        }

        if (!$('input[name=lastname]').val()) {
            $('input[name=lastname]').focus();
            return false;
        }


        if ($('input[name=postalcode]').length) {
            if ($('input[name=postalcode]').val()) {
                //             alert($('input[name=zipcode]').val().length);
                if ($('input[name=postalcode]').val().length > 5) {
                    $('input[name=postalcode]').focus();
                    alert("S'il vous plaît entrer le code postal valide");
                    return false;

                    alert("S'il vous plaît entrer le code postal valide");



                } else {
                    return true;
                }
            } else {
                $('input[name=postalcode]').focus();
                alert("S'il vous plaît entrer le code postal valide");
                return false;

                alert("S'il vous plaît entrer le code postal valide");



            }
        }
    });
    $("#UserAddFormCollective").submit(function () {
        if ($('input[name="pwd"]').val()) {
            if ($('input[name="pwd"]').val().length < 6) {
                if ($('input[name="pwd"]').siblings('small').length > 0) {
                    $('input[name="pwd"]').siblings('small').text('At least 6 characters');
                } else {
                    $('<small style="color:red">At least 6 characters</small>').insertAfter($('input[name="pwd"]'));
                }
                return false;
            } else {
                if ($('input[name="repassword"]').val()) {
                    if ($('input[name="pwd"]').val() != $('input[name="repassword"]').val()) {
                        if ($('input[name="repassword"]').siblings('small').length > 0) {
                            $('input[name="repassword"]').siblings('small').text('Le mot de passe ne correspond pas');
                        } else {
                            $('<small style="color:red">Le mot de passe ne correspond pas</small>').insertAfter($('input[name="repassword"]'));
                        }
                        $('html, body').animate({
                            scrollTop: $("#repass").offset().top
                        }, 500);
                        return false;

                    } else {
                        $('input[name="pwd"]').siblings('small').text('');
                    }
                    $('input[name="pwd"]').siblings('small').text('');
                } else {
                    if ($('input[name="repassword"]').siblings('small').length > 0) {
                        $('input[name="repassword"]').siblings('small').text('Please enter confirm password');
                    } else {
                        $('<small style="color:red">Please enter confirm password</small>').insertAfter($('input[name="repassword"]'));
                    }
                    return false;
                }
            }
        }
        if ($('input[name=org_name]').val() == '') {
            alert('Orgenization name is required');
            $('input[name=org_name]').focus();
            return false;
        }


        if ($('input[name=postalcode]').length) {
            if ($('input[name=postalcode]').val()) {
                //             alert($('input[name=zipcode]').val().length);
                if ($('input[name=postalcode]').val().length > 5) {
                    $('input[name=postalcode]').focus();
                    alert("S'il vous plaît entrer le code postal valide");
                    return false;

                    alert("S'il vous plaît entrer le code postal valide");



                } else {
                    return true;
                }
            } else {
                $('input[name=postalcode]').focus();
                alert("S'il vous plaît entrer le code postal valide");
                return false;

                alert("S'il vous plaît entrer le code postal valide");



            }
        }
    });
</script>
<script src="<?php echo $this->request->webroot ?>js/intlTelInput.js"></script>
<script>

    $(document).ready(function () {

        var phone_ok = 1;

        var input22 = document.querySelector("#tel1");

        var iti22 = window.intlTelInput(input22, {
            initialCountry: "ch",
            preferredCountries: ["ch", "fr"],
            formatOnDisplay: false,
            utilsScript: "<?php echo $this->request->webroot ?>js/utils.js"
        });
        input22.addEventListener("countrychange", function () {
            // do something with iti.getSelectedCountryData()
            $(input22).val('');
            $(input22).attr('value', '');
        });
        input22.addEventListener('blur', function () {

            if (input22.value.length > 0) {
                if (input22.value.trim()) {
                    if (iti22.isValidNumber()) {
                        // enable submit button
                        var countrycode = iti22.getSelectedCountryData()['dialCode'];
                        var phoneno = $(input22).val().replace("+" + countrycode + " ", "");

                        $(input22).val("+" + countrycode + " " + phoneno);
                        $("#register").removeAttr('disabled', 'disabled');
                        phone_ok = 1;

                    } else {
                        phone_ok = 0;
                        $(input22).focus();
                        $("#register").attr('disabled', 'disabled');
                        alert("S'il vous plaît entrer le format de téléphone valide1");
                    }
                }
            } else {
                $("#register").removeAttr('disabled', 'disabled');
            }
        });

        var input = document.querySelector("#tel2");

        var iti = window.intlTelInput(input, {
            initialCountry: "ch",
            preferredCountries: ["ch", "fr"],
            formatOnDisplay: false,
            utilsScript: "<?php echo $this->request->webroot ?>js/utils.js"
        });
        input.addEventListener("countrychange", function () {
            // do something with iti.getSelectedCountryData()
            $(input).val('');
            $(input).attr('value', '');
        });
        input.addEventListener('blur', function () {

            if (input.value.length > 0) {
                if (input.value.trim()) {
                    if (iti.isValidNumber() && phone_ok == 1) {
                        // enable submit button
                        var countrycode = iti.getSelectedCountryData()['dialCode'];
                        var phoneno = $(input).val().replace("+" + countrycode + " ", "");

                        $(input).val("+" + countrycode + " " + phoneno);
                        $("#register").removeAttr('disabled', 'disabled');

                    } else {
                        $(input).focus();
                        $("#register").attr('disabled', 'disabled');
                        alert("S'il vous plaît entrer le format de téléphone valide2");
                    }
                }
            } else {
                $("#register").removeAttr('disabled', 'disabled');
            }
        });



    });
    function validate_phone(phone_select = null) {
        var input = document.querySelector(phone_select), form = document.querySelector("form");

        var iti = window.intlTelInput(input, {
            initialCountry: "ch",
            preferredCountries: ["ch", "fr"],
            formatOnDisplay: false,
            utilsScript: "<?php echo $this->request->webroot ?>js/utils.js"
        });

        input.addEventListener("countrychange", function () {
            // do something with iti.getSelectedCountryData()
            $(phone_select).val('');
            $(phone_select).attr('value', '');
        });

        form.addEventListener("submit", function (e) {
            e.preventDefault();
            var num = iti.getNumber(),
                    valid = iti.isValidNumber();
            if (valid == false) {
                $(phone_select).focus();
                $(phone_select).addClass('fa-frown-o field_error').removeClass('fa-smile-o field_ok');
                alert("S'il vous plaît entrer le format de téléphone valide");
            } else {
                var countrycode = iti.getSelectedCountryData()['dialCode'];
                var phoneno = $(input).val().replace("+" + countrycode + " ", "");
                $(phone_select).val("+" + countrycode + " " + phoneno);
                $(this).trigger('submit');
            }
        }, false);

    }



//validate_phone('#tel1');

    $(".showpass-wrapper").on("click", function () {
        $(".password-wrapper").fadeToggle();
    });

    $("input[name=publicprofile]").on("change", function () {
        if ($(this).val() == 'public') {
            //alert(1);
            $("input[name=tel1public]").prop("checked", true);
            $("input[name=emailpublic]").prop("checked", true);
            if ($("input[name=tel2").val() != "") {
                $("input[name=tel2public]").prop("checked", true);
            }
        } else {
            $("input[name=tel1public]").prop("checked", false);
            $("input[name=emailpublic]").prop("checked", false);
            if ($("input[name=tel2").val() != "") {
                $("input[name=tel2public]").prop("checked", false);
            }
            // alert(0);
        }

    });

</script>