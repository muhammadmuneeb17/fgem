<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />-->
<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>-->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>-->
<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/css/select2.min.css"rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/js/select2.min.js"></script> -->

<link href="<?php echo $this->request->webroot ?>front/css/multiselect.css" rel="stylesheet" />

<script src="<?php echo $this->request->webroot ?>front/js/multiselect.js"></script>

<style>
    div.avatar{
        height: 200px;
        width: 200px;
        background-position: center;
        background-size: cover;
        border-radius:50%;
    }
    .formHeading{
        padding: 10px 10px;
        border-bottom: 1px solid #ccc;
    }
    .groupFields{
        display: inline-block;
        width: 100%;
        padding: 0 15px;
        margin-bottom: 10px;
    }
    .groupFields .col-xs-12{
        background-color: #f8f8f8;
    }
    .groupFields input{
        background-color: #fff;
    }
    .crossButton{
        margin: 5px 0px;
        font-size: 18px;
        cursor: pointer;
        color: gray;
    }
    /*CHECKBOXEXS DESIGN START*/
    .styled-input--diamond .styled-input-single {
        padding-left: 45px;
    }
    .styled-input--diamond label:before, .styled-input--diamond label:after {
        border-radius: 0;
    }
    .styled-input--diamond label:before {
        transform: rotate(45deg);
    }
    .styled-input--diamond input[type="radio"]:checked + label:after, .styled-input--diamond input[type="checkbox"]:checked + label:after {
        transform: rotate(45deg);
        opacity: 1;
    }
    .styled-input-single {
        position: relative;
        padding: 0px 0 5px 40px;
        text-align: left;
    }
    .styled-input-single label {
        cursor: pointer;
        font-size: 14px;
    }
    .styled-input-single label:before, .styled-input-single label:after {
        content: '';
        position: absolute;
        top: 50%;
        border-radius: 50%;
    }
    .styled-input-single label:before {
        left: 15px;
        width: 15px;
        height: 15px;
        margin: -15px 0 0;
        //background: #f7f7f7;
        //box-shadow: 0 0 1px grey;
        border: 1px solid #37b2b2 !important;
    }
    .styled-input-single label:after {
        left: 19px;
        width: 7px;
        height: 7px;
        margin: -11px 0 0;
        opacity: 0;
        background: #37b2b2;
        transform: translate3d(-40px, 0, 0) scale(0.5);
        transition: opacity 0.25s ease-in-out, transform 0.25s ease-in-out;
    }
    .styled-input-single input[type="radio"], .styled-input-single input[type="checkbox"] {
        position: absolute;
        top: 0;
        left: -9999px;
        visibility: hidden;
    }
    .styled-input-single input[type="radio"]:checked + label:after, .styled-input-single input[type="checkbox"]:checked + label:after {
        transform: translate3d(0, 0, 0);
        opacity: 1;
    }
    .styled-input--square label:before, .styled-input--square label:after {
        border-radius: 0;
    }
    .styled-input--rounded label:before {
        border-radius: 10px;
    }
    .styled-input--rounded label:after {
        border-radius: 6px;
    }
    .styled-input--diamond .styled-input-single {
        padding-left: 45px;
    }
    .styled-input--diamond label:before, .styled-input--diamond label:after {
        border-radius: 0;
    }
    .styled-input--diamond label:before {
        transform: rotate(45deg);
    }
    .styled-input--diamond input[type="radio"]:checked + label:after, .styled-input--diamond input[type="checkbox"]:checked + label:after {
        transform: rotate(45deg);
        opacity: 1;
    }
    /*CHECKBOXEXS DESIGN END*/
    .dynamic_cities{
        padding: 0;
        width:100%;
        margin-bottom: 0; 
    }
    .dynamic_cities li{
        padding:10px;
        border-bottom: 1px solid #eee;
    }
    .dynamic_cities li:hover{
        background-color: #eeeeee4d;
    }
    .search-result{
        background-color: white;
        min-height: 0;
        max-height: 200px;
        overflow: auto;
        /*border: none;*/
        border-top: none;
        position: absolute;
        z-index: 1 !important;
        width: 96%;
    }
    .searched_user p{
        margin:0;
    }
    .searched_user{
        cursor: pointer;
    }
    .search-result ul{
        margin: 0;
    }
    .ms-has-selections button {
        padding: 10px 15px;
    }

</style>

<?php
$supervisor = '';
foreach ($registeration->user->member_roles as $role):
    if ($role->role_id == 11) {
        $supervisor = 'checked';
    }
endforeach;
?>
<div class="services form">
    <?php
    $activeProfile = 'active';
    $activeProfileDiv = 'active in';
    $activeBusiness = '';
    $activeBusinessDiv = '';
    if (isset($this->request->params['pass'][1]) && $this->request->params['pass'][1] == 'business') {
        $activeProfile = '';
        $activeProfileDiv = '';
        $activeBusiness = 'active';
        $activeBusinessDiv = 'active in';
    }

    if ($registeration->membertype->type != 2) {
        ?>
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item <?= $activeProfile ?>">
                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Profile</a>
            </li>
            <li class="nav-item <?= $activeBusiness ?>">
                <a onclick="" class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Pratique de la médiation</a>
            </li>
        </ul>
    <?php } ?>
    <p style="padding:0 10px;color:red;margin:10px 0;">
        <span style="font-weight:bold;">ATTENTION !!!</span> Vous vous engagez formellement en remplissant votre espace membre à refléter l'exacte vérité quant aux dates et aux qualifications que vous indiquerez. La FGeM ne saurait être tenue responsable d'indications erronées publiées sur son site. 
    </p>
    <div class="tab-content" id="pills-tabContent">

        <div class="tab-pane fade <?= $activeProfileDiv; ?>" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
            <?php echo $this->Form->create($registeration, array('type' => 'file', 'class' => 'form-horizontal', 'id' => 'PageEditForm')); ?>

            <?php if ($registeration->membertype->type == 2) { ?>
                <fieldset>
                    <legend> Editez votre Profil
                        <a href="<?php echo $this->request->webroot ?>Registerations" style="font-size:14px;" class="pull-right text-info">[Précédent]</a>
                    </legend>
                    <div class="form-group required">
                        <label for="Member Type" class="col-md-2 control-label">Type de Membre</label>
                        <div class="col-md-10">
                            <?php echo $this->Form->input('membertype_id', array('div' => false, 'label' => false, 'class' => 'form-control', 'required', 'options' => $membertypes, 'disabled')); ?>
                        </div>
                    </div>  
                    <div class="form-group required">
                        <label for="Member Type" class="col-md-2 control-label notrequired">Enterprise</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('enterprise', array('div' => false, 'label' => false, 'class' => 'form-control')); ?>
                        </div>
                        <label for="Member Type" class="col-md-2 control-label">Site Web</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('site_web', array('div' => false, 'label' => false, 'class' => 'form-control')); ?>
                        </div>
                    </div>  
                    <div class="form-group required">
                        <label  class="col-md-2 control-label required" for="persons_in_company">Nombre de membres ou d'employés</label>
                        <div class="col-md-4">
                            <?php
                            echo $this->Form->control('persons_in_company', [
                                'class' => 'form-control', 'id' => 'persons_in_company', 'style' => 'height:40px;', 'label' => false, 'div' => false, 'required' => 'required']);
                            ?>
                        </div>  
                    </div>  
                    <p style="font-weight:bold;">Prénom et Nom de la personne de contact</p>
                    <div class="form-group required">
                        <label for="Member Type" class="col-md-2 control-label">Prénom</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('firstname', array('div' => false, 'label' => false, 'class' => 'form-control', 'required', 'value' => $registeration['user']['first_name'])); ?>
                        </div>   
                        <label for="Member Type" class="col-md-2 control-label">Nom de famille</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('lastname', array('div' => false, 'label' => false, 'class' => 'form-control', 'required', 'value' => $registeration['user']['last_name'])); ?>
                        </div> 
                    </div> 

                    <div class="form-group required">
                        <label for="Member Type" class="col-md-2 control-label">Adresse 1</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('address1', array('div' => false, 'label' => false, 'class' => 'form-control', 'required')); ?>
                        </div>   
                        <label for="Member Type" class="col-md-2 control-label notrequired">Adresse 2</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('address2', array('div' => false, 'label' => false, 'class' => 'form-control')); ?>
                        </div>
                    </div>  



                    <div class="form-group required">
                        <label for="Member Type" class="col-md-2 control-label">Ville</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('city', array('div' => false, 'label' => false, 'class' => 'form-control', 'required')); ?>
                        </div> 
                        <label for="Member Type" class="col-md-2 control-label">Code postal</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('postalcode', array('div' => false, 'label' => false, 'class' => 'form-control', 'required')); ?>
                        </div>

                    </div>



                    <div class="form-group required">
                        <label for="Member Type" class="col-md-2 control-label">Tel 1</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('tel1', array('div' => false, 'label' => false, 'class' => 'form-control', 'required')); ?>
                        </div> 
                        <label for="Member Type" class="col-md-2 control-label notrequired">Tel 2</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('tel2', array('div' => false, 'label' => false, 'class' => 'form-control')); ?>
                        </div> 
                    </div>

                    <div class="form-group required">
                        <label for="Member Type" class="col-md-2 control-label">Email</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('email', array('div' => false, 'label' => false, 'class' => 'form-control', 'required', 'readonly', 'value' => $registeration['user']['email'])); ?>
                        </div>   

                        <label for="password" class="col-md-2 control-label notrequired">Mot de passe</label>	
                        <div class="col-md-4">  
                            <?php echo $this->Form->control('password', array('type' => 'password', 'div' => false, 'label' => false, 'class' => 'form-control', 'style' => '', 'required' => false)); ?>
                        </div>
                    </div>

                    <div class="form-group required">
                        <label for="image" class="col-md-3 control-label">Image <br /><small class="text-danger">[Meilleure Taille: 1340px * 620px]</small></label>
                        <span class="btn btn-default btn-file">
                            Téléchager <input type="file" name="image" class="section-image" id="sectionImage" onchange="readURL(this)">
                        </span>
                        <br />
                        <?php if ($registeration->image) { ?>
                            <img id="show_section_uploaded_image"  src="<?php echo $this->request->webroot ?>img/slider/thumbnail/<?php echo $registeration->image ?>"  class="section-up-image" alt=""/>
                        <?php } else { ?>
                            <img id="show_section_uploaded_image"    class="section-up-image" alt=""/>    
                        <?php } ?>
                    </div>
                    <div class="form-group required" style="padding-left: 15px;">
                        <?php echo $this->Form->input('person_description', array('div' => false, 'label' => 'Description', 'class' => 'form-control')); ?>
                    </div>

                </fieldset>
            <?php } else { ?>
                <fieldset>
                    <legend>Editez votre Profil
                        <a href="<?php echo $this->request->webroot ?>Registerations" style="font-size:14px;" class="pull-right text-info">[Précédent]</a>
                    </legend>
                    <div class="form-group required">
                        <label for="Member Type" class="col-md-2 control-label">Type de Membre</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('membertype_id', array('div' => false, 'label' => false, 'class' => 'form-control', 'required', 'options' => $membertypes)); ?>
                        </div>
                        <label for="Member Type" class="col-md-2 control-label">Site Web</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('site_web', array('div' => false, 'label' => false, 'class' => 'form-control')); ?>
                        </div>
                    </div>  
                    <div class="form-group required">
                        <label for="Member Type" class="col-md-2 control-label">Prénom</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('firstname', array('div' => false, 'label' => false, 'class' => 'form-control', 'required', 'value' => $registeration['user']['first_name'])); ?>
                        </div>   
                        <label for="Member Type" class="col-md-2 control-label">Nom de famille</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('lastname', array('div' => false, 'label' => false, 'class' => 'form-control', 'required', 'value' => $registeration['user']['last_name'])); ?>
                        </div> 
                    </div> 
                    <div class="form-group required">

                        <label for="Member Type" class="col-md-2 control-label">Profession</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('profession', array('div' => false, 'label' => false, 'class' => 'form-control', 'required')); ?>
                        </div> 
                        <label for="Member Type" class="col-md-2 control-label notrequired">Enterprise</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('enterprise', array('div' => false, 'label' => false, 'class' => 'form-control')); ?>
                        </div>
                    </div>  

                    <div class="form-group required">
                        <label for="Member Type" class="col-md-2 control-label">Adresse 1</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('address1', array('div' => false, 'label' => false, 'class' => 'form-control', 'required')); ?>
                        </div>   
                        <label for="Member Type" class="col-md-2 control-label notrequired">Adresse 2</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('address2', array('div' => false, 'label' => false, 'class' => 'form-control')); ?>
                        </div>
                    </div>  



                    <div class="form-group required">
                        <label for="Member Type" class="col-md-2 control-label">Ville</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('city', array('div' => false, 'label' => false, 'class' => 'form-control', 'required', 'value' => strtolower($registeration->city))); ?>
                        </div>  
                        <label for="Member Type" class="col-md-2 control-label">Code postal</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('postalcode', array('div' => false, 'label' => false, 'class' => 'form-control', 'required')); ?>
                        </div>

                    </div>



                    <div class="form-group required">
                        <label for="Member Type" class="col-md-2 control-label">Tel 1</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('tel1', array('div' => false, 'label' => false, 'class' => 'form-control', 'required')); ?>
                        </div> 
                        <label for="Member Type" class="col-md-2 control-label notrequired">Tel 2</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('tel2', array('div' => false, 'label' => false, 'class' => 'form-control')); ?>
                        </div> 
                    </div>

                    <div class="form-group required">

                        <label for="Member Type" class="col-md-2 control-label">Email</label>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('email', array('div' => false, 'label' => false, 'class' => 'form-control', 'required', 'readonly', 'value' => $registeration['user']['email'])); ?>
                        </div>  

                        <label for="password" class="col-md-2 control-label notrequired">Mot de passe</label>	
                        <div class="col-md-4">  
                            <?php echo $this->Form->control('password', array('type' => 'password', 'div' => false, 'label' => false, 'class' => 'form-control', 'style' => '', 'required' => false)); ?>
                        </div>
                    </div>

                    <div class="form-group required" style="padding-left: 15px;">
                        <?php echo $this->Form->input('is_superviser', array('type' => 'checkbox', 'div' => false, 'label' => 'Est superviseur', $supervisor)); ?>

                    </div>
                    <div class="form-group required">
                        <label for="image" class="col-md-3 control-label">Image <br /><small class="text-danger">[Meilleure Taille: 1340px * 620px]</small></label>
                        <span class="btn btn-default btn-file">
                            Téléchager <input type="file" name="image" class="section-image" id="sectionImage" onchange="readURL(this)">
                        </span>
                        <br />
                        <?php if ($registeration->image) { ?>
                            <img id="show_section_uploaded_image"  src="<?php echo $this->request->webroot ?>img/slider/thumbnail/<?php echo $registeration->image ?>"  class="section-up-image" alt=""/>
                        <?php } else { ?>
                            <img id="show_section_uploaded_image"    class="section-up-image" alt=""/>    
                        <?php } ?>
                    </div>


                </fieldset>
            <?php } ?>
            <div class="form-group">
                <div class="col-md-11 text-center">
                    <?php
                    echo $this->Form->input('Enregistrer', array('type' => 'submit', 'div' => false, 'label' => false, 'class' => 'btn btn-primary btn-lg'));
                    echo $this->Form->end();
                    ?>
                </div>
            </div>
        </div>
        <?php
        if ($registeration->membertype->type != 2) {
            ?>
            <div class="tab-pane fade <?= $activeBusinessDiv; ?>" id="pills-profile" role="tabpanel" aria-labelledby="pills-contact-tab">
                <div class="bootstrap snippet">
                    <?= $this->Form->create('', array('url' => '/Registerations/saveBusinessInfo/' . $registeration->id, 'id' => 'businessForm', 'type' => 'file')) ?>

                    <div class="row">

                        <div class="col-sm-12" style="min-height: 300px;">

                            <div class="form-group">
                                <h4 class="formHeading" style="font-weight:bold">INFORMATIONS SUR LE MÉDIATEUR</h4>
                            </div><!--/tab-pane-->

                            <div class="clearfix">&nbsp;</div>
                            <div class="form-group">

                                <div class="col-xs-12">
                                    <label for="mediator"><h4>Êtes-vous un médiateur<small class="text-danger">*</small></h4></label>
                                    <?php
                                    $mediator_questionVal = '';
                                    $presentation_textVal = '';
                                    $accredsArray = array();
                                    $medArray = array();
                                    $lanArray = array();
                                    $asVal = 0;
                                    $accept_info = 0;

                                    if ($registrations['ombudsman']) {
                                        $presentation_textVal = $registrations['ombudsman']['presentation_text'];
                                        $mediator_questionVal = $registrations['ombudsman']['mediator_question'];
                                        $mediator_questionVal = $registrations['ombudsman']['mediator_question'];
                                        $asVal = ($registrations['ombudsman']['assermentation'] != null) ? $registrations['ombudsman']['assermentation'] : 0;
                                        $accept_info = ($registrations['ombudsman']['accept_info']) ? 1 : 0;


                                        if ($registrations['ombudsman']['ombudsmanaccreds']) {
                                            foreach ($registrations['ombudsman']['ombudsmanaccreds'] as $acreed) {
                                                array_push($accredsArray, $acreed['accreditation_id']);
                                            }
                                        }
                                        if ($registrations['ombudsman']['ombudsmanlevels']) {
                                            foreach ($registrations['ombudsman']['ombudsmanlevels'] as $lang) {
                                                array_push($lanArray, $lang['language_id']);
                                            }
                                        }
                                        if ($registrations['ombudsman']['ombudsmanmeds']) {
                                            foreach ($registrations['ombudsman']['ombudsmanmeds'] as $meds) {
                                                array_push($medArray, $meds['medtype_id']);
                                            }
                                        }
                                    }
                                    $args = array('Oui' => 'Oui', 'Non' => 'Non');
                                    echo $this->Form->control('mediator_question', [
                                        'class' => 'form-control', 'options' => $args, 'value' => $mediator_questionVal, 'default' => '', 'style' => 'height:40px;', 'label' => false, 'div' => false, 'required']);
                                    ?>

                                </div>

                            </div>

                            <div class="form-group mediationTypes">

                                <div class="col-xs-12">
                                    <label for=""><h4>Types de médiation<small class="text-danger">*</small></h4></label>
                                    <?php foreach ($medtype as $medtype): ?>
                                        <?php
                                        $medCheck = '';
                                        if (in_array($medtype['id'], $medArray)) {
                                            $medCheck = 'checked';
                                        }
                                        //echo $this->Form->checkbox('ombudsmanmeds.mediat[]', ['value' => $medtype['id'], $medCheck,
                                        // 'label' => false, 'div' => false, 'hiddenField' => false]);
                                        //echo " " . $medtype['names'];
                                        ?>
                                        <div class="single-col">
                                            <div class="styled-input-container styled-input--diamond" style="float:left">
                                                <div class="styled-input-single">
                                                    <input value="<?php echo $medtype['id']; ?>" type="checkbox" name="ombudsmanmeds[mediat][]" id="mediation<?php echo $medtype['id']; ?>" <?php echo $medCheck; ?>/>
                                                    <label for="mediation<?php echo $medtype['id']; ?>"><?php echo $medtype['names']; ?></label>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?> 
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">

                                    <div class="col-xs-12">
                                        <label for="mediation"><h4>Langues<small class="text-danger">*</small></h4></label>
                                        <select class="form-control" id="multipleLanguages" name="ombudsmanlevels.mediation[]" multiple="multiple" style="width:100%" required>
                                            <?php foreach ($languages as $k => $l) {
                                                ?>
                                                <option value="<?= $k ?>" <?= in_array($k, $lanArray) ? 'selected' : ''; ?> ><?= $l ?></option>    
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div id="myLanguages">
                                    <?php
                                    if (isset($registrations['ombudsman']['ombudsmanlevels'])) {

                                        foreach ($registrations['ombudsman']['ombudsmanlevels'] as $k => $l) {
                                            ?>
                                            <div class="form-group">
                                                <div class="col-xs-12 col-md-4" style="padding:0 50px;">
                                                    <label for=""><h4>Niveau <?= $languages[$l['language_id']] ?><small class="text-danger">*</small></h4></label>
                                                    <select name="ombudsmanlevels.language_level[]" class="form-control">
                                                        <option value="basic" <?= ($l['language_level'] == 'basic') ? 'selected' : '' ?> >Basique</option>
                                                        <option value="conversational" <?= ($l['language_level'] == 'conversational') ? 'selected' : '' ?>>Moyen</option>
                                                        <option value="fluent" <?= ($l['language_level'] == 'fluent') ? 'selected' : '' ?>>Courant</option>
                                                    </select>

                                                </div>
                                            </div>

                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <label for=""><h4>Assermentation<small class="text-danger">*</small></h4></label>
                                        <?php
//                    debug($asVal);
                                        $args = array('1' => 'Oui', '0' => 'Non');
                                        echo $this->Form->control('assermentation', [
                                            'class' => 'form-control', 'options' => $args, 'default' => '', 'style' => 'height:40px;', 'label' => false, 'div' => false, 'value' => $asVal, 'required']);
                                        ?>
                                        <br/>
                                    </div>
                                </div>

                                <?php
                                $staric = '<small class="text-danger">*</small>';
                                $isRequired = 'required';
                                if ($asVal == 0) {
                                    $staric = '';
                                    $isRequired = '';
                                }
                                if (isset($registrations['ombudsman']['ombudsmanswears']) && !empty($registrations['ombudsman']['ombudsmanswears'])) {
                                    foreach ($registrations['ombudsman']['ombudsmanswears'] as $oK => $owears):
                                        ?>
                                        <div class="groupFields assermentation-wrapper" id="fieldPlace<?php echo $oK; ?>">
                                            <div class="col-xs-12">
                                                <span onclick="closeDiv(<?php echo $oK; ?>)" class="pull-right crossButton">&times;</span>
                                                <label for="first_name"><h4>Lieu d'Assermentation <?php echo $staric; ?></h4></label>
                                                <?php
                                                echo $this->Form->control('place[]', [
                                                    'class' => 'form-control', 'default' => '', 'style' => 'height:40px;', 'label' => false, 'div' => false, 'value' => $owears['place'], $isRequired]);
                                                ?>
                                            </div>
                                            <div class="col-xs-12">
                                                <label for=""><h4>Date d'Assermentation (jj.mm.aaaa - 28.02.1999)</h4></label>
                                                <?php
                                                $args = array('1' => 'Oui', '0' => 'Non');
                                                echo $this->Form->control('date_of_auth[]', [
                                                    'class' => 'form-control datepicker', 'style' => 'height:40px;', 'label' => false, 'type' => 'text', 'div' => false, 'value' => ($owears['date_of_auth']) ? date('m/d/Y', strtotime($owears['date_of_auth'])) : '']);
                                                ?>
                                                <br/>
                                            </div>
                                        </div>
                                        <?php
                                    endforeach;
                                }else {
                                    ?>
                                    <div class="groupFields assermentation-wrapper" id="fieldPlace0" style="display:none">
                                        <div class="col-xs-12">
                                            <label for="first_name"><h4>Lieu d'Assermentation</h4></label>
                                            <?php
                                            echo $this->Form->control('place[]', [
                                                'class' => 'form-control', 'default' => '', 'style' => 'height:40px;', 'label' => false, 'div' => false]);
                                            ?>
                                        </div>
                                        <div class="col-xs-12">
                                            <label for=""><h4>Date d'Assermentation (jj.mm.aaaa - 28.02.1999)</h4></label>
                                            <?php
                                            $args = array('1' => 'Oui', '0' => 'Non');
                                            echo $this->Form->control('date_of_auth[]', [
                                                'class' => 'form-control datepicker', 'default' => '', 'style' => 'height:40px;', 'label' => false, 'type' => 'text', 'div' => false]);
                                            ?>
                                            <br/>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="col-xs-12" id="addMoreAssermentation" style="text-align:right;display:none;">
                                    <span class="btn btn-success btn-xs btn-asser" onclick='addField()'><i class="fa fa-plus" aria-hidden="true"></i> Autre assermentation</span>
                                </div>

                                <div class="col-xs-12" style="padding-top:15px;display:none;" id="acceptInfoDiv">
                                    <div class="single-col">
                                        <div class="styled-input-container styled-input--diamond" style="float:left">
                                            <div class="styled-input-single">
                                                <input name="accept_info" type="checkbox" value="1" type="checkbox" id="accept_info" <?= ($accept_info == 1) ? 'checked' : ''; ?> />
                                                <label for="accept_info" style="width: 95%;">
                                                    <!--Je suis disponible pour assurer des permanences dans le cadre de la Permanence Info-Médiation, PIM. (Seules les membres à jour de leur cotisation et ayant publié leur profile dans l'Annuaire de la FGeM seront pris en compte pour la PIM)-->
                                                    Je suis disponible pour assurer des permanences et je m'inscris au tableau des médiateurs permanents de la Permanence Info Médiation (PIM).
                                                    Je demande à ce que le règlement de la PIM me parvienne pour lecture, puis signature et retour à la FGeM.
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">

                                <div class="col-xs-12">
                                    <label for=""><h4>Texte de présentation<small class="text-danger">*</small></h4></label>
                                    <?php
                                    echo $this->Form->control('presentation_text', [
                                        'label' => false, 'type' => 'textarea', 'div' => false, 'value' => $presentation_textVal, 'required', 'class' => 'form-control']);
                                    ?>

                                </div>
                            </div>
                            <div class="mediationTypes">
                                <?php
                                if (isset($registrations['ombudsman']['ombudsmanformations']) && !empty($registrations['ombudsman']['ombudsmanformations'])) {
                                    foreach ($registrations['ombudsman']['ombudsmanformations'] as $oF => $oFormation):
                                        ?>
                                        <div class="groupFields" id="fieldFormation<?= $oF; ?>" style="margin-top:10px">
                                            <div class="col-xs-12">
                                                <span onclick="deleteFormation(<?= $oF; ?>,<?= $oFormation['id']; ?>)" class="pull-right crossButton"><i class="fa fa-trash-o"></i></span>
                                                <label><h4>Formation</h4></label>
                                                <input type="text" id="formationId<?= 'edit' . $oFormation['id']; ?>" dynamic_val="<?= 'edit' . $oFormation['id']; ?>" name="ombudsmanformations.formation[]" placeholder="Formation" value="<?php echo $oFormation['formation']; ?>" class="form-control name_list searching"  style="height:40px;" autocomplete="off">
                                            </div>
                                            <div class="col-xs-12">
                                                <div class="search-result" id="search-result<?= 'edit' . $oFormation['id']; ?>">
                                                </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <label for="">
                                                    <h4>Date de certification (jj.mm.aaaa - 28.02.1999)</h4>
                                                </label>
                                                <input type="text" name="ombudsmanformations.date[]" placeholder="Date de certification " class="datepicker form-control" value="<?php echo date('m/d/Y', strtotime($oFormation['certificate_date'])); ?>" style="height: 40px;">
                                            </div>                
                                            <div class="col-xs-12">
                                                <label for="image" class="col-md-12" style="padding:0">Copie de la certification</label>
                                                <input type="file" name="image[]" class="section-image" id="sectionImage" onchange="readURL(this)">
                                                <br/>
                                            </div>
                                            <div class="col-xs-12">
                                                <img width="100px" src="<?php echo $this->request->webroot . 'img/formations/' . $oFormation['copy_upload'] ?>" alt="Image Not Available">
                                                <input type="hidden" name="ombudsmanformations.image_id[]" value="<?= $oFormation['id']; ?>">
                                                <input type="hidden" name="ombudsmanformations.image_name[]" value="<?= $oFormation['copy_upload']; ?>">
                                                <br/>
                                                <br/>
                                            </div>
                                        </div>
                                        <?php
                                    endforeach;
                                }else {
                                    ?>
                                    <div class="groupFields" id="fieldFormation0" style="margin-top:10px">
                                        <div class="col-xs-12">
                                            <label><h4>Formation</h4></label>
                                            <input type="text" id="formationId" name="ombudsmanformations.formation[]" placeholder="Formation" class="form-control name_list searching"  style="height:40px;" autocomplete="off">
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="search-result" id="search-result">
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <label for="">
                                                <h4>Date de certification (jj.mm.aaaa - 28.02.1999)</h4>
                                            </label>
                                            <input type="text" name="ombudsmanformations.date[]" placeholder="Date de certification " class="datepicker form-control"   style="height: 40px;">
                                        </div>                
                                        <div class="col-xs-12">
                                            <label for="image" class="col-md-12" style="padding:0">Copie de la certification</label>
                                            <input type="file" name="image[]" class="section-image" id="sectionImage" onchange="readURL(this)">
                                            <br/>
                                        </div>
                                    </div>
                                <?php } ?>
                                <br />
                                <div class="col-xs-12" style="text-align:right;">
                                    <span class="btn btn-success btn-xs" id="addFormationButton" onclick='addFormation()'><i class="fa fa-plus" aria-hidden="true"></i> Autres Formations</span>
                                </div>

                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <label for=""><h4>ACCRÉDITATION<small class="text-danger">*</small></h4></label>
                                        <?php foreach ($acc as $key => $acc): ?>
                                            <?php
                                            $accreCheck = '';
                                            if (in_array($acc['id'], $accredsArray)) {
                                                $accreCheck = 'checked';
                                            }
                                            //echo $this->Form->checkbox('accre[]', ['value' => $acc['id'], 'label' => false, 'id' => '//a11y-issue-' . $key, 'div' => false, 'hiddenField' => false, $accreCheck]);
                                            //echo '&nbsp;' . $acc['name'] . '&nbsp;';
                                            ?>
                                            <div class="single-col">
                                                <div class="styled-input-container styled-input--diamond" style="float:left">
                                                    <div class="styled-input-single">
                                                        <input value="<?php echo $acc['id']; ?>" type="checkbox" name="accre[]" id="acc<?php echo $acc['id']; ?>" <?php echo $accreCheck; ?> />
                                                        <label for="acc<?php echo $acc['id']; ?>"><?php echo $acc['name']; ?></label>
                                                    </div>

                                                </div>
                                            </div>
                                        <?php endforeach; ?> 
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12 text-center">
                                    <button class="btn btn-lg btn-success pull-right" type="submit" id='register' style="border-radius: 0;border-color: #18202E;background:#18202E;  margin-top:43px;"> Enregistrer</button>

                                </div>

                            </div>

                        </div>
                    </div><!--/row-->


                    <?= $this->Form->end(); ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
</div>
<script>
    $(function () {
        if ($('select[name=mediator_question]').val() == 'Non') {
            $('input[name="ombudsmanmeds[mediat][]"]').prop('checked', false);
            $('#multipleLanguages').attr('required', false);
            $('.mediationTypes').hide();
        } else {
            $('#multipleLanguages').attr('required', true);
            $('.mediationTypes').show();
        }
        $('select[name=mediator_question]').change(function () {
            if ($('select[name=mediator_question]').val() == 'Non') {
                $('input[name="ombudsmanmeds[mediat][]"]').prop('checked', false);
                $('#multipleLanguages').attr('required', false);
                $('.mediationTypes').hide();
            } else {
                $('#multipleLanguages').attr('required', true);
                $('.mediationTypes').show();
            }
        });

//        addMoreAssermentation
//        fieldPlace0
        if ($('select[name=assermentation]').val() == 0) {
            $('#addMoreAssermentation').hide();
            $('#fieldPlace0').hide();
        } else {
            $('#addMoreAssermentation').show();
            $('#fieldPlace0').show();
        }
        $('select[name=assermentation]').change(function () {
            if ($('select[name=assermentation]').val() == 0) {
                $('#addMoreAssermentation').hide();
                $('#fieldPlace0').hide();
            } else {
                $('#addMoreAssermentation').show();
                $('#fieldPlace0').show();
            }

        });
    });
    function put_value(value, dynamic_val) {
//        alert(dynamic_val);
        if (typeof dynamic_val == 'undefined') {
            dynamic_val = '';
        }
        $('#formationId' + dynamic_val).val(value);
        $('#search-result' + dynamic_val).hide();
        $('#search-result' + dynamic_val).css({'border': 'none'});

    }
    function closeDiv(id) {
        $('#fieldPlace' + id).remove();
    }
    function closeFormation(id) {
        $('#fieldFormation' + id).remove();
    }
    function deleteFormation(row_id, id) {
        if (confirm('Are you sure you want to delete')) {
            row_id = row_id;
            $.ajax({
                url: "<?php echo $this->request->webroot . 'Registerations/deleteFormation' ?>",
                type: "GET",
                data: 'id=' + id,
                success: function (data)
                {
                    data = JSON.parse(data);
                    if (data.flag == 1) {
                        $('#fieldFormation' + row_id).remove();
                        alert(data.message);
                    } else {
                        alert(data.message);
                    }
                }
            });
        }
    }
    function addField() {
        var dynamicVal = $('div[id^=fieldPlace]').length;

        var insertAfterId = dynamicVal - 1;
        var html = '<div class="groupFields assermentation-wrapper" id="fieldPlace' + dynamicVal + '">\n\
<div class="col-xs-12">\n\
<span onclick="closeDiv(' + dynamicVal + ')" class="pull-right crossButton">&times;</span>\n\
<label for="first_name"><h4>Lieu d\'Assermentation<small class="text-danger">*</small></h4></label>\n\
<div class="input text"><input name="place[]" class="form-control" style="height:40px;" id="place" value="" type="text" required></div></div>\n\
<div class="col-xs-12">\n\
<label for=""><h4>Date d\'Assermentation (jj.mm.aaaa - 28.02.1999)<small class="text-danger">*</small></h4></label>\n\
<div class="input text"><input name="date_of_auth[]" class="form-control datepicker" style="height:40px;" value="" type="text" required><br/></div></div>\n\
    </div>';
        $(html).insertAfter($('#fieldPlace' + insertAfterId));

    }
    function addFormation() {
        var dynamicVal = $('div[id^=fieldFormation]').length;
        var insertAfterId = dynamicVal - 1;
        var html = '<div class="groupFields" id="fieldFormation' + dynamicVal + '" style="margin-top:10px">\n\
<div class="col-xs-12">\n\
<span onclick="closeFormation(' + dynamicVal + ')" class="pull-right crossButton">&times;</span>\n\
<label><h4>Formation <small class="text-danger">*</small></h4></label>\n\
<input name="ombudsmanformations.formation[]" id="formationId' + dynamicVal + '" placeholder="Formation" dynamic_val="' + dynamicVal + '" class="form-control name_list searching" style="height:40px;" type="text" required autocomplete="off">\n\
    </div><div class="col-xs-12">\n\
                            <div class="search-result" id="search-result' + dynamicVal + '">\n\
                            </div>\n\
                        </div>\n\
<div class="col-xs-12">\n\
<label for="">\n\
<h4>Date de certification (jj.mm.aaaa - 28.02.1999)<small class="text-danger">*</small></h4>\n\
    </label>\n\
<input name="ombudsmanformations.date[]" placeholder="Date de certification" class="datepicker form-control" style="height: 40px;" type="text" required>\n\
    </div><div class="col-xs-12">\n\
<label for="image" class="col-md-12" style="padding:0">Copie de la certification <small class="text-danger">*</small></label>\n\
<input name="image[]" class="section-image" id="sectionImage" onchange="readURL(this)" type="file" required>\n\
<br>\n\
    </div>\n\
    </div>';
        $(html).insertAfter($('#fieldFormation' + insertAfterId));

    }
    var formation = $('#fieldFormation0 input[name="ombudsmanformations.formation[]"]').val();
    if (formation != '') {

        $('#fieldFormation0 input[name="ombudsmanformations.date[]"]').prop('required', true);
        $('#fieldFormation0 input[name="ombudsmanformations.date[]"]').siblings('label').children('h4').append('<small class="text-danger">*</small>');
//        $('#fieldFormation0 input[name="image[]"]').prop('required', true);
        $('#fieldFormation0 input[name="image[]"]').siblings('label').append('<small class="text-danger">*</small>');

    } else {
        $('#fieldFormation0 input[name="ombudsmanformations.date[]"]').prop('required', false);
        $('#fieldFormation0 input[name="ombudsmanformations.date[]"]').siblings('label').children('h4').children('small').remove();
//        $('#fieldFormation0 input[name="image[]"]').prop('required', false);
        $('#fieldFormation0 input[name="image[]"]').siblings('label').children('small').remove();

    }
    $(document).ready(function () {
        $(document).on('focusout', '.searching', function () {
            var searching = $(this);
            var dynamic_val = searching.attr('dynamic_val');
            if (typeof searching.attr('dynamic_val') == 'undefined') {
                dynamic_val = '';
            }
            setTimeout(function () {
                $('#search-result' + dynamic_val).hide();

            }, 300);

        });
        $(document).on('keyup', '.searching', function () {
            var value = $(this).val();
            var dynamic_val = '';
            if (typeof $(this).attr('dynamic_val') != 'undefined') {
                dynamic_val = $(this).attr('dynamic_val');
            }

            $('#search-result' + dynamic_val).show();
            $('#search-result' + dynamic_val).css({'border': '1px solid #ccc'});
            if (value != '')
            {
                $.ajax({
                    type: 'GET',
                    url: '<?php echo $this->request->webroot ?>Users/formation',
                    data: 'field=' + value,
                    contentType: 'json',
                    success: function (data)
                    {
                        var data = JSON.parse(data);
                        if ($.isEmptyObject(data))
                        {
                            var divsearch = $('#search-result' + dynamic_val).empty();
                            var app = "";
                            $('#search-result' + dynamic_val).append(app);
                        } else
                        {
                            $('#search-result' + dynamic_val).empty();
                            var icon;
                            var loginWithSocial;
                            var path = '';
                            var app = "<ul class='dynamic_cities'>";
                            $.each(data, function (key, value) {
                                icon = value.profile_image;
                                loginWithSocial = value.loginwithsocial;

                                $('#search-result' + dynamic_val).empty();
                                app += "<li class='searched_user'>"
                                        + "<p onclick='put_value(" + '"' + value + '"' + "," + '"' + dynamic_val + '"' + ")'>" + value + "</p>"
                                        + "</li>";
                            });
                            app += "</ul>";

                            $('#search-result' + dynamic_val).append(app);
                        }
                    }
                });
                //end
            } else {
                $('#search-result' + dynamic_val).hide();
            }
        });





        $('#fieldFormation0 input[name="ombudsmanformations.formation[]"]').change(function () {
            var formation = $('#fieldFormation0 input[name="ombudsmanformations.formation[]"]').val();
            if (formation != '') {
                var check = $('#fieldFormation0 input[name="ombudsmanformations.date[]"]').siblings('label').children('h4').children('small').length;
                if (check == 0) {
                    $('#fieldFormation0 input[name="ombudsmanformations.date[]"]').prop('required', true);
                    $('#fieldFormation0 input[name="ombudsmanformations.date[]"]').siblings('label').children('h4').append('<small class="text-danger">*</small>');
//                $('#fieldFormation0 input[name="image[]"]').prop('required', true);
                    $('#fieldFormation0 input[name="image[]"]').siblings('label').append('<small class="text-danger">*</small>');
                    $('#fieldFormation0 input[name="image[]"]').prop('required', true);
                }
            } else {
                $('#fieldFormation0 input[name="ombudsmanformations.date[]"]').prop('required', false);
                $('#fieldFormation0 input[name="ombudsmanformations.date[]"]').siblings('label').children('h4').children('small').remove();
//                $('#fieldFormation0 input[name="image[]"]').prop('required', false);
                $('#fieldFormation0 input[name="image[]"]').siblings('label').children('small').remove();
                $('#fieldFormation0 input[name="image[]"]').prop('required', false);

            }
        });



        $('#businessForm').submit(function () {
            var mediation = $('input[name="ombudsmanmeds[mediat][]"]:checked').val();

            if ($('select[name=mediator_question]').val() == 'Oui') {
                if (typeof mediation == 'undefined') {
                    alert('MÃ©diation is required');
                    $('input[name="ombudsmanmeds[mediat][]"]').focus();
                    return false;
                }
                var accre = $('input[name="accre[]"]:checked').val();
                if (typeof accre == 'undefined') {
                    alert('ACCRÃ‰DITATION is required');
                    $('input[name="accre[]"]').focus();
                    return false;
                }
                var formation = $('#fieldFormation0 input[name="ombudsmanformations.formation[]"]').val();
                if (formation != '') {
                    if ($('#fieldFormation0 input[name="ombudsmanformations.date[]"]').val() == '') {
                        alert('Date de certification is required');
                        $('#fieldFormation0 input[name="ombudsmanformations.date[]"]').focus();
                        return false;

                    }

                }
            }


        });
        /*jQuery('.js-example-basic-multiple').select2();*/

        $(document).on('change', '#multipleLanguages', function () {
            var values = $(this).val();
            if (values == null) {
                $('#myLanguages').html('');
                return false;
            }
            $.ajax({
                url: "<?php echo $this->request->webroot . 'Registerations/languagae_experience' ?>",
                type: "GET",
                data: 'values=' + values,
                success: function (data)
                {
                    data = JSON.parse(data);
                    $('#myLanguages').html(data);
                }
            });
        });
        if ($('#fieldPlace0 input[name="place[]"]').val() == '') {
            $('#acceptInfoDiv').hide();
            $('#acceptInfoDiv input[type=checkbox]').prop('checked', false);
        } else {
            $('#acceptInfoDiv').show();

        }

        $('#fieldPlace0 input[name="place[]"]').change(function () {
//        alert();
            if ($('#fieldPlace0 input[name="place[]"]').val() == '') {
                $('#acceptInfoDiv').hide();
                $('#acceptInfoDiv input[type=checkbox]').prop('checked', false);

            } else {
                $('#acceptInfoDiv').show();

            }
        });
        $('#assermentation').change(function () {
            if ($(this).val() == 1) {
                //                $('#fieldPlace0 label h4').append('<small class="text-danger">*</small>');
                $('#fieldPlace0 input[name="place[]"]').parent().siblings('label').children('h4').append('<small class="text-danger">*</small>');
                $('#fieldPlace0 input[name="place[]"]').prop('required', true);

                $('#fieldPlace0 input[name="date_of_auth[]"]').parent().siblings('label').children('h4').append('<small class="text-danger">*</small>');
                $('#fieldPlace0 input[name="date_of_auth[]"]').prop('required', true);
            } else {
                $('#fieldPlace0 label h4').children('small').remove();
                $('#fieldPlace0 input[name="place[]"]').prop('required', false);
                
                $('#fieldPlace0 label h4').children('small').remove();
                $('#fieldPlace0 input[name="date_of_auth[]"]').prop('required', false);

            }
        });

        $(document).on('click', '.btn_remove', function () {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
        });

        setTimeout(function () {
            $('input[name=pwd]').val('');

        }, 100);

        $('input[name=pwd]').blur(function () {
            if ($(this).val()) {
                if ($(this).val().length < 6) {
                    if ($(this).siblings('small').length > 0) {
                        $(this).siblings('small').text('At least 6 characters');
                    } else {
                        $('<small style="color:red">At least 6 characters</small>').insertAfter($(this));
                    }
                    $('input[name=repassword]').prop('disabled', true);
                    return false;
                } else {
                    $('input[name=repassword]').prop('disabled', false);
                    $(this).siblings('small').text('');
                }
                $('input[name=repassword]').val('');
            }
        });
        $('input[name=repassword]').blur(function () {
            if ($(this).val()) {
                if ($('input[name="pwd"]').val() != $(this).val()) {
                    if ($(this).siblings('small').length > 0) {
                        $(this).siblings('small').text('Confirm Password does not match');
                    } else {
                        $('<small style="color:red">Confirm Password does not match</small>').insertAfter($(this));
                    }
                    return false;

                } else {
                    $(this).siblings('small').text('');
                }
            }
        });
        var readURL = function (input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    //                        console.log(e.target);
                    //                        $('.avatar').attr('style', 'background-image:url("' + e.target.result + '")');
                    $('.avatar').css("background-image", "url('" + e.target.result + "')");
                    ;
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
        $(".file-upload").on('change', function () {
            readURL(this);
        });
    });

    $("#UserAddForm").submit(function () {
        if ($('input[name="pwd"]').val()) {
            if ($('input[name="pwd"]').val().length < 6) {
                if ($('input[name="pwd"]').siblings('small').length > 0) {
                    $('input[name="pwd"]').siblings('small').text('At least 6 characters');
                } else {
                    $('<small style="color:red">At least 6 characters</small>').insertAfter($('input[name="pwd"]'));
                }
                return false;
            } else {
                if ($('input[name="repassword"]').val()) {
                    if ($('input[name="pwd"]').val() != $('input[name="repassword"]').val()) {
                        if ($('input[name="repassword"]').siblings('small').length > 0) {
                            $('input[name="repassword"]').siblings('small').text('Confirm Password does not match');
                        } else {
                            $('<small style="color:red">Confirm Password does not match</small>').insertAfter($('input[name="repassword"]'));
                        }
                        return false;

                    } else {
                        $('input[name="pwd"]').siblings('small').text('');
                    }
                    $('input[name="pwd"]').siblings('small').text('');
                } else {
                    if ($('input[name="repassword"]').siblings('small').length > 0) {
                        $('input[name="repassword"]').siblings('small').text('Please enter confirm password');
                    } else {
                        $('<small style="color:red">Please enter confirm password</small>').insertAfter($('input[name="repassword"]'));
                    }
                    return false;
                }
            }
        }
        /*
         
         var phone_pat = /^\(?(\+\d{3})\)?[-\. ]?(\d{3})[-\. ]?(\d{2})[-\. ]?(\d{2})$/;
         var phone = $("#tel1").val();
         //alert(phone);
         
         if ($('input[name=tel1]').val()) {
         //code for further validation
         if (!phone_pat.test(phone)) {
         
         $('input[name=tel1]').focus();
         $('label[for=tel1] i').addClass('fa-frown-o field_error').removeClass('fa-smile-o field_ok');
         alert("S'il vous plaÃ®t entrer le format de tÃ©lÃ©phone valide");
         return false;
         
         alert("S'il vous plaÃ®t entrer le format de tÃ©lÃ©phone valide");
         
         
         
         } else {
         
         $('label[for=tel1] i').addClass('fa-smile-o field_ok').removeClass('fa-frown-o field_error');
         }
         
         } else {
         $('input[name=tel1]').focus();
         $('label[for=tel1] i').addClass('fa-frown-o field_error').removeClass('fa-smile-o field_ok');
         alert("S'il vous plaÃ®t entrer le format de tÃ©lÃ©phone valide");
         return false;
         
         alert("S'il vous plaÃ®t entrer le format de tÃ©lÃ©phone valide");
         
         
         
         }
         */
        if (!$('input[name=first_name]').val()) {
            $('input[name=firstname]').focus();
            return false;
        }

        if (!$('input[name=lastname]').val()) {
            $('input[name=lastname]').focus();
            return false;
        }


        if ($('input[name=postalcode]').length) {
            if ($('input[name=postalcode]').val()) {
                //             alert($('input[name=zipcode]').val().length);
                if ($('input[name=postalcode]').val().length > 5) {
                    $('input[name=postalcode]').focus();
                    alert("S'il vous plaÃ®t entrer le code postal valide");
                    return false;

                    alert("S'il vous plaÃ®t entrer le code postal valide");



                } else {
                    return true;
                }
            } else {
                $('input[name=postalcode]').focus();
                alert("S'il vous plaÃ®t entrer le code postal valide");
                return false;

                alert("S'il vous plaÃ®t entrer le code postal valide");



            }
        }
    });
    $('#multipleLanguages').multiselect({
        columns: 1,
        placeholder: '',
        search: true
    });

    $("#assermentation").on("change", function () {
        if ($(this).val() == 0) {
            $('.assermentation-wrapper input').attr("value", "");
            $('.assermentation-wrapper').hide();
            $('.btn-asser').hide();
            $('#acceptInfoDiv input[type=checkbox]').prop('checked', false);
            $('#acceptInfoDiv').hide();

        } else {
            $('.assermentation-wrapper').show();
            $('.btn-asser').show();
            $('#acceptInfoDiv input[type=checkbox]').prop('checked', false);
            $('#acceptInfoDiv').show();
        }
    });
</script>
