<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Registeration $registeration
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Registeration'), ['action' => 'edit', $registeration->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Registeration'), ['action' => 'delete', $registeration->id], ['confirm' => __('Are you sure you want to delete # {0}?', $registeration->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Registerations'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Registeration'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Membertypes'), ['controller' => 'Membertypes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Membertype'), ['controller' => 'Membertypes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="registerations view large-9 medium-8 columns content">
    <h3><?= h($registeration->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Membertype') ?></th>
            <td><?= $registeration->has('membertype') ? $this->Html->link($registeration->membertype->name, ['controller' => 'Membertypes', 'action' => 'view', $registeration->membertype->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Profession') ?></th>
            <td><?= h($registeration->profession) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Username') ?></th>
            <td><?= h($registeration->username) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Image') ?></th>
            <td><?= h($registeration->image) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($registeration->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Address1') ?></th>
            <td><?= h($registeration->address1) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Address2') ?></th>
            <td><?= h($registeration->address2) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('City') ?></th>
            <td><?= h($registeration->city) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Enterprise') ?></th>
            <td><?= h($registeration->enterprise) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tel1') ?></th>
            <td><?= h($registeration->tel1) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tel2') ?></th>
            <td><?= h($registeration->tel2) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($registeration->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Site Web') ?></th>
            <td><?= h($registeration->site_web) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($registeration->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Postalcode') ?></th>
            <td><?= $this->Number->format($registeration->postalcode) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pay') ?></th>
            <td><?= $this->Number->format($registeration->pay) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($registeration->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $registeration->status ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
