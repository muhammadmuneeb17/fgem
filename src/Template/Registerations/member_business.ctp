<?php
/**
 * @var \App\View\AppView $this
 */
$array = explode('/', $_SERVER['REQUEST_URI']);
$tabId = end($array);
//debug($user);
$fName = $this->request->session()->read('Auth.User.first_name');
$lName = $this->request->session()->read('Auth.User.last_name');
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/js/select2.min.js"></script>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/css/select2.min.css" rel="stylesheet" />
<?php // exit; ?>
<style>
    div.avatar{
        height: 200px;
        width: 200px;
        background-position: center;
        background-size: cover;
        border-radius:50%;
    }
    .formHeading{
        padding: 10px 10px;
        border-bottom: 1px solid #ccc;
    }
    .groupFields{
        display: inline-block;
        width: 100%;
        padding: 0 15px;
        margin-bottom: 10px;
    }
    .groupFields .col-xs-12{
        background-color: #f8f8f8;
    }
    .groupFields input{
        background-color: #fff;
    }
    .crossButton{
        margin: 5px 0px;
        font-size: 18px;
        cursor: pointer;
        color: gray;
    }
</style>

<div class="container bootstrap snippet">
    <?= $this->Form->create('', array('type' => 'file')) ?>

    <div class="row">

        <div class="col-md-12" style="min-height: 300px;">

            <div class="form-group">
                <h4 class="formHeading" style="font-weight:bold">INFORMATIONS SUR LE MÉDIATEUR</h4>
            </div><!--/tab-pane-->

            <div class="clearfix">&nbsp;</div>
            <div class="form-group">

                <div class="col-xs-12">
                    <label for="mediator"><h4>Êtes-vous un médiateur<small class="text-danger">*</small></h4></label>
                    <?php
    $mediator_questionVal = '';
    $presentation_textVal = '';
    $accredsArray = array();
    $medArray = array();
    $lanArray = array();
    $asVal = 0;

    if ($registrations['ombudsman']) {
        $presentation_textVal = $registrations['ombudsman']['presentation_text'];
        $mediator_questionVal = $registrations['ombudsman']['mediator_question'];
        $mediator_questionVal = $registrations['ombudsman']['mediator_question'];
        $asVal = $registrations['ombudsman']['assermentation'];

        if ($registrations['ombudsman']['ombudsmanaccreds']) {
            foreach ($registrations['ombudsman']['ombudsmanaccreds'] as $acreed) {
                array_push($accredsArray, $acreed['accreditation_id']);
            }
        }
        if ($registrations['ombudsman']['ombudsmanlevels']) {
            foreach ($registrations['ombudsman']['ombudsmanlevels'] as $lang) {
                array_push($lanArray, $lang['language_id']);
            }
        }
        if ($registrations['ombudsman']['ombudsmanmeds']) {
            foreach ($registrations['ombudsman']['ombudsmanmeds'] as $meds) {
                array_push($medArray, $meds['medtype_id']);
            }
        }
    }
    $args = array('Oui' => 'Oui', 'Non' => 'Non');
    echo $this->Form->control('mediator_question', [
        'class' => 'form-control', 'options' => $args, 'value' => $mediator_questionVal, 'default' => '', 'style' => 'height:40px;', 'label' => false, 'div' => false, 'required']);
                    ?>

                </div>

            </div>

            <div class="form-group">

                <div class="col-xs-12">
                    <label for="médiation"><h4>Types de médiation<small class="text-danger">*</small></h4></label>
                    <?php foreach ($medtype as $medtype): ?>
                    <?php
                    $medCheck = '';
                    if (in_array($medtype['id'], $medArray)) {
                        $medCheck = 'checked';
                    }
                    echo $this->Form->checkbox('ombudsmanmeds.mediat[]', ['value' => $medtype['id'], $medCheck,
                                                                          'label' => false, 'div' => false, 'hiddenField' => false]);
                    echo $medtype['names'];
                    ?>

                    <?php endforeach; ?> 
                </div>
            </div>
            <div class="form-group">

                <div class="col-xs-12">
                    <label for="mediation"><h4>Langues<small class="text-danger">*</small></h4></label>
                    <select class="js-example-basic-multiple" id="multipleLanguages" name="ombudsmanlevels.mediation[]" multiple="multiple" style="width:100%" required>
                        <?php foreach ($languages as $k => $l) {
                        ?>
                        <option value="<?= $k ?>" <?= in_array($k, $lanArray) ? 'selected' : ''; ?> ><?= $l ?></option>    
                        <?php
}
                        ?>
                    </select>
                </div>
            </div>
            <div id="myLanguages">
                <?php
                if (isset($registrations['ombudsman']['ombudsmanlevels'])) {

                    foreach ($registrations['ombudsman']['ombudsmanlevels'] as $k => $l) {
                ?>
                <div class="form-group">
                    <div class="col-xs-12 col-md-4" style="padding:0 50px;">
                        <label for="médiation"><h4>Niveau <?= $languages[$l['language_id']] ?><small class="text-danger">*</small></h4></label>
                        <select name="ombudsmanlevels.language_level[]" class="form-control">
                            <option value="basic" <?= ($l['language_level'] == 'basic') ? 'selected' : '' ?> >Basique</option>
                            <option value="conversational" <?= ($l['language_level'] == 'conversational') ? 'selected' : '' ?>>Moyen</option>
                            <option value="fluent" <?= ($l['language_level'] == 'fluent') ? 'selected' : '' ?>>Courant</option>
                        </select>

                    </div>
                </div>

                <?php
                    }
                }
                ?>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <label for="médiation"><h4>Assermentation<small class="text-danger">*</small></h4></label>
                    <?php
                    $args = array('1' => 'Oui', '0' => 'Non');
                    echo $this->Form->control('assermentation', [
                        'class' => 'form-control', 'options' => $args, 'default' => '', 'style' => 'height:40px;', 'label' => false, 'div' => false, 'value' => $asVal, 'required']);
                    ?>

                </div>
            </div>

            <?php
            //            debug($asVal);
            $staric = '<small class="text-danger">*</small>';
            $isRequired = 'required';
            if ($asVal == 0) {
                $staric = '';
                $isRequired = '';
            }
            if (isset($registrations['ombudsman']['ombudsmanswears']) && !empty($registrations['ombudsman']['ombudsmanswears'])) {
                foreach ($registrations['ombudsman']['ombudsmanswears'] as $oK => $owears):
            ?>
            <div class="groupFields" id="fieldPlace<?php echo $oK; ?>">
                <div class="col-xs-12">
                    <span onclick="closeDiv(<?php echo $oK; ?>)" class="pull-right crossButton">×</span>
                    <label for="first_name"><h4>Lieu d'Assermentation <?php echo $staric; ?></h4></label>
                    <?php
                echo $this->Form->control('place[]', [
                    'class' => 'form-control', 'default' => '', 'style' => 'height:40px;', 'label' => false, 'div' => false, 'value' => $owears['place'], $isRequired]);
                    ?>
                </div>
                <div class="col-xs-12">
                    <label for="médiation"><h4>Date d'Assermentation (jj.mm.aaaa - 28.02.1999) <?php echo $staric; ?></h4></label>
                    <?php
                $args = array('1' => 'Oui', '0' => 'Non');
                echo $this->Form->control('date_of_auth[]', [
                    'class' => 'form-control datepicker', 'style' => 'height:40px;', 'label' => false, 'type' => 'text', 'div' => false, 'value' => date('m/d/Y', strtotime($owears['date_of_auth'])), $isRequired]);
                    ?>
                </div>
            </div>
            <?php
                endforeach;
            }else {
            ?>
            <div class="groupFields" id="fieldPlace0">
                <div class="col-xs-12">
                    <label for="first_name"><h4>Lieu d'Assermentation</h4></label>
                    <?php
                echo $this->Form->control('place[]', [
                    'class' => 'form-control', 'default' => '', 'style' => 'height:40px;', 'label' => false, 'div' => false]);
                    ?>
                </div>
                <div class="col-xs-12">
                    <label for="médiation"><h4>Date d'Assermentation (jj.mm.aaaa - 28.02.1999)</h4></label>
                    <?php
                $args = array('1' => 'Oui', '0' => 'Non');
                echo $this->Form->control('date_of_auth[]', [
                    'class' => 'form-control datepicker', 'default' => '', 'style' => 'height:40px;', 'label' => false, 'type' => 'text', 'div' => false]);
                    ?>
                </div>
            </div>
            <?php } ?>
            <div class="col-xs-12" style="text-align:right;">
                <span class="btn btn-success btn-xs" onclick='addField()'><i class="fa fa-plus" aria-hidden="true"></i> Other swearing</span>
            </div>

            <div class="form-group">

                <div class="col-xs-12">
                    <label for="médiation"><h4>Texte de présentation<small class="text-danger">*</small></h4></label>
                    <?php
                    echo $this->Form->control('presentation_text', [
                        'label' => false, 'type' => 'textarea', 'div' => false, 'value' => $presentation_textVal, 'required', 'class' => 'form-control']);
                    ?>

                </div>
            </div>
            <?php
            if (isset($registrations['ombudsman']['ombudsmanformations']) && !empty($registrations['ombudsman']['ombudsmanformations'])) {
                foreach ($registrations['ombudsman']['ombudsmanformations'] as $oF => $oFormation):
            ?>
            <div class="groupFields" id="fieldFormation<?= $oF; ?>" style="margin-top:10px">
                <div class="col-xs-12">
                    <span onclick="deleteFormation(<?= $oF; ?>,<?= $oFormation['id']; ?>)" class="pull-right crossButton"><i class="fa fa-trash-o"></i></span>
                    <label><h4>Training<small class="text-danger">*</small></h4></label>
                    <input type="text" name="ombudsmanformations.formation[]" placeholder="Formation" value="<?php echo $oFormation['formation']; ?>" class="form-control name_list"  style="height:40px;" required="required">
                </div>
                <div class="col-xs-12">
                    <label for="médiation">
                        <h4>Certification date (dd.mm.yyy - 28.02.1999) <small class="text-danger">*</small></h4>
                    </label>
                    <input type="text" name="ombudsmanformations.date[]" placeholder="Date de certification " class="datepicker form-control" value="<?php echo date('m/d/Y', strtotime($oFormation['certificate_date'])); ?>" style="height: 40px;" required>
                </div>                
                <div class="col-xs-12">
                    <label for="image" class="col-md-12" style="padding:0">Copie de la certification</label>
                    <input type="file" name="image[]" class="section-image" id="sectionImage" onchange="readURL(this)">
                    <br/>
                </div>
                <div class="col-xs-12">
                    <img width="100px" src="<?php echo $this->request->webroot . 'img/formations/' . $oFormation['copy_upload'] ?>">
                    <input type="hidden" name="ombudsmanformations.image_id[]" value="<?= $oFormation['id']; ?>">
                    <input type="hidden" name="ombudsmanformations.image_name[]" value="<?= $oFormation['copy_upload']; ?>">
                    <br/>
                </div>
            </div>
            <?php
                endforeach;
            }else {
            ?>
            <div class="groupFields" id="fieldFormation0" style="margin-top:10px">
                <div class="col-xs-12">
                    <label><h4>Training<small class="text-danger">*</small></h4></label>
                    <input type="text" name="ombudsmanformations.formation[]" placeholder="Formation" class="form-control name_list"  style="height:40px;" required>
                </div>
                <div class="col-xs-12">
                    <label for="médiation">
                        <h4>Certification date (dd.mm.yyy - 28.02.1999) <small class="text-danger">*</small></h4>
                    </label>
                    <input type="text" name="ombudsmanformations.date[]" placeholder="Date de certification " class="datepicker form-control"   style="height: 40px;" required>
                </div>                
                <div class="col-xs-12">
                    <label for="image" class="col-md-12" style="padding:0">Copie de la certification</label>
                    <input type="file" name="image[]" class="section-image" id="sectionImage" onchange="readURL(this)" required>
                    <br/>
                </div>
            </div>
            <?php } ?>
            <div class="col-xs-12" style="text-align:right;">
                <span class="btn btn-success btn-xs" id="addFormationButton" onclick='addFormation()'><i class="fa fa-plus" aria-hidden="true"></i> Autres Formations</span>
            </div>

            <div class="form-group">
                <div class="col-xs-12">
                    <label for="médiation"><h4>ACCRÉDITATION<small class="text-danger">*</small></h4></label>
                    <?php foreach ($acc as $key => $acc): ?>
                    <?php
                    $accreCheck = '';
                    if (in_array($acc['id'], $accredsArray)) {
                        $accreCheck = 'checked';
                    }
                    echo $this->Form->checkbox('accre[]', ['value' => $acc['id'],
                                                           'label' => false, 'id' => 'a11y-issue-' . $key, 'div' => false, 'hiddenField' => false, $accreCheck]);
                    echo '&nbsp;<label for="a11y-issue-' . $key . '">' . $acc['name'] . '</label>&nbsp;';
                    ?>

                    <?php endforeach; ?> 
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12 text-center">
                    <button class="btn btn-lg btn-success" type="submit" id='register' style="border-radius: 0;border-color: #147172;background:#147172;  margin-top:43px;height:50px;">  Enregistrer</button>


                </div>

            </div>
            <!--            <div class="form-group">
<div class="col-xs-12">
<button class="btn btn-lg btn-success btn-block" type="submit" id='register' style="border-radius: 0;border-color: #147172;background:#147172;  margin-top:43px;height:50px;"> Enregistrer</button>
<br>
<br>
<br>
</div>
</div>-->
        </div>
    </div><!--/row-->


</div>
<?= $this->Form->end(); ?>
</div>
<script>
    function closeDiv(id) {
        $('#fieldPlace' + id).remove();
    }
    function closeFormation(id) {
        $('#fieldFormation' + id).remove();
    }
    function deleteFormation(row_id, id) {
        if (confirm('Are you sure you want to delete')) {
            row_id = row_id;
            $.ajax({
                url: "<?php echo $this->request->webroot . 'Registerations/deleteFormation' ?>",
                type: "GET",
                data: 'id=' + id,
                success: function (data)
                {
                    data = JSON.parse(data);
                    if (data.flag == 1) {
                        $('#fieldFormation' + row_id).remove();
                        alert(data.message);
                    } else {
                        alert(data.message);
                    }
                }
            });
        }
    }
    function addField() {
        var dynamicVal = $('div[id^=fieldPlace]').length;

        var insertAfterId = dynamicVal - 1;
        var html = '<div class="groupFields" id="fieldPlace' + dynamicVal + '">\n\
<div class="col-xs-12">\n\
<span onclick="closeDiv(' + dynamicVal + ')" class="pull-right crossButton">×</span>\n\
<label for="first_name"><h4>Lieu d\'Assermentation<small class="text-danger">*</small></h4></label>\n\
<div class="input text"><input name="place[]" class="form-control" style="height:40px;" id="place" value="" type="text" required></div></div>\n\
<div class="col-xs-12">\n\
<label for="médiation"><h4>Date d\'Assermentation (jj.mm.aaaa - 28.02.1999)<small class="text-danger">*</small></h4></label>\n\
<div class="input text"><input name="date_of_auth[]" class="form-control datepicker" style="height:40px;" value="" type="text" required></div></div>\n\
    </div>';
        $(html).insertAfter($('#fieldPlace' + insertAfterId));

    }
    function addFormation() {
        var dynamicVal = $('div[id^=fieldFormation]').length;
        var insertAfterId = dynamicVal - 1;
        var html = '<div class="groupFields" id="fieldFormation' + dynamicVal + '" style="margin-top:10px">\n\
<div class="col-xs-12">\n\
<span onclick="closeFormation(' + dynamicVal + ')" class="pull-right crossButton">×</span>\n\
<label><h4>Training<small class="text-danger">*</small></h4></label>\n\
<input name="ombudsmanformations.formation[]" placeholder="Formation" class="form-control name_list" style="height:40px;" type="text" required>\n\
    </div>\n\
<div class="col-xs-12">\n\
<label for="médiation">\n\
<h4>Certification date (dd.mm.yyy - 28.02.1999) <small class="text-danger">*</small></h4>\n\
    </label>\n\
<input name="ombudsmanformations.date[]" placeholder="Date de certification" class="datepicker form-control" style="height: 40px;" type="text" required>\n\
    </div><div class="col-xs-12">\n\
<label for="image" class="col-md-12" style="padding:0">Copie de la certification</label>\n\
<input name="image[]" class="section-image" id="sectionImage" onchange="readURL(this)" type="file" required>\n\
<br>\n\
    </div>\n\
    </div>';
        $(html).insertAfter($('#fieldFormation' + insertAfterId));

    }
    $(document).ready(function () {
        jQuery('.js-example-basic-multiple').select2();
        $(document).on('change', '#multipleLanguages', function () {
            var values = $(this).val();
            if (values == null) {
                $('#myLanguages').html('');
                return false;
            }
            $.ajax({
                url: "<?php echo $this->request->webroot . 'Registerations/languagae_experience' ?>",
                type: "GET",
                data: 'values=' + values,
                success: function (data)
                {
                    data = JSON.parse(data);
                    $('#myLanguages').html(data);
                }
            });
        });



        $('#assermentation').change(function () {
            if ($(this).val() == 1) {
                $('#fieldPlace0 label h4').append('<small class="text-danger">*</small>');
                $('#fieldPlace0 input').prop('required', true);
            } else {
                $('#fieldPlace0 label h4').children('small').remove();
                $('#fieldPlace0 input').prop('required', false);

            }
        });

        $(document).on('click', '.btn_remove', function () {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
        });

        setTimeout(function () {
            $('input[name=pwd]').val('');

        }, 100);

        $('input[name=pwd]').blur(function () {
            if ($(this).val()) {
                if ($(this).val().length < 6) {
                    if ($(this).siblings('small').length > 0) {
                        $(this).siblings('small').text('At least 6 characters');
                    } else {
                        $('<small style="color:red">At least 6 characters</small>').insertAfter($(this));
                    }
                    $('input[name=repassword]').prop('disabled', true);
                    return false;
                } else {
                    $('input[name=repassword]').prop('disabled', false);
                    $(this).siblings('small').text('');
                }
                $('input[name=repassword]').val('');
            }
        });
        $('input[name=repassword]').blur(function () {
            if ($(this).val()) {
                if ($('input[name="pwd"]').val() != $(this).val()) {
                    if ($(this).siblings('small').length > 0) {
                        $(this).siblings('small').text('Confirm Password does not match');
                    } else {
                        $('<small style="color:red">Confirm Password does not match</small>').insertAfter($(this));
                    }
                    return false;

                } else {
                    $(this).siblings('small').text('');
                }
            }
        });
        var readURL = function (input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    //                        console.log(e.target);
                    //                        $('.avatar').attr('style', 'background-image:url("' + e.target.result + '")');
                    $('.avatar').css("background-image", "url('" + e.target.result + "')");
                    ;
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
        $(".file-upload").on('change', function () {
            readURL(this);
        });
    });

    $("#UserAddForm").submit(function () {
        if ($('input[name="pwd"]').val()) {
            if ($('input[name="pwd"]').val().length < 6) {
                if ($('input[name="pwd"]').siblings('small').length > 0) {
                    $('input[name="pwd"]').siblings('small').text('At least 6 characters');
                } else {
                    $('<small style="color:red">At least 6 characters</small>').insertAfter($('input[name="pwd"]'));
                }
                return false;
            } else {
                if ($('input[name="repassword"]').val()) {
                    if ($('input[name="pwd"]').val() != $('input[name="repassword"]').val()) {
                        if ($('input[name="repassword"]').siblings('small').length > 0) {
                            $('input[name="repassword"]').siblings('small').text('Confirm Password does not match');
                        } else {
                            $('<small style="color:red">Confirm Password does not match</small>').insertAfter($('input[name="repassword"]'));
                        }
                        return false;

                    } else {
                        $('input[name="pwd"]').siblings('small').text('');
                    }
                    $('input[name="pwd"]').siblings('small').text('');
                } else {
                    if ($('input[name="repassword"]').siblings('small').length > 0) {
                        $('input[name="repassword"]').siblings('small').text('Please enter confirm password');
                    } else {
                        $('<small style="color:red">Please enter confirm password</small>').insertAfter($('input[name="repassword"]'));
                    }
                    return false;
                }
            }
        }


        var phone_pat = /^\(?(\+\d{3})\)?[-\. ]?(\d{3})[-\. ]?(\d{2})[-\. ]?(\d{2})$/;
        var phone = $("#tel1").val();
        //alert(phone);

        if ($('input[name=tel1]').val()) {
            //code for further validation
            if (!phone_pat.test(phone)) {

                $('input[name=tel1]').focus();
                $('label[for=tel1] i').addClass('fa-frown-o field_error').removeClass('fa-smile-o field_ok');
                alert("S'il vous plaît entrer le format de téléphone valide");
                return false;

                alert("S'il vous plaît entrer le format de téléphone valide");



            } else {

                $('label[for=tel1] i').addClass('fa-smile-o field_ok').removeClass('fa-frown-o field_error');
            }

        } else {
            $('input[name=tel1]').focus();
            $('label[for=tel1] i').addClass('fa-frown-o field_error').removeClass('fa-smile-o field_ok');
            alert("S'il vous plaît entrer le format de téléphone valide");
            return false;

            alert("S'il vous plaît entrer le format de téléphone valide");



        }

        if (!$('input[name=first_name]').val()) {
            $('input[name=firstname]').focus();
            return false;
        }

        if (!$('input[name=lastname]').val()) {
            $('input[name=lastname]').focus();
            return false;
        }


        if ($('input[name=postalcode]').length) {
            if ($('input[name=postalcode]').val()) {
                //             alert($('input[name=zipcode]').val().length);
                if ($('input[name=postalcode]').val().length > 5) {
                    $('input[name=postalcode]').focus();
                    alert("S'il vous plaît entrer le code postal valide");
                    return false;

                    alert("S'il vous plaît entrer le code postal valide");



                } else {
                    return true;
                }
            } else {
                $('input[name=postalcode]').focus();
                alert("S'il vous plaît entrer le code postal valide");
                return false;

                alert("S'il vous plaît entrer le code postal valide");



            }
        }
    });
</script>