<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Registeration[]|\Cake\Collection\CollectionInterface $registerations
  */
?>

<div class="panel-head-info">
    <p class="pull-left info-text">Membres Débiteurss</p>
            <!--<a href="<?php // echo $this->request->webroot ?>pages" style="font-size:14px;" class="btn btn-primary btn-blue-custom pull-right text-info"><i class="fa fa-angle-left"></i> Go Back</a>-->
    <div class="clearfix"></div>
    <?php 
//    if($permission == 2){
//    ?>
    
<!--    <a href="<?php // echo $this->request->webroot ?>registerations/add" class="btn btn-success"><i class="fa fa-plus"></i> Add Registration</a>-->
     <?php //} ?>
</div>
<div class="container-fluid content-container" style="">

    <div class="row">
        <div id="no-more-tables" >
            <table class="col-md-12 table-bordered table-striped table-condensed cf" >
                <thead class="cf">
                    <tr class="text-nowrap">
                        <th scope="col"><?= $this->Paginator->sort('image','Photo') ?></th>
                         <!-- <th scope="col"><? //$this->Paginator->sort('profession','Profession') ?></th> -->
                         
                         <th scope="col"><?= $this->Paginator->sort('name', 'Prénom') ?></th>
                         <th scope="col"><?= $this->Paginator->sort('lastname', 'Nom de famille') ?></th>
                        <!-- <th scope="col"><? //$this->Paginator->sort('membertype_id','Type') ?></th> -->
                       
<!--                        <th scope="col"><? $this->Paginator->sort('username') ?></th>-->
                        
<!--                        <th scope="col"><? $this->Paginator->sort('password') ?></th>-->
<!--                        <th scope="col"><? $this->Paginator->sort('address2','Address 2') ?></th>-->
<!--                        <th scope="col"><? $this->Paginator->sort('postalcode','Postal Code') ?></th>-->
<!--                        <th scope="col"><?$this->Paginator->sort('city','City') ?></th>-->
<!--                        <th scope="col"><? $this->Paginator->sort('pay','Pay') ?></th>-->
                        
                        <!-- <th scope="col"><? //$this->Paginator->sort('tel1','Telephone 1') ?></th> -->
<!--                        <th scope="col"><? $this->Paginator->sort('tel2','Telephone 2') ?></th>-->
<!--                        <th scope="col"><? $this->Paginator->sort('email','Email') ?></th>-->
<!--                        <th scope="col"><? $this->Paginator->sort('status','Status') ?></th>-->
<!--                        <th scope="col"><? $this->Paginator->sort('site_web','Web Url') ?></th>-->
                         <th scope="col"><?= $this->Paginator->sort('amount_due','Montant dû') ?></th>
<th scope="col" class="actions"><?= __('Action') ?></th>
                     
                    </tr>
                </thead>
                <tbody>
                           <?php foreach ($member_owed as $registeration): 
                               //debug($registeration['user']['email']);exit;
                               ?>
                    
            <tr >
                <?php
                  $image = 'cyber1550500683.png';
                            if ($registeration->image ) {

                                $path = $registeration['image'] ;
                                 $path = $this->request->webroot.'img/slider/'.$path;
                            }  else{
                                    $path = $this->request->webroot.'img/Users/'.$image;
                                }
                                ?>
                <td data-title="Photo"><img src="<?php echo $path; ?>" height='50'/></td>
                <!-- <td data-title="Profession"><? //h($registeration['profession']) ?></td> -->
                <td data-title="Prénom"><?= h($registeration['first_name']) ?></td>
                <td data-title="Nom de famille"><?= h($registeration['last_name']) ?></td>
                 <!-- <td data-title="Membership Type"><? //h($registeration['description']) ?></td> -->
                
                
<!--                <td><? h($registeration->username) ?></td>-->
                
<!--                <td><? h($registeration->password) ?></td>-->
               
<!--                <td data-title="Address 2"><? h($registeration->address2) ?></td>-->
<!--                <td data-title="Postal Code"><? $this->Number->format($registeration->postalcode) ?></td>-->
<!--                <td data-title="City"><? h($registeration->city) ?></td>-->
<!--                <td data-title="Pay"><? $this->Number->format($registeration->pay) ?></td>-->
                
                <td data-title="Montant dû"><?= h($registeration['amount_due']) ?></td>
<!--                <td data-title="Telephone 2"><? h($registeration->tel2) ?></td>-->
<!--                <td data-title="Email"><? h($registeration['user']['email']) ?></td>-->
<!--                <td data-title="Status"><?php
//                                if ($registeration->status) {
//                                    echo $this->Html->link(__('Active'), array('controller' => 'registerations', 'action' => 'change_status', $registeration->id, 0), array('class' => 'btn btn-success', 'onclick' => "return confirm('This account is active do you want to inactive it')", 'escape' => false));
//                                } else {
//                                    echo $this->Html->link(__('In active'), array('controller' => 'registerations', 'action' => 'change_status', $registeration->id, 1), array('class' => 'btn btn-danger', 'onclick' => "return confirm('This account is Inactive do you want to active it')", 'escape' => false));
//                                }
                                ?></td>-->
<!--                <td data-title="Web Url"><? h($registeration->site_web) ?></td>-->
                <td  data-title="Action" class="actions">
      <?php //echo $this->Html->link(__('View'), array('controller' => 'users', 'action' => 'view',$user->id),array('class'=>'btn btn-primary')); ?>
        <?php echo $this->Html->link(__('Paiements'), array('controller' => 'Accounts', 'action' => 'index',$registeration['rid']),array('class'=>'btn btn-primary','escape'=>false)); ?>

                          <?php
                         
                       if($permission == 2){
                             
        echo $this->Html->link(__('<i class="fa fa-edit"></i> Modifier'), array('controller' => 'registerations', 'action' => 'edit',$registeration['rid']),array('class'=>'btn btn-success','escape'=>false)); 
      
    echo "&nbsp;";  
                echo $this->Form->postLink(__('<i class="fa fa-trash-o"></i> Effacer'), ['action' => 'delete', $registeration['rid']], ['escape'=>false,'class'=>'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $registeration['rid'])]);
                                                               // }
                //echo $this->Html->link(__('<i class="fa fa-trash"></i> Delete'), ['controller'=>'users','action' => 'delete', $user->id], ['escape'=>false,'class'=>'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $user->username)]);
                            
              //echo $this->Form->postLink(__('<i class="fa fa-trash"></i> Delete'), ['action' => 'delete', $user->id], ['escape'=>false,'class'=>'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $user->id)]); 
                                                        } ?>
                        </td>
                    </tr>

            
            <?php endforeach; ?>
                </tbody>
            </table>
            </form>
        </div>
    </div>

    <br />
    <div class="paginator">
        <ul class="pagination">
            <?php
                echo $this->Paginator->prev(__('retour'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                echo $this->Paginator->next(__('suivant'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
            ?>
        </ul>
        <p class="small"><?= $this->Paginator->counter(['format' => __('Page {{page}} de {{pages}}, affiche {{current}} entrée(s) sur un de total {{count}}')]) ?></p>
    </div>
</div>
