<style>
    @media (max-width:990px) and (min-width:320px) {
        .customButtons{
            width: 200px !important;
        }
    }
    .customButtons{
        border: 1px solid #147172;
        background: none;
        /*padding: 25px 90px;*/
        width: 350px;
        padding: 0;
        height: 85px;
        font-size: 20px;
        color: #147172;
        font-weight: bold;
        margin-bottom: 10px;
    }
    .customButtons:hover{
        background: #147172;
        color: white;
        transition: all .4s ease;
        -webkit-transition: all .4s ease;
    }
    .customButtons:focus{
        background: #147172;
        color: white;
    }
    .activeMember{
        background: #147172;
        color: white;
    }
    .MyRadio{
        margin-right:10px !important;
    }
    .terms:hover{
        text-decoration: underline; 
    }
    .TermsAndConditionButon{
        border-color: #147172 !important;
        background: #147172 !important;
    }
</style>
<?php
/**
 * @var \App\View\AppView $this
 */
?>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>


<hr>
<div class="container bootstrap snippet" style="min-height: 502px;">


    <div class="row">
        <div class="col-sm-1">
        </div>
        <div class="col-sm-10">
            <!--            <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#home">Home</a></li>
                            <li><a data-toggle="tab" href="#messages">Menu 1</a></li>
                            <li><a data-toggle="tab" href="#settings">Menu 2</a></li>
                          </ul>-->


            <div class="tab-content">
                <div class="tab-pane active" id="home">
                    <!--<hr>-->
                    <h3 style="text-align:center;margin: 30px 0;font-weight: bold;">TYPE DE MEMBRE</h3>

                    <!--                    <div class="row">
                                            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12" style="text-align:center;">
                    
                                                <button class="customButtons activeMember" memberType='individualOptions'>Membre Individuel</button>
                    
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12" style="text-align:center;">
                                                <button class="customButtons" memberType='collectiveOptions'>Membre Collectif</button>
                                            </div>
                                        </div>-->
                    <div class="row">
                        <div class="offset-1 col-5 col-xs-12" style="text-align:center;">

                            <button class="customButtons activeMember" membertype="individualOptions">Membre Individuel</button>

                        </div>
                        <div class="col-5 col-xs-12" style="text-align:center;">
                            <button class="customButtons" membertype="collectiveOptions">Membre Collectif</button>
                        </div>
                    </div>
                    <!--                  <form class="form" action="##" method="post" id="registrationForm">-->
                    <?= $this->Form->create($user, array('type' => 'file', 'id' => 'UserAdd')) ?>
                    <div class="form-group">
                        <?php // debug($membertypes->toArray()); ?>
                        <div class="col-xs-12" id="individualOptions">
                            <label for="membertype_id" id="labelMembership"><h4>
                                    Type d'adhésion <small class="text-danger">*</small></h4>
                            </label>
                            <?php // echo $this->Form->control('membertype_id', ['class' => 'form-control', 'options' => $membertypes, 'style' => 'height:40px;', 'label' => false, 'required' => 'required']); ?>
                            <?php
                            foreach ($membertypes as $mt):
                                $addClass = 'individualOptions';
                                $setStyle = '';
                                if ($mt->type == 2) {
                                    $addClass = 'collectiveOptions';
                                    $setStyle = 'display:none;';
                                }
                                ?>
                                <div class="<?= $addClass; ?>" style="<?= $setStyle; ?>">
                                    <label for="membertype-id-<?= $mt->id; ?>">
                                        <input name="membertype_id" value="<?= $mt->id; ?>" id="membertype-id-<?= $mt->id; ?>" class="MyRadio" type="radio" required><?php echo $mt->name; ?>
                                    </label>
                                    <br/>
                                    <?php echo $mt->more_info; ?>
                                </div>
                                <?php
                            endforeach;
                            ?>
                        </div>

                    </div>

                    <div class="collectiveOptions" style="display:none;">
                        <div class="form-group">

                            <div class="col-xs-12">
                                <label for="org-name"><h4>Nom de l'organisation <small class="text-danger">*</small></h4></label>

                                <?php
                                echo $this->Form->control('org_name', [
                                    'class' => 'form-control', 'style' => 'height:40px;', 'label' => false]);
                                ?>
                            </div>
                        </div>
                        <p style="font-size: 16px;padding: 0 15px;font-weight: bold;">Prénom et Nom de la personne de contact</p>
                    </div>
                    <div class="form-group">

                        <div class="col-xs-6">
                            <label for="first-name"><h4>Prénom <small class="text-danger">*</small></h4></label>

                            <?php
                            echo $this->Form->control('first_name', [
                                'class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'required' => 'required']);
                            ?>
                        </div>
                    </div>

                    <div class="form-group">

                        <div class="col-xs-6">
                            <label for="lastname"><h4>Nom de famille <small class="text-danger">*</small></h4></label>
                            <?php
                            echo $this->Form->control('lastname', [
                                'class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'required' => 'required']);
                            ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-6">
                            <label for="pass"><h4>Mot de passe <small class="text-danger">*</small></h4></label>
                            <?php
                            echo $this->Form->control('password', [
                                'class' => 'form-control', 'type' => 'password', 'style' => 'height:40px;', 'id' => 'pass', 'label' => false, 'required' => 'required']);
                            ?>
                        </div>
                    </div>
                    <div class="form-group">

                        <div class="col-xs-6">
                            <label for="repass"><h4>Confirmez le mot de passe <small class="text-danger">*</small></h4></label>
                            <?php
                            echo $this->Form->control('repassword', [
                                'class' => 'form-control', 'type' => 'password', 'style' => 'height:40px;', 'label' => false, 'id' => 'repass', 'required' => 'required']);
                            ?> </div>
                    </div>
                    <div class="form-group">

                        <div class="col-xs-6">
                            <label for="email"><h4>Votre identifiant (adresse mail valide) <small class="text-danger">*</small></h4></label>
                            <?php
                            echo $this->Form->control('email', [
                                'class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'required' => 'required']);
                            ?>

                        </div>

                        <div class="col-xs-6">
                            <label for="confirmemail"><h4>Confirmez Votre identifiant (adresse mail valide) <small class="text-danger">*</small></h4></label>
                            <?php
                            echo $this->Form->control('confirmemail', [
                                'class' => 'form-control', 'style' => 'height:40px;', 'label' => false, 'required' => 'required']);
                            ?>

                        </div>
                    </div>
                    <div class="col-xs-12">
                        <a class="terms" href="#" data-toggle="modal" data-target="#termsAndConditions">Veuillez acceptez conditions generales en cliquant ici.</a>
                        <button class="btn btn-lg btn-success btn-block" type="submit" id='register' style="border-radius: 0;border-color: #147172;background:#147172;  margin-top:43px;height:40px;"> S'INCRIRE</button>
                        <br/>
                        <br/>
                    </div>
                    <?php echo $this->Form->end(); ?>
                </div><!--/tab-pane-->
                <!--             <div class="tab-pane" id="messages">
                               
                               <h2></h2>
                               
                               <hr>
                                  <form class="form" action="##" method="post" id="registrationForm">
                                      <div class="form-group">
                                          
                                          <div class="col-xs-6">
                                              <label for="first_name"><h4>First name</h4></label>
                                              <input type="text" class="form-control" name="first_name" id="first_name" placeholder="first name" title="enter your first name if any.">
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          
                                          <div class="col-xs-6">
                                            <label for="last_name"><h4>Last name</h4></label>
                                              <input type="text" class="form-control" name="last_name" id="last_name" placeholder="last name" title="enter your last name if any.">
                                          </div>
                                      </div>
                          
                                      <div class="form-group">
                                          
                                          <div class="col-xs-6">
                                              <label for="phone"><h4>Phone</h4></label>
                                              <input type="text" class="form-control" name="phone" id="phone" placeholder="enter phone" title="enter your phone number if any.">
                                          </div>
                                      </div>
                          
                                      <div class="form-group">
                                          <div class="col-xs-6">
                                             <label for="mobile"><h4>Mobile</h4></label>
                                              <input type="text" class="form-control" name="mobile" id="mobile" placeholder="enter mobile number" title="enter your mobile number if any.">
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          
                                          <div class="col-xs-6">
                                              <label for="email"><h4>Email</h4></label>
                                              <input type="email" class="form-control" name="email" id="email" placeholder="you@email.com" title="enter your email.">
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          
                                          <div class="col-xs-6">
                                              <label for="email"><h4>Location</h4></label>
                                              <input type="email" class="form-control" id="location" placeholder="somewhere" title="enter a location">
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          
                                          <div class="col-xs-6">
                                              <label for="password"><h4>Password</h4></label>
                                              <input type="password" class="form-control" name="password" id="password" placeholder="password" title="enter your password.">
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          
                                          <div class="col-xs-6">
                                            <label for="password2"><h4>Verify</h4></label>
                                              <input type="password" class="form-control" name="password2" id="password2" placeholder="password2" title="enter your password2.">
                                          </div>
                                      </div>
                                      <div class="form-group">
                                           <div class="col-xs-12">
                                                <br>
                                                <button class="btn btn-lg btn-success" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                                                <button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                                            </div>
                                      </div>
                                </form>
                               
                             </div>/tab-pane
                             <div class="tab-pane" id="settings">
                                        
                                
                                  <hr>
                                  <form class="form" action="##" method="post" id="registrationForm">
                                      <div class="form-group">
                                          
                                          <div class="col-xs-6">
                                              
                                              <label for="first_name"><h4>First name</h4></label>
                                              
                                              <input type="text" class="form-control" name="first_name" id="first_name" placeholder="first name" title="enter your first name if any.">
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          
                                          <div class="col-xs-6">
                                            <label for="last_name"><h4>Last name</h4></label>
                                              <input type="text" class="form-control" name="last_name" id="last_name" placeholder="last name" title="enter your last name if any.">
                                          </div>
                                      </div>
                          
                                      <div class="form-group">
                                          
                                          <div class="col-xs-6">
                                              <label for="phone"><h4>Phone</h4></label>
                                              <input type="text" class="form-control" name="phone" id="phone" placeholder="enter phone" title="enter your phone number if any.">
                                          </div>
                                      </div>
                          
                                      <div class="form-group">
                                          <div class="col-xs-6">
                                             <label for="mobile"><h4>Mobile</h4></label>
                                              <input type="text" class="form-control" name="mobile" id="mobile" placeholder="enter mobile number" title="enter your mobile number if any.">
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          
                                          <div class="col-xs-6">
                                              <label for="email"><h4>Email</h4></label>
                                              <input type="email" class="form-control" name="email" id="email" placeholder="you@email.com" title="enter your email.">
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          
                                          <div class="col-xs-6">
                                              <label for="email"><h4>Location</h4></label>
                                              <input type="email" class="form-control" id="location" placeholder="somewhere" title="enter a location">
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          
                                          <div class="col-xs-6">
                                              <label for="password"><h4>Password</h4></label>
                                              <input type="password" class="form-control" name="password" id="password" placeholder="password" title="enter your password.">
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          
                                          <div class="col-xs-6">
                                            <label for="password2"><h4>Verify</h4></label>
                                              <input type="password" class="form-control" name="password2" id="password2" placeholder="password2" title="enter your password2.">
                                          </div>
                                      </div>
                                      <div class="form-group">
                                           <div class="col-xs-12">
                                                <br>
                                                
                                                <button class="btn btn-lg btn-success pull-right" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                                                <button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                                            </div>
                                      </div>
                                </form>
                              </div>
                               
                              </div>/tab-pane-->
            </div><!--/tab-content-->

        </div><!--/col-9-->
        <div class="col-sm-1">
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="termsAndConditions" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div style="padding:10px;color:white;background-color:#147172;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 style="width: 90%;text-align: justify;">
                        <!--Avant de pouvoir continuer, vous devez accepter les conditions générales. Pour procéder, merci de bien vouloir cliquer dans l'emplacement prévu à cet effet se trouvant à la fin de ce texte-->
                        <?= $termsAndConditions->title; ?>
                    </h4>
                </div>
                <div class="modal-body" style="height:600px;overflow: auto;">
                    <!--                    <div class="elm_wp">
                                            <div class="elementor elementor-12283 elementor-bc-flex-widget" data-elementor-id="12283" data-elementor-settings="[]" data-elementor-type="page">
                                                <div class="elementor-inner">
                                                    <div class="elementor-section-wrap">
                                                        <section class="elementor-element elementor-element-cca88a5 elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-element_type="section" data-id="cca88a5" data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}" style="width: 100%; left: 0px;">
                                                            <div class="elementor-container elementor-column-gap-default">
                                                                <div class="elementor-row">
                                                                    <div class="elementor-element elementor-element-2e50c08 elementor-column elementor-col-100 elementor-top-column" data-element_type="column" data-id="2e50c08">
                                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                                            <div class="elementor-widget-wrap">
                                                                                <div class="elementor-element elementor-element-4b2c474 elementor-widget elementor-widget-text-editor" data-element_type="widget" data-id="4b2c474" data-widget_type="text-editor.default">
                                                                                    <div class="elementor-widget-container">
                                                                                        <div class="elementor-text-editor elementor-clearfix">
                                                                                            <p>Conditions de l&rsquo;utilisation des outils interactifs du site de la FGeM (N.B: L&rsquo;emploi du pluriel regroupe la forme masculine et f&eacute;minine) L&rsquo;association FGeM dite : F&eacute;d&eacute;ration)</p>
                    
                                                                                            <p> La F&eacute;d&eacute;ration Genevoise M&eacute;diations f&eacute;d&egrave;re les structures genevoises de m&eacute;diation en regroupant les associations de m&eacute;diation et l&rsquo;ensemble des m&eacute;diateurs. Ses statuts d&eacute;finissent ses objectifs. Tous ses membres alors utilisateurs des outils interactifs mis &agrave; leur disposition, s&rsquo;engagent &agrave; respecter tant ses statuts que les pr&eacute;sentes conditions d&rsquo;utilisation.</p>
                    
                                                                                            <p> &nbsp; &nbsp; &nbsp; 1.Outils Interactifs Les outils interactifs mis &agrave; disposition des membres de la FGeM sont :<br />
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; a.&nbsp;&nbsp; &nbsp;Une plateforme Internet de diffusion d&rsquo;informations diverses relatives &agrave; la m&eacute;diation et &agrave; l&rsquo;organisation d&rsquo;&eacute;v&eacute;nements li&eacute;s &agrave; la m&eacute;diation.<br />
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; b.&nbsp;&nbsp; &nbsp;Un annuaire &eacute;lectronique de m&eacute;diateurs permettant aux membres de publier leur profil en mentionnant les formations qu&rsquo;ils ont suivies et en d&eacute;crivant leur pratique ainsi que leurs comp&eacute;tences.<br />
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; c.&nbsp;&nbsp; &nbsp;Des &eacute;l&eacute;ments de codes permettant de r&eacute;pliquer et diss&eacute;miner des informations relatives &agrave; des activit&eacute;s de m&eacute;diation publi&eacute;s sur le site de la FGeM par ses membres.</p>
                    
                                                                                            <p> &nbsp; &nbsp; &nbsp; 2.&nbsp;&nbsp; &nbsp;Annuaire &eacute;lectronique &ndash; Profil des membres<br />
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  a.&nbsp;&nbsp; &nbsp;Tous les membres doivent compl&eacute;ter avec exactitude et tenir &agrave; jour les informations contenues dans le r&eacute;pertoire qui d&eacute;finit leur profil.&nbsp;<br />
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  b.&nbsp;&nbsp; &nbsp;La candidature d&rsquo;un nouveau membre ne pourra pas &ecirc;tre prise en consid&eacute;ration tant que le profil n&rsquo;aura pas &eacute;t&eacute; compl&eacute;t&eacute;.<br />
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  c.&nbsp;&nbsp; &nbsp;Les informations contenues dans le profil peuvent &ecirc;tre consult&eacute;es en tout temps par les membres du comit&eacute;.&nbsp;<br />
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  d.&nbsp;&nbsp; &nbsp;La publication ne pourra &ecirc;tre effective qu&rsquo;apr&egrave;s validation des cotisations re&ccedil;ues.&nbsp;</p>
                    
                                                                                            <p> &nbsp; &nbsp; &nbsp; 3.&nbsp;&nbsp; &nbsp;Administrateur du Site Internet<br />
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  a.&nbsp;&nbsp; &nbsp;La t&acirc;che d&rsquo;administrateur du site peut &ecirc;tre d&eacute;l&eacute;gu&eacute;e par le comit&eacute; &agrave; un tiers.&nbsp;<br />
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  b.&nbsp;&nbsp; &nbsp;Tout administrateur du site est tenu par un devoir de strict confidentialit&eacute; et ne peuvent en aucun cas communiquer &agrave; des tiers une quelconque information figurant dans le profil d&rsquo;un utilisateur sans son consentement explicite.</p>
                    
                                                                                            <p>&nbsp; &nbsp; &nbsp; 4.&nbsp;&nbsp; &nbsp;Contenu du Profil<br />
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;a.&nbsp;&nbsp; &nbsp;Les membres choisissent eux-m&ecirc;mes les informations qu&rsquo;ils souhaitent rendre publique.&nbsp;<br />
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; b.&nbsp;&nbsp; &nbsp;La FGeM ne pourra pas &ecirc;tre tenue pour responsable si des informations publi&eacute;es par un membre sont utilis&eacute;es contre lui.<br />
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; c.&nbsp;&nbsp; &nbsp;Les membres sont seuls responsable des informations contenues dans leur profil.&nbsp;</p>
                    
                                                                                            <p>&nbsp; &nbsp; &nbsp;  5.&nbsp;&nbsp; &nbsp;V&eacute;rification des donn&eacute;es des membres et commission ad hoc<br />
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  a.&nbsp;&nbsp; &nbsp;Un administrateur du site ou une personne mandat&eacute;e par le comit&eacute; de la FGeM peut &ecirc;tre amen&eacute; &agrave; v&eacute;rifier les informations &eacute;dit&eacute;es par un membre dans son profil.&nbsp;<br />
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  b.&nbsp;&nbsp; &nbsp;Si un membre fait &eacute;tat de qualifications qu&rsquo;il ne poss&egrave;de pas ou a abus&eacute; des moyens mis &agrave; sa disposition pour promouvoir des activit&eacute;s qui ne sont pas en accord avec les objectifs de la F&eacute;d&eacute;ration, son profil pourra &ecirc;tre suspendu et le membre en question pourra &ecirc;tre exclu de la F&eacute;d&eacute;ration.<br />
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; c.&nbsp;&nbsp; &nbsp;Dans le cas o&ugrave; une telle proc&eacute;dure devait &ecirc;tre engag&eacute;e, le membre sera inform&eacute; dans un d&eacute;lai de 30 jours par simple Courrier &eacute;lectronique avec une invitation &agrave; s&rsquo;expliquer.&nbsp;<br />
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  d.&nbsp;&nbsp; &nbsp;Le cas &eacute;ch&eacute;ant, un membre aura la possibilit&eacute; de recourir contre la d&eacute;cision prise dans un d&eacute;lai de 30 jours en demandant l&rsquo;organisation d&rsquo;une m&eacute;diation &agrave; ses frais.</p>
                    
                                                                                            <p>&nbsp; &nbsp; &nbsp; 6.&nbsp;&nbsp; &nbsp;Type d&rsquo;informations<br />
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; a.&nbsp;&nbsp; &nbsp;Les informations publi&eacute;es par les membres par l&rsquo;interm&eacute;diaire de la plateforme de diffusion d&rsquo;information de la F&eacute;d&eacute;ration doivent imp&eacute;rativement &ecirc;tre en ad&eacute;quation avec la m&eacute;diation ou &ecirc;tre associ&eacute;es &agrave; une activit&eacute; en lien avec des comp&eacute;tences pouvant &ecirc;tre associ&eacute;s &agrave; la m&eacute;diation.<br />
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; b.&nbsp;&nbsp; &nbsp;Le comit&eacute; de la FGeM, peut d&eacute;cider en tout temps de supprimer les informations ne r&eacute;pondant pas aux objectifs de la F&eacute;d&eacute;ration.<br />
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; c.&nbsp;&nbsp; &nbsp;Les informations doivent entrer dans le cadre des buts de la F&eacute;d&eacute;ration.</p>
                    
                                                                                            <p>&nbsp; &nbsp; &nbsp; 7.&nbsp;&nbsp; &nbsp;Droits d&rsquo;utilisation<br />
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; a.&nbsp;&nbsp; &nbsp;La publication du profil des membres ainsi que l&rsquo;utilisation de la plateforme de diffusion d&rsquo;informations est r&eacute;serv&eacute;e aux seuls membres qui sont &agrave; jour de leurs cotisations.&nbsp;<br />
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; b.&nbsp;&nbsp; &nbsp;Les droits d&rsquo;utilisation des outils interactifs seront automatiquement suspendus pass&eacute; le d&eacute;lai de paiement des cotisations annuelles dues ou de tout autre solde d&ucirc;.<br />
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; c.&nbsp;&nbsp; &nbsp;Les droits pourront &ecirc;tre r&eacute;tablis par le tr&eacute;sorier apr&egrave;s r&eacute;ception de l&rsquo;int&eacute;gralit&eacute; des cotisations et sommes dues.</p>
                    
                                                                                            <p>&nbsp; &nbsp; &nbsp; 8.&nbsp;&nbsp; &nbsp;R&eacute;solution des diff&eacute;rends<br />
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; a.&nbsp;&nbsp; &nbsp;Les membres, utilisateurs des outils mis &agrave; leur disposition par la FGeM, s&rsquo;engagent en cas de litige &agrave; avoir pr&eacute;alablement recours &agrave; une m&eacute;diation pour r&eacute;soudre tout conflit pouvant &eacute;merger de l&rsquo;utilisation des outils et/ou de la plateforme de la F&eacute;d&eacute;ration.&nbsp;<br />
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;b.&nbsp;&nbsp; &nbsp;Les pr&eacute;sentes conditions sont r&eacute;gies par le droit suisse. En cas d&rsquo;&eacute;chec de la m&eacute;diation pr&eacute;alable, tout litige en r&eacute;sultant sera exclusivement jug&eacute; par les tribunaux comp&eacute;tents du Canton de Gen&egrave;ve, sous r&eacute;serve de recours au tribunal f&eacute;d&eacute;ral.<br />
                                                                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; c.&nbsp;&nbsp; &nbsp;La F&eacute;d&eacute;ration conserve toutefois la facult&eacute; de faire valoir ses droits au domicile du d&eacute;posant ou devant tout autre tribunal comp&eacute;tent.&nbsp;</p>
                    
                                                                                            <p>En cliquant sur le bouton lu et accept&eacute;, vous vous engagez &agrave; accepter et &agrave; respecter les conditions d&rsquo;utilisation d&eacute;finis ci-dessus.<br />
                                                                                                &nbsp; &nbsp; &nbsp;
                                                                                            </p>
                                                                                            <br/>
                                                                                            <br/>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </section>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>-->
                    <?= $termsAndConditions->description; ?>
                    <div style="text-align:right;margin-bottom:20px;">
                        <span class="button-checkbox">
                            <button type="button" class="btn btn-lg" data-color="success  TermsAndConditionButon">Lu et Accept&eacute;</button>
                            <input value="1" type="checkbox" name="isAgree" id="isAgree" class="hidden">
                        </span>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
            </div>

        </div>
    </div>
</div><!--/row-->
<script>
    $(function () {
        $('.button-checkbox').each(function () {

            // Settings
            var $widget = $(this),
                    $button = $widget.find('button'),
                    $checkbox = $widget.find('input:checkbox'),
                    color = $button.data('color'),
                    settings = {
                        on: {
                            icon: 'glyphicon glyphicon-check'
                        },
                        off: {
                            icon: 'glyphicon glyphicon-unchecked'
                        }
                    };

            // Event Handlers
            $button.on('click', function () {
                $checkbox.prop('checked', !$checkbox.is(':checked'));
                $checkbox.triggerHandler('change');
                updateDisplay();
            });
            $checkbox.on('change', function () {
                updateDisplay();
            });

            // Actions
            function updateDisplay() {
                var isChecked = $checkbox.is(':checked');

                // Set the button's state
                $button.data('state', (isChecked) ? "on" : "off");

                // Set the button's icon
                $button.find('.state-icon')
                        .removeClass()
                        .addClass('state-icon ' + settings[$button.data('state')].icon);

                // Update the button's color
                if (isChecked) {
                    $button
                            .removeClass('btn-default')
                            .addClass('btn-' + color + ' active');
                } else {
                    $button
                            .removeClass('btn-' + color + ' active')
                            .addClass('btn-default');
                }
            }

            // Initialization
            function init() {

                updateDisplay();

                // Inject the icon if applicable
                if ($button.find('.state-icon').length == 0) {
                    $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
                }
            }
            init();
        });
    });
    $("#UserAdd").submit(function () {
        if (typeof $('#isAgree:checked').val() == 'undefined') {
            $('#termsAndConditions').modal('show');
//            alert('Vous devez avoir accepté nos conditions générales avant de poursuivre');
            return false;
        }
        if (!$('input[name=first_name]').val()) {
            $('input[name=firstname]').focus();
            return false;
        }

        if (!$('input[name=lastname]').val()) {
            $('input[name=lastname]').focus();
            return false;
        }
        if ($('input[name=password]').val().length < 6) {
            if ($('input[name=password]').siblings('small').length > 0) {
                $('input[name=password]').siblings('small').text('Veuillez entrer 6 caractères au minimum');
            } else {
                $('<small style="color:red">Veuillez entrer 6 caractères au minimum</small>').insertAfter($('input[name=password]'));
            }
            return false;
        }
        if ($('input[name=password]').val() != $('input[name=repassword]').val()) {
            if ($('input[name=repassword]').siblings('small').length > 0) {
                $('input[name=repassword]').siblings('small').text("les deux mots de passe ne sont pas pareil.");
            } else {
                $("<small style='color:red'>les deux mots de passe ne sont pas pareil.</small>").insertAfter($('input[name=repassword]'));
            }
            return false;
        }
        if ($('input[name=confirmemail]').val() != $('input[name=email]').val()) {
            if ($('input[name=confirmemail]').siblings('small').length > 0) {
                $('input[name=confirmemail]').siblings('small').text("L'adresse de messagerie ne correspond pas");
            } else {
                $("<small style='color:red'>L'adresse de messagerie ne correspond pas.</small>").insertAfter($('input[name=confirmemail]'));
            }
            return false;
        }
    });
    $('input[name=password]').blur(function () {
        if ($(this).val()) {
            if ($(this).val().length < 6) {
                $('label[for=password] i').addClass('fa-frown-o field_error').removeClass('fa-smile-o field_ok');
                if ($(this).siblings('small').length > 0) {
                    $(this).siblings('small').text('Veuillez entrer 6 caractères au minimum');
                } else {
                    $('<small style="color:red">Veuillez entrer 6 caractères au minimum</small>').insertAfter($(this));
                }
                return false;
            }
        } else {
            $('label[for=password] i').addClass('fa-frown-o field_error').removeClass('fa-smile-o field_ok');
            return false;
        }
    });
    $('input[name=repassword]').blur(function () {
        if ($(this).val()) {
            if ($('input[name="password"]').val() != $(this).val()) {
                if ($(this).siblings('small').length > 0) {
                    $(this).siblings('small').text('les deux mots de passe ne sont pas pareil.');
                } else {
                    $('<small style="color:red">les deux mots de passe ne sont pas pareil.</small>').insertAfter($(this));
                }
                $('label[for=repassword] i').addClass('fa-frown-o field_error').removeClass('fa-smile-o field_ok');
                return false;

            } else {
                $(this).siblings('small').text('');
                $('label[for=repassword] i').addClass('fa-smile-o field_ok').removeClass('fa-frown-o field_error');
            }
        } else {
            $('label[for=repassword] i').addClass('fa-frown-o field_error').removeClass('fa-smile-o field_ok');
            return false;
        }
    });


    $("#ind").hide();
    $("#col").hide();
    $("#mem").click(function () {
        $("#ind").show();
        $("#col").hide();
    });
    $("#mem2").click(function () {
        $("#col").show();
        $("#ind").hide();
    });
    $(document).ready(function () {

        $('.customButtons').click(function () {
            $("input[type=radio]").attr('checked', false);
            var type = $(this).attr('memberType');
            $('button').removeClass('activeMember');
            $('button[memberType=' + type + ']').addClass('activeMember');

            $('input[name=org_name]').val('');
            $('input[name=first_name]').val('');
            $('input[name=lastname]').val('');

            if (type == 'individualOptions') {
                $('#labelMembership , .individualOptions').show('slow');
                $('input[name=org_name]').attr('required', false);
                $('input[name=first_name]').attr('required', true);
                $('input[name=lastname]').attr('required', true);
                $('.collectiveOptions').hide();
            } else {
                $('input[name=org_name]').attr('required', true);
                $('input[name=first_name]').attr('required', false);
                $('input[name=lastname]').attr('required', false);


                $('.individualOptions').hide();
                $('#labelMembership, .collectiveOptions').show('slow');
            }

        });
    });


</script>



