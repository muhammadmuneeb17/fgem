<div class="panel-head-info">
    <p class="pull-left info-text">Membres</p>
    <!--<a href="<?php // echo $this->request->webroot            ?>pages" style="font-size:14px;" class="btn btn-primary btn-blue-custom pull-right text-info"><i class="fa fa-angle-left"></i> Go Back</a>-->
    <div class="clearfix"></div>
    <?php
    //    if($permission == 2){
    //    
    ?>

    <!--    <a href="<?php // echo $this->request->webroot           ?>registerations/add" class="btn btn-success"><i class="fa fa-plus"></i> Add Registration</a>-->
    <?php //}  ?>
</div>
<div class="container-fluid content-container" style="">

    <div class="row">
        <div id="no-more-tables" >
            <table class="col-md-12 table-bordered table-striped table-condensed cf" id="myTable">
                <thead class="cf">
                    <tr class="text-nowrap">
                        <th scope="col"><?= $this->Paginator->sort('image', 'Photo') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('profession', 'Profession') ?></th>

                        <th scope="col"><?= $this->Paginator->sort('name', 'Nom Complet') ?></th>
                        <!-- <th scope="col"><? //$this->Paginator->sort('lastname', 'Nom de famille') ?></th> -->
                        <th scope="col"><?= $this->Paginator->sort('membertype_id', 'Type') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('tel1', 'Téléphone 1') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('status', 'Statut') ?></th>
                        <!--                        <th scope="col"><? $this->Paginator->sort('site_web','Web Url') ?></th>-->
                        <th scope="col"><?= $this->Paginator->sort('modified', 'Modifie') ?></th>
                        <!--<th scope="col">Supervisor</th>-->

                        <th scope="col" class="actions"><?= __('Action') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($registerations as $registeration):
//                        $supervisor = '<a class="btn btn-danger btn-xs" onclick="return confirm(' . "'" . "Cet utilisateur n'est pas un superviseur. Voulez-vous l'ajouter en tant que superviseur?" . "'" . ')" href="' . $this->request->webroot . 'Registerations/supervisor/' . $registeration->user->id . '/1">Not Supervisor</a>';
//                        foreach ($registeration->user->member_roles as $role):
//                            if ($role->role_id == 11) {
//                                $supervisor = '<a class="btn btn-success btn-xs" onclick="return confirm(' . "'" . "Cet utilisateur est un superviseur voulez-vous supprimer cet accès?" . "'" . ')" href="' . $this->request->webroot . 'Registerations/supervisor/' . $registeration->user->id . '/0">Supervisor</a>';
//                            }
//                        endforeach;
                        ?>
                        <tr >
                            <?php
                            $image = 'cyber1550500683.png';
                            if ($registeration->image) {

                                $path = $registeration->image;
                                $path = $this->request->webroot . 'img/slider/' . $path;
                            } else {
                                $path = $this->request->webroot . 'img/Users/' . $image;
                            }
                            ?>
                            <td data-title="Photo"><img alt="Photo" src="<?php echo $path; ?>" height='50' width="50"/></td>
                            <td data-title="Profession"><?= h($registeration->profession) ?></td>
                            <td data-title="Nom Complet"><?= h($registeration['user']['first_name']) ?> <?= h($registeration['user']['last_name']) ?></td>
                            <!-- <td data-title="Nom de famille"><? //h($registeration['user']['last_name']) ?></td> -->
                            <td data-title="Type"><?= h($registeration->membertype->description) ?></td>


                                                                    <!--                <td><? h($registeration->username) ?></td>-->

                                                                    <!--                <td><? h($registeration->password) ?></td>-->

                                                                    <!--                <td data-title="Address 2"><? h($registeration->address2) ?></td>-->
                                                                    <!--                <td data-title="Postal Code"><? $this->Number->format($registeration->postalcode) ?></td>-->
                                                                    <!--                <td data-title="City"><? h($registeration->city) ?></td>-->
                                                                    <!--                <td data-title="Pay"><? $this->Number->format($registeration->pay) ?></td>-->

                            <td data-title="Téléphone 1"><?= h($registeration->tel1) ?></td>
                            <!--                <td data-title="Telephone 2"><? h($registeration->tel2) ?></td>-->
                            <!--                <td data-title="Email"><? h($registeration['user']['email']) ?></td>-->
                            <!--                <td data-title="Status"><?php
                            //                                if ($registeration->status) {
                            //                                    echo $this->Html->link(__('Active'), array('controller' => 'registerations', 'action' => 'change_status', $registeration->id, 0), array('class' => 'btn btn-success', 'onclick' => "return confirm('This account is active do you want to inactive it')", 'escape' => false));
                            //                                } else {
                            //                                    echo $this->Html->link(__('In active'), array('controller' => 'registerations', 'action' => 'change_status', $registeration->id, 1), array('class' => 'btn btn-danger', 'onclick' => "return confirm('This account is Inactive do you want to active it')", 'escape' => false));
                            //                                }
                            ?></td>-->
                            <!--                <td data-title="Web Url"><? h($registeration->site_web) ?></td>-->
                            <td data-title="Statut"><?php
                            $apr = "";
                                if ($registeration->user->status == 2 && $registeration->status == 0 && $registeration->user->suspend == 0) {
                                    echo "<strong class='text-danger'>PENDING</strong>";
                                } elseif ($registeration->user->suspend == 1) {
                                    echo "<strong class='text-warning'>SUSPENDED</strong>";
                                } else if ($registeration->user->status <= 1 && $registeration->status == 0 && $registeration->user->suspend == 0) {
                                    echo "<strong class= 'text-primary'>NEW</strong>";
                                } else {
                                    $apr = 1;
                                    echo "<strong class= 'text-success'>APPROVED</strong>";
                                }
                                ?>
                            </td>
                            <td data-title="Modifie"><?php echo h(date('d.m.y', strtotime($registeration->modified))); ?>&nbsp;</td>
<!--                            <td data-title="Supervisor">
                               < $supervisor; ?>
                            </td>-->
                            <td  data-title="Action" class="actions">
                                <?php //echo $this->Html->link(__('View'), array('controller' => 'users', 'action' => 'view',$user->id),array('class'=>'btn btn-primary'));    ?>
                                <?php echo $this->Html->link(__('<span class="fa fa-money"></span>'), array('controller' => 'Accounts', 'action' => 'index', $registeration->id), array('class' => 'btn btn-primary', 'escape' => false,'data-toggle'=>"tooltip", 'title'=>"Paiement")); ?>

                                <?php
                                
                                if ($permission == 2) {

                                    echo $this->Html->link(__('<i class="fa fa-edit"></i>'), array('controller' => 'registerations', 'action' => 'edit', $registeration->id), array('class' => 'btn btn-success', 'escape' => false,'data-toggle'=>"tooltip", 'title'=>"Modifier"));
                                    ?>

                                    <?php
                                    echo $this->Form->postLink(__('<i class="fa fa-trash-o"></i>'), ['action' => 'delete', $registeration->id], ['escape' => false, 'class' => 'btn btn-danger', 'confirm' => __('Etes-vous sûr que vous voulez supprimer # {0}?', $registeration->id),'data-toggle'=>"tooltip", 'title'=>"Effacer"])."&nbsp;";

                                    if($registeration->user->suspend == 0 && $apr == 1) {
                                    echo $this->Form->postLink(__('<i class="fa fa-pause"></i>'), ['action' => 'suspend', $registeration->user_id], ['escape' => false, 'class' => 'btn btn-warning', 'confirm' => __('Êtes-vous sûr de vouloir suspendre ce compte? # {0}?', $registeration->user_id),'data-toggle'=>"tooltip", 'title'=>"Suspendre"]);
                                    } else if($registeration->user->suspend == 1) {
                                    echo $this->Form->postLink(__('<i class="fa fa-play"></i>'), ['action' => 'activate', $registeration->user_id], ['escape' => false, 'class' => 'btn btn-success', 'confirm' => __('Êtes-vous sûr de vouloir activer ce compte?? # {0}?', $registeration->user_id),'data-toggle'=>"tooltip", 'title'=>"Activer"]);
                                    } 

                                    // }
                                    //echo $this->Html->link(__('<i class="fa fa-trash"></i> Delete'), ['controller'=>'users','action' => 'delete', $user->id], ['escape'=>false,'class'=>'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $user->username)]);
                                    //echo $this->Form->postLink(__('<i class="fa fa-trash"></i> Delete'), ['action' => 'delete', $user->id], ['escape'=>false,'class'=>'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $user->id)]); 
                                }
                                ?>
                            </td>
                        </tr>

                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            </form>
        </div>
    </div>

    <br />
    <div class="paginator">
        <ul class="pagination">
            <?php
            echo $this->Paginator->prev(__('Précédent'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
            echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1));
            echo $this->Paginator->next(__('Suivante'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
            ?>
        </ul>
        <p class="small"><?= $this->Paginator->counter(['format' => __('Page {{page}} de {{pages}}, affiche {{current}} entrée(s) sur un de total {{count}}')]) ?></p>
    </div>
</div>
<script>

    function sortTable(table, order) {
        var asc = order === 'desc',
                tbody = table.find('tbody');

        tbody.find('tr').sort(function (a, b) {
            if (asc) {
                return $('td:nth-child(6)', b).text().localeCompare($('td:nth-child(6)', a).text());
            } else {
                return $('td:nth-child(6)', a).text().localeCompare($('td:nth-child(6)', b).text());
            }
        }).appendTo(tbody);
    }


    sortTable($('#myTable'), 'desc');

</script>