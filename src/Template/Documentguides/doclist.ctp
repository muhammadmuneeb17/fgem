<div class="container margintop-20">
    <div class="row">
        <div class="col-sm-3 col-sm-offset-0">
            <?php 
            
    if($permission == 2){
    ?>
            <a href="<?php echo $this->request->webroot ?>documentguides/add" class="btn btn-primary btn-add-top"><i class="fa fa-plus"></i> Ajouter un Document</a>
             <?php } ?>
        </div>
    </div>
</div>
<div class="container content-container">

    <div class="row">
        <div id="no-more-tables">
           <form method="post" id="myForm" action="<?php echo $this->request->webroot; ?>documentguides/udapteorder">
            <table class="col-md-12 table-bordered table-striped table-condensed cf">
                
                <thead class="cf">
                    <tr>
                        <th><?php echo $this->Paginator->sort('Titre'); ?></th>
                        <th><?php echo $this->Paginator->sort('fichier'); ?></th>
                        <th><?php echo $this->Paginator->sort('Statut'); ?></th>
                        <th><?php echo $this->Paginator->sort('Ordre'); ?></th>
                        <th><?php echo $this->Paginator->sort('Modifié'); ?></th>
                        <!--<th><?php // echo $this->Paginator->sort('modified'); ?></th>-->
                        <th class="actions"><?php echo __('Action'); ?></th>
                    </tr>
                </thead>
                <tbody>
                            <?php foreach ($documentguides as $documentguide): ?>
                    <tr>
                        <td data-title="Titre"><?php echo h($documentguide->title); ?>&nbsp;</td>
                        <td data-title="fichier"><a href="<?php echo $this->request->webroot; ?>img/documentguide/<?php echo h($documentguide->file); ?>" width='150'/><?php echo h($documentguide->file); ?></a></td>
                        <td data-title="Statut"><?php 
                    if($documentguide->status == 0) {
                        $status = "Deactive";
                    }
                    else {
                        $status = "Active";
                    }
                    echo $status; ?>&nbsp</td>
                        <td data-title="Ordre">
                            <?php echo $this->Form->control('documentguide_order',array('type'=>'number','value'=>$documentguide->ordinal,'name'=>'data[ordinal][]','div'=>false,'label'=>false,'class'=>'form-control','required'=>'required')); ?>
                        <?php echo $this->Form->control('documentguide_id',array('type'=>'hidden','value'=>$documentguide->id,'name'=>'data[documentguide_id][]','div'=>false,'label'=>false,'class'=>'form-control')); ?>
                        <td data-title="Modifié"><?php echo h(date('d.m.y H:i:s',strtotime($documentguide->modified))); ?>&nbsp;</td>
                        <!--<td data-title="Modified"><?php // echo h($Silder['documentguide['modified); ?>&nbsp;</td>-->
                        <td  data-title="Action" class="actions">
                              <?php 
                       if($permission == 2){
                           ?>
			<?php echo $this->Html->link(__('Modifier'), array('controller' => 'documentguides', 'action' => 'edit',$documentguide->id),array('class'=>'btn btn-success')); ?>
			<?php 
                        echo $this->Html->link(__('<i class="fa fa-trash-o"></i> Effacer'), ['action' => 'delete', $documentguide->id], ['escape'=>false,'class'=>'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $documentguide->title)]); ?>
		        <?php } ?>
                        </td>
                    </tr>
            <?php endforeach; ?>
                </tbody>
               
            </table>
               </form>
            <button class="btn btn-custom col-xs-offset-6 col-md-offset-6" style="margin-top:10px;margin-bottom:10px;" id="orderupdate_btn" >Mise à Jour de la Commande</button>
        </div>
    </div>
    <br />
    
        
    <div class="paginator">
        <ul class="pagination">
            <?php
                echo $this->Paginator->prev(__('Précédent'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                echo $this->Paginator->next(__('Suivante'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
            ?>
        </ul>
        
        <p class="small"><?= $this->Paginator->counter(['format' => __('Page {{page}} de {{pages}}, affiche {{current}} entrée(s) sur un de total {{count}}')]) ?></p>
    </div>
        
</div>
<script type="text/javascript">
    $(document).ready(function() {
       $("#orderupdate_btn").click(function() {
           $("#myForm").submit();
       });
    });
</script>