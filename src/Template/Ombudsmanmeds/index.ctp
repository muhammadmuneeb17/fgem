<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Ombudsmanmed[]|\Cake\Collection\CollectionInterface $ombudsmanmeds
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Ombudsmanmed'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Medtypes'), ['controller' => 'Medtypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Medtype'), ['controller' => 'Medtypes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ombudsmanmeds index large-9 medium-8 columns content">
    <h3><?= __('Ombudsmanmeds') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ombudsman_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('medtype_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ombudsmanmeds as $ombudsmanmed): ?>
            <tr>
                <td><?= $this->Number->format($ombudsmanmed->id) ?></td>
                <td><?= $this->Number->format($ombudsmanmed->ombudsman_id) ?></td>
                <td><?= $ombudsmanmed->has('medtype') ? $this->Html->link($ombudsmanmed->medtype->id, ['controller' => 'Medtypes', 'action' => 'view', $ombudsmanmed->medtype->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $ombudsmanmed->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ombudsmanmed->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $ombudsmanmed->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ombudsmanmed->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} de {{pages}}, affiche {{current}} entrée(s) sur un de total {{count}}')]) ?></p>
    </div>
</div>
