<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $ombudsmanmed->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $ombudsmanmed->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Ombudsmanmeds'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Medtypes'), ['controller' => 'Medtypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Medtype'), ['controller' => 'Medtypes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ombudsmanmeds form large-9 medium-8 columns content">
    <?= $this->Form->create($ombudsmanmed) ?>
    <fieldset>
        <legend><?= __('Edit Ombudsmanmed') ?></legend>
        <?php
            echo $this->Form->control('ombudsman_id');
            echo $this->Form->control('medtype_id', ['options' => $medtypes]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
