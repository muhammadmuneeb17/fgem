<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Ombudsmanmed $ombudsmanmed
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Ombudsmanmed'), ['action' => 'edit', $ombudsmanmed->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Ombudsmanmed'), ['action' => 'delete', $ombudsmanmed->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ombudsmanmed->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Ombudsmanmeds'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ombudsmanmed'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Medtypes'), ['controller' => 'Medtypes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Medtype'), ['controller' => 'Medtypes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="ombudsmanmeds view large-9 medium-8 columns content">
    <h3><?= h($ombudsmanmed->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Medtype') ?></th>
            <td><?= $ombudsmanmed->has('medtype') ? $this->Html->link($ombudsmanmed->medtype->id, ['controller' => 'Medtypes', 'action' => 'view', $ombudsmanmed->medtype->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($ombudsmanmed->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ombudsman Id') ?></th>
            <td><?= $this->Number->format($ombudsmanmed->ombudsman_id) ?></td>
        </tr>
    </table>
</div>
