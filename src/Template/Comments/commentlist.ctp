<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Comment[]|\Cake\Collection\CollectionInterface $comments
  */
?>
<div class="panel-head-info">
    <p class="pull-left info-text">Commentaire - <strong><?php echo $comments->toArray()[0]['account']['registeration']['user']['first_name'] . ' ' . $comments->toArray()[0]['account']['registeration']['user']['last_name'] ; ?></strong> </p>
            <!--<a href="<?php // echo $this->request->webroot ?>pages" style="font-size:14px;" class="btn btn-primary btn-blue-custom pull-right text-info"><i class="fa fa-angle-left"></i> Go Back</a>-->
    <div class="clearfix"></div>
    <?php 
//    if($permission == 2){
//    ?>
    
<!--    <a href="<?php  echo $this->request->webroot ?>comments/add?id=<?php echo $id;?>&accont_id=<?php echo $accont_id;?>" class="btn btn-success"><i class="fa fa-plus"></i> Add Comments</a>-->
     <a href="<?php echo $this->request->webroot ?>accounts/index/<?php echo $accont_id;?>" style="font-size:14px; margin-top: -10px;" class="btn btn-primary btn-blue-custom pull-right text-info"><i class="fa fa-angle-left"></i> Précédent</a>
</div>
     </br>

<div class="container-fluid content-container" style="">

    <div class="row">
        <div id="no-more-tables">
            <table class="col-md-12 table-bordered table-striped table-condensed cf" >
                <thead class="cf">
                    <tr>
                        
                        <th scope="col"><?= $this->Paginator->sort('account_id', 'Identifiant de Paiement') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('comment', 'Commentaire') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('user_id','Ajouté par') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('modified','Modifie') ?></th>
<!--                        <th scope="col" class="actions"><?= __('Action') ?></th>-->
                   </tr>
                </thead>
                <tbody>
                       <?php foreach ($comments as $comment): ?>
            <tr>
               
                <td data-title="Identifiant de Paiement"><?= $comment->has('account') ? $this->Html->link($comment->account->id, ['controller' => 'Accounts', 'action' => 'view', $comment->account->id]) : '' ?></td>
                <td data-title="Commentaire"><?php echo  substr($comment->comment,0,65);?>...</td>
                <td data-title="Ajouté par"><?= $comment->has('user') ? $this->Html->link($comment->user->user_name, ['controller' => 'Users', 'action' => 'view', $comment->user->id]) : '' ?></td>
                
                <td data-title="Modifie"><?php echo h(date('d.m.y',strtotime($comment->modified))); ?></td>
<!--                <td class="actions">
                    <?php 
                      // if($permission == 2){                                                                                              
                                
//				echo $this->Html->link(__('<i class="fa fa-edit"></i> Edit'), array('controller' => 'comments', 'action' => 'edit','id' =>$id,'accont_id'=>$accont_id,'com_id'=>$comment->id),array('class'=>'btn btn-success','escape'=>false)); 
//			
//			
//								echo $this->Form->postLink(__('<i class="fa fa-trash-o"></i> Delete'), array('controller' => 'comments','action' => 'delete' ,'id' =>$id,'accont_id'=>$accont_id,'com_id'=>$comment->id), ['escape'=>false,'class'=>'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $comment->id)]);
//                                                               // }
								//echo $this->Html->link(__('<i class="fa fa-trash"></i> Delete'), ['controller'=>'users','action' => 'delete', $user->id], ['escape'=>false,'class'=>'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $user->username)]);
                            
							//echo $this->Form->postLink(__('<i class="fa fa-trash"></i> Delete'), ['action' => 'delete', $user->id], ['escape'=>false,'class'=>'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $user->id)]); 
                                                       // } ?></td>-->
                    
                </td>
            </tr>
            <?php endforeach; ?>
                </tbody>
            </table>
            </form>
        </div>
    </div>

    <br />
    <div class="paginator">
        <ul class="pagination">
            <?php
                echo $this->Paginator->prev(__('retour'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                echo $this->Paginator->next(__('suivant'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
            ?>
        </ul>
        <p class="small"><?= $this->Paginator->counter(['format' => __('Page {{page}} de {{pages}}, affiche {{current}} entrée(s) sur un de total {{count}}')]) ?></p>
    </div>
</div>

