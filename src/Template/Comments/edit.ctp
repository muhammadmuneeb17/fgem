<?php
/**
  * @var \App\View\AppView $this
  */
?>
<?php
/**
  * @var \App\View\AppView $this
  */
$array = explode('/', $_SERVER['REQUEST_URI']);
$accId = end($array);
?>
<div class="panel-head-info">
    <p class="pull-left info-text">Edit Comments</p>
<!--    <a href="<?php echo $this->request->webroot ?>accounts/index?accont_id=<?php echo $accont_id;?>&id=<?php echo $id;?>" style="font-size:14px;" class="btn btn-primary btn-blue-custom pull-right text-info"><i class="fa fa-angle-left"></i> Go Back</a>-->
    <div class="clearfix"></div>

</div>
<div class="users form">
<?php echo $this->Form->create($comment,array('type'=>'file','class'=>'form-horizontal','id'=>'SliderAddForm')); ?>
    <fieldset>
        <div class="form-group required">
            <label for="title" class="col-md-3 control-label">Comment</label>
            <div class="col-md-10">
                
	<?php echo $this->Form->control('comment',array('div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
                <?php echo $this->Form->control('account_id',array('type'=>'hidden','value' => $id ,'div'=>false,'label'=>false,'class'=>'form-control','required')); 
                $userid = $this->request->session()->read('Auth.User.id');
                echo $this->Form->control('user_id', ['type' => 'hidden','value'=>$userid]);
                ?>
                
            </div> 
        </div>  
        
        
        
        </fieldset>
    <div class="form-group">
        <div class="col-md-12 text-center">
        <?php 
        echo $this->Form->control('Edit Comment',array('type'=>'submit','div'=>false,'label'=>false,'class'=>'btn btn-default btn-blue-custom btn-lg'));
        echo $this->Form->end(); ?>
        </div>
    </div>
        
                </div>

        
      
    
</div>