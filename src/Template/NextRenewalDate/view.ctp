<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\NextRenewalDate $nextRenewalDate
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Next Renewal Date'), ['action' => 'edit', $nextRenewalDate->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Next Renewal Date'), ['action' => 'delete', $nextRenewalDate->id], ['confirm' => __('Are you sure you want to delete # {0}?', $nextRenewalDate->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Next Renewal Date'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Next Renewal Date'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="nextRenewalDate view large-9 medium-8 columns content">
    <h3><?= h($nextRenewalDate->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($nextRenewalDate->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Year') ?></th>
            <td><?= $this->Number->format($nextRenewalDate->year) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Month') ?></th>
            <td><?= $this->Number->format($nextRenewalDate->month) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Day') ?></th>
            <td><?= $this->Number->format($nextRenewalDate->day) ?></td>
        </tr>
    </table>
</div>
