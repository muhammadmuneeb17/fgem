<div class="panel-head-info">
    <p class="pull-left info-text">Type de membres</p>
    <div class="clearfix"></div>
</div>
<div class="users form">
    <?php echo $this->Form->create($nextRenewalDate); ?>
    <div class="required col-md-12">
        <?php
        echo $this->Form->input('year', array('label' => 'Année', 'class' => 'form-control', 'required', 'options' => $years));
        ?>
    </div>

    <div class="required col-md-12">
        <?php
        echo $this->Form->input('amount', array('type' => 'text', 'label' => 'Montant', 'class' => 'form-control', 'required'));
        ?>
    </div>
    <div class="clearfix">&nbsp;</div>

    <div class="col-md-12 text-center">
        <?php
        echo $this->Form->control('Soumettre', array('type' => 'submit', 'div' => false, 'label' => false, 'class' => 'btn btn-default btn-blue-custom btn-lg'));
        echo $this->Form->end();
        ?>
    </div>
    <br/>
    <br/>
    <br/>
</div>