<?php
//debug($nextRenewalDate->year);
$year = $nextRenewalDate->year;
$month = $nextRenewalDate->month;
$day = $nextRenewalDate->day;
//debug($month);
?>
<div class="panel-head-info">
    <p class="pull-left info-text">Edit Next Payment Date</p>
    <div class="clearfix"></div>

</div>
<div class="users form">
    <?php echo $this->Form->create($nextRenewalDate,array('type'=>'file','class'=>'form-horizontal','id'=>'UserAddForm')); ?>


    <div class="required col-md-4">
        <label for="year" class="control-label">Select Year</label>
        <div class="">
            <?php  $args = array();
            echo $this->Form->input('year',array('options'=>$args,'div'=>false,'label'=>false,'class'=>'form-control','required', 'id' => 'year')); ?>
        </div> 
    </div>

    <div class="required col-md-4">
        <label for="month" class="control-label">Select Month</label>
        <div class="">

            <?php  $args = array();
            echo $this->Form->input('month',array('options'=>$args,'div'=>false,'label'=>false,'class'=>'form-control','required', 'id' => 'month')); ?>
        </div> 
    </div>

    <div class="required col-md-4">
        <label for="day" class="control-label">Select Day</label>
        <div class="">

            <?php  $args = array();
            echo $this->Form->input('day',array('options'=>$args,'div'=>false,'label'=>false,'class'=>'form-control','required', 'id' => 'day')); ?>
        </div> 
    </div>
    <div class="clearfix">&nbsp;</div>
    <div class="form-group">
        <div class="col-md-12 text-center">
            <?php 
            echo $this->Form->control('Update',array('type'=>'submit','div'=>false,'label'=>false,'class'=>'btn btn-default btn-blue-custom btn-lg'));
            echo $this->Form->end(); ?>
        </div>
    </div>

</div>

<script>
    $(document).ready(function(){
        var date = new Date();
        var year = date.getFullYear() - 1;
        var options = '<option value="">Select Year</option>';
        for(var i = 0; i < 3; i++){
            year = year + 1;
            options = options + '<option value="'+year+'">'+year+'</option>';

        }

        $('#year').html(options);
        $('#year').val('<?php echo $year;?>');

        $('#year').change(function(){
            var month = 0;
            var monthName = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];

            var options = '<option value="">Select Month</option>';
            var monthDisplayName = '';
            for(var i = 0; i < 12; i++){
                month = month + 1;

                monthDisplayName = monthName[i];
                if(month < 10){
                    options = options + '<option value="0'+month+'">'+monthDisplayName+'</option>';
                } else{
                    options = options + '<option value="'+month+'">'+monthDisplayName+'</option>';
                }
            } 
            $('#month').html(options);
        });

        $('#month').change(function(){
            var size = 31;
            var day = 0;
            var options = '<option value="">Select Month</option>';
            for(var i = 0; i < size; i++){
                day = day + 1;
                if(day < 10){
                    options = options + '<option value="0'+day+'">0'+day+'</option>';
                } else{
                    options = options + '<option value="'+day+'">'+day+'</option>';
                }
            }

            $('#day').html(options);

        });

        var month = 0;
        var monthName = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];

        var options = '<option value="">Select Month</option>';
        var monthDisplayName = '';
        for(var i = 0; i < 12; i++){
            month = month + 1;

            monthDisplayName = monthName[i];
            if(month < 10){
                options = options + '<option value="0'+month+'">'+monthDisplayName+'</option>';
            } else{
                options = options + '<option value="'+month+'">'+monthDisplayName+'</option>';
            }
        } 
        $('#month').html(options);

        if('<?php echo $month;?>' < 10){
            $('#month').val('0<?php echo $month;?>');
        } else{
            $('#month').val('<?php echo $month;?>');
        }



        var size = 31;
        var day = 0;
        var options = '<option value="">Select Month</option>';
        for(var i = 0; i < size; i++){
            day = day + 1;
            if(day < 10){
                options = options + '<option value="0'+day+'">0'+day+'</option>';
            } else{
                options = options + '<option value="'+day+'">'+day+'</option>';
            }
        }

        $('#day').html(options);
        $('#day').val('<?php echo $day;?>');

        if('<?php echo $day;?>' < 10){
            $('#day').val('0<?php echo $day;?>');
        } else{
            $('#day').val('<?php echo $day;?>');
        }


    });
</script>
