<?php

/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
  */
$monthName = ['1'=>"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
?>
<div class="panel-head-info">
    <!--<a href="<?php // echo $this->request->webroot ?>pages" style="font-size:14px;" class="btn btn-primary btn-blue-custom pull-right text-info"><i class="fa fa-angle-left"></i> Go Back</a>-->
    <div class="clearfix"></div>
    <?php 
    //if($permission == 2){
    ?>
    <p class="pull-left info-text">Votre prochaine échéance</p>
    <div class="clearfix"></div>

    <a href="<?php echo $this->request->webroot ?>NextRenewalDate/add" class="btn btn-success"><i class="fa fa-plus"></i> Ajouter la date de renouvellement suivante</a>
    <?php //} ?>
</div>
<div class="container content-container" style="">

    <div class="row">
        <div id="no-more-tables">
            <table class="col-md-12 table-bordered table-striped table-condensed cf">
                <thead class="cf">
                    <tr>
                        <th><?= $this->Paginator->sort('day','Journée') ?></th>
                        <th><?= $this->Paginator->sort('month','Mois') ?></th>
                        <th><?= $this->Paginator->sort('year','Année') ?></th>

                        <th class="actions"><?php echo __('Action'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($nextRenewalDate as $nextDate): 

                    ?>
                    <tr>
                        <td data-title="Journée"><?php echo $nextDate->day; ?></td>
                        <td data-title="Mois"><?php
                            
                         echo $monthName[$nextDate->month]; ?>&nbsp;</td>
                        <td data-title="Année"><?php echo h($nextDate->year); ?>&nbsp;</td>

                        <td  data-title="Action" class="actions">

                            <?php 

                            echo $this->Html->link(__('<i class="fa fa-edit"></i>'), array('controller' => 'NextRenewalDate', 'action' => 'edit',$nextDate->id),array('class'=>'btn btn-success','escape'=>false,'data-toggle'=>"tooltip",'title'=>'Modifier')); 

                            ?>
                            <?php
                            echo $this->Form->postLink(__('<i class="fa fa-trash-o"></i>'), ['action' => 'delete', $nextDate->id], ['escape'=>false,'class'=>'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $nextDate->day.'.'.$nextDate->month.'.'.$nextDate->year),'data-toggle'=>"tooltip",'title'=>'Effacer']);

                            ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

    <br />
    <div class="paginator">
        <ul class="pagination">
            <?php
            echo $this->Paginator->prev(__('retour'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
            echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
            echo $this->Paginator->next(__('suivant'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
            ?>
        </ul>
        <p class="small"><?= $this->Paginator->counter(['format' => __('Page {{page}} de {{pages}}, affiche {{current}} entrée(s) sur un de total {{count}}')]) ?></p>
    </div>
</div>