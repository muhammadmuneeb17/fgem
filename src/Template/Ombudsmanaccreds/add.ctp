<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Ombudsmanaccreds'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="ombudsmanaccreds form large-9 medium-8 columns content">
    <?= $this->Form->create($ombudsmanaccred) ?>
    <fieldset>
        <legend><?= __('Add Ombudsmanaccred') ?></legend>
        <?php
            echo $this->Form->control('ombudsman_id');
            echo $this->Form->control('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
