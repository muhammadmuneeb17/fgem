<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Ombudsmanaccred[]|\Cake\Collection\CollectionInterface $ombudsmanaccreds
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Ombudsmanaccred'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ombudsmanaccreds index large-9 medium-8 columns content">
    <h3><?= __('Ombudsmanaccreds') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ombudsman_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ombudsmanaccreds as $ombudsmanaccred): ?>
            <tr>
                <td><?= $this->Number->format($ombudsmanaccred->id) ?></td>
                <td><?= $this->Number->format($ombudsmanaccred->ombudsman_id) ?></td>
                <td><?= h($ombudsmanaccred->name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $ombudsmanaccred->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ombudsmanaccred->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $ombudsmanaccred->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ombudsmanaccred->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} de {{pages}}, affiche {{current}} entrée(s) sur un de total {{count}}')]) ?></p>
    </div>
</div>
