<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Ombudsmanaccred $ombudsmanaccred
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Ombudsmanaccred'), ['action' => 'edit', $ombudsmanaccred->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Ombudsmanaccred'), ['action' => 'delete', $ombudsmanaccred->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ombudsmanaccred->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Ombudsmanaccreds'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ombudsmanaccred'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="ombudsmanaccreds view large-9 medium-8 columns content">
    <h3><?= h($ombudsmanaccred->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($ombudsmanaccred->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($ombudsmanaccred->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ombudsman Id') ?></th>
            <td><?= $this->Number->format($ombudsmanaccred->ombudsman_id) ?></td>
        </tr>
    </table>
</div>
