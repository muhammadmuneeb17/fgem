<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Accreditation $accreditation
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Accreditation'), ['action' => 'edit', $accreditation->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Accreditation'), ['action' => 'delete', $accreditation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $accreditation->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Accreditations'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Accreditation'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Ombudsmanaccreds'), ['controller' => 'Ombudsmanaccreds', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ombudsmanaccred'), ['controller' => 'Ombudsmanaccreds', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="accreditations view large-9 medium-8 columns content">
    <h3><?= h($accreditation->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($accreditation->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($accreditation->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Ombudsmanaccreds') ?></h4>
        <?php if (!empty($accreditation->ombudsmanaccreds)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Ombudsman Id') ?></th>
                <th scope="col"><?= __('Accreditation Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($accreditation->ombudsmanaccreds as $ombudsmanaccreds): ?>
            <tr>
                <td><?= h($ombudsmanaccreds->id) ?></td>
                <td><?= h($ombudsmanaccreds->ombudsman_id) ?></td>
                <td><?= h($ombudsmanaccreds->accreditation_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Ombudsmanaccreds', 'action' => 'view', $ombudsmanaccreds->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Ombudsmanaccreds', 'action' => 'edit', $ombudsmanaccreds->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Ombudsmanaccreds', 'action' => 'delete', $ombudsmanaccreds->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ombudsmanaccreds->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
