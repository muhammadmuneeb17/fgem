<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\TicketCalendar[]|\Cake\Collection\CollectionInterface $ticketCalendars
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Ticket Calendar'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ticketCalendars index large-9 medium-8 columns content">
    <h3><?= __('Ticket Calendars') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('type') ?></th>
                <th scope="col"><?= $this->Paginator->sort('date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('assigned_by') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ticketCalendars as $ticketCalendar): ?>
            <tr>
                <td><?= $this->Number->format($ticketCalendar->id) ?></td>
                <td><?= $ticketCalendar->has('user') ? $this->Html->link($ticketCalendar->user->id, ['controller' => 'Users', 'action' => 'view', $ticketCalendar->user->id]) : '' ?></td>
                <td><?= $this->Number->format($ticketCalendar->type) ?></td>
                <td><?= h($ticketCalendar->date) ?></td>
                <td><?= $this->Number->format($ticketCalendar->assigned_by) ?></td>
                <td><?= h($ticketCalendar->created) ?></td>
                <td><?= h($ticketCalendar->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $ticketCalendar->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ticketCalendar->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $ticketCalendar->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ticketCalendar->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} de {{pages}}, affiche {{current}} entrée(s) sur un de total {{count}}')]) ?></p>
    </div>
</div>
