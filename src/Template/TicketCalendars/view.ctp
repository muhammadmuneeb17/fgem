<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\TicketCalendar $ticketCalendar
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Ticket Calendar'), ['action' => 'edit', $ticketCalendar->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Ticket Calendar'), ['action' => 'delete', $ticketCalendar->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ticketCalendar->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Ticket Calendars'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ticket Calendar'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="ticketCalendars view large-9 medium-8 columns content">
    <h3><?= h($ticketCalendar->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $ticketCalendar->has('user') ? $this->Html->link($ticketCalendar->user->id, ['controller' => 'Users', 'action' => 'view', $ticketCalendar->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($ticketCalendar->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Type') ?></th>
            <td><?= $this->Number->format($ticketCalendar->type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Assigned By') ?></th>
            <td><?= $this->Number->format($ticketCalendar->assigned_by) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($ticketCalendar->date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($ticketCalendar->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($ticketCalendar->modified) ?></td>
        </tr>
    </table>
</div>
