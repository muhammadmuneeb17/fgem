<?php

/**
  * @var \App\View\AppView $this
  */
?>
<div class="panel-head-info">
    <p class="pull-left info-text">Add Guide Content</p>
    <a href="<?php echo $this->request->webroot ?>documentguides/doclist" style="font-size:14px;" class="btn btn-primary btn-blue-custom pull-right text-info"><i class="fa fa-angle-left"></i> Go Back</a>
    <div class="clearfix"></div>

</div>
<div class="users form">
<?php echo $this->Form->create('documentguide',array('type'=>'file','class'=>'form-horizontal')); ?>
    <fieldset>
        <div class="form-group required">
            <label for="title" class="col-md-3 control-label">Title</label>
            <div class="col-md-10">
	<?php echo $this->Form->control('title',array('div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
            </div> 
        </div> 
        <div class="form-group required">
            <label for="description" class="col-md-3 control-label">Description</label>
            <div class="col-md-10">
    <?php echo $this->Form->control('description',array('type'=>'textarea','div'=>false,'label'=>false,'class'=>'form-control ckeditor','required')); ?>
            </div> 
        </div>   
        
        
       

    </fieldset>
    <div class="form-group">
        <div class="col-md-12 text-center">
        <?php 
        echo $this->Form->control('Add',array('type'=>'submit','div'=>false,'label'=>false,'class'=>'btn btn-default btn-blue-custom btn-lg'));
        echo $this->Form->end(); ?>
        </div>
    </div>
</div>