<div class="container margintop-20">
    <div class="row">
        <div class="col-sm-3 col-sm-offset-0">
            <?php 
            
    if($permission == 2){
    ?>
            <a href="<?php echo $this->request->webroot ?>documentguides/add" class="btn btn-primary btn-add-top"><i class="fa fa-plus"></i> Add Document</a>
             <?php } ?>
        </div>
    </div>
</div>
<div class="container content-container">

    <div class="row">
        <div id="no-more-tables">
           <form method="post" id="myForm" action="<?php echo $this->request->webroot; ?>documentguides/udapteorder">
            <table class="col-md-12 table-bordered table-striped table-condensed cf">
                
                <thead class="cf">
                    <tr>
                        <th><?php echo $this->Paginator->sort('title'); ?></th>
                        <th><?php echo $this->Paginator->sort('file'); ?></th>
                        <th><?php echo $this->Paginator->sort('status'); ?></th>
                        <th><?php echo $this->Paginator->sort('order'); ?></th>
                        <th><?php echo $this->Paginator->sort('modified'); ?></th>
                        <!--<th><?php // echo $this->Paginator->sort('modified'); ?></th>-->
                        <th class="actions"><?php echo __('Actions'); ?></th>
                    </tr>
                </thead>
                <tbody>
                            <?php foreach ($documentguides as $documentguide): ?>
                    <tr>
                        <td data-title="Title"><?php echo h($documentguide->title); ?>&nbsp;</td>
                        <td data-title="Photo"><a href="<?php echo $this->request->webroot; ?>img/documentguide/<?php echo h($documentguide->file); ?>" width='150'/><?php echo h($documentguide->file); ?></a></td>
                        <td data-title="Status"><?php 
                    if($documentguide->status == 0) {
                        $status = "Deactive";
                    }
                    else {
                        $status = "Active";
                    }
                    echo $status; ?>&nbsp</td>
                        <td data-title="Order">
                            <?php echo $this->Form->control('documentguide_order',array('type'=>'number','value'=>$documentguide->ordinal,'name'=>'data[ordinal][]','div'=>false,'label'=>false,'class'=>'form-control','required'=>'required')); ?>
                        <?php echo $this->Form->control('documentguide_id',array('type'=>'hidden','value'=>$documentguide->id,'name'=>'data[documentguide_id][]','div'=>false,'label'=>false,'class'=>'form-control')); ?>
                        <td data-title="Created"><?php echo h(date('d.m.y H:i:s',strtotime($documentguide->modified))); ?>&nbsp;</td>
                        <!--<td data-title="Modified"><?php // echo h($Silder['documentguide['modified); ?>&nbsp;</td>-->
                        <td  data-title="Actions" class="actions">
                              <?php 
                       if($permission == 2){
                           ?>
			<?php echo $this->Html->link(__('Edit'), array('controller' => 'documentguides', 'action' => 'edit',$documentguide->id),array('class'=>'btn btn-success')); ?>
			<?php 
                        echo $this->Html->link(__('<i class="fa fa-trash-o"></i> Delete'), ['action' => 'delete', $documentguide->id], ['escape'=>false,'class'=>'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $documentguide->title)]); ?>
		        <?php } ?>
                        </td>
                    </tr>
            <?php endforeach; ?>
                </tbody>
               
            </table>
               </form>
            <button class="btn btn-custom col-xs-offset-6 col-md-offset-6" style="margin-top:10px;margin-bottom:10px;" id="orderupdate_btn" >Update Order</button>
        </div>
    </div>
    <br />
    
        
    <div class="paginator">
        <ul class="pagination">
            <?php
                echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
            ?>
        </ul>
        
        <p class="small"><?= $this->Paginator->counter(['format' => __('Page {{page}} de {{pages}}, affiche {{current}} entrée(s) sur un de total {{count}}')]) ?></p>
    </div>
        
</div>
<script type="text/javascript">
    $(document).ready(function() {
       $("#orderupdate_btn").click(function() {
           $("#myForm").submit();
       });
    });
</script>