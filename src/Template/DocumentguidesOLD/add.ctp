<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="panel-head-info">
    <p class="pull-left info-text">Add Document</p>
    <a href="<?php echo $this->request->webroot ?>documentguides" style="font-size:14px;" class="btn btn-primary btn-blue-custom pull-right text-info"><i class="fa fa-angle-left"></i> Go Back</a>
    <div class="clearfix"></div>

</div>
<div class="users form">
<?php echo $this->Form->create('Documentguide',array('type'=>'file','class'=>'form-horizontal','id'=>'DocumentguideAddForm')); ?>
    <fieldset>
        <div class="form-group required">
            <label for="title" class="col-md-3 control-label">Title</label>
            <div class="col-md-10">
	<?php echo $this->Form->control('title',array('div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
            </div> 
        </div>   
        
        <div class="form-group required">
            <label for="image" class="col-md-1 control-label">File </label>
            <input type="file" name="doc" class="">
        </div>
        <?php	echo $this->Form->control('status',array('type'=>'hidden','div'=>false,'label'=>false,'class'=>'form-control','value'=>'1')); ?>
  
      
    </fieldset>
    <div class="form-group">
        <div class="col-md-12 text-center">
        <?php 
        echo $this->Form->control('Add Document',array('type'=>'submit','div'=>false,'label'=>false,'class'=>'btn btn-default btn-blue-custom btn-lg'));
        echo $this->Form->end(); ?>
        </div>
    </div>
</div>


<script>
$('#DocumentguideAddForm').submit(function (e) {
    if ($("#sectionImage").val() == "") {
        $('.mymsg').html('');
                $('.mymsg').append("<div class='error'>Documentguide image is missing</div>");
                $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top, scrollLeft: 0}, 300);
                return false;
    }
    if ($("#sectionImage").val() != "") {
                    var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
                    if ($.inArray($("#sectionImage").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                        $(this).find(".btn-file").css("border", "1px solid red");
                        alert("Invalid Documentguide image format,Only formats are allowed : " + fileExtension.join(', '));
                        e.preventDefault();
                    }
                }
});
</script>