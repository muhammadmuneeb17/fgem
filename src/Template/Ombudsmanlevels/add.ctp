<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Ombudsmanlevels'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Languages'), ['controller' => 'Languages', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Language'), ['controller' => 'Languages', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ombudsmanlevels form large-9 medium-8 columns content">
    <?= $this->Form->create($ombudsmanlevel) ?>
    <fieldset>
        <legend><?= __('Add Ombudsmanlevel') ?></legend>
        <?php
            echo $this->Form->control('ombudsman_id');
            echo $this->Form->control('language_id', ['options' => $languages]);
            echo $this->Form->control('language_level');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
