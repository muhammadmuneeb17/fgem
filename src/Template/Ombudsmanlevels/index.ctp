<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Ombudsmanlevel[]|\Cake\Collection\CollectionInterface $ombudsmanlevels
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Ombudsmanlevel'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Languages'), ['controller' => 'Languages', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Language'), ['controller' => 'Languages', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ombudsmanlevels index large-9 medium-8 columns content">
    <h3><?= __('Ombudsmanlevels') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ombudsman_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('language_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('language_level') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ombudsmanlevels as $ombudsmanlevel): ?>
            <tr>
                <td><?= $this->Number->format($ombudsmanlevel->id) ?></td>
                <td><?= $this->Number->format($ombudsmanlevel->ombudsman_id) ?></td>
                <td><?= $ombudsmanlevel->has('language') ? $this->Html->link($ombudsmanlevel->language->name, ['controller' => 'Languages', 'action' => 'view', $ombudsmanlevel->language->id]) : '' ?></td>
                <td><?= h($ombudsmanlevel->language_level) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $ombudsmanlevel->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ombudsmanlevel->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $ombudsmanlevel->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ombudsmanlevel->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} de {{pages}}, affiche {{current}} entrée(s) sur un de total {{count}}')]) ?></p>
    </div>
</div>
