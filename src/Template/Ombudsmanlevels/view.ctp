<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Ombudsmanlevel $ombudsmanlevel
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Ombudsmanlevel'), ['action' => 'edit', $ombudsmanlevel->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Ombudsmanlevel'), ['action' => 'delete', $ombudsmanlevel->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ombudsmanlevel->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Ombudsmanlevels'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ombudsmanlevel'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Languages'), ['controller' => 'Languages', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Language'), ['controller' => 'Languages', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="ombudsmanlevels view large-9 medium-8 columns content">
    <h3><?= h($ombudsmanlevel->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Language') ?></th>
            <td><?= $ombudsmanlevel->has('language') ? $this->Html->link($ombudsmanlevel->language->name, ['controller' => 'Languages', 'action' => 'view', $ombudsmanlevel->language->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Language Level') ?></th>
            <td><?= h($ombudsmanlevel->language_level) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($ombudsmanlevel->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ombudsman Id') ?></th>
            <td><?= $this->Number->format($ombudsmanlevel->ombudsman_id) ?></td>
        </tr>
    </table>
</div>
