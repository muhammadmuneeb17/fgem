<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Ombudsmanswears'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="ombudsmanswears form large-9 medium-8 columns content">
    <?= $this->Form->create($ombudsmanswear) ?>
    <fieldset>
        <legend><?= __('Add Ombudsmanswear') ?></legend>
        <?php
            echo $this->Form->control('place');
            echo $this->Form->control('date_of_auth');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
