<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Ombudsmanswear[]|\Cake\Collection\CollectionInterface $ombudsmanswears
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Ombudsmanswear'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ombudsmanswears index large-9 medium-8 columns content">
    <h3><?= __('Ombudsmanswears') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('place') ?></th>
                <th scope="col"><?= $this->Paginator->sort('date_of_auth') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ombudsmanswears as $ombudsmanswear): ?>
            <tr>
                <td><?= $this->Number->format($ombudsmanswear->id) ?></td>
                <td><?= h($ombudsmanswear->place) ?></td>
                <td><?= h($ombudsmanswear->date_of_auth) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $ombudsmanswear->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ombudsmanswear->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $ombudsmanswear->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ombudsmanswear->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} de {{pages}}, affiche {{current}} entrée(s) sur un de total {{count}}')]) ?></p>
    </div>
</div>
