<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Ombudsmanswear $ombudsmanswear
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Ombudsmanswear'), ['action' => 'edit', $ombudsmanswear->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Ombudsmanswear'), ['action' => 'delete', $ombudsmanswear->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ombudsmanswear->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Ombudsmanswears'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ombudsmanswear'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="ombudsmanswears view large-9 medium-8 columns content">
    <h3><?= h($ombudsmanswear->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Place') ?></th>
            <td><?= h($ombudsmanswear->place) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($ombudsmanswear->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date Of Auth') ?></th>
            <td><?= h($ombudsmanswear->date_of_auth) ?></td>
        </tr>
    </table>
</div>
