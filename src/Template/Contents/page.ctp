<?php if ($page['is_front'] == 0) { ?>
    <div class="titan-cta-area titan-cta-area-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="cta-text  wow fadeInRight" style="visibility: visible; animation-name: fadeInRight;">
                        <h2 class="text-center"><?php echo strtoupper($page['title']); ?></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php }
?>

<?php
/* -- Get the slider if active for current page -- */
if ($page['show_slider'] == 1) {
    echo $this->element('slider');
}
?>
<?php //debug($page); ?>
<?php if (empty($page['contents'])) { ?>
    <div class="satisfied-area section-padding">
        <div class="container">
            <h2 class='text-center'> No Content Found</h2>
        </div>
    </div>
<?php } else if ($page['is_contact'] == 0) { ?>
    <!-- check if page is not a contact page -->
    <?php
    $count = 0;
    foreach ($page['contents'] as $key => $page_data):
        ?>
        <!-- display content for default template  -->
        <?php if ($page_data['template'] == "DEFAULT") { ?>
            <!-- check if default template has image -->
            <?php if ($page_data['image']) { ?>
                <div class="introduce-area" style="background-image:url('<?php echo $this->request->webroot ?>img/content/<?php echo $page_data['image']; ?>')">
                    <div class="cover-overlay"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="introduce-content">
                                    <h2 class="text-center"><?php echo ucwords($page_data['title']); ?></h2>
                                    <p><?php echo ucfirst($page_data['description']); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <div class="satisfied-area section-padding">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="satisfied-content">
                                    <h2 style="font-size:28px;"><?php echo $page_data['title']; ?></h2>
                                    <?php echo $page_data['description']; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>

        <!-- display content for parallax template  -->
        <?php if ($page_data['template'] == "PARALLAX") { ?>
            <section class="parallax-section">
                <div class="parallax-overlay"></div>
                <div id="slide2" class="parallaxslide" style="background-image:url('<?php echo $this->request->webroot ?>img/content/<?php echo $page_data['image']; ?>')">
                    <?php if ($count / 2 == 0) { ?>
                        <div class="title pull-right">
                            <p>
                                <span class="highlight whitetxt"><?php echo $page_data['title']; ?></span>
                            </p>
                            <?php echo $page_data['description']; ?>
                        </div>
                    <?php } else { ?>
                        <div class="title pull-left">
                            <p>
                                <span class="highlight whitetxt"><?php echo $page_data['title']; ?></span>
                            </p>
                            <?php echo $page_data['description']; ?>
                        </div>
                    <?php } ?>
                    <div class="clearfix"></div>              
                </div>
            </section>
            <?php
            $count++;
        }
        ?>



    <?php endforeach; ?>


    <!-- check if page is a contact page -->
    <?php
} elseif ($page['is_contact'] == 1) {
    //debug($page);
    $count = count($page['contents']) - 1;
    ?>
    <div class="contact-page-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="contact-page-left">
                        <script type="text/javascript">
                            function initialize() {
                                var myLatlng = new google.maps.LatLng(<?php echo $page['contents'][$count]['lat'] ?>,<?php echo $page['contents'][$count]['lng'] ?>);

                                var mapOptions = {
                                    center: myLatlng,
                                    zoom: 17,
                                    disableDefaultUI: false,
                                    streetViewControl: false,
                                    //mapTypeId: google.maps.MapTypeId.DEFAULT,
                                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                                    mapTypeControl: true,
                                    mapTypeControlOptions: {
                                        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                                        position: google.maps.ControlPosition.RIGHT_BOTTOM

                                    }


                                };
                                var image = new google.maps.MarkerImage(
                                        "<?php echo $this->request->webroot ?>img/bluedot.png",
                                        null, // size
                                        null, // origin
                                        new google.maps.Point(8, 8), // anchor (move to center of marker)
                                        new google.maps.Size(17, 17) // scaled size (required for Retina display icon)
                                        );
                                var map = new google.maps.Map(document.getElementById('mymap'),
                                        mapOptions);

                                var marker = new google.maps.Marker({
                                    position: myLatlng,
                                    //icon: image,
                                    map: map,
                                    title: "Company Name"
                                });

                                //adding info window to the marker

                                var infowindow = new google.maps.InfoWindow({
                                    content: '<div class="info">Company Name</div>'
                                });

                                //adding event to the marker

                                google.maps.event.addListener(marker, 'click', function () {
                                    // Calling the open method of the infoWindow 
                                    infowindow.open(map, marker);
                                });
                                infoWindow = new google.maps.InfoWindow();
                                infowindow.open(map, marker);
                                google.maps.event.addListenerOnce(map, 'idle', function () {
                                    google.maps.event.trigger(map, 'resize');
                                    map.setCenter(myLatlng);

                                });

                            }
                            //google.maps.event.addDomListener(window, 'load', infowindow);
                            function changeMapTypeId(mapType) {
                                map.setMapTypeId(mapType);
                            }

                            function zoomInTheMap() {
                                var zoom = map.getZoom();
                                zoom = zoom + 2;
                                map.setZoom(zoom);
                            }
                        </script>
                        <?php if ($page['contents'][$count]['show_form'] == 1) { ?>
                            <h2>Écrivez-nous!</h2>
                            <div id="contact_results">

                            </div>
                            <form onsubmit="return false;" id="mail_form">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input type="text" name="fname" id="fname" placeholder="Prénom*" required="required" />
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" name="lname" id="lname" placeholder="Nom de famille*" required="required" />
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <!--
                                                                <div class="col-sm-6">
                                                                    <input type="text" name="phone" id="phone" placeholder="Téléphone*" required="required">
                                                                </div>
                                    -->
                                    <div class="col-sm-12">
                                        <input type="email" name="email" id="email" placeholder="adresse email*" required="required">
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <textarea name="message" placeholder="Message*" id="message" required="required"></textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="submit" value="Envoyer" id="submitForm">
                                        <p><img style="display:none;" alt="Sending ..." src="./img/ajax-loader.png" class="ajax-loader"></p>
                                    </div>
                                </div>
                            </form>
                            <?php
                        } else {
                            if ($page['contents'][$count]['lat'] != "" && $page['contents'][$count]['lng'] != "") {
                                ?>
                                <div class="map-box" >
                                    <div id="mymap"></div>
                                    <script>
                                        initialize();
                                    </script> 
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="contact-page-right">
                        <h2><?php echo $page['contents'][$count]['title']; ?></h2>
                        <div class="company-information">
                            <a href="tel:<?php echo $page['contents'][$count]['phone'] ?>">
                                <div class="company-information-icon">
                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                </div>
                                <?php echo $page['contents'][$count]['phone'] ?>
                            </a>
                            <?php if ($page['contents'][$count]['mobile']) { ?>
                                <a href="tel:<?php echo $page['contents'][$count]['mobile'] ?>">
                                    <div class="company-information-icon">
                                        <i class="fa fa-mobile" aria-hidden="true"></i>
                                    </div>
                                    <?php echo $page['contents'][$count]['mobile']; ?>
                                </a>
                            <?php } ?>
                            <a href="mailto:<?php echo $page['contents'][$count]['email'] ?>">
                                <div class="company-information-icon">
                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                </div>
                                <?php echo $page['contents'][$count]['email'] ?>
                            </a>
                            <a class="company-information-home">
                                <div class="company-information-icon">
                                    <i class="fa fa-home" aria-hidden="true"></i>
                                </div>
                                <?php echo $page['contents'][$count]['description'] ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <?php if ($page['contents'][$count]['lat'] != "" && $page['contents'][$count]['lng'] != "") {
                ?>
                <div class="map-box" >
                    <div id="mymap"></div>
                    <script>
                        initialize();
                    </script> 
                </div>
            <?php } ?>


        </div>
    </div>


<?php } ?>


