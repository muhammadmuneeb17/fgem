<div class="subpage form">    
	<?php echo $this->Form->create($content,array('type'=>'file','class'=>'form-horizontal','id' => 'ContactEditForm')); ?>
    <fieldset>
        <legend><p class="text-center info-text" >You Are Editing Contact Page</strong></p>
            <a href="<?php echo $this->request->webroot ?>contentTypes/listPages/" style="font-size:14px;" class="pull-right text-info">[Go Back]</a>
        </legend>
        <div class="form-group required">
            <label for="Title" class="col-md-2 control-label">Title</label>
            <div class="col-md-10">
	<?php echo $this->Form->input('title',array('div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
	<?php echo $this->Form->input('content_type_id',array('value'=>$id,'type'=>'hidden','div'=>false,'label'=>false,'class'=>'form-control')); ?>
            </div>   
        </div> 
<div class="form-group required">
            <label for="lat" class="col-md-2 control-label remove_required">Latitude</label>
            <div class="col-md-10">
	<?php echo $this->Form->input('lat',array('div'=>false,'label'=>false,'class'=>'form-control','id'=>'lat')); ?>
            </div>   
        </div> 
<div class="form-group required">
            <label for="lng" class="col-md-2 control-label remove_required">Longitude</label>
            <div class="col-md-10">
	<?php echo $this->Form->input('lng',array('div'=>false,'label'=>false,'class'=>'form-control','id'=>'lng')); ?>
            </div>   
        </div> 
<div class="form-group required">
            <label for="phone" class="col-md-2 control-label">Phone</label>
            <div class="col-md-10">
	<?php echo $this->Form->input('phone',array('div'=>false,'label'=>false,'class'=>'form-control','id'=>'phone','required')); ?>
            </div>   
        </div>
        <div class="form-group required">
            <label for="phone" class="col-md-2 control-label remove_required">Mobile</label>
            <div class="col-md-10">
	<?php echo $this->Form->input('mobile',array('div'=>false,'label'=>false,'class'=>'form-control','id'=>'mobile')); ?>
            </div>   
        </div>
<div class="form-group required">
            <label for="email" class="col-md-2 control-label">Email</label>
            <div class="col-md-10">
	<?php echo $this->Form->input('email',array('div'=>false,'label'=>false,'class'=>'form-control','id'=>'email','required')); ?>
            </div>   
        </div> 		
        <div class="form-group required">
            <label for="description" class="col-md-2 control-label">Address</label>	
            <div class="col-md-10">
            <?php echo $this->Form->input('description',array('type'=>'textarea','div'=>false,'label'=>false,'class'=>'form-control ckeditor')); ?>
            </div>   
        </div>   
		<!-- <div class="form-group required">
		 <label for="template" class="col-md-2 control-label">Template</label>	
		   <div class="col-md-10">
        <?php  //$temp_args = array(''=>'Select Template','DEFAULT'=>'DEFAULT','PARALLAX'=>'PARALLAX');
		 ?>
		</div>
</div>-->
        <div class="form-group required">
            <label for="status" class="col-md-2 control-label">Status</label>	
            <div class="col-md-10"> 
                
              <?php  
              echo $this->Form->input('template',array('type'=>'hidden','value'=>'DEFAULT','div'=>false,'label'=>false,'class'=>'form-control','required'));
              $args = array(''=>'Select Status','0'=>'Inactive','1'=>'Active');
		echo $this->Form->input('status',array('options'=>$args,'default'=>'','div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
            </div>
        </div>
<!--        <div class="form-group required">
            <label for="photo" class="col-md-2 control-label remove_required">Photo </label>	
            <div class="col-md-10"> 
                <span class="btn btn-default btn-file">
                    Browse <input type="file" name="image" class="section-image" id="sectionImage" onchange="readURL(this)">
                </span>
				<small class="text-danger">[Best size: 1050px * 800px]</small>
                <br /><img id="show_section_uploaded_image"  src="<?php if(isset($content->image)) { echo $this->request->webroot; ?>img/content/thumbnail/<?php echo h($content->image); } ?>"  class="section-up-image" alt=""/>
	<?php	//echo $this->Form->input('photo',array('type'=>'file','div'=>false,'label'=>false,'class'=>'form-control')); ?>
            
			</div> 
			
        </div>   -->
		<?php // if($content && $content->image != "" && $content->template == "DEFAULT") { ?>
<!--			<div class="form-group required" id="remove_image_box">
            <label for="remove_image" class="col-md-2 control-label remove_required">Remove Photo </label>	
				   <div class="col-md-10 checkbox"> 
					<input type="checkbox" name="remove_image" value="1" id="remove_image" class=""/>
				  </div>
			</div>-->
		<?php // } ?>
		<div class="form-group required">
            <label for="show_contact" class="col-md-2 control-label remove_required">Show Contact Form</label>	
				   <div class="col-md-10 checkbox"> 
					<input type="checkbox" name="show_form" value="1" id="show_contact" <?php if($content->show_form == 1) { echo "checked='checked'"; } ?>/>
				  </div>
			</div>
    </fieldset>
    <div class="form-group">
        <div class="col-md-11 text-center">
        <?php 
        echo $this->Form->input('Update',array('type'=>'submit','div'=>false,'label'=>false,'class'=>'btn btn-default btn-blue-custom btn-lg'));
        echo $this->Form->end(); ?>
        </div>
    </div>
</div>

<script>
    $('#ContactEditForm').submit(function (e) {
	    var $container = $("html,body");
        var $scrollTo = $('.mymsg');
        var title = $("#title").val().length;
		var phone = $("#phone").val();
        var mobile = $("#mobile").val();
		var email = $("#email").val();
        var description = CKEDITOR.instances.description.getData().length;
        var status = $("#status").val();
        var photo = $("#sectionImage").val();
		var template = $("#template").val();
		var lat = $("#lat").val();
		var lng = $("#lng").val();
		var email_pat = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var number_pat = /^[+]*[ 0-9 ]+$/;
		var imgsrc = $("#show_section_uploaded_image").attr('src');
        if (title < 1) {
            $('.mymsg').html('');
            $('.mymsg').append("<div class='error'>Title is missing or too short</div>");
            $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top, scrollLeft: 0}, 300);
            return false;
        }
		if (lat != "" && lng == "") {
            $('.mymsg').html('');
            $('.mymsg').append("<div class='error'>Longitude value is missing</div>");
            $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top, scrollLeft: 0}, 300);
            return false;
        }
		if (lat == "" && lng != "") {
            $('.mymsg').html('');
            $('.mymsg').append("<div class='error'>Latitude value is missing</div>");
            $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top, scrollLeft: 0}, 300);
            return false;
        }
		if (email == "") {
                     $('.mymsg').html('');
            $('.mymsg').append("<div class='error'>Email is required</div>");
            $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top, scrollLeft: 0}, 300);
            return false;
                }
           if (!email_pat.test(email))
                {
                   $('.mymsg').html('');
            $('.mymsg').append("<div class='error'>Invalid Email Address</div>");
            $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top, scrollLeft: 0}, 300);
            return false;
                }
             if (phone == "" || !number_pat.test(phone)) {
                      $('.mymsg').html('');
            $('.mymsg').append("<div class='error'>Phone number missing or invalid</div>");
            $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top, scrollLeft: 0}, 300);
            return false;
                }
	  if(mobile != "") {
              
              if (!number_pat.test(mobile)) {
                      $('.mymsg').html('');
            $('.mymsg').append("<div class='error'>Mobile number is invalid</div>");
            $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top, scrollLeft: 0}, 300);
            return false;
                }
          }	
        if (description < 1) {
            $('.mymsg').html('');
            $('.mymsg').append("<div class='error'>Address is missing or too short</div>");
            $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top, scrollLeft: 0}, 300);
            return false;
        }
        if (status == "") {
            $('.mymsg').html('');
            $('.mymsg').append("<div class='error'>Select status</div>");
            $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top, scrollLeft: 0}, 300);
            return false;
        }
		if(template == "PARALLAX" && imgsrc == "" ) {
        if (photo == "") {
            $('.mymsg').html('');
            $('.mymsg').append("<div class='error'>Image is missing</div>");
            $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top, scrollLeft: 0}, 300);
            return false;
        } 
		}
        if (photo != "") {
            var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
            if ($.inArray(photo.split('.').pop().toLowerCase(), fileExtension) == -1) {
                $(this).find(".btn-file").css("border", "1px solid red");
                alert("Invalid content image format,Only formats are allowed : " + fileExtension.join(', '));
                e.preventDefault();
            }
        }
    });
	
	$("#template").on("change",function(){
		if(this.value == "PARALLAX") {
			$("#remove_image_box").hide();
			$("#remove_image").prop('checked', false);
		} else if(this.value == "DEFAULT") {
			$("#remove_image_box").show();
		}
	});
</script>