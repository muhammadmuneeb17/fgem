<div class="subpage form">    
	<?php echo $this->Form->create($content,array('type'=>'file','class'=>'form-horizontal','id' => 'SubPageEditForm')); ?>
    <fieldset>
        <legend><p class="text-center info-text" >Vous êtes en train de modifier une sous-page de <strong><?php echo $content->content_type['title']; ?></strong></p>
            <a href="<?php echo $this->request->webroot ?>contents/listSubPages/<?php echo $content->content_type['id']; ?>" style="font-size:14px;" class="pull-right text-info">[Précédent]</a>
        </legend>
        <div class="form-group required">
            <label for="Title" class="col-md-2 control-label">Titre</label>
            <div class="col-md-10">
	<?php echo $this->Form->input('title',array('div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
	<?php echo $this->Form->input('content_type_id',array('value'=>$content->content_type['id'],'type'=>'hidden','div'=>false,'label'=>false,'class'=>'form-control')); ?>
            </div>   
        </div>   
        <div class="form-group required">
            <label for="description" class="col-md-2 control-label">La description</label>	
            <div class="col-md-10">
            <?php echo $this->Form->input('description',array('type'=>'textarea','div'=>false,'label'=>false,'class'=>'form-control ckeditor','required')); ?>
            </div>   
        </div>   
		<div class="form-group required">
		 <label for="template" class="col-md-2 control-label">Modèle</label>	
		   <div class="col-md-10">
<?php  $temp_args = array(''=>'Choisir un Modèle','DEFAULT'=>'Défaut','PARALLAX'=>'Parallaxe');
		echo $this->Form->input('template',array('options'=>$temp_args,'default'=>'','div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
		</div>
</div>
        <div class="form-group required">
            <label for="status" class="col-md-2 control-label">Statut</label>	
            <div class="col-md-10"> 
              <?php  $args = array(''=>'Sélectionnez le Statut','0'=>'Inactive','1'=>'Active');
		echo $this->Form->input('status',array('options'=>$args,'default'=>'','div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
            </div>
        </div>
        <div class="form-group required">
            <label for="photo" class="col-md-2 control-label remove_required">Photo </label>	
            <div class="col-md-10"> 
                <span class="btn btn-default btn-file">
                    Feuilleter <input type="file" name="image" class="section-image" id="sectionImage" onchange="readURL(this)">
                </span>
				<small class="text-danger">[Meilleure Taille: 1050px * 800px]</small>
                <br /><img id="show_section_uploaded_image"  src="<?php if($content->image) { echo $this->request->webroot; ?>img/content/thumbnail/<?php echo h($content->image); } ?>"  class="section-up-image" alt=""/>
	<?php	//echo $this->Form->input('photo',array('type'=>'file','div'=>false,'label'=>false,'class'=>'form-control')); ?>
            
			</div> 
			
        </div>   
		<?php if($content->image != "" && $content->template == "DEFAULT") { ?>
			<div class="form-group required" id="remove_image_box">
            <label for="remove_image" class="col-md-2 control-label remove_required">Remove Photo </label>	
				   <div class="col-md-10 checkbox"> 
					<input type="checkbox" name="remove_image" value="1" id="remove_image" class=""/>
				  </div>
			</div>
		<?php } ?>
    </fieldset>
    <div class="form-group">
        <div class="col-md-11 text-center">
        <?php 
        echo $this->Form->input('Enregistrer',array('type'=>'submit','div'=>false,'label'=>false,'class'=>'btn btn-default btn-blue-custom btn-lg'));
        echo $this->Form->end(); ?>
        </div>
    </div>
</div>

<script>
    $('#SubPageEditForm').submit(function (e) {
	    var $container = $("html,body");
        var $scrollTo = $('.mymsg');
        var title = $("#title").val().length;
        var description = CKEDITOR.instances.description.getData().length;
        var status = $("#status").val();
        var photo = $("#sectionImage").val();
		var template = $("#template").val();
		var imgsrc = $("#show_section_uploaded_image").attr('src');
        if (title < 1) {
            $('.mymsg').html('');
            $('.mymsg').append("<div class='error'>Title is missing or too short</div>");
            $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top, scrollLeft: 0}, 300);
            return false;
        }
        if (description < 1) {
            $('.mymsg').html('');
            $('.mymsg').append("<div class='error'>Description is missing or too short</div>");
            $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top, scrollLeft: 0}, 300);
            return false;
        }
        if (status == "") {
            $('.mymsg').html('');
            $('.mymsg').append("<div class='error'>Select status</div>");
            $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top, scrollLeft: 0}, 300);
            return false;
        }
		if(template == "PARALLAX" && imgsrc == "" ) {
        if (photo == "") {
            $('.mymsg').html('');
            $('.mymsg').append("<div class='error'>Image is missing</div>");
            $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top, scrollLeft: 0}, 300);
            return false;
        } 
		}
        if (photo != "") {
            var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
            if ($.inArray(photo.split('.').pop().toLowerCase(), fileExtension) == -1) {
                $(this).find(".btn-file").css("border", "1px solid red");
                alert("Invalid content image format,Only formats are allowed : " + fileExtension.join(', '));
                e.preventDefault();
            }
        }
    });
	
	$("#template").on("change",function(){
		if(this.value == "PARALLAX") {
			$("#remove_image_box").hide();
			$("#remove_image").prop('checked', false);
		} else if(this.value == "DEFAULT") {
			$("#remove_image_box").show();
		}
	});
</script>