<div class="container content-container" style="max-width:800px">
    <br />
    <legend><p class="text-center info-text" >Vous consultez la sous-page de <strong><?php echo $content->content_type['title']; ?></strong></p>
        <a href="<?php echo $this->request->webroot ?>contents/listSubPages/<?php echo $content->content_type['id']; ?>" style="font-size:14px;" class="pull-right text-info">[Précédent]</a>
    </legend>
    <div class="row">
        <div>
            <table class="col-md-12 table-bordered table-striped table-condensed cf viewtbl">


                <tr>
                    <th>Titre</th>
                    <td><?php 
                        if($content->title) {
                            echo h(ucwords($content->title)); } else { echo "No Title"; } ?>&nbsp;</td>
                </tr>
                <tr>
                    <th>La description</th>
                    <td><?php echo $content->description; ?>&nbsp;</td>
                </tr>
                <tr>
                    <th>Modèle</th>
                    <td><?php  echo $content->template; ?>&nbsp;</td>
                </tr>
                <th>Image</th>
                <td>
                    <?php if($content->image) { ?>
                    <img src="<?php echo $this->request->webroot ?>img/content/thumbnail/<?php echo $content->image; ?>" alt="No Image Found"/>
                    <?php } else { ?>
                    No Image Found
                    <?php } ?>
                    &nbsp;</td>
                </tr>
            <tr>
                <th>Statut</th>
                <td><?php if($content->status == 1) { $status = "Active"; } else { $status = "In Active"; } echo $status; ?>&nbsp;</td>
            </tr>

            <tr>
                <th>Modifie</th>
                <td><?php echo h(date('d.m.y',strtotime($content->modified))); ?>&nbsp;</td>
            </tr>   
            </table>
    </div>
</div>
<br />

</div>
