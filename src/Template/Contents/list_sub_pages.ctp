<div class="container margintop-20">
    <h2 class="text-center info-text" >Vous regardez <strong><?php echo $contentType['title']; ?></strong> Sous pages</h2>
	<div class="row">
        <div class="col-sm-12 col-sm-offset-0">
			
            <a href="<?php echo $this->request->webroot ?>contents/addSubpage/<?php echo $contentType['id']; ?>" class="btn btn-primary btn-add-top"><i class="fa fa-plus"></i> Ajouter une sous-page</a>
			 <a href="<?php echo $this->request->webroot ?>ContentTypes/listPages/<?php echo $contentType['id']; ?>" style="font-size:14px;" class="pull-right text-info">[Précédent]</a>
	   </div>
    </div>
</div>
<div class="container content-container">

    <div class="row">
        <div id="no-more-tables">
		<form method="post" id="myForm" action="<?php echo $this->request->webroot; ?>Contents/udapteorder">
            <table class="col-md-12 table-bordered table-striped table-condensed cf">
                <thead class="cf">
                    <tr>
                        <th><?php echo $this->Paginator->sort('Titre'); ?></th>
                        <th><?php echo $this->Paginator->sort('photo'); ?></th>
						<th><?php echo $this->Paginator->sort('Modèle'); ?></th>
                        <th><?php echo $this->Paginator->sort('Statut'); ?></th>
						<th><?php echo $this->Paginator->sort('Ordinale'); ?></th>
                        <th><?php echo $this->Paginator->sort('Modifie'); ?></th>
                        <!--<th><?php // echo $this->Paginator->sort('modified'); ?></th>-->
                        <th class="actions"><?php echo __('Action'); ?></th>
                    </tr>
                </thead>
                <tbody>
                            <?php foreach ($subpages as $subpage): ?>
                    <tr>
                        <td data-title="Titre"><?php echo h($subpage->title); ?>&nbsp;</td>
						
						<td data-title="photo">
						<?php if($subpage->image) { ?>
						<img src="<?php echo $this->request->webroot; ?>img/content/thumbnail/<?php echo h($subpage->image); ?>" width='150'/>
						<?php } else { ?>
						<img src="<?php echo $this->request->webroot; ?>img/content/thumbnail/no-image.svg" width='150'/>
						<?php } ?>
						</td>
                        <td data-title="Modèle"><?php echo h($subpage->template); ?>&nbsp;</td>
						<td data-title="Statut"><?php 
                    if($subpage->status == 0) {
                        $status = "Inactive";
                    }
                    else {
                        $status = "Active";
                    }
                    echo $status; ?>&nbsp</td>
					<td data-title="Ordinale">
					
		
        
                            <?php echo $this->Form->control('ordinal',array('type'=>'number','value'=>$subpage->ordinal,'name'=>'data[ordinal][]','div'=>false,'label'=>false,'class'=>'form-control','required'=>'required','style'=>'max-width:80px;')); ?>
                        <?php echo $this->Form->control('content_id',array('type'=>'hidden','value'=>$subpage->id,'name'=>'data[content_id][]','div'=>false,'label'=>false,'class'=>'form-control')); ?>
						 <?php echo $this->Form->control('content_type_id',array('type'=>'hidden','value'=>$contentType['id'],'name'=>'data[content_type_id]','div'=>false,'label'=>false,'class'=>'form-control')); ?>
						</td>
                        <td data-title="Modifie"><?php echo h(date('d.m.y',strtotime($subpage->modified))); ?>&nbsp;</td>
                       
                        <td  data-title="Action" class="actions">
			<?php echo $this->Html->link(__('Vue'), array('controller' => 'contents', 'action' => 'viewSubpage',$subpage->id),array('class'=>'btn btn-primary')); ?>
                        <?php echo $this->Html->link(__('Modifier'), array('controller' => 'contents', 'action' => 'editSubpage',$subpage->id),array('class'=>'btn btn-success')); ?>
			<?php 
                        echo $this->Html->link(__('<i class="fa fa-trash-o"></i> Effacer'), ['action' => 'delete', $subpage->id], ['escape'=>false,'class'=>'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $subpage->title)]); 
                        
                        ?>
		
		</td>
                    </tr>
<?php endforeach; ?>
                </tbody>
            </table>
			</form>
				<button class="btn btn-custom col-xs-offset-6 col-md-offset-6" style="font-weight:bold;width:150px;margin-top:10px;margin-bottom:10px;" id="orderupdate_btn" >Update</button>
        </div>
    </div>
    <br />
    
        
    <div class="paginator">
        <ul class="pagination">
            <?php
                echo $this->Paginator->prev(__('Précédent'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                echo $this->Paginator->next(__('Suivante'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
            ?>
        </ul>
        
        <p class="small"><?= $this->Paginator->counter(['format' => __('Page {{page}} de {{pages}}, affiche {{current}} entrée(s) sur un de total {{count}}')]) ?></p>
    </div>
        
</div>
<script type="text/javascript">
    $(document).ready(function() {
       $("#orderupdate_btn").click(function() {
           $("#myForm").submit();
       });
    });
</script>