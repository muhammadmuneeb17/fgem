
<style>
    .header-top-area {
        position: absolute;
        z-index: 999999;
        width: 100%;
    }
    
    .header-top-right a {
        color: #fff;
        text-shadow: 1px 1px 1px #000;
        font-weight: bold;
    }
    .header-area a {
        color: #fff !important;
        text-shadow: 1px 1px 1px #000;
        font-weight: bold;
    }

    #sentence {
        overflow: hidden;
        padding: 20px;
    }

    #sentence > li, #sentence > ul { display: inline; }

    #textSlider {
        overflow: visible !important;
        text-align: left;
        display: inline;
        position: relative;
        height: 16px;
    }

    .adj {
        white-space: nowrap;
        list-style: none;
        position: absolute;
        line-height: .3em;
        -webkit-transform: translateY(60px);
        -ms-transform: translateY(60px);
        transform: translateY(60px);
    }

    .slide-in {
        -webkit-transform: translateY(0);
        -ms-transform: translateY(0);
        transform: translateY(0);
        -webkit-transition: 0.3s;
        transition: 0.3s;
    }

    .slide-out {
        -webkit-transform: translateY(-60px);
        -ms-transform: translateY(-60px);
        transform: translateY(-60px);
        -webkit-transition: 0.3s;
        transition: 0.3s;
    }


</style>



<script>
    var adjs = [];

</script>

<?php 

foreach($sliders as $slider){

?>
<script>
    adjs.push('<?php echo $slider->title; ?>');

</script>
<?php
                            } 
?>

<script>
    function test(){
        //prepare Your data array with img urls

        //start with id=0 after 5 seconds
        var thisId=0;

        window.setInterval(function(){
            $('#thisImg').html("<li>"+adjs[thisId]+"</li>");
            
            thisId++; //increment data array id
            if (thisId== adjs.length - 1) thisId=0; //repeat from start

        },2000);        
    }
</script>
<!-- slider-area Start -->
<div class="slider-area">
    <div class="home-page-sliders">

        <div class="home-page-single-slider" style="background-image:url('<?php echo $this->request->webroot?>img/slider/<?php echo $sliders[0]->image; ?>')">
            <div class="home-page-single-slider-table">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="slider-text">
                                <div class="">
                                    <div class="text-center">
                                        <h2 class="slider_title wow fadeInDown">
                                        <?php echo $slider->title; ?></h2>
                                        <?php echo $slider->image_alt; ?>
                                        

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
<!-- slider-area End -->
<div class="clearfix">&nbsp;</div>

<!-- Button trigger modal -->
<div class="container">
    <div class="row">
        <a href="#exampleModal" data-toggle="modal">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="modal_btn">
                    <div class="pull-left">
                        <img class="modal_image" src="<?php echo $this->request->webroot;?>img/modalimage.jpg" alt="">
                    </div>
                    <div class="pull-left mg_left">
                        <h4 class="">
                            Journée d’études sur la justice restaurative
                        </h4>
                        <span><i class="fa fa-calendar"></i> 22.05.2019</span>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <span><i class="fa fa-clock-o"></i> 09:00</span>
                    </div>
                </div>
            </div>
        </a>
        <div class="clearfix">&nbsp;</div>
    </div>
</div>
<!-- Modal -->







