<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $ticketConversation->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $ticketConversation->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Ticket Conversations'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Tickets'), ['controller' => 'Tickets', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ticket'), ['controller' => 'Tickets', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ticketConversations form large-9 medium-8 columns content">
    <?= $this->Form->create($ticketConversation) ?>
    <fieldset>
        <legend><?= __('Edit Ticket Conversation') ?></legend>
        <?php
            echo $this->Form->control('ticket_id', ['options' => $tickets, 'empty' => true]);
            echo $this->Form->control('assigned_to');
            echo $this->Form->control('message');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
