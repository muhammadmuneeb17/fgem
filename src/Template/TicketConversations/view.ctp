<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\TicketConversation $ticketConversation
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Ticket Conversation'), ['action' => 'edit', $ticketConversation->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Ticket Conversation'), ['action' => 'delete', $ticketConversation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ticketConversation->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Ticket Conversations'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ticket Conversation'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tickets'), ['controller' => 'Tickets', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ticket'), ['controller' => 'Tickets', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="ticketConversations view large-9 medium-8 columns content">
    <h3><?= h($ticketConversation->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Ticket') ?></th>
            <td><?= $ticketConversation->has('ticket') ? $this->Html->link($ticketConversation->ticket->id, ['controller' => 'Tickets', 'action' => 'view', $ticketConversation->ticket->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($ticketConversation->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Assigned To') ?></th>
            <td><?= $this->Number->format($ticketConversation->assigned_to) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($ticketConversation->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($ticketConversation->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Message') ?></h4>
        <?= $this->Text->autoParagraph(h($ticketConversation->message)); ?>
    </div>
</div>
