<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\TicketConversation[]|\Cake\Collection\CollectionInterface $ticketConversations
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Ticket Conversation'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tickets'), ['controller' => 'Tickets', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ticket'), ['controller' => 'Tickets', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ticketConversations index large-9 medium-8 columns content">
    <h3><?= __('Ticket Conversations') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ticket_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('assigned_to') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ticketConversations as $ticketConversation): ?>
            <tr>
                <td><?= $this->Number->format($ticketConversation->id) ?></td>
                <td><?= $ticketConversation->has('ticket') ? $this->Html->link($ticketConversation->ticket->id, ['controller' => 'Tickets', 'action' => 'view', $ticketConversation->ticket->id]) : '' ?></td>
                <td><?= $this->Number->format($ticketConversation->assigned_to) ?></td>
                <td><?= h($ticketConversation->created) ?></td>
                <td><?= h($ticketConversation->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $ticketConversation->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ticketConversation->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $ticketConversation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ticketConversation->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} de {{pages}}, affiche {{current}} entrée(s) sur un de total {{count}}')]) ?></p>
    </div>
</div>
