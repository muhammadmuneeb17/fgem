<?php ?>
<div class="panel-head-info">
        <p class="pull-left info-text">What would you like to do today?</p>
    <div class="clearfix"></div>
</div>
<div class="container content-container">
    <div class="row">
        <div class="col-lg-4">
            <?php 
            echo $this->Html->link(
                    '<i class="fa fa-edit"></i> Add Page',
                    '/contentTypes/addPage',
                    ['class' => 'btn btn-primary btn-large select-btn','escape'=>false]); 
            ?>
        </div>
        <div class="col-lg-4">
            <?php 
            echo $this->Html->link(
                    '<i class="fa fa-list"></i> List Pages',
                    '/contentTypes/listPages',
                    ['class' => 'btn btn-success btn-large select-btn','escape'=>false]); 
            ?>
        </div>
        <div class="col-lg-4">
            <?php 
            echo $this->Html->link(
                    '<i class="fa fa-envelope"></i> View Contacts',
                    '/contacts/contactlist',
                    ['class' => 'btn btn-warning btn-large select-btn','escape'=>false]); 
            ?>
        </div>
    </div>
</div>