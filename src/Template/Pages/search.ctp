<?php 
use Cake\Datasource\ConnectionManager;
?>
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    } 
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/css/select2.min.css" rel="stylesheet" />
<div class="container-fluid" style="min-height:530px;">
    <div class="">
        <div class="col-md-12 search_bg">
            <div style="padding-top: 10px;">
                <h3 style="" class="col-md-7 col-sm-12 f8_labels">Membres</h3>
                <div class="">
                <form method="get" action="<?php echo $this->request->webroot . 'Pages/search'; ?>">
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <?php  if(isset($this->request->params['?']['keyword'])) {
                        $keyword = $this->request->params['?']['keyword'];
                    } else {
                        $keyword = "";
                    }?>
                        <?php
                        echo $this->Form->control('keyword', [
                            'class' => 'form-control searchbox', 'label' => false, 'placeholder' => 'recherché par nom, téléphone, Votre identifiant (adresse mail valide)', 'style' => 'height:40px;color:#fff;','id' => 'first_name','required','value' => $keyword]);
                        ?>
                    </div>
                    <div class="col-md-1 col-sm-12 col-xs-12" style="padding-left:0px;"> 
                        <button  class="btn btn-blue-custom pull-right" style="background:none;border:none;color:#fff"><i class="fa fa-search"></i> Chercher</button>
                    </div>

                </form>
            </div>
            </div>
        </div>
        <div class="col-md-3 search-aside">
        <h3 class="filter_heading">Filter</h3>
        <form method="get" action="<?php echo $this->request->webroot . 'Pages/search'; ?>">
            <div class="">
                <div class="col-md-12">
                    <?php  if(isset($this->request->params['?']['key_word'])) {
                        $key_word = $this->request->params['?']['key_word'];
                    } else {
                        $key_word = "";
                    }?>
                    <label class="filter_label" for="key_words">Chercher mots-clés</label>
                    <input type="text" style="height:35px;" name="key_word" placeholder="Recherche de membres ..." class="form-control searchbox" value="<?php echo $key_word; ?>"/>
                    <input type="hidden" name="advance_search" value="1">
                    <div class="clearfix"></div>
                    <hr class="hr_margin">
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-12">
                    

                    <label class="filter_label" for="mediation">Types de médiation <span class="pull-right"><a href="javascript:" class="medi-collapse"><i class="fa fa-plus"></i></a></span></label>
                    <div class="advance_search_box medi-filter" style="display: none;">
                        <?php
                           if(isset($this->request->params['?']['mediation'])) {
                             $medi_param = $this->request->params['?']['mediation'];
                           } 
                        ?>
                        <?php 
                        $selected = "";
                        foreach($mediations as $mediation){
                            if($medi_param != "") {
                            if(in_array($mediation->id, $medi_param)) {
                                    $selected = "checked='checked'";
                                } else {
                                    $selected = "";
                                }
                            }
                        ?>
                        <div class="single-col">
                        <div class="styled-input-container styled-input--diamond">
                          <div class="styled-input-single">
                            <input value="<?php echo $mediation->id;?>" type="checkbox" name="mediation[]" id="mediation<?php echo $mediation->id;?>" <?php echo $selected; ?> />
                            <label for="mediation<?php echo $mediation->id;?>"><?php echo $mediation->names;?></label>
                          </div>
                      </div>
                    </div>
                        <?php } ?>
                    </div>
                    <div class="clearfix"></div>
                    <hr class="hr_margin">
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-12">
                    <label class="filter_label" for="accreditation">Accréditation <span class="pull-right"><a href="javascript:" class="acce-collapse"><i class="fa fa-plus"></i></a></span></label>
                    <div class="advance_search_box acce-filter" style="display: none;">
                        <?php
                           if(isset($this->request->params['?']['accreditation'])) {
                             $acce_param = $this->request->params['?']['accreditation'];
                           } 
                        ?>

                        <?php 
                        $selected = "";
                        foreach($accreds as $accred){
                            if($acce_param != "") {
                            if(in_array($accred->id, $acce_param)) {
                                    $selected = "checked='checked'";
                                } else {
                                    $selected = "";
                                }
                            }
                        ?>
                        <div class="single-col">
                            <div class="styled-input-container styled-input--diamond">
                              <div class="styled-input-single">
                                <input value="<?php echo $accred->id;?>" id="accreditation<?php echo $accred->id;?>" type="checkbox" name="accreditation[]" <?php echo $selected; ?> />
                                <label for="accreditation<?php echo $accred->id;?>"><?php echo $accred->name;?></label>
                              </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="clearfix"></div>
                    <hr class="hr_margin">
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12">
                    <label class="filter_label" for="languages">Langues</label><br>
                    <div class="">
                        <?php
                        if(isset($this->request->params['?']['languages'])) {

                            $lan_param = $this->request->params['?']['languages'];
                         }
                        ?>
                        <select class="js-example-basic-multiple" id="multipleLanguages" name="languages[]" multiple="multiple" style="width:100%;">

                            <?php
                            $selected = "";
                             foreach ($languages as $k => $l) {
                                if($lan_param != "") {
                                    if(in_array($k, $lan_param)) {
                                        $selected = "selected='selected'";
                                    } else {
                                        $selected = "";
                                    }
                                }

                            ?>
                            <option value="<?= $k ?>" <?php echo $selected; ?>><?= $l ?></option>    
                            <?php
}
                            ?>
                        </select>

                    </div>
                    <div class="clearfix"></div>
                    <hr class="hr_margin">
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12">
                    <label for=""></label><br>
                    <button  class="btn advance_search_members" style="width:100%;"><i class="fa fa-search"></i> Chercher</button>
                </div>
            </div>
        </form>
    </div>
        <div class="col-md-9">

            <?php if($this->request->params['?']['advance_search'] == 1 || isset($this->request->params['?']['keyword'])) {
                $conn = ConnectionManager::get('default');
                    echo "<div class='advance_search_result'>Résultat de recherche de membre: ";
                    if($this->request->params['?']['key_word']) {
                        echo "<strong>Mots clés: </strong> ".$this->request->params['?']['key_word'];
                    }
                    if($this->request->params['?']['keyword']) {
                        echo "<strong>Mots clés: </strong> ".$this->request->params['?']['keyword'];
                    }
                    if($this->request->params['?']['mediation']) {
                        echo "<strong> Types de médiation:</strong> ";
                        $med_results = [];
                        foreach($this->request->params['?']['mediation'] as $key => $med) {


                      $med_query = $conn->execute('SELECT `names`'
                            . ' FROM medtypes '
                            
                            
                            . 'where id="' . $med .'"');
                         $med_name = $med_query->fetchAll('assoc')[0];
                         $med_results[] = $med_name['names'];
                        }
                            $med_result = implode($med_results,", ");
                            echo $med_result;
                    }
                    if($this->request->params['?']['accreditation']) {
                        echo "<strong> Accréditation:</strong> ";
                        $accre_results = [];
                        foreach($this->request->params['?']['accreditation'] as $key => $accre) {

                  

                      $accre_query = $conn->execute('SELECT `name`'
                            . ' FROM accreditations '
                            
                            
                            . 'where id="' . $accre .'"');
                    $accre_name = $accre_query->fetchAll('assoc')[0];
                   //debug($med_name);exit;

                            $accre_results[] = $accre_name['name'];
                        }
                        $accre_result = implode($accre_results,", ");;
                        echo $accre_result;
                    }
                    if($this->request->params['?']['languages']) {
                        echo "<strong> Langues:</strong> ";
                        $lang_results = [];
                        foreach($this->request->params['?']['languages'] as $lang) {

                  

                      $lang_query = $conn->execute('SELECT `name`'
                            . ' FROM languages '
                            
                            
                            . 'where id="' . $lang .'"');
                    $lang_name = $lang_query->fetchAll('assoc')[0];
                   //debug($med_name);exit;

                            $lang_results[] = $lang_name['name'];
                        }
                        $lang_result = implode($lang_results,', ');
                        echo $lang_result;
                    }
                    echo "</div>";
                } 

                
                ?>

            <?php 

            if(isset($search) and !empty($search) ){
                foreach ($search as $search){ 
                    //    debug($search);
                    //                debug($search);
            ?>
            <div class="col-md-6 col-lg-4">
            <div class="custom_box" onclick="showDetails(<?php echo $search['user_id']?>)">
                <?php
            if ($search['image'] && file_exists(WWW_ROOT . 'img' . DS . 'slider' . DS . $search['image'])) {
                $p = $this->request->webroot . 'img/slider/' . $search['image'];
                ?>

                <?php
            } else {
                $p = $this->request->webroot . 'img/Users/cyber1550500683.png';
            }
                ?>
                <div class="profile_image" style="background-image: url('<?php echo $p;?>')">

                </div>
                <div class="member_profile_info">
                    <h3 class="profile_name"><?php echo ucwords($search['first_name'].' '.$search['last_name']);?></h3>
                    <p class='member-profession pull-left'>
                    <?php 
                        if($search['mediator_question'] == "Oui"){
                            echo "Médiateur";

                        } else{
                            echo '';
                        }
                        if($search['mediator_question'] == "Oui" && $search['accept_info'] == 1) {
                            echo ", ";
                        }
                        if($search['accept_info'] == 1) {
                            echo "Assermenté";
                        } else {
                            echo '';
                        }
                    ?>
                    </p>
                        <?php 
        
        if($search['emailpublic'] == 1){
            echo "<p class='member-email'>".$search['mail']."</p>";

        } else{
            echo '';
        }
                        ?>
                        
                    
                </div>
            </div>
        </div>
            <?php }
            } else{
                echo '<h2 class="text-center" style="margin-top:20px;">désolé aucun disque trouvé</h2>';
            }
            ?>
        </div>
        <div class="clearfix">&nbsp;</div>
    </div>
</div>
<script>

</script>
