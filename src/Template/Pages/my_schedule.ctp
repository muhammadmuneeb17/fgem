<style>
    table tr td{
        text-align: left;
    }
</style>
<div class="container" style="background: #fff;">
    <div class="row">
        <div class="col-lg-12" style="text-align:right;">
            <a onclick="return window.history.back()" class="btn btn-primary" style="margin:10px 0;">Retourner</a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12" style="text-align:right;">
            <table class="table table-responsive table-srtiped table-bordered">
                <tr>
                    <th>Partir de la date</th>
                    <th>à ce jour</th>
                    <!--<th>Action</th>-->
                </tr>
                <?php foreach ($myCals as $myCal):
                    ?>
                    <tr>
                        <td><?php echo $myCal->from_date->i18nFormat('dd-MM-YYY'); ?></td>
                        <td><?php echo $myCal->to_date->i18nFormat('dd-MM-YYY'); ?></td>
                        <td>
                            <!--<a onclick="viewDays(<?= $myCal->id; ?>)" class="btn btn-primary">vue</a>-->
                            <a onclick="return confirm('êtes-vous sûr')" href="<?php echo $this->request->webroot.'Tickets/deleteScedule/'.$myCal->id.'/my_schedule'; ?>" class="btn btn-danger">Effacer</a>
                            <!--<a onclick="addHoliday(<?= $myCal->id; ?>)" class="btn btn-primary">Add Holidays</a>-->
                        </td>

                    </tr>
                    <?php
                endforeach;
                ?>
            </table>
        </div>
    </div>
    <!--    <div id="addCalendars" class="modal fade" role="dialog">
            <div class="modal-dialog">
    
                 Modal content
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add Holidays to your days</h4>
                    </div>
                    <form method="post" action="" id="scheduleForm">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="hidden" class="form-control" name="ticketId" value="">
                                </div>
                                <div class="col-md-12" style="margin-bottom:10px;">
                                    <input type="checkbox" id="monday" name="off_day[]"  value="1">&nbsp; <label for="monday">Monday</label>
                                </div>
                                <div class="col-md-12" style="margin-bottom:10px;">
                                    <input id="tuesday" type="checkbox" name="off_day[]" value="2">&nbsp; <label for="tuesday">Tuesday</label>
                                </div>
                                <div class="col-md-12" style="margin-bottom:10px;">
                                    <input id="wednesday" type="checkbox" name="off_day[]" value="3">&nbsp; <label for="wednesday">Wednesday</label>
                                </div>
                                <div class="col-md-12" style="margin-bottom:10px;">
                                    <input id="thursday" type="checkbox" name="off_day[]" value="4">&nbsp; <label for="thursday">Thursday</label>
                                </div>
                                <div class="col-md-12" style="margin-bottom:10px;">
                                    <input id="friday" type="checkbox" name="off_day[]" value="5">&nbsp; <label for="friday">Friday</label>
                                </div>
                                <div class="col-md-12" style="margin-bottom:10px;">
                                    <input id="saturday" type="checkbox" name="off_day[]" value="6">&nbsp; <label for="saturday">Saturday</label>
                                </div>
                                <div class="col-md-12" style="margin-bottom:10px;">
                                    <input id="sunday" type="checkbox" name="off_day[]" value="0">&nbsp; <label for="sunday">Sunday</label>
                                </div>
    
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </form>
    
                </div>
    
            </div>
        </div>-->
    <div id="ViewDatesModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">All days of the given dates</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <form method="post" action="" id="scheduleForm">
                            <input type="hidden" class="form-control" name="ticketId" value="">
                            <div class="col-md-12" id="datesHtml">
                            </div>
                            <div class="modal-footer">
                                <!--<button type="submit" class="btn btn-primary">Submit</button>-->
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    function addHoliday(id) {
        $('input[name=ticketId]').val(id);
        $('#addCalendars').modal('show');
    }
    function viewDays(id) {
        $('input[name=ticketId]').val(id);
        $.ajax({
            url: "<?php echo $this->request->webroot . 'Pages/getDates' ?>",
            type: "GET",
            data: "calenderId=" + id,
            success: function (data)
            {
                $('#datesHtml').html(data);
                $('#ViewDatesModal').modal('show');

            }

        });
    }

</script>