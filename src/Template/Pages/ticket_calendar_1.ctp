<link rel="stylesheet" href="<?= $this->request->webroot ?>event_calendar/css/monthly.css">
<style type="text/css">
    body {
        font-family: Calibri;
        background-color: #f0f0f0;
        padding: 0em 1em;
    }
    #mycalendar {
        width: 100%;
        margin: 2em auto 0 auto;
        max-width: 80em;
        border: 1px solid #666;
    }
    .monthly-week{
        background-color: #fff;
    }
    .monthly-week:hover{
        background-color: #f8f8f8;
    }
    .monthly-day{
        background: transparent;
    }
    body{
        padding:0;
    }
    .monthly-today .monthly-day-number {
        background: #217db3;
    }
    .monthly-header{
        font-size: 18px;
        font-weight: bold;
    }
</style>
<div class="container">
<!--    <div class="row">
        <div class="col-lg-12">
            <a href="<?= $this->request->webroot; ?>Pages/my_schedule" class="btn btn-primary pull-right">My Schedules</a>

        </div>
    </div>-->

    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
            <div class="monthly" id="mycalendar" style="margin:0;"></div>

        </div>
        <div class="col-lg-1"></div>

    </div>
</div>
<div id="addCalendars" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Schedule</h4>
            </div>
            <form method="post" action="" id="scheduleForm">

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="hidden" class="form-control" name="from_date" required="" >
                        </div>
                        <div class="col-md-6">
                            <input type="hidden" class="form-control" name="to_date">
                        </div>
                        <div class="col-md-12">
                            <?php
                            $req = '';
                            if ($this->request->session()->read('Auth.User.role_id') == 1) {
                                $req = 'required';
                            }
                            ?>
                            <select id="user_id" class="form-control" name="user_id" <?= $req; ?> >
                                <option value="">Select Users</option>

                                <?php foreach ($users as $key => $user): ?>
                                    <option value="<?= $key; ?>"><?= $user ?></option>
                                <?php endforeach; ?>

                            </select>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>

        </div>

    </div>
</div>
<script type="text/javascript" src="<?= $this->request->webroot ?>event_calendar/js/jquery.js"></script>
<script type="text/javascript" src="<?= $this->request->webroot ?>event_calendar/js/monthly.js"></script>

<?php if ($this->request->session()->read('Auth.User.role_id') == 1) { ?>
    <script>
        function newCalendar(str) {
            var getDate = getMonday(new Date(str));
            $('#scheduleForm input[name=from_date]').val(getDate);
            $('#addCalendars').modal('show');

        }
    </script>
<?php } else { ?>
    <script>
        function newCalendar(str) {
            var getDate = getMonday(new Date(str));
            $('#scheduleForm input[name=from_date]').val(getDate);
            //        $('#addCalendars').modal('show');
            if (confirm('Are you sure you want to this week to calender')) {
                $('#scheduleForm').submit();
            }

        }
    </script>
<?php } ?>

<script type="text/javascript">

    function formatDate(date) {
        var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [year, month, day].join('/');
    }
//    getting monday OR week first day
    function getMonday(d) {
        d = new Date(d);
        var day = d.getDay(),
                diff = d.getDate() - day + (day == 0 ? -6 : 1), // adjust when day is sunday
                weekMonth = d.getMonth() + 1;
        return formatDate(d.setDate(diff));
    }
    $(function () {
        $.ajax({
            url: "<?php echo $this->request->webroot . 'Pages/ticketCalendar2' ?>",
            type: "GET",
            success: function (data)
            {
                data = JSON.parse(data);
                var sampleEvents = data;
                $('#mycalendar').monthly({
                    weekStart: 'Mon',
                    mode: 'event',
                    dataType: 'json',
                    events: sampleEvents
                });

            }
        });
    });

</script>
