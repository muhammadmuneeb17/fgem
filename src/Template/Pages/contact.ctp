<section id="contact" class="contact-wrapper">
        <div class="title text-center">
                <h2>Contact Us</h2>
                <p>Because we value your feedback!</p>
                <hr>
        </div><!-- end title -->
        <div class="contact_tab text-center">
           <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade active in" id="tab1">
                        <div class="container">
            <div class=" text-center">
                <div class="clearfix"></div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div data-scroll-reveal-complete="true" data-scroll-reveal-initialized="true" data-scroll-reveal-id="18" class="contact-box" data-scroll-reveal="enter from the bottom after 0.6s">
                        <i class="fa fa-envelope-o aligncenter"></i>
                        <h2><a href="mailto:+11234567890" class="contactlink">CONTACT@SRISHTI.COM</a></h2>
                    </div>
                </div>
        
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div data-scroll-reveal-complete="true" data-scroll-reveal-initialized="true" data-scroll-reveal-id="19" class="contact-box" data-scroll-reveal="enter from the bottom after 0.6s">
                        <i class="fa fa-map-marker aligncenter"></i>
                        <h2>SRISHTI DANCE ACADMEY, FLORIDA </h2>
                    </div>
                </div>
        
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div data-scroll-reveal-complete="true" data-scroll-reveal-initialized="true" data-scroll-reveal-id="20" class="contact-box" data-scroll-reveal="enter from the bottom after 0.6s">
                         <i class="fa fa-phone aligncenter"></i>
                        <h2><a href="tel:+11234567890" class="contactlink">+1 123 456 7890</a></h2>
                    </div>
                </div>
            </div> <!-- end title -->
            </div> <div class="container">
                                <form data-scroll-reveal-complete="true" data-scroll-reveal-initialized="true" data-scroll-reveal-id="17" class="frontform" action="<?php echo $this->request->webroot ?>contact" name="contactform" method="post" data-scroll-reveal="enter from the bottom after 0.4s">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <input name="name" id="name" class="form-control" placeholder="Full Name" type="text"> 
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <input name="email" id="email" class="form-control" placeholder="Email Address" type="text"> 
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <input name="phone" id="subject" class="form-control" placeholder="Phone" type="text"> 
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <textarea class="form-control" name="message" id="comments" rows="6" placeholder="Message"></textarea>
                                </div>
                                <div class="text-center">
                                    <button type="submit" value="SEND" id="submit" class="btn btn-lg btn-contact btn-primary"><i class="fa fa-paper-plane"></i> SUBMIT</button>
                                </div>
                            </form> <!-- End Form -->
                        </div> <!-- End Container -->
                    </div><!-- End Tab Pane -->
             
            </div><!-- /end my tab content -->  
        </div><!-- /contact_tab -->  
    
        
        
    </section>