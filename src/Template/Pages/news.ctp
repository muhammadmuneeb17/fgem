<?php ?>
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

<!-- Bootstrap Core Css -->

<!-- Custom Css -->

<style>
    .bg-green {
        background-color: #4CAF50 !important;
        color: #fff; }
    .bg-green .content .text,
    .bg-green .content .number {
        color: #fff !important; }

    .bg-light-green {
        background-color: #8BC34A !important;
        color: #fff; }
    .bg-light-green .content .text,
    .bg-light-green .content .number {
        color: #fff !important; }

    .bg-pink {
        background-color: #E91E63 !important;
        color: #fff; }
    .bg-pink .content .text,
    .bg-pink .content .number {
        color: #fff !important; }
    /* Infobox ===================================== */
    .info-box {
        box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
        height: 80px;
        display: flex;
        cursor: default;
        background-color: #fff;
        position: relative;
        overflow: hidden;
        margin-bottom: 30px; }
    .info-box .icon {
        display: inline-block;
        text-align: center;
        background-color: rgba(0, 0, 0, 0.12);
        width: 80px; }
    .info-box .icon i {
        color: #fff;
        font-size: 50px;
        line-height: 80px; }
    .info-box .icon .chart.chart-bar {
        height: 100%;
        line-height: 100px; }
    .info-box .icon .chart.chart-bar canvas {
        vertical-align: baseline !important; }
    .info-box .icon .chart.chart-pie {
        height: 100%;
        line-height: 123px; }
    .info-box .icon .chart.chart-pie canvas {
        vertical-align: baseline !important; }
    .info-box .icon .chart.chart-line {
        height: 100%;
        line-height: 115px; }
    .info-box .icon .chart.chart-line canvas {
        vertical-align: baseline !important; }
    .info-box .content {
        display: inline-block;
        padding: 7px 10px; }
    .info-box .content .text {
        font-size: 13px;
        margin-top: 11px;
        color: #555; }
    .info-box .content .number {
        font-weight: normal;
        font-size: 26px;
        margin-top: -4px;
        color: #555; }

    .info-box.hover-zoom-effect .icon {
        overflow: hidden; }
    .info-box.hover-zoom-effect .icon i {
        -moz-transition: all 0.3s ease;
        -o-transition: all 0.3s ease;
        -webkit-transition: all 0.3s ease;
        transition: all 0.3s ease; }

    .info-box.hover-zoom-effect:hover .icon i {
        opacity: 0.4;
        -moz-transform: rotate(-32deg) scale(1.4);
        -ms-transform: rotate(-32deg) scale(1.4);
        -o-transform: rotate(-32deg) scale(1.4);
        -webkit-transform: rotate(-32deg) scale(1.4);
        transform: rotate(-32deg) scale(1.4); }

    .info-box.hover-expand-effect:after {
        background-color: rgba(0, 0, 0, 0.05);
        content: ".";
        position: absolute;
        left: 80px;
        top: 0;
        width: 0;
        height: 100%;
        color: transparent;
        -moz-transition: all 0.95s;
        -o-transition: all 0.95s;
        -webkit-transition: all 0.95s;
        transition: all 0.95s; }

    .info-box.hover-expand-effect:hover:after {
        width: 100%; }

    .info-box-2 {
        box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
        height: 80px;
        display: flex;
        cursor: default;
        background-color: #fff;
        position: relative;
        overflow: hidden;
        margin-bottom: 30px; }
    .info-box-2 .icon {
        display: inline-block;
        text-align: center;
        width: 80px; }
    .info-box-2 .icon i {
        color: #fff;
        font-size: 50px;
        line-height: 80px; }
    .info-box-2 .chart.chart-bar {
        height: 100%;
        line-height: 105px; }
    .info-box-2 .chart.chart-bar canvas {
        vertical-align: baseline !important; }
    .info-box-2 .chart.chart-pie {
        height: 100%;
        line-height: 123px; }
    .info-box-2 .chart.chart-pie canvas {
        vertical-align: baseline !important; }
    .info-box-2 .chart.chart-line {
        height: 100%;
        line-height: 115px; }
    .info-box-2 .chart.chart-line canvas {
        vertical-align: baseline !important; }
    .info-box-2 .content {
        display: inline-block;
        padding: 7px 10px; }
    .info-box-2 .content .text {
        font-size: 13px;
        margin-top: 11px;
        color: #555; }
    .info-box-2 .content .number {
        font-weight: normal;
        font-size: 26px;
        margin-top: -4px;
        color: #555; }

    .info-box-2.hover-zoom-effect .icon {
        overflow: hidden; }
    .info-box-2.hover-zoom-effect .icon i {
        -moz-transition: all 0.3s ease;
        -o-transition: all 0.3s ease;
        -webkit-transition: all 0.3s ease;
        transition: all 0.3s ease; }

    .info-box-2.hover-zoom-effect:hover .icon i {
        opacity: 0.4;
        -moz-transform: rotate(-32deg) scale(1.4);
        -ms-transform: rotate(-32deg) scale(1.4);
        -o-transform: rotate(-32deg) scale(1.4);
        -webkit-transform: rotate(-32deg) scale(1.4);
        transform: rotate(-32deg) scale(1.4); }

    .info-box-2.hover-expand-effect:after {
        background-color: rgba(0, 0, 0, 0.05);
        content: ".";
        position: absolute;
        left: 0;
        top: 0;
        width: 0;
        height: 100%;
        color: transparent;
        -moz-transition: all 0.95s;
        -o-transition: all 0.95s;
        -webkit-transition: all 0.95s;
        transition: all 0.95s; }

    .info-box-2.hover-expand-effect:hover:after {
        width: 100%; }

    .info-box-3 {
        box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
        height: 80px;
        display: flex;
        cursor: default;
        background-color: #fff;
        position: relative;
        overflow: hidden;
        margin-bottom: 30px; }
    .info-box-3 .icon {
        position: absolute;
        right: 10px;
        bottom: 2px;
        text-align: center; }
    .info-box-3 .icon i {
        color: rgba(0, 0, 0, 0.15);
        font-size: 60px; }
    .info-box-3 .chart {
        margin-right: 5px; }
    .info-box-3 .chart.chart-bar {
        height: 100%;
        line-height: 50px; }
    .info-box-3 .chart.chart-bar canvas {
        vertical-align: baseline !important; }
    .info-box-3 .chart.chart-pie {
        height: 100%;
        line-height: 34px; }
    .info-box-3 .chart.chart-pie canvas {
        vertical-align: baseline !important; }
    .info-box-3 .chart.chart-line {
        height: 100%;
        line-height: 40px; }
    .info-box-3 .chart.chart-line canvas {
        vertical-align: baseline !important; }
    .info-box-3 .content {
        display: inline-block;
        padding: 7px 16px; }
    .info-box-3 .content .text {
        font-size: 13px;
        margin-top: 11px;
        color: #555; }
    .info-box-3 .content .number {
        font-weight: normal;
        font-size: 26px;
        margin-top: -4px;
        color: #555; }

    .info-box-3.hover-zoom-effect .icon i {
        -moz-transition: all 0.3s ease;
        -o-transition: all 0.3s ease;
        -webkit-transition: all 0.3s ease;
        transition: all 0.3s ease; }

    .info-box-3.hover-zoom-effect:hover .icon i {
        opacity: 0.4;
        -moz-transform: rotate(-32deg) scale(1.4);
        -ms-transform: rotate(-32deg) scale(1.4);
        -o-transform: rotate(-32deg) scale(1.4);
        -webkit-transform: rotate(-32deg) scale(1.4);
        transform: rotate(-32deg) scale(1.4); }

    .info-box-3.hover-expand-effect:after {
        background-color: rgba(0, 0, 0, 0.05);
        content: ".";
        position: absolute;
        left: 0;
        top: 0;
        width: 0;
        height: 100%;
        color: transparent;
        -moz-transition: all 0.95s;
        -o-transition: all 0.95s;
        -webkit-transition: all 0.95s;
        transition: all 0.95s; }

    .info-box-3.hover-expand-effect:hover:after {
        width: 100%; }

    .info-box-4 {
        box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
        height: 80px;
        display: flex;
        cursor: default;
        background-color: #fff;
        position: relative;
        overflow: hidden;
        margin-bottom: 30px; }
    .info-box-4 .icon {
        position: absolute;
        right: 10px;
        bottom: 2px;
        text-align: center; }
    .info-box-4 .icon i {
        color: rgba(0, 0, 0, 0.15);
        font-size: 60px; }
    .info-box-4 .chart {
        margin-right: 5px; }
    .info-box-4 .chart.chart-bar {
        height: 100%;
        line-height: 50px; }
    .info-box-4 .chart.chart-bar canvas {
        vertical-align: baseline !important; }
    .info-box-4 .chart.chart-pie {
        height: 100%;
        line-height: 34px; }
    .info-box-4 .chart.chart-pie canvas {
        vertical-align: baseline !important; }
    .info-box-4 .chart.chart-line {
        height: 100%;
        line-height: 40px; }
    .info-box-4 .chart.chart-line canvas {
        vertical-align: baseline !important; }
    .info-box-4 .content {
        display: inline-block;
        padding: 7px 16px; }
    .info-box-4 .content .text {
        font-size: 13px;
        margin-top: 11px;
        color: #555; }
    .info-box-4 .content .number {
        font-weight: normal;
        font-size: 26px;
        margin-top: -4px;
        color: #555; }

    .info-box-4.hover-zoom-effect .icon i {
        -moz-transition: all 0.3s ease;
        -o-transition: all 0.3s ease;
        -webkit-transition: all 0.3s ease;
        transition: all 0.3s ease; }

    .info-box-4.hover-zoom-effect:hover .icon i {
        opacity: 0.4;
        -moz-transform: rotate(-32deg) scale(1.4);
        -ms-transform: rotate(-32deg) scale(1.4);
        -o-transform: rotate(-32deg) scale(1.4);
        -webkit-transform: rotate(-32deg) scale(1.4);
        transform: rotate(-32deg) scale(1.4); }

    .info-box-4.hover-expand-effect:after {
        background-color: rgba(0, 0, 0, 0.05);
        content: ".";
        position: absolute;
        left: 0;
        top: 0;
        width: 0;
        height: 100%;
        color: transparent;
        -moz-transition: all 0.95s;
        -o-transition: all 0.95s;
        -webkit-transition: all 0.95s;
        transition: all 0.95s; }

    .info-box-4.hover-expand-effect:hover:after {
        width: 100%; }
    </style>
    <!-- Widgets -->
    <div class="panel-head-info">
    <p class="info-text text-centered" style="text-align:center">Saisir des News</p>
    <div class="clearfix"></div>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h2 class="text-center">Arrive bientôt</h2>
        </div>
    </div>

</div>


