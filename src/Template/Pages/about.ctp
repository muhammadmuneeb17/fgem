<section id="about" class="about-wrapper">
    <div class="container">
        <div data-scroll-reveal-complete="true" data-scroll-reveal-initialized="true" data-scroll-reveal-id="2" data-scroll-reveal="enter from the bottom after 0.3s" class="title text-center">
            <h2>Why Us?</h2>
            <p>Because we are professionals...</p>
            <hr>
        </div><!-- end title -->
    </div><!-- end container -->

    <div class="clearfix">
            <div class="container">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="widget">
                        <p>Smt. Shruthi Shoby, after her 10 years of intensive course in Bharatnatyam from Navajyothi Nrithakalalayam,she was fascinated by the graceful sways of the Mohiniattam style of dance. she undertook the direct tutelage of Kalamandalam Smt. Kavitha Krishnakumar and has completed over 15 years under her guidance.
                            Shruthi also developed a fondness for the fleeting moves and vivid expression of Kuchipudi and has been taking regular classes from Kalaimamani Sailaja since 2004.
                            Shruthi was an avid dancer during her college days and has several accolades to her credit. She was the first prize winner for Mohiniyattam at the Calicut University for two consecutive years. She has performed across the country for many renowned festivals and Sabhas .To name the few , South Zone Culturals; Natyanjali Chidambaram; Tamil Isai Sangam, Pollachi; Mookambika Dance Festival, Tirupathi Brahmotsavam, Mamallapuram Dance Festivals-Tamil Nadu,Nishagandhi –Kerala,Konark Dance Festival-Odissa. She is a graded artist in Trivandrum Doordarshan.
                            Shruthi has been in the judging panel for several Disrtict and Zonal level dance competitions in Kerala and Tamilnadu.
                            She is recipient of “Natya Kalaranjini” award from Marumalarchi Dance and Music Academy and the “Nadana Jyothi” title by the Navajyothi Nrithya Kalalayam.
                            Her dance performances were reviewed and appreciated in National and regional news papers.
                            Shruthi runs her own institutuion ‘Srishti’,School of classical Dance in Chennai, where she trains her students in Bharathanatyam and Mohiniattam and Kuchipudi successfully. </p>

                    </div><!-- end widget -->
                </div><!-- end col-lg-6 -->
            </div><!-- end container -->
        
    </div>
</section>