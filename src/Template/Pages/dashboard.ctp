<?php ?>
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

<!-- Bootstrap Core Css -->

<!-- Custom Css -->

<style>
    .bg-green {
        background-color: #4CAF50 !important;
        color: #fff; }
    .bg-green .content .text,
    .bg-green .content .number {
        color: #fff !important; }

    .bg-light-green {
        background-color: #8BC34A !important;
        color: #fff; }
    .bg-light-green .content .text,
    .bg-light-green .content .number {
        color: #fff !important; }

    .bg-pink {
        background-color: #E91E63 !important;
        color: #fff; }
    .bg-pink .content .text,
    .bg-pink .content .number {
        color: #fff !important; }
    /* Infobox ===================================== */
    .info-box {
        box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
        height: 80px;
        display: flex;
        cursor: default;
        background-color: #fff;
        position: relative;
        overflow: hidden;
        margin-bottom: 30px; }
    .info-box .icon {
        display: inline-block;
        text-align: center;
        background-color: rgba(0, 0, 0, 0.12);
        width: 80px; }
    .info-box .icon i {
        color: #fff;
        font-size: 50px;
        line-height: 80px; }
    .info-box .icon .chart.chart-bar {
        height: 100%;
        line-height: 100px; }
    .info-box .icon .chart.chart-bar canvas {
        vertical-align: baseline !important; }
    .info-box .icon .chart.chart-pie {
        height: 100%;
        line-height: 123px; }
    .info-box .icon .chart.chart-pie canvas {
        vertical-align: baseline !important; }
    .info-box .icon .chart.chart-line {
        height: 100%;
        line-height: 115px; }
    .info-box .icon .chart.chart-line canvas {
        vertical-align: baseline !important; }
    .info-box .content {
        display: inline-block;
        padding: 7px 10px; }
    .info-box .content .text {
        font-size: 13px;
        margin-top: 11px;
        color: #555; }
    .info-box .content .number {
        font-weight: normal;
        font-size: 26px;
        margin-top: -4px;
        color: #555; }

    .info-box.hover-zoom-effect .icon {
        overflow: hidden; }
    .info-box.hover-zoom-effect .icon i {
        -moz-transition: all 0.3s ease;
        -o-transition: all 0.3s ease;
        -webkit-transition: all 0.3s ease;
        transition: all 0.3s ease; }

    .info-box.hover-zoom-effect:hover .icon i {
        opacity: 0.4;
        -moz-transform: rotate(-32deg) scale(1.4);
        -ms-transform: rotate(-32deg) scale(1.4);
        -o-transform: rotate(-32deg) scale(1.4);
        -webkit-transform: rotate(-32deg) scale(1.4);
        transform: rotate(-32deg) scale(1.4); }

    .info-box.hover-expand-effect:after {
        background-color: rgba(0, 0, 0, 0.05);
        content: ".";
        position: absolute;
        left: 80px;
        top: 0;
        width: 0;
        height: 100%;
        color: transparent;
        -moz-transition: all 0.95s;
        -o-transition: all 0.95s;
        -webkit-transition: all 0.95s;
        transition: all 0.95s; }

    .info-box.hover-expand-effect:hover:after {
        width: 100%; }

    .info-box-2 {
        box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
        height: 80px;
        display: flex;
        cursor: default;
        background-color: #fff;
        position: relative;
        overflow: hidden;
        margin-bottom: 30px; }
    .info-box-2 .icon {
        display: inline-block;
        text-align: center;
        width: 80px; }
    .info-box-2 .icon i {
        color: #fff;
        font-size: 50px;
        line-height: 80px; }
    .info-box-2 .chart.chart-bar {
        height: 100%;
        line-height: 105px; }
    .info-box-2 .chart.chart-bar canvas {
        vertical-align: baseline !important; }
    .info-box-2 .chart.chart-pie {
        height: 100%;
        line-height: 123px; }
    .info-box-2 .chart.chart-pie canvas {
        vertical-align: baseline !important; }
    .info-box-2 .chart.chart-line {
        height: 100%;
        line-height: 115px; }
    .info-box-2 .chart.chart-line canvas {
        vertical-align: baseline !important; }
    .info-box-2 .content {
        display: inline-block;
        padding: 7px 10px; }
    .info-box-2 .content .text {
        font-size: 13px;
        margin-top: 11px;
        color: #555; }
    .info-box-2 .content .number {
        font-weight: normal;
        font-size: 26px;
        margin-top: -4px;
        color: #555; }

    .info-box-2.hover-zoom-effect .icon {
        overflow: hidden; }
    .info-box-2.hover-zoom-effect .icon i {
        -moz-transition: all 0.3s ease;
        -o-transition: all 0.3s ease;
        -webkit-transition: all 0.3s ease;
        transition: all 0.3s ease; }

    .info-box-2.hover-zoom-effect:hover .icon i {
        opacity: 0.4;
        -moz-transform: rotate(-32deg) scale(1.4);
        -ms-transform: rotate(-32deg) scale(1.4);
        -o-transform: rotate(-32deg) scale(1.4);
        -webkit-transform: rotate(-32deg) scale(1.4);
        transform: rotate(-32deg) scale(1.4); }

    .info-box-2.hover-expand-effect:after {
        background-color: rgba(0, 0, 0, 0.05);
        content: ".";
        position: absolute;
        left: 0;
        top: 0;
        width: 0;
        height: 100%;
        color: transparent;
        -moz-transition: all 0.95s;
        -o-transition: all 0.95s;
        -webkit-transition: all 0.95s;
        transition: all 0.95s; }

    .info-box-2.hover-expand-effect:hover:after {
        width: 100%; }

    .info-box-3 {
        box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
        height: 80px;
        display: flex;
        cursor: default;
        background-color: #fff;
        position: relative;
        overflow: hidden;
        margin-bottom: 30px; }
    .info-box-3 .icon {
        position: absolute;
        right: 10px;
        bottom: 2px;
        text-align: center; }
    .info-box-3 .icon i {
        color: rgba(0, 0, 0, 0.15);
        font-size: 60px; }
    .info-box-3 .chart {
        margin-right: 5px; }
    .info-box-3 .chart.chart-bar {
        height: 100%;
        line-height: 50px; }
    .info-box-3 .chart.chart-bar canvas {
        vertical-align: baseline !important; }
    .info-box-3 .chart.chart-pie {
        height: 100%;
        line-height: 34px; }
    .info-box-3 .chart.chart-pie canvas {
        vertical-align: baseline !important; }
    .info-box-3 .chart.chart-line {
        height: 100%;
        line-height: 40px; }
    .info-box-3 .chart.chart-line canvas {
        vertical-align: baseline !important; }
    .info-box-3 .content {
        display: inline-block;
        padding: 7px 16px; }
    .info-box-3 .content .text {
        font-size: 13px;
        margin-top: 11px;
        color: #555; }
    .info-box-3 .content .number {
        font-weight: normal;
        font-size: 26px;
        margin-top: -4px;
        color: #555; }

    .info-box-3.hover-zoom-effect .icon i {
        -moz-transition: all 0.3s ease;
        -o-transition: all 0.3s ease;
        -webkit-transition: all 0.3s ease;
        transition: all 0.3s ease; }

    .info-box-3.hover-zoom-effect:hover .icon i {
        opacity: 0.4;
        -moz-transform: rotate(-32deg) scale(1.4);
        -ms-transform: rotate(-32deg) scale(1.4);
        -o-transform: rotate(-32deg) scale(1.4);
        -webkit-transform: rotate(-32deg) scale(1.4);
        transform: rotate(-32deg) scale(1.4); }

    .info-box-3.hover-expand-effect:after {
        background-color: rgba(0, 0, 0, 0.05);
        content: ".";
        position: absolute;
        left: 0;
        top: 0;
        width: 0;
        height: 100%;
        color: transparent;
        -moz-transition: all 0.95s;
        -o-transition: all 0.95s;
        -webkit-transition: all 0.95s;
        transition: all 0.95s; }

    .info-box-3.hover-expand-effect:hover:after {
        width: 100%; }

    .info-box-4 {
        box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
        height: 80px;
        display: flex;
        cursor: default;
        background-color: #fff;
        position: relative;
        overflow: hidden;
        margin-bottom: 30px; }
    .info-box-4 .icon {
        position: absolute;
        right: 10px;
        bottom: 2px;
        text-align: center; }
    .info-box-4 .icon i {
        color: rgba(0, 0, 0, 0.15);
        font-size: 60px; }
    .info-box-4 .chart {
        margin-right: 5px; }
    .info-box-4 .chart.chart-bar {
        height: 100%;
        line-height: 50px; }
    .info-box-4 .chart.chart-bar canvas {
        vertical-align: baseline !important; }
    .info-box-4 .chart.chart-pie {
        height: 100%;
        line-height: 34px; }
    .info-box-4 .chart.chart-pie canvas {
        vertical-align: baseline !important; }
    .info-box-4 .chart.chart-line {
        height: 100%;
        line-height: 40px; }
    .info-box-4 .chart.chart-line canvas {
        vertical-align: baseline !important; }
    .info-box-4 .content {
        display: inline-block;
        padding: 7px 16px; }
    .info-box-4 .content .text {
        font-size: 13px;
        margin-top: 11px;
        color: #555; }
    .info-box-4 .content .number {
        font-weight: normal;
        font-size: 26px;
        margin-top: -4px;
        color: #555; }

    .info-box-4.hover-zoom-effect .icon i {
        -moz-transition: all 0.3s ease;
        -o-transition: all 0.3s ease;
        -webkit-transition: all 0.3s ease;
        transition: all 0.3s ease; }

    .info-box-4.hover-zoom-effect:hover .icon i {
        opacity: 0.4;
        -moz-transform: rotate(-32deg) scale(1.4);
        -ms-transform: rotate(-32deg) scale(1.4);
        -o-transform: rotate(-32deg) scale(1.4);
        -webkit-transform: rotate(-32deg) scale(1.4);
        transform: rotate(-32deg) scale(1.4); }

    .info-box-4.hover-expand-effect:after {
        background-color: rgba(0, 0, 0, 0.05);
        content: ".";
        position: absolute;
        left: 0;
        top: 0;
        width: 0;
        height: 100%;
        color: transparent;
        -moz-transition: all 0.95s;
        -o-transition: all 0.95s;
        -webkit-transition: all 0.95s;
        transition: all 0.95s; }

    .info-box-4.hover-expand-effect:hover:after {
        width: 100%; }</style>
    <!-- Widgets -->
    <div class="panel-head-info">
    <p class="info-text text-centered">Tableau de bord</p>
    <div class="clearfix"></div>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-pink hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">playlist_add_check</i>
                </div>
                <div class="content">
                    <div class="text">NOUVEAUX MEMBRES</div>
                    <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"><?php
                        if (!empty($user)) {
                            echo $user;
                        } else {
                            echo '0';
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-green hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">help</i>
                </div>
                <div class="content">
                    <div class="text">MEMBRES EN COURS</div>
                    <div class="number count-to" data-from="0" data-to="257" data-speed="1000" data-fresh-interval="20"><?php
                        if (!empty($user_pend)) {
                            echo $user_pend;
                        } else {
                            echo '0';
                        }
                        ?></div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="info-box bg-light-green hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">verified_user</i>
                </div>
                <div class="content">
                    <div class="text">Membres Acceptés</div>
                    <div class="number count-to" data-from="0" data-to="243" data-speed="1000" data-fresh-interval="20"><?php
                        if (!empty($user_app)) {
                            echo $user_app;
                        } else {
                            echo '0';
                        }
                        ?></div>
                </div>
            </div>
        </div>
        <!--                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box bg-orange hover-expand-effect">
                                <div class="icon">
                                    <i class="material-icons">view_list</i>
                                </div>
                                <div class="content">
                                    <div class="text">MEMBER LIST</div>
                                    <div class="number count-to" data-from="0" data-to="1225" data-speed="1000" data-fresh-interval="20">500</div>
                                </div>
                            </div>
                        </div>-->
    </div>
    <!-- #END# Widgets -->
</div>


<div class="container" style="background-color:transparent;box-shadow: 0px 0px;">

    <div class="row">

        <div class="col-lg-6" >
            <div style="background-color:#f8f8f8; box-shadow: 2px 2px 5px #999;">
                <p class="pull-left info-text" style="font-size:20px;padding: 8px;">Membres en attente</p>
                <div id="no-more-tables" >
                    <table class="col-md-12 table-bordered table-striped table-condensed cf" >
                        <thead class="cf">
                            <tr class="text-nowrap">

                                <th scope="col"><?= $this->Paginator->sort('membertype_id', 'Type d\'adhésion') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('name', 'Prénom') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('lastname', 'Nom de famille') ?></th>
       <!--                         <th scope="col"><? $this->Paginator->sort('enterprise','Enterprise') ?></th>-->
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($reg_pend) {
                                foreach ($reg_pend as $reg_pend):
                                    if ($reg_pend['accounts'] != null) {
                                        //debug($registeration['user']['email']);exit;
                                        ?>
                                        <tr >
                                            <td data-title="Type d\'adhésion"><?= h($reg_pend->membertype->description) ?></td>
                                            <td data-title="Prénom"><?= h($reg_pend['user']['first_name']) ?></td>
                                            <td data-title="Nom de famille"><?= h($reg_pend['user']['last_name']) ?></td>
                            <!--                <td data-title="Enterprise"><? h($registeration->enterprise) ?></td>-->
                                        </tr>

                                        </tr>
                                        <?php
                                    } endforeach;
                            }
                            ?>
                        </tbody>
                    </table>

                </div>
                <?php
//            echo $this->Html->link(
//                    '<i class="fa fa-edit"></i> Add Page',
//                    '/contentTypes/addPage',
//                    ['class' => 'btn btn-primary btn-large select-btn','escape'=>false]); 
                ?>
                <div class="col-lg-2" style="float:right; margin-top: 10px; margin-bottom: 10px; margin-right: 20px;">
                    <?php echo $this->Html->link(__(' Voir tout'), array('controller' => 'Registerations', 'action' => 'pending'), array('class' => 'btn btn-success', 'escape' => false));
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-lg-6">
            <div style="background-color:#f8f8f8; box-shadow: 2px 2px 5px #999;">
                <p class="pull-left info-text" style="font-size:20px;padding: 8px;">Membres Acceptés</p>
                <div id="no-more-tables" >
                    <table class="col-md-12 table-bordered table-striped table-condensed cf" >
                        <thead class="cf">
                            <tr class="text-nowrap">


                                <th scope="col"><?= $this->Paginator->sort('membertype_id', 'Type d\'adhésion') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('name', 'Prénom') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('lastname', 'Nom de famille') ?></th>
       <!--                         <th scope="col"><? $this->Paginator->sort('enterprise','Enterprise') ?></th>-->


                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($reg_app as $reg_app):
                                //debug($registeration['user']['email']);exit;
                                ?>

                                <tr >


                                    <td data-title="Type d\'adhésion"><?= h($reg_app->membertype->description) ?></td>
                                    <td data-title="Prénom"><?= h($reg_app['user']['first_name']) ?></td>
                                    <td data-title="Nom de famille"><?= h($reg_app['user']['last_name']) ?></td>
                    <!--                <td data-title="Enterprise"><? h($registeration->enterprise) ?></td>-->



                                </tr>

                                </tr>
                            <?php endforeach; ?>
                        </tbody>


                    </table>

                </div>
                <?php
//            echo $this->Html->link(
//                    '<i class="fa fa-list"></i> List Pages',
//                    '/contentTypes/listPages',
//                    ['class' => 'btn btn-success btn-large select-btn','escape'=>false]); 
                ?>
                <div class="col-lg-2" style="float:right; margin-top: 10px; margin-bottom: 10px; margin-right: 20px;">
                    <?php echo $this->Html->link(__(' Voir tout'), array('controller' => 'Registerations', 'action' => 'approved'), array('class' => 'btn btn-success', 'escape' => false));
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-lg-6">
            <div style="background-color:#f8f8f8; box-shadow: 2px 2px 5px #999;">
                <p class="pull-left info-text" style="font-size:20px;padding: 8px;">Membres Débiteurss</p>
                <div id="no-more-tables" >
                    <table class="col-md-12 table-bordered table-striped table-condensed cf" >
                        <thead class="cf">
                            <tr class="text-nowrap">


                                <th scope="col"><?= $this->Paginator->sort('Membertype.membertype_id', 'Type') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('User.name', 'Prénom') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('User.lastname', 'Nom de Famille') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('amount_due', 'montante') ?></th>
                                <th scope="col">Action</th>


                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($member_owed as $memberowed):
                                //debug($registeration['user']['email']);exit;
                                ?>

                                <tr >


                                    <td data-title="Type"><?= h($memberowed['description']) ?></td>
                                    <td data-title="Prénom"><?= h($memberowed['first_name']) ?></td>
                                    <td data-title="Nom de Famille"><?= h($memberowed['last_name']) ?></td>
                                    <td data-title="montante"><?= h($memberowed['amount_due']) ?></td>
                    <!--                <td data-title="Enterprise"><? h($registeration->enterprise) ?></td>-->
                                    <td>
                                        <?php echo $this->Html->link(__('Paiements'), array('controller' => 'Accounts', 'action' => 'index', $memberowed['rid']), array('class' => 'btn btn-primary', 'escape' => false)); ?>
                                    </td>
                                </tr>

                                </tr>
                            <?php endforeach; ?>
                        </tbody>


                    </table>

                </div>
                <?php
//            echo $this->Html->link(
//                    '<i class="fa fa-list"></i> List Pages',
//                    '/contentTypes/listPages',
//                    ['class' => 'btn btn-success btn-large select-btn','escape'=>false]); 
                ?>
                <div class="col-lg-2" style="float:right; margin-top: 10px; margin-bottom: 10px; margin-right: 20px;">
                    <?php echo $this->Html->link(__(' Voir tout'), array('controller' => 'Registerations', 'action' => 'members_owed'), array('class' => 'btn btn-success', 'escape' => false));
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="col-lg-6">
            <div style="background-color:#f8f8f8; box-shadow: 2px 2px 5px #999;">
                <p class="pull-left info-text" style="font-size:20px;padding: 8px;">Membres Inactifs ou Suspendus</p>
                <div id="no-more-tables" >
                    <table class="col-md-12 table-bordered table-striped table-condensed cf" >
                        <thead class="cf">
                            <tr class="text-nowrap">


                                <th scope="col"><?= $this->Paginator->sort('Membertype.membertype_id', 'Type') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('User.name', 'Prénom') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('User.lastname', 'Nom de Famillee') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('amount_due', 'montante') ?></th>
                                <th scope="col">Action</th>


                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($suspended as $smember):
                                //debug($registeration['user']['email']);exit;
                                ?>

                                <tr >


                                    <td data-title="Type"><?= h($smember['registeration']['membertype']['description']) ?></td>
                                    <td data-title="Prénom"><?= h($smember['registeration']['user']['first_name']) ?></td>
                                    <td data-title="Nom de Famillee"><?= h($smember['registeration']['user']['last_name']) ?></td>
                                    <td data-title="montante"><?= h($smember['amount_due']) ?></td>
                    <!--                <td data-title="Enterprise"><? h($registeration->enterprise) ?></td>-->
                                    <td>
                                        <?php echo $this->Html->link(__('Payments'), array('controller' => 'Accounts', 'action' => 'index', $smember['registeration_id']), array('class' => 'btn btn-primary', 'escape' => false)); ?>
                                    </td>
                                </tr>

                                </tr>
                            <?php endforeach; ?>
                        </tbody>


                    </table>

                </div>
                <?php
//            echo $this->Html->link(
//                    '<i class="fa fa-list"></i> List Pages',
//                    '/contentTypes/listPages',
//                    ['class' => 'btn btn-success btn-large select-btn','escape'=>false]); 
                ?>
                <div class="col-lg-2" style="float:right; margin-top: 10px; margin-bottom: 10px; margin-right: 20px;">
                    <?php echo $this->Html->link(__(' Voir tout'), array('controller' => 'registerations', 'action' => 'suspended_members'), array('class' => 'btn btn-success', 'escape' => false));
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

</div>
</div>

