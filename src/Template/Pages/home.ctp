<?php ?>
<!--/SLIDER SECTION -->
    <section id="home" class="sliderwrapper clearfix">
	
       <div class="tp-banner-container">
            <div class="tp-banner">
                <ul>
       			  <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                        <!-- MAIN IMAGE -->
						<div class="video-container">
						
  <video id="video" loop=true preload="auto">
                                <source src='<?php echo $this->request->webroot ?>video/vid1.mp4' type='video/mp4' id="vidya"/>
                        </video>
</div>
                        
        				<div class="tp-dottedoverlay twoxtwo"></div>
 
                        <!-- LAYER NR. 3 -->
                         <div class="tp-caption rev-video  customin customout start"
                            data-x="right"
                            data-hoffset="0"
                            data-y="140"
                            data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="1000"
                            data-start="500"
                            data-easing="Back.easeInOut"
                            data-endspeed="300"><hr class="topline"><h2 class="videoOverlayTitle bollywoodTitle">Bollywood Dance<br> Srishti Dance Academy <br><a href="<?php echo $this->request->webroot ?>Schedules/dance/<?php echo $dance[0]['name']; ?>" class="btn btn-success btn-orange">Join Now</a></h2>
							<h2 class="videoOverlayTitle kuchipudiTitle" style="display:none">Kuchipudi Dance<br> Srishti Dance Academy <br><a href="<?php echo $this->request->webroot ?>Schedules/dance/<?php echo $dance[1]['name']; ?>" class="btn btn-success btn-white">Join Now</a></h2>
							<h2 class="videoOverlayTitle indiafestTitle" style="display:none">India Fest Dance<br> Srishti Dance Academy <br><a href="<?php echo $this->request->webroot ?>Schedules/dance/<?php echo $dance[2]['name']; ?>" class="btn btn-success btn-green">Join Now</a></h2>
							<hr class="bottomline">
                        </div>
    
                        <!-- LAYER NR. 4 -->
                        <!-- <div class="tp-caption rev-video2 customin customout start"
                            data-x="center"
                            data-hoffset="0"
                            data-y="340"
                            data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                            data-speed="2200"
                            data-start="500"
                            data-easing="Back.easeInOut"
                            data-endspeed="300"><p> Growth Your Business <br> With Highly Customize Design</p>
                        </div>-->
                    </li>
		          </ul>
		          <div class="tp-bannertimer"></div>
            </div>
		</div>
		<div class="brand-promotion">
				<div class="container">
					<div class="media row">
						<div class="col-sm-4">
							<a href="javascript:" id="bollywoodVideo" class="videoLink">
							<div class="brand-content wow fadeIn animated dlinkOrange"  style="visibility: visible; -webkit-animation: fadeIn 700ms 300ms;">
								<div class="media-body">							
									<h3><?php echo ucfirst($dance[0]['name']); ?></h3>
								</div>
							</div>	
							</a>							
						</div>
						<div class="col-sm-4">
						<a href="javascript:" id="kuchipudiVideo‪" class="videoLink">
							<div class="brand-content wow fadeIn animated dlinkWhite" style="visibility: visible; -webkit-animation: fadeIn 700ms 400ms;">
								<div class="media-body">							
									<h3><?php echo ucfirst($dance[1]['name']); ?></h3>
								</div>
							</div>
						</a>							
						</div>
						<div class="col-sm-4">
						<a href="javascript:" id="indiafestVideo" class="videoLink">
							<div class="brand-content wow fadeIn animated dlinkGreen"  style="visibility: visible; -webkit-animation: fadeIn 700ms 500ms;">
								<div class="media-body">							
									<h3><?php echo ucfirst($dance[2]['name']); ?></h3>
								</div>
							</div>						
						</div>
					</div>
				</div>
			</div>
    </section><!-- end slider-wrapper -->  


<script type="text/javascript" src="<?php echo $this->request->webroot ?>rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
<script type="text/javascript" src="<?php echo $this->request->webroot ?>rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<script type="text/javascript">
    var revapi;
    jQuery(document).ready(function () {
        revapi = jQuery('.tp-banner').revolution(
                {
                    delay: 9000,
                    startwidth: 1170,
                    startheight: 500,
                    hideThumbs: 10,
                    fullWidth: "on",
                    fullScreen: "on",
                    fullScreenOffsetContainer: "",
                    stopAfterLoops: 0,
                    stopAtSlide: 1,
                });
    });	//ready
    $(window).on("load", function () {

        $("#video").get(0).play();
    });
    $(function () {
        $('#bollywoodVideo').click(function () {

            $("#vidya").attr("src", "<?php echo $this->request->webroot ?>video/vid1.mp4");
            $("#video")[0].load();
            $("#video")[0].play();
            $(".videoOverlayTitle").hide();
            $(".bollywoodTitle").fadeIn();
        });
        $('#kuchipudiVideo‪').click(function () {

            $("#vidya").attr("src", "<?php echo $this->request->webroot ?>video/revslider.mp4");
            $("#video")[0].load();
            $("#video")[0].play();
            $(".videoOverlayTitle").hide();
            $(".kuchipudiTitle").fadeIn();
        });
        $('#indiafestVideo').click(function () {

            $("#vidya").attr("src", "<?php echo $this->request->webroot ?>video/vid2.mp4");
            $("#video")[0].load();
            $("#video")[0].play();
            $(".videoOverlayTitle").hide();
            $(".indiafestTitle").fadeIn();
        });
    });
</script>