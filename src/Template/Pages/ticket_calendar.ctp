<link rel="stylesheet" href="<?= $this->request->webroot ?>event_calendar/css/monthly.css">
<style type="text/css">
    body {
        font-family: Calibri;
        background-color: #f0f0f0;
        padding: 0em 1em;
    }
    #mycalendar {
        width: 100%;
        margin: 2em auto 0 auto;
        max-width: 80em;
        border: 1px solid #666;
    }
    .monthly-week{
        background-color: #fff;
    }
    .monthly-week:hover{
        background-color: #f8f8f8;
    }
    .monthly-day{
        background: transparent;
    }
    body{
        padding:0;
    }
    .monthly-today .monthly-day-number {
        background: #217db3;
    }
    .monthly-header{
        font-size: 24px;
        font-weight: bold;
    }
</style>
<div class="container">
    <!--    <div class="row">
            <div class="col-lg-12">
                <a href="<?= $this->request->webroot; ?>Pages/my_schedule" class="btn btn-primary pull-right">My Schedules</a>
    
            </div>
        </div>-->

    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
            <div class="monthly" id="mycalendar" style="margin:0;"></div>

        </div>
        <div class="col-lg-1"></div>

    </div>
</div>
<div id="addCalendars" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Ajouter un horaire</h4>
            </div>
            <form method="post" action="" id="scheduleForm">
                <div class="modal-body">
                    <input type="hidden" class="form-control" name="from_date" required="" >
                    <input type="hidden" class="form-control" name="current_date" required="" >
                    <input type="hidden" class="form-control" name="to_date">
                    <input type="hidden" name="action">
                    <div class="row">
                        <div class="col-md-12">
                            <?php
                            if ($superviser) {
                                ?>
                                <select id="user_id" class="form-control" name="user_id" required>
                                    <option value="">Select Users</option>

                                    <?php foreach ($users as $key => $user): ?>
                                        <option value="<?= $key; ?>"><?= $user ?></option>
                                    <?php endforeach; ?>

                                </select>
                                <p id="userIdError" style="display:none;color:red;">Veuillez sélectionner un utilisateur</p>
                            <?php } ?>
                        </div>
                    </div>
                    <br/>
                    <div class="row" style="display:none;">
                        <div class="col-md-6">
                            <a style="width:150px;" href="#" onclick="valueInAction('week')"  class="btn btn-primary">Nouvelle Semaine</a>
                        </div>
                        <div class="col-md-6">
                            <a style="width:150px;" href="#" onclick="valueInAction('delete week')"  class="btn btn-danger">Supprimer la semaine</a>
                        </div>
                        <br/>
                        <br/>
                        <div class="col-md-6">
                            <a style="width:150px;" href="#" onclick="valueInAction('holiday')"  class="btn btn-warning">Ajouter des vacances</a>
                        </div>
                        <div class="col-md-6">
                            <a style="width:150px;" href="#" onclick="valueInAction('delete holiday')" class="btn btn-danger">Supprimer les vacances</a>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <?php
                    if ($superviser) {
                        ?>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    <?php } ?>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?= $this->request->webroot ?>event_calendar/js/jquery.js"></script>
<script type="text/javascript" src="<?= $this->request->webroot ?>event_calendar/js/monthly.js"></script>
<?php if ($superviser) { ?>
    <script>
                                    function newCalendar(str) {
                                        var day = new Date(str);
                                        var nextDay = new Date(day);
                                        nextDay.setDate(day.getDate() + 1);
                                        //        alert(nextDay); // May 01 2000   
                                        var currentDate = formatDate(nextDay);
                                        var getDate = getMonday(nextDay);
                                        $('#scheduleForm input[name=from_date]').val(getDate);
                                        $('#scheduleForm input[name=current_date]').val(currentDate);
                                        $('#scheduleForm input[name=action]').val('week');
                                        $('#addCalendars').modal('show');//for further option just uncomment this
    //                                        valueInAction('week');// for further options just comment this

                                    }
    </script>
<?php } else { ?>
    <script>
        function newCalendar(str) {
            var day = new Date(str);
            var nextDay = new Date(day);
            nextDay.setDate(day.getDate() + 1);
            //        alert(nextDay); // May 01 2000   
            var currentDate = formatDate(nextDay);
            var getDate = getMonday(nextDay);
            $('#scheduleForm input[name=from_date]').val(getDate);
            $('#scheduleForm input[name=current_date]').val(currentDate);
            valueInAction('week');// for further options just comment this

        }
    </script>
<?php } ?>

<script type="text/javascript">
    function valueInAction(action) {
        var superviser = '<?= $superviser; ?>';
//        alert(superviser);
        if (superviser == 1) {
            if ($('select[name=user_id]').val() == '') {
                $('select[name=user_id]').focus();
                $('#userIdError').fadeIn(1500);
                return false;
            }
        }
        $('#scheduleForm input[name=action]').val(action);

//        if (confirm('êtes-vous sûr')) {
        $('#scheduleForm').submit();
//        }
    }
    function formatDate(date) {
        var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
        return [year, month, day].join('/');
    }
    //    getting monday OR week first day
    function getMonday(d) {
        d = new Date(d);
        var day = d.getDay(),
                diff = d.getDate() - day + (day == 0 ? -6 : 1), // adjust when day is sunday
                weekMonth = d.getMonth() + 1;
        return formatDate(d.setDate(diff));
    }
    $(function () {
        $.ajax({
            url: "<?php echo $this->request->webroot . 'Pages/ticketCalendar2' ?>",
            type: "GET",
            success: function (data)
            {
                data = JSON.parse(data);
                var sampleEvents = data;
                $('#mycalendar').monthly({
                    weekStart: 'Mon',
                    mode: 'event',
                    dataType: 'json',
                    events: sampleEvents,
                    monthNames: ['janvier', 'février', 'mars', 'avril ', 'mai', 'Juin', 'juillet', 'août', 'septembre', 'octobre', 'november', 'décembre']
                    ,dayNames:['DIM','LUN','MAR','MER','JEU','VEN','SAM']//sun to sat
                });

            }
        });
    });

</script>
