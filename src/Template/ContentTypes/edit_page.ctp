<div class="services form">
    <?php echo $this->Form->create($contentType,array('type'=>'file','class'=>'form-horizontal','id' => 'PageEditForm')); ?>
    <fieldset>
        <legend><?php echo __('Modifier la Page'); ?> | <?php echo $contentType->title; ?>
            <a href="<?php echo $this->request->webroot ?>ContentTypes/listPages" style="font-size:14px;" class="pull-right text-info">[Précédent]</a>
        </legend>
        <div class="form-group required">
            <label for="Title" class="col-md-1 control-label">Titre</label>
            <div class="col-md-10">
	<?php echo $this->Form->input('title',array('div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
            </div>   
        </div>   
        <div class="form-group required">
            <label for="Slug" class="col-md-1 control-label">Limace</label>
            <div class="col-md-10">
    <?php echo $this->Form->input('slug',array('div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
            </div>   
        </div>  

        <div class="form-group required">
            <label for="status" class="col-md-1 control-label">Statut</label>	
            <div class="col-md-10"> 
              <?php  $args = array(''=>'Sélectionnez le Statut','0'=>'Inactive','1'=>'Active');
		echo $this->Form->input('status',array('options'=>$args,'default'=>'','div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
            </div>
        </div> 
    </fieldset>
    <div class="form-group">
        <div class="col-md-11 text-center">
        <?php 
        echo $this->Form->input('Enregistrer',array('type'=>'submit','div'=>false,'label'=>false,'class'=>'btn btn-default btn-blue-custom btn-lg'));
        echo $this->Form->end(); ?>
        </div>
    </div>
</div>