<div class="container margintop-20">
    <div class="row">
        <div class="col-sm-3 col-sm-offset-0">
            <?php 
            
    if($permission == 2){
    ?>
            <a href="<?php echo $this->request->webroot ?>ContentTypes/add_page" class="btn btn-primary btn-add-top"><i class="fa fa-plus"></i> Ajouter une page</a>
            <?php } ?>
        </div>
    </div>
</div>
<div class="container content-container">

    <div class="row">
        <div id="no-more-tables">
			<form method="post" id="myForm" action="<?php echo $this->request->webroot; ?>ContentTypes/udapteorder">
            <table class="col-md-12 table-bordered table-striped table-condensed cf">
                <thead class="cf">
                    <tr>
                        <th>
						<?php echo $this->Paginator->sort('Titre'); ?></th>
                        <th><?php echo $this->Paginator->sort('Accueil'); ?></th>
						<th><?php echo $this->Paginator->sort('Afficher le Curseur'); ?></th>
						<th><?php echo $this->Paginator->sort('Zone pour Contacts'); ?></th>
						<th><?php echo $this->Paginator->sort('user_id','Ajouté par'); ?></th>
						<th><?php echo $this->Paginator->sort('Statut'); ?></th>
						<th><?php echo $this->Paginator->sort('Ordinale'); ?></th>
                        <th><?php echo $this->Paginator->sort('Modifie'); ?></th>
                        <th class="actions"><?php echo __('Action'); ?></th>
                    </tr>
                </thead>
                <tbody>
                            <?php	
							$count = 1;
							foreach ($pages as $page): ?>
                    <tr>
                        <td data-title="Titre"><?php echo h($page->title); ?>&nbsp;</td>
						<td data-title="Est Devant">
						<input type="radio" name="front"  <?php if($page->is_front == 1) { echo "checked='checked'"; } ?> onChange="is_front(this.id)" id="isfront<?php echo $count; ?>"/>&nbsp;
							<input type="hidden" name="data[is_front][]"  <?php if($page->is_front == 1) { echo "value='1'"; } else { echo "value='0'"; } ?>  class="allfront isfront<?php echo $count; ?>"/>
						</td>
						<td data-title="Afficher le Curseur">
						<input type="checkbox" name="show_slider" value="1"  <?php if($page->show_slider == 1) { echo "checked='checked'"; } ?> onChange="showslider(this)" id="show_slider<?php echo $count; ?>"/>&nbsp;
							<input type="hidden" name="data[show_slider][]"  <?php if($page->show_slider == 1) { echo "value='1'"; } else { echo "value='0'"; } ?>  class="show_slider<?php echo $count; ?>"/>
						</td>
						<td data-title="Est le Contact">
						<input type="radio" name="contact"  <?php if($page->is_contact == 1) { echo "checked='checked'"; } ?> onChange="is_contact(this.id)" id="iscontact<?php echo $count; ?>"/>&nbsp;
							<input type="hidden" name="data[is_contact][]"  <?php if($page->is_contact == 1) { echo "value='1'"; } else { echo "value='0'"; } ?>  class="allcontact iscontact<?php echo $count; ?>"/>
						</td>
						<td data-title="Ajouté par"><?php echo h($page->user->user_name); ?>&nbsp;</td>
                        <td data-title="Statut"><?php 
                    if($page->status == 0) {
                        $status = "Inactive";
                    }
                    else {
                        $status = "Active";
                    }
                    echo $status; ?>&nbsp</td>
					<td data-title="Ordinale">
					
		
        
                            <?php echo $this->Form->control('ordinal',array('type'=>'number','value'=>$page->ordinal,'name'=>'data[ordinal][]','div'=>false,'label'=>false,'class'=>'form-control','required'=>'required','style'=>'max-width:80px;')); ?>
                        <?php echo $this->Form->control('content_id',array('type'=>'hidden','value'=>$page->id,'name'=>'data[content_id][]','div'=>false,'label'=>false,'class'=>'form-control')); ?>
						
						</td>
                        <td data-title="Modifie"><?php echo h(date('d.m.y',strtotime($page->modified))); ?>&nbsp;</td>
                        <td  data-title="Action" class="actions">
			<?php echo $this->Html->link(__('<i class="fa fa-file-text-o" aria-hidden="true"></i>'), array('controller' => 'Contents', 'action' => 'listSubPages',$page->id),array('escape'=>false,'class'=>'btn btn-primary','data-toggle'=>"tooltip",'title'=>'Voir les rubriques')); ?>
                             <?php 
                       if($permission == 2){
                           ?>
                        <?php echo $this->Html->link(__('<span class="fa fa-edit"></span>'), array('controller' => 'ContentTypes', 'action' => 'editPage',$page->id),array('escape'=>false,'class'=>'btn btn-success','data-toggle'=>"tooltip",'title'=>'Modifier')); ?>
			<?php 
                        echo $this->Html->link(__('<i class="fa fa-trash-o"></i>'), ['controller' => 'ContentTypes','action' => 'delete', $page->id], ['escape'=>false,'class'=>'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $page->title),'data-toggle'=>"tooltip",'title'=>'Effacer']); ?>
                       <?php } ?>
		</td>
		
                    </tr>
	<?php $count++; endforeach; ?>
                </tbody>
            </table>
			</form>
				<button class="btn btn-custom col-xs-offset-6 col-md-offset-6" style="font-weight:bold;width:150px;margin-top:10px;margin-bottom:10px;" id="orderupdate_btn" >Enregistrer</button>
			</div>
    </div>
    <br />
    
    <div class="paginator">
        <ul class="pagination">
            <?php
                echo $this->Paginator->prev(__('Précédent'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                echo $this->Paginator->next(__('Suivant'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
            ?>
        </ul>
        
        <p class="small"><?= $this->Paginator->counter(['format' => __('Page {{page}} de {{pages}}, affiche {{current}} entrée(s) sur un de total {{count}}')]) ?></p>
    </div>
        
</div>
<script type="text/javascript">
    $(document).ready(function() {
       $("#orderupdate_btn").click(function() {
           $("#myForm").submit();
       });
    });
	function is_front(value) {
		var sourceId = value;
		$(".allfront").attr("value","0");
		$("."+sourceId).attr("value","1");
	}
	function is_contact(value) {
		var sourceId = value;
		$(".allcontact").attr("value","0");
		$("."+sourceId).attr("value","1");
	}
	function showslider(data) {
		var sourceId = data.id;
		if($("#"+sourceId).is(":checked")) {
			$("."+sourceId).attr("value","1");
		} else {
			$("."+sourceId).attr("value","0");
		}
		
	}
</script>