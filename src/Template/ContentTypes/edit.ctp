<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $contentType->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $contentType->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Content Types'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Contents'), ['controller' => 'Contents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Content'), ['controller' => 'Contents', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="contentTypes form large-9 medium-8 columns content">
    <?= $this->Form->create($contentType) ?>
    <fieldset>
        <legend><?= __('Edit Content Type') ?></legend>
        <?php
            echo $this->Form->control('title');
            echo $this->Form->control('template');
            echo $this->Form->control('is_front');
            echo $this->Form->control('added_by');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
