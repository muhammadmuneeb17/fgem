<div class="services form">
    <?php echo $this->Form->create($page,array('type'=>'file','class'=>'form-horizontal','id' => 'PageAddForm')); ?>
    <fieldset>
        <legend><?php echo __('Ajouter une page'); ?>
            <a href="<?php echo $this->request->webroot ?>ContentTypes/listPages" style="font-size:14px;" class="pull-right text-info">[Précédent]</a>
        </legend>
        <div class="form-group required">
            <label for="Title" class="col-md-1 control-label">Titre</label>
            <div class="col-md-10">
	<?php echo $this->Form->input('title',array('div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
            </div>   
        </div> 
        <div class="form-group required">
            <label for="Slug" class="col-md-1 control-label">Limace</label>
            <div class="col-md-10">
    <?php echo $this->Form->input('slug',array('div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
            </div>   
        </div>   

        <div class="form-group required">
            <label for="status" class="col-md-1 control-label">Statut</label>	
            <div class="col-md-10"> 
              <?php  $args = array(''=>'Sélectionnez le Statut','0'=>'Inactive','1'=>'Active');
		echo $this->Form->input('status',array('options'=>$args,'default'=>'','div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
            </div>
        </div> 
    </fieldset>
    <div class="form-group">
        <div class="col-md-11 text-center">
        <?php 
        echo $this->Form->input('Ajouter',array('type'=>'submit','div'=>false,'label'=>false,'class'=>'btn btn-default btn-blue-custom btn-lg'));
        echo $this->Form->end(); ?>
        </div>
    </div>
</div>

<script>
    $('#ServiceAddForm').submit(function (e) {
        var $container = $("html,body");
        var $scrollTo = $('.mymsg');
        var title = $("#title").val().length;
        var description = CKEDITOR.instances.description.getData().length;
        var status = $("#status").val();
        var photo = $("#sectionImage").val();
        if (title < 1) {
            $('.mymsg').html('');
            $('.mymsg').append("<div class='error'>Title is missing or too short</div>");
            $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top, scrollLeft: 0}, 300);
            return false;
        }
        if (description < 1) {
            $('.mymsg').html('');
            $('.mymsg').append("<div class='error'>Description is missing or too short</div>");
            $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top, scrollLeft: 0}, 300);
            return false;
        }
        if (status == "") {
            $('.mymsg').html('');
            $('.mymsg').append("<div class='error'>Select status</div>");
            $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top, scrollLeft: 0}, 300);
            return false;
        }
        if (photo == "") {
            $('.mymsg').html('');
            $('.mymsg').append("<div class='error'>Image is missing</div>");
            $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top, scrollLeft: 0}, 300);
            return false;
        }
        if (photo != "") {
            var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
            if ($.inArray(photo.split('.').pop().toLowerCase(), fileExtension) == -1) {
                $(this).find(".btn-file").css("border", "1px solid red");
                alert("Invalid content image format,Only formats are allowed : " + fileExtension.join(', '));
                e.preventDefault();
            }
        }
    });
</script>