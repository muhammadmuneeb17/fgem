<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\ContentType[]|\Cake\Collection\CollectionInterface $contentTypes
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Content Type'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Contents'), ['controller' => 'Contents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Content'), ['controller' => 'Contents', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="contentTypes index large-9 medium-8 columns content">
    <h3><?= __('Content Types') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('title') ?></th>
                <th scope="col"><?= $this->Paginator->sort('template') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_front') ?></th>
                <th scope="col"><?= $this->Paginator->sort('added_by') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($contentTypes as $contentType): ?>
            <tr>
                <td><?= $this->Number->format($contentType->id) ?></td>
                <td><?= h($contentType->title) ?></td>
                <td><?= h($contentType->template) ?></td>
                <td><?= $this->Number->format($contentType->is_front) ?></td>
                <td><?= $this->Number->format($contentType->added_by) ?></td>
                <td><?= h($contentType->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $contentType->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $contentType->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $contentType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $contentType->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} de {{pages}}, affiche {{current}} entrée(s) sur un de total {{count}}')]) ?></p>
    </div>
</div>
