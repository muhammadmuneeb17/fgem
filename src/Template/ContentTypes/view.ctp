<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\ContentType $contentType
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Content Type'), ['action' => 'edit', $contentType->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Content Type'), ['action' => 'delete', $contentType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $contentType->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Content Types'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Content Type'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Contents'), ['controller' => 'Contents', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Content'), ['controller' => 'Contents', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="contentTypes view large-9 medium-8 columns content">
    <h3><?= h($contentType->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($contentType->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Template') ?></th>
            <td><?= h($contentType->template) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($contentType->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Front') ?></th>
            <td><?= $this->Number->format($contentType->is_front) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Added By') ?></th>
            <td><?= $this->Number->format($contentType->added_by) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($contentType->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Contents') ?></h4>
        <?php if (!empty($contentType->contents)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Image') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('Content Type Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($contentType->contents as $contents): ?>
            <tr>
                <td><?= h($contents->id) ?></td>
                <td><?= h($contents->title) ?></td>
                <td><?= h($contents->description) ?></td>
                <td><?= h($contents->image) ?></td>
                <td><?= h($contents->status) ?></td>
                <td><?= h($contents->content_type_id) ?></td>
                <td><?= h($contents->user_id) ?></td>
                <td><?= h($contents->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Contents', 'action' => 'view', $contents->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Contents', 'action' => 'edit', $contents->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Contents', 'action' => 'delete', $contents->id], ['confirm' => __('Are you sure you want to delete # {0}?', $contents->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
