<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Typemediation $typemediation
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Typemediation'), ['action' => 'edit', $typemediation->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Typemediation'), ['action' => 'delete', $typemediation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $typemediation->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Typemediations'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Typemediation'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="typemediations view large-9 medium-8 columns content">
    <h3><?= h($typemediation->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($typemediation->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($typemediation->id) ?></td>
        </tr>
    </table>
</div>
