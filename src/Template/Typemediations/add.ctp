<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Typemediations'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="typemediations form large-9 medium-8 columns content">
    <?= $this->Form->create($typemediation) ?>
    <fieldset>
        <legend><?= __('Add Typemediation') ?></legend>
        <?php
            echo $this->Form->control('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
