<?php

/**
  * @var \App\View\AppView $this
  */
?>
<div class="panel-head-info">
    <p class="pull-left info-text">Curseur d'édition | <?php echo $slider->title; ?></p>
    <a href="<?php echo $this->request->webroot ?>sliders" style="font-size:14px;" class="btn btn-primary btn-blue-custom pull-right text-info"><i class="fa fa-angle-left"></i> Précédent</a>
    <div class="clearfix"></div>

</div>
<div class="users form">
    <?php echo $this->Form->create($slider,array('type'=>'file','class'=>'form-horizontal','id'=>'SliderEditForm')); ?>
    <fieldset>
        <div class="form-group required">
            <label for="title" class="col-md-3 control-label">Titre</label>
            <div class="col-md-10">
                <?php echo $this->Form->control('title',array('div'=>false,'label'=>false,'class'=>'form-control','required')); ?>
            </div> 
        </div>   
        <div class="form-group required">
            <label for="image_alt" class="col-md-3 control-label">Image Alt Texte</label>
            <div class="col-md-10">
                <?php echo $this->Form->control('image_alt',array('type'=>'textarea','div'=>false,'label'=>false,'class'=>'form-control ckeditor','required')); ?>
            </div> 
        </div>   

        <div class="form-group required">
            <label for="status" class="col-md-3 control-label">Statut</label>
            <div class="col-md-10">
                <?php 
                $status_option = array('0' => 'Deactive' , '1' =>'Active');
                echo $this->Form->select("status", $status_option,["default"=>$slider->status,"empty"=>"Select Status","class" => "form-control required","id"=>"section-status"]); ?>


            </div>
        </div>
        <div class="form-group required">
            <label for="image" class="col-md-3 control-label">Image <br /><small class="text-danger">[Meilleure Taille: 1340px * 620px]</small></label>
            <span class="btn btn-default btn-file">
                Feuilleter <input type="file" name="image" class="section-image" id="sectionImage" onchange="readURL(this)">
            </span>
            <br /><img id="show_section_uploaded_image"  src="<?php echo $this->request->webroot ?>img/slider/thumbnail/<?php echo $slider->image ?>"  class="section-up-image" alt=""/>
        </div>


    </fieldset>
    <div class="form-group">
        <div class="col-md-12 text-center">
            <?php 
    echo $this->Form->control('Sauvegarder',array('type'=>'submit','div'=>false,'label'=>false,'class'=>'btn btn-default btn-blue-custom btn-lg'));
                       echo $this->Form->end(); ?>
        </div>
    </div>
</div>


<script>
    $('#SliderEditForm').submit(function (e) {
        if ($("#sectionImage").val() != "") {
            var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
            if ($.inArray($("#sectionImage").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                $(this).find(".btn-file").css("border", "1px solid red");
                alert("Invalid slider image format,Only formats are allowed : " + fileExtension.join(', '));
                e.preventDefault();
            }
        }
    });
</script>