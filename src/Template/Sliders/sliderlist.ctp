<div class="container margintop-20">
    <div class="row">
        <div class="col-sm-3 col-sm-offset-0">
            <?php 
            
    if($permission == 2){
    ?>
            <a href="<?php echo $this->request->webroot ?>sliders/add" class="btn btn-primary btn-add-top"><i class="fa fa-plus"></i> Ajouter une image</a>
             <?php } ?>
        </div>
    </div>
</div>
<div class="container content-container">

    <div class="row">
        <div id="no-more-tables">
           <form method="post" id="myForm" action="<?php echo $this->request->webroot; ?>sliders/udapteorder">
            <table class="col-md-12 table-bordered table-striped table-condensed cf">
                
                <thead class="cf">
                    <tr>
                        <th><?php echo $this->Paginator->sort('Titre'); ?></th>
                        <th><?php echo $this->Paginator->sort('photo'); ?></th>
                        <th><?php echo $this->Paginator->sort('Statut'); ?></th>
                        <th><?php echo $this->Paginator->sort('Ordre'); ?></th>
                        <th><?php echo $this->Paginator->sort('Modifié'); ?></th>
                        <!--<th><?php // echo $this->Paginator->sort('modified'); ?></th>-->
                        <th class="actions"><?php echo __('Action'); ?></th>
                    </tr>
                </thead>
                <tbody>
                            <?php foreach ($sliders as $slider): ?>
                    <tr>
                        <td data-title="Titre"><?php echo h($slider->title); ?>&nbsp;</td>
                        <td data-title="photo"><img src="<?php echo $this->request->webroot; ?>img/slider/thumbnail/<?php echo h($slider->image); ?>" width='150'/></td>
                        <td data-title="Statut"><?php 
                    if($slider->status == 0) {
                        $status = "DésactivéModifier
Effacer";
                    }
                    else {
                        $status = "Active";
                    }
                    echo $status; ?>&nbsp</td>
                        <td data-title="Ordre">
                            <?php echo $this->Form->control('slider_order',array('type'=>'number','value'=>$slider->ordinal,'name'=>'data[ordinal][]','div'=>false,'label'=>false,'class'=>'form-control','required'=>'required')); ?>
                        <?php echo $this->Form->control('slider_id',array('type'=>'hidden','value'=>$slider->id,'name'=>'data[slider_id][]','div'=>false,'label'=>false,'class'=>'form-control')); ?>
                        <td data-title="Modifié"><?php echo h(date('d.m.y H:i:s',strtotime($slider->modified))); ?>&nbsp;</td>
                        <!--<td data-title="Modified"><?php // echo h($Silder['Slider['modified); ?>&nbsp;</td>-->
                        <td  data-title="Action" class="actions">
                              <?php 
                       if($permission == 2){
                           ?>
			<?php echo $this->Html->link(__('<i class="fa fa-edit"></i>'), array('controller' => 'sliders', 'action' => 'edit',$slider->id),array('class'=>'btn btn-success','data-toggle'=>"tooltip",'title'=>'Modifier','escape'=>false)); ?>
			<?php 
                        echo $this->Html->link(__('<i class="fa fa-trash-o"></i>'), ['action' => 'delete', $slider->id], ['escape'=>false,'class'=>'btn btn-danger','confirm' => __('Are you sure you want to delete # {0}?', $slider->title),'data-toggle'=>"tooltip",'title'=>'Effacer']); ?>
		        <?php } ?>
                        </td>
                    </tr>
            <?php endforeach; ?>
                </tbody>
               
            </table>
               </form>
            <button class="btn btn-custom col-xs-offset-6 col-md-offset-6" style="margin-top:10px;margin-bottom:10px;" id="orderupdate_btn" >Mise à Jour de la Commande</button>
        </div>
    </div>
    <br />
    
        
    <div class="paginator">
        <ul class="pagination">
            <?php
                echo $this->Paginator->prev(__('Précédent'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                echo $this->Paginator->next(__('Suivante'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
            ?>
        </ul>
        
        <p class="small"><?= $this->Paginator->counter(['format' => __('Page {{page}} de {{pages}}, affiche {{current}} entrée(s) sur un de total {{count}}')]) ?></p>
    </div>
        
</div>
<script type="text/javascript">
    $(document).ready(function() {
       $("#orderupdate_btn").click(function() {
           $("#myForm").submit();
       });
    });
</script>