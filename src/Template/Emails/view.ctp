<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Email $email
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Email'), ['action' => 'edit', $email->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Email'), ['action' => 'delete', $email->id], ['confirm' => __('Are you sure you want to delete # {0}?', $email->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Emails'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Email'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Emailtypes'), ['controller' => 'Emailtypes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Emailtype'), ['controller' => 'Emailtypes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="emails view large-9 medium-8 columns content">
    <h3><?= h($email->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Emailtype') ?></th>
            <td><?= $email->has('emailtype') ? $this->Html->link($email->emailtype->id, ['controller' => 'Emailtypes', 'action' => 'view', $email->emailtype->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($email->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('From Person') ?></th>
            <td><?= h($email->from_person) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('To Person') ?></th>
            <td><?= h($email->to_person) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Subject') ?></th>
            <td><?= h($email->subject) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Message') ?></th>
            <td><?= h($email->message) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($email->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($email->modified) ?></td>
        </tr>
    </table>
</div>
