<?php
/**
  * @var \App\View\AppView $this
  */
?>
<?php
/**
  * @var \App\View\AppView $this
  */

?>
<div class="panel-head-info">
    <p class="pull-left info-text">Add Email</p>
     <div class="clearfix"></div>

</div>
<div class="users form">
<?php echo $this->Form->create($email,array('type'=>'file','class'=>'form-horizontal','id'=>'UserAddForm')); ?>
    <fieldset>
        <div class="form-group required">
            <label for="title" class="col-md-3 control-label">Email Type</label>
            <div class="col-md-10">
                
	<?php echo $this->Form->control('emailtype_id',array('div'=>false,'label'=>false,'class'=>'form-control','required','options' => $emailtypes)); ?>
                 </div> 
        </div>  
        
        
        <div class="form-group required">
            <label for="title" class="col-md-3 control-label">Title</label>
            <div class="col-md-10">
                
	<?php echo $this->Form->control('title',array('div'=>false,'label'=>false,'class'=>'form-control','required'));
        
        ?>
            </div> 
        </div>
        
        
<!--        <div class="form-group required">
            <label for="title" class="col-md-3 control-label">From</label>
            <div class="col-md-10">
                
	<?php //echo $this->Form->control('from_person',array('div'=>false,'label'=>false,'class'=>'form-control','required'));
        
        ?>
            </div> 
        </div>-->
        
        </fieldset>
<!--     <div class="form-group required">
            <label for="title" class="col-md-3 control-label">To</label>
            <div class="col-md-10">
                
	<?php //echo $this->Form->control('to_person',array('div'=>false,'label'=>false,'class'=>'form-control datepicker','required','type'=>'text')); ?>
            </div> 
        </div>-->
       <div class="form-group required">
            <label for="title" class="col-md-3 control-label">Interval</label>
            <div class="col-md-10">
                
	<?php echo $this->Form->control('time_interval',array('div'=>false,'label'=>false,'class'=>'form-control','required','type'=>'text')); ?>
            </div> 
        </div>
    <fieldset>
        <div class="form-group required">
            <label for="title" class="col-md-3 control-label">Subject</label>
            <div class="col-md-10">
                
	<?php echo $this->Form->control('subject',array('div'=>false,'label'=>false,'class'=>'form-control','required','type'=>'text')); ?>
             
                
                
            </div> 
        </div>  
        
        
        
        </fieldset>
    <div class="form-group required">
            <label for="message" class="col-md-3 control-label" style="width: 100%;">Message
<br>
Insturctions: use {first_name} for user's first name and/or use {last_name} for user's last name
<br>
Example: Dear {first_name} {last_name}, 
  
</label>
            <div class="col-md-10">
                
	<?php echo $this->Form->control('message',array('div'=>false,'label'=>false,'class'=>'form-control ckeditor','required','type'=>'textarea')); ?>
             
                
            </div> 
        </div>  
      
    
    <div class="form-group">
        <div class="col-md-12 text-center">
        <?php 
        echo $this->Form->control('Add Email',array('type'=>'submit','div'=>false,'label'=>false,'class'=>'btn btn-default btn-blue-custom btn-lg'));
        echo $this->Form->end(); ?>
        </div>
    </div>
        
                </div>

    
</div>

