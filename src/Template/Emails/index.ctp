<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Email[]|\Cake\Collection\CollectionInterface $emails
  */

?>
<div class="panel-head-info">
    <p class="pull-left info-text">Modèles de courrier électronique</p>
            <!--<a href="<?php // echo $this->request->webroot ?>pages" style="font-size:14px;" class="btn btn-primary btn-blue-custom pull-right text-info"><i class="fa fa-angle-left"></i> Go Back</a>-->
    <div class="clearfix"></div>
    <?php 
//    if($permission == 2){
//    ?>
    
<!--    <a href="<?php // echo $this->request->webroot ?>registerations/add" class="btn btn-success"><i class="fa fa-plus"></i> Add Registration</a>-->
     <?php //} ?>
</div>
<div class="container-fluid content-container" style="">

    <div class="row">
        <div id="no-more-tables" style="overflow-x:auto;">
            <table class="col-md-12 table-bordered table-striped table-condensed cf" >
                <thead class="cf">
                    <tr>
                        <!--<th scope="col">< $this->Paginator->sort('emailtype_id','Type') ?></th>-->
                        <th scope="col"><?= $this->Paginator->sort('title','Titre') ?></th>
<!--                        <th scope="col"><? $this->Paginator->sort('from_person','From') ?></th>-->
<!--                        <th scope="col"><? $this->Paginator->sort('to_person','To') ?></th>-->
                        <th scope="col"><?= $this->Paginator->sort('subject','Sujet du message') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('message','Message') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('time_interval','Intervalle') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('modified','Modifié') ?></th>
                        <th scope="col" class="actions"><?= __('Action') ?></th>
                    </tr>
                </thead>
                <tbody>
                           <?php foreach ($emails as $email):
                               //debug($email->emailtype->email_type);exit;
                               ?>
            <tr>
              <?php //echo $search = str_replace("{first_name,last_name}", "muhammad muneeb", $email->message);exit;?>
                
                <!--<td data-title="Type">< $email->emailtype->email_type; ?></td>-->
                <td data-title="Titre"><?= h($email->title) ?></td>
<!--                <td><? h($email->from_person) ?></td>-->
<!--                <td><? h($email->to_person) ?></td>-->
                <td data-title="Sujet du message"><?= h($email->subject) ?></td>
                <td data-title="Message"><?php echo  substr($email->message,0,65);?>...</td>
                 <td data-title="Intervalle"><?= h($email->time_interval) ?></td>
                <td data-title="Modifié"><?php echo h(date('d.m.y',strtotime($email->modified))); ?></td>
                <td data-title="Action" class="actions">
                  <?php echo $this->Html->link(__('<i class="fa fa-edit"></i>'), array('controller' => 'Emails', 'action' => 'edit',$email->id),array('class'=>'btn btn-success','escape'=>false,'data-toggle'=>"tooltip",'title'=>'Modifier')); 
                  ?>
                </td>
            </tr>
            <?php endforeach; ?>
                </tbody>
            </table>
            </form>
        </div>
    </div>

    <br />
    <div class="paginator">
        <ul class="pagination">
            <?php
                echo $this->Paginator->prev(__('retour'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                echo $this->Paginator->next(__('suivant'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
            ?>
        </ul>
        <p class="small"><?= $this->Paginator->counter(['format' => __('Page {{page}} de {{pages}}, affiche {{current}} entrée(s) sur un de total {{count}}')]) ?></p>
    </div>
</div>
