<?php
/**
  * @var \App\View\AppView $this
  */
//debug($email['emailtype']['email_type']);exit;
?>
<div class="panel-head-info">
    <p class="pull-left info-text">Edit Email - <strong><?php echo $email['emailtype']['email_type'];?></strong></p>
<a href="<?php echo $this->request->webroot ?>Emails" style="font-size:14px;" class="btn btn-primary btn-blue-custom pull-right text-info"><i class="fa fa-angle-left"></i> Go Back</a>
     <div class="clearfix"></div>

</div>
<div class="users form">
<?php echo $this->Form->create($email,array('type'=>'file','class'=>'form-horizontal','id'=>'UserAddForm')); ?>
    <fieldset>
<!--        <div class="form-group required">
            <label for="title" class="col-md-3 control-label">Email Type</label>
            <div class="col-md-10">
                
	<?php //echo $this->Form->control('emailtype_id',array('div'=>false,'label'=>false,'class'=>'form-control','required','options' => $emailtypes)); ?>
                 </div> 
        </div>  -->
        
        
        <div class="form-group required">
            <label for="title" class="col-md-3 control-label">Title</label>
            <div class="col-md-10">
                
	<?php echo $this->Form->control('title',array('div'=>false,'label'=>false,'class'=>'form-control','required'));
        
        ?>
            </div> 
        </div>
        
        
<!--        <div class="form-group required">
            <label for="title" class="col-md-3 control-label">From</label>
            <div class="col-md-10">
                
	<?php //echo $this->Form->control('from_person',array('div'=>false,'label'=>false,'class'=>'form-control','required'));
        
        ?>
            </div> 
        </div>-->
        
        </fieldset>
     <div class="form-group required">
            <?php if($email['emailtype']['id'] == 4 || $email['emailtype']['id'] == 6 || $email['emailtype']['id'] == 7) { ?>
                <label for="title" class="col-md-3 control-label">Interval After Due Date</label>
            <?php } else { ?>
                 <label for="title" class="col-md-3 control-label">Interval Before Due Date</label>   
            <?php } ?>
            <div class="col-md-10">
                
	<?php echo $this->Form->control('time_interval',array('div'=>false,'label'=>false,'class'=>'form-control','required','type'=>'text')); ?>
            </div> 
<?php if($email['emailtype']['id'] == 6 || $email['emailtype']['id'] == 7) { ?>
            <label for="title" class="col-md-3 control-label">Late Fee Amount</label>   
            
            <div class="col-md-10">
                
    <?php echo $this->Form->control('late_fee',array('div'=>false,'label'=>false,'class'=>'form-control','required','type'=>'text')); ?>
            </div> 
<?php } ?>
        </div>
    <fieldset>
        <div class="form-group required">
            <label for="title" class="col-md-3 control-label">Subject</label>
            <div class="col-md-10">
                
	<?php echo $this->Form->control('subject',array('div'=>false,'label'=>false,'class'=>'form-control','required','type'=>'text')); ?>
             
                
                
            </div> 
        </div>  
        
        
        
        </fieldset>
    <div class="form-group required">
            <label for="message" class="col-md-3 control-label" style="width: 100%;">Message
<br>
Insturctions: use {first_name} for user's first name and/or use {last_name} for user's last name
<br>
Example: Dear {first_name} {last_name}, 
  
</label>
            <div class="col-md-10">
                
	<?php echo $this->Form->control('message',array('div'=>false,'label'=>false,'class'=>'form-control ckeditor','required','type'=>'textarea')); ?>
             
                
            </div> 
        </div>  
      
    
    <div class="form-group">
        <div class="col-md-12 text-center">
        <?php 
        echo $this->Form->control('Edit Email',array('type'=>'submit','div'=>false,'label'=>false,'class'=>'btn btn-default btn-blue-custom btn-lg'));
        echo $this->Form->end(); ?>
        </div>
    </div>
        
                </div>

    
</div>
