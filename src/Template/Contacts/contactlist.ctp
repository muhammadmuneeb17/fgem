<?php?>
<div id="contactMessage" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-blue-custom" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="panel-head-info">
    <p class="pull-left info-text">Contacts List</p>
            <a href="<?php echo $this->request->webroot ?>pages" style="font-size:14px;" class="btn btn-primary btn-blue-custom pull-right text-info"><i class="fa fa-angle-left"></i> Go Back</a>
<div class="clearfix"></div>
</div>
<div class="container content-container contacts form">
    
    <div class="row">
        <div id="no-more-tables">
            <table class="col-md-12 table-bordered table-striped table-condensed cf">
        		<thead class="cf">
        			<tr>
        		<th><?php echo $this->Paginator->sort('fname','First Name'); ?></th>
			<th><?php echo $this->Paginator->sort('lname','Last Name'); ?></th>
			<th><?php echo $this->Paginator->sort('email'); ?></th>
<th><?php echo $this->Paginator->sort('phone'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
        			</tr>
        		</thead>
        		<tbody>
                            <?php foreach ($contacts as $contact): ?>
	<tr>
		<td data-title="First Name"><?php echo h(ucfirst($contact->fname)); ?>&nbsp;</td>
		<td data-title="Last Name"><?php echo h(ucfirst($contact->lname)); ?>&nbsp;</td>
		<td data-title="Email"><?php echo h($contact->email); ?>&nbsp;
                <input type="hidden" value= "<?php echo $contact->message; ?>" class="contact-message<?php echo $contact->id; ?>" id="<?php echo  ucfirst($contact->fname)." ".ucfirst($contact->lname); ?>"/>
                </td>
<td data-title="Phone"><?php 


echo h($contact->phone); ?>&nbsp;</td>
		<td data-title="Modified"><?php echo h(date('d.m.y',strtotime($contact->modified))); ?>&nbsp;</td>
		<td  data-title="Actions" class="actions">
                        <a href="javascript:" class="btn btn-primary view-message" id="contact-message<?php echo $contact->id; ?>"><i class="fa fa-envelope"></i> View Message</a>
			<?php //echo $this->Html->link(__('<i class="fa fa-envelope"></i> View Message'), array('controller' => 'contacts', 'action' => 'view',$contact->id),array('class'=>'btn btn-primary view-message','id'=>'contact-message'.$contact->id,'escape'=>false)); ?>
			<?= $this->Form->postLink(__('<i class="fa fa-trash"></i> Delete'), ['action' => 'delete', $contact->id], ['class'=>'btn btn-danger','escape'=>false,'confirm' => __('Are you sure you want to delete # {0}?', $contact->fname)]) ?>
		</td>
	</tr>
<?php endforeach; ?>
        		</tbody>
        	</table>
        </div>
    </div>
    <br />
<div class="paginator">
        <?php if(count($contacts) > 20 ) { ?>
        <ul class="pagination">
            <?php
                echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
            ?>
        </ul>
        <?php } ?>
        <p class="small"><?= $this->Paginator->counter(['format' => __('Page {{page}} de {{pages}}, affiche {{current}} entrée(s) sur un de total {{count}}')]) ?></p>
    </div>
</div>

<script>
$(".view-message").on('click',function(){
    var msg_class = $(this).attr('id');
    
    var msg_content = $("."+msg_class).val();
    
    var flname = $("."+msg_class).attr('id');
   
    $(".modal-title").text(flname+" Message")
    $(".modal-body").text(msg_content);
    $("#contactMessage").modal('show');
});

</script>