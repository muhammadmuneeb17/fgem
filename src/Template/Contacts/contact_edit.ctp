<div class="services form">
    <?php echo $this->Form->create($contact,array('type'=>'file','class'=>'form-horizontal','id' => 'ContactAddForm')); ?>
    <fieldset>
        <legend><?php echo __('Edit Contact Us'); ?>
            <a href="<?php echo $this->request->webroot ?>contacts/contactlist" style="font-size:14px;" class="pull-right text-info">[Go Back]</a>
        </legend>
        <div class="form-group">
            <label for="Phone" class="col-md-2 control-label">Phone</label>
            <div class="col-md-9">
	<?php echo $this->Form->input('phone',array('div'=>false,'label'=>false,'class'=>'form-control')); ?>
            </div>   
        </div>   
        <div class="form-group">
            <label for="Email" class="col-md-2 control-label">Email</label>
            <div class="col-md-9">
	<?php echo $this->Form->input('email',array('type'=>'email','div'=>false,'label'=>false,'class'=>'form-control')); ?>
            </div>   
        </div>   
        <div class="form-group">
            <label for="Address" class="col-md-2 control-label">Address</label>
            <div class="col-md-9">
	<?php echo $this->Form->input('address',array('type'=>'textarea','div'=>false,'label'=>false,'class'=>'form-control')); ?>
            </div>   
        </div>    
        <div class="form-group">
            <label for="Lat" class="col-md-2 control-label">Latitude</label>
            <div class="col-md-9">
	<?php echo $this->Form->input('lat',array('div'=>false,'label'=>false,'class'=>'form-control')); ?>
            </div>   
        </div>   
        <div class="form-group">
            <label for="Lng" class="col-md-2 control-label">Longitude</label>
            <div class="col-md-9">
	<?php echo $this->Form->input('lng',array('div'=>false,'label'=>false,'class'=>'form-control')); ?>
            </div>   
        </div>   

          
    </fieldset>
    <div class="form-group">
        <div class="col-md-11 text-center">
        <?php 
        echo $this->Form->input('Save',array('type'=>'submit','div'=>false,'label'=>false,'class'=>'btn btn-default btn-blue-custom btn-lg'));
        echo $this->Form->end(); ?>
        </div>
    </div>
</div>