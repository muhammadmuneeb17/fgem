<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\MemberRole $memberRole
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Member Role'), ['action' => 'edit', $memberRole->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Member Role'), ['action' => 'delete', $memberRole->id], ['confirm' => __('Are you sure you want to delete # {0}?', $memberRole->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Member Roles'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Member Role'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Roles'), ['controller' => 'Roles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Role'), ['controller' => 'Roles', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="memberRoles view large-9 medium-8 columns content">
    <h3><?= h($memberRole->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $memberRole->has('user') ? $this->Html->link($memberRole->user->id, ['controller' => 'Users', 'action' => 'view', $memberRole->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Role') ?></th>
            <td><?= $memberRole->has('role') ? $this->Html->link($memberRole->role->name, ['controller' => 'Roles', 'action' => 'view', $memberRole->role->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($memberRole->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($memberRole->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($memberRole->modified) ?></td>
        </tr>
    </table>
</div>
