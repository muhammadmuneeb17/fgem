<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\WeekHoliday[]|\Cake\Collection\CollectionInterface $weekHolidays
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Week Holiday'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Ticket Calendars'), ['controller' => 'TicketCalendars', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ticket Calendar'), ['controller' => 'TicketCalendars', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="weekHolidays index large-9 medium-8 columns content">
    <h3><?= __('Week Holidays') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ticket_calendar_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($weekHolidays as $weekHoliday): ?>
            <tr>
                <td><?= $this->Number->format($weekHoliday->id) ?></td>
                <td><?= $weekHoliday->has('user') ? $this->Html->link($weekHoliday->user->id, ['controller' => 'Users', 'action' => 'view', $weekHoliday->user->id]) : '' ?></td>
                <td><?= $weekHoliday->has('ticket_calendar') ? $this->Html->link($weekHoliday->ticket_calendar->id, ['controller' => 'TicketCalendars', 'action' => 'view', $weekHoliday->ticket_calendar->id]) : '' ?></td>
                <td><?= h($weekHoliday->date) ?></td>
                <td><?= h($weekHoliday->created) ?></td>
                <td><?= h($weekHoliday->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $weekHoliday->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $weekHoliday->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $weekHoliday->id], ['confirm' => __('Are you sure you want to delete # {0}?', $weekHoliday->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} de {{pages}}, affiche {{current}} entrée(s) sur un de total {{count}}')]) ?></p>
    </div>
</div>
