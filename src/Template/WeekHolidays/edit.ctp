<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $weekHoliday->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $weekHoliday->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Week Holidays'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Ticket Calendars'), ['controller' => 'TicketCalendars', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Ticket Calendar'), ['controller' => 'TicketCalendars', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="weekHolidays form large-9 medium-8 columns content">
    <?= $this->Form->create($weekHoliday) ?>
    <fieldset>
        <legend><?= __('Edit Week Holiday') ?></legend>
        <?php
            echo $this->Form->control('user_id', ['options' => $users, 'empty' => true]);
            echo $this->Form->control('ticket_calendar_id', ['options' => $ticketCalendars, 'empty' => true]);
            echo $this->Form->control('date', ['empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
