<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\WeekHoliday $weekHoliday
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Week Holiday'), ['action' => 'edit', $weekHoliday->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Week Holiday'), ['action' => 'delete', $weekHoliday->id], ['confirm' => __('Are you sure you want to delete # {0}?', $weekHoliday->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Week Holidays'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Week Holiday'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Ticket Calendars'), ['controller' => 'TicketCalendars', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ticket Calendar'), ['controller' => 'TicketCalendars', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="weekHolidays view large-9 medium-8 columns content">
    <h3><?= h($weekHoliday->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $weekHoliday->has('user') ? $this->Html->link($weekHoliday->user->id, ['controller' => 'Users', 'action' => 'view', $weekHoliday->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ticket Calendar') ?></th>
            <td><?= $weekHoliday->has('ticket_calendar') ? $this->Html->link($weekHoliday->ticket_calendar->id, ['controller' => 'TicketCalendars', 'action' => 'view', $weekHoliday->ticket_calendar->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($weekHoliday->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($weekHoliday->date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($weekHoliday->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($weekHoliday->modified) ?></td>
        </tr>
    </table>
</div>
