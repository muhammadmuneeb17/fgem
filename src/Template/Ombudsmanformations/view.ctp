<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Ombudsmanformation $ombudsmanformation
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Ombudsmanformation'), ['action' => 'edit', $ombudsmanformation->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Ombudsmanformation'), ['action' => 'delete', $ombudsmanformation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ombudsmanformation->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Ombudsmanformations'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ombudsmanformation'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="ombudsmanformations view large-9 medium-8 columns content">
    <h3><?= h($ombudsmanformation->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Formation') ?></th>
            <td><?= h($ombudsmanformation->formation) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Copy Upload') ?></th>
            <td><?= h($ombudsmanformation->copy_upload) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($ombudsmanformation->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ombudsman Id') ?></th>
            <td><?= $this->Number->format($ombudsmanformation->ombudsman_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Certificate Date') ?></th>
            <td><?= h($ombudsmanformation->certificate_date) ?></td>
        </tr>
    </table>
</div>
