<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $ombudsmanformation->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $ombudsmanformation->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Ombudsmanformations'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="ombudsmanformations form large-9 medium-8 columns content">
    <?= $this->Form->create($ombudsmanformation) ?>
    <fieldset>
        <legend><?= __('Edit Ombudsmanformation') ?></legend>
        <?php
            echo $this->Form->control('ombudsman_id');
            echo $this->Form->control('formation');
            echo $this->Form->control('certificate_date');
            echo $this->Form->control('copy_upload');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
