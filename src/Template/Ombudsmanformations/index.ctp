<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Ombudsmanformation[]|\Cake\Collection\CollectionInterface $ombudsmanformations
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Ombudsmanformation'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ombudsmanformations index large-9 medium-8 columns content">
    <h3><?= __('Ombudsmanformations') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ombudsman_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('formation') ?></th>
                <th scope="col"><?= $this->Paginator->sort('certificate_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('copy_upload') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ombudsmanformations as $ombudsmanformation): ?>
            <tr>
                <td><?= $this->Number->format($ombudsmanformation->id) ?></td>
                <td><?= $this->Number->format($ombudsmanformation->ombudsman_id) ?></td>
                <td><?= h($ombudsmanformation->formation) ?></td>
                <td><?= h($ombudsmanformation->certificate_date) ?></td>
                <td><?= h($ombudsmanformation->copy_upload) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $ombudsmanformation->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ombudsmanformation->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $ombudsmanformation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ombudsmanformation->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} de {{pages}}, affiche {{current}} entrée(s) sur un de total {{count}}')]) ?></p>
    </div>
</div>
