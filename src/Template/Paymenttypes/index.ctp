<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Paymenttype[]|\Cake\Collection\CollectionInterface $paymenttypes
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Paymenttype'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Accounts'), ['controller' => 'Accounts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Account'), ['controller' => 'Accounts', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="paymenttypes index large-9 medium-8 columns content">
    <h3><?= __('Paymenttypes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('payment_name') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($paymenttypes as $paymenttype): ?>
            <tr>
                <td><?= $this->Number->format($paymenttype->id) ?></td>
                <td><?= h($paymenttype->payment_name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $paymenttype->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $paymenttype->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $paymenttype->id], ['confirm' => __('Are you sure you want to delete # {0}?', $paymenttype->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} de {{pages}}, affiche {{current}} entrée(s) sur un de total {{count}}')]) ?></p>
    </div>
</div>
