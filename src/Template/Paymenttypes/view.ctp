<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Paymenttype $paymenttype
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Paymenttype'), ['action' => 'edit', $paymenttype->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Paymenttype'), ['action' => 'delete', $paymenttype->id], ['confirm' => __('Are you sure you want to delete # {0}?', $paymenttype->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Paymenttypes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Paymenttype'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Accounts'), ['controller' => 'Accounts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Account'), ['controller' => 'Accounts', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="paymenttypes view large-9 medium-8 columns content">
    <h3><?= h($paymenttype->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Payment Name') ?></th>
            <td><?= h($paymenttype->payment_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($paymenttype->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Accounts') ?></h4>
        <?php if (!empty($paymenttype->accounts)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Registeration Id') ?></th>
                <th scope="col"><?= __('Paymenttype Id') ?></th>
                <th scope="col"><?= __('Amount Paid') ?></th>
                <th scope="col"><?= __('Amount Due') ?></th>
                <th scope="col"><?= __('Amout Date') ?></th>
                <th scope="col"><?= __('Due Date') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($paymenttype->accounts as $accounts): ?>
            <tr>
                <td><?= h($accounts->id) ?></td>
                <td><?= h($accounts->registeration_id) ?></td>
                <td><?= h($accounts->paymenttype_id) ?></td>
                <td><?= h($accounts->amount_paid) ?></td>
                <td><?= h($accounts->amount_due) ?></td>
                <td><?= h($accounts->amout_date) ?></td>
                <td><?= h($accounts->due_date) ?></td>
                <td><?= h($accounts->user_id) ?></td>
                <td><?= h($accounts->status) ?></td>
                <td><?= h($accounts->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Accounts', 'action' => 'view', $accounts->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Accounts', 'action' => 'edit', $accounts->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Accounts', 'action' => 'delete', $accounts->id], ['confirm' => __('Are you sure you want to delete # {0}?', $accounts->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
