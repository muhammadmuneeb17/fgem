<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Ombudsmanaccreds Controller
 *
 * @property \App\Model\Table\OmbudsmanaccredsTable $Ombudsmanaccreds
 *
 * @method \App\Model\Entity\Ombudsmanaccred[] paginate($object = null, array $settings = [])
 */
class OmbudsmanaccredsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Ombudsmen']
        ];
        $ombudsmanaccreds = $this->paginate($this->Ombudsmanaccreds);

        $this->set(compact('ombudsmanaccreds'));
        $this->set('_serialize', ['ombudsmanaccreds']);
    }

    /**
     * View method
     *
     * @param string|null $id Ombudsmanaccred id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ombudsmanaccred = $this->Ombudsmanaccreds->get($id, [
            'contain' => ['Ombudsmen']
        ]);

        $this->set('ombudsmanaccred', $ombudsmanaccred);
        $this->set('_serialize', ['ombudsmanaccred']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ombudsmanaccred = $this->Ombudsmanaccreds->newEntity();
        if ($this->request->is('post')) {
            $ombudsmanaccred = $this->Ombudsmanaccreds->patchEntity($ombudsmanaccred, $this->request->getData());
            if ($this->Ombudsmanaccreds->save($ombudsmanaccred)) {
                $this->Flash->success(__('The ombudsmanaccred has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ombudsmanaccred could not be saved. Please, try again.'));
        }
        $ombudsmen = $this->Ombudsmanaccreds->Ombudsmen->find('list', ['limit' => 200]);
        $this->set(compact('ombudsmanaccred', 'ombudsmen'));
        $this->set('_serialize', ['ombudsmanaccred']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ombudsmanaccred id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ombudsmanaccred = $this->Ombudsmanaccreds->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ombudsmanaccred = $this->Ombudsmanaccreds->patchEntity($ombudsmanaccred, $this->request->getData());
            if ($this->Ombudsmanaccreds->save($ombudsmanaccred)) {
                $this->Flash->success(__('The ombudsmanaccred has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ombudsmanaccred could not be saved. Please, try again.'));
        }
        $ombudsmen = $this->Ombudsmanaccreds->Ombudsmen->find('list', ['limit' => 200]);
        $this->set(compact('ombudsmanaccred', 'ombudsmen'));
        $this->set('_serialize', ['ombudsmanaccred']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ombudsmanaccred id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ombudsmanaccred = $this->Ombudsmanaccreds->get($id);
        if ($this->Ombudsmanaccreds->delete($ombudsmanaccred)) {
            $this->Flash->success(__('The ombudsmanaccred has been deleted.'));
        } else {
            $this->Flash->error(__('The ombudsmanaccred could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
