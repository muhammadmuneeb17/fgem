<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;
use Cake\Routing\Router;
/**
 * Contacts Controller
 *
 * @property \App\Model\Table\ContactsTable $Contacts
 *
 * @method \App\Model\Entity\Contact[] paginate($object = null, array $settings = [])
 */
class ContactsController extends AppController
{

    public function isAuthorized($user) {
        if (in_array($this->request->action, ['edit'])) {
            $id = (int) $this->request->params['pass'][0];
            if ($id == $user['id']) {
                return true;
            }
        }
        if (in_array($this->request->action, ['logout'])) {
            return true;
        }

        // Admin can access every action
        if ($this->viewVars['actionPermission'] != "") {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function contactlist()
    {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 1 || $permission == 2) {

        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $contacts = $this->paginate($this->Contacts);


        $this->set(compact('contacts'));
        $this->set('_serialize', ['contacts']);
        $this->viewBuilder()->setLayout('backend');
    }

    /**
     * View method
     *
     * @param string|null $id Contact id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 1 || $permission == 2) {

        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $contact = $this->Contacts->get($id, [
            'contain' => []
        ]);

        $this->set('contact', $contact);
        $this->set('_serialize', ['contact']);
        $this->viewBuilder()->setLayout('backend');
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function contactAdd()
    {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 2) {

        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $contact = $this->Contacts->newEntity();
        if ($this->request->is('post')) {
            $contact = $this->Contacts->patchEntity($contact, $this->request->getData());
            if ($this->Contacts->save($contact)) {
                $this->Flash->success(__('The contact has been saved.'));

                return $this->redirect(['action' => 'contactlist']);
            }
            $this->Flash->error(__('The contact could not be saved. Please, try again.'));
        }
        $this->set(compact('contact'));
        $this->set('_serialize', ['contact']);
        $this->viewBuilder()->setLayout('backend');
    }

    /**
     * Edit method
     *
     * @param string|null $id Contact id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function contactEdit($id = null)
    {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 2) {

        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $contact = $this->Contacts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $contact = $this->Contacts->patchEntity($contact, $this->request->getData());
            if ($this->Contacts->save($contact)) {
                $this->Flash->success(__('The contact has been saved.'));

                return $this->redirect(['action' => 'contactlist']);
            }
            $this->Flash->error(__('The contact could not be saved. Please, try again.'));
        }
        $this->set(compact('contact'));
        $this->set('_serialize', ['contact']);
        $this->viewBuilder()->setLayout('backend');
    }

    /**
     * Delete method
     *
     * @param string|null $id Contact id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 2) {

        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $this->request->allowMethod(['post', 'delete']);
        $contact = $this->Contacts->get($id);
        if ($this->Contacts->delete($contact)) {
            $this->Flash->success(__('The contact has been deleted.'));
        } else {
            $this->Flash->error(__('The contact could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'contactlist']);
    }
    public function submitform() {
        $this->viewBuilder()->setLayout('');
        $this->autoRender = false;

        $contact = $this->Contacts->newEntity();
        if ($this->request->is('post')) {
            //            debug($this->request->data);
            //            exit;
            $this->request->data['Contact']['fname'] = $this->request->data['user_fname'];
            $this->request->data['Contact']['lname'] = $this->request->data['user_lname'];
            $this->request->data['Contact']['email'] = $this->request->data['user_email'];
//            $this->request->data['Contact']['phone'] = $this->request->data['phone_number'];
            $this->request->data['Contact']['message'] = $this->request->data['msg'];
            $this->request->data['Contact']['status'] = 1;
            $this->request->data['Contact']['modified'] = date('Y-m-d H:i:s',time());

            $contact = $this->Contacts->patchEntity($contact, $this->request->data['Contact']);
            //            debug($contact);
            //            exit;
            if ($this->Contacts->save($contact)) {
                //echo json_encode(array('type' => 'success', 'text' => $contact));
                //  echo "success";
                //    exit;
                /*  $themeColor = "#242424";
                $fontColor = "#2CCACB";
                $primaryColor = "#D40000";
                $subjectColor = "#ffffff";
                $Email = new Email();
                $Email->from(array('info@domain.com' => 'Company Name'));
                $Email->emailFormat('both');
                    $Email->subject('Customer Inquiry');
                    $Email->to("zuber@cyberclouds.com");


                $message = "<html><head>
			<style>
				h2 {
					background-color:$themeColor;
					color:$fontColor;
					text-align:center;
				}
				table {
					border:1px solid $themeColor;
                                        background-color: #f8f8f8; 
				}
				tr , td , th{
					border:1px solid $themeColor;
				}
.subject {
background-color: $primaryColor;
padding:10px 5px;
}
h3 {
color: $subjectColor;
text-align:center;
}
img {
display:block;
margin:auto;
width:300px;
}
			</style>	
			</head><body>
                        <div class='subject'><img src='http://company.com/img/backend-logo.png' width='300'/>
<h3>" . $type . " Inquiry</h3></div>
			<table cellpadding='10' align='center' width='100%' cellspacing='0'>
				<tr>
					<th>Full Name</th>
					<td>" . ucwords($this->request->data['Contact']['fname']) . ' ' . ucwords($this->request->data['Contact']['lname']) . "</td>	
				</tr>
				<tr>
					<th>Email</th>
					<td>" . $this->request->data['Contact']['email'] . "</td>	
				</tr>

				<tr>
					<th>Message</th>
					<td>" . $this->request->data['Contact']['message'] . "</td>	
				</tr>
			</table>
<br />
</body></html>";

                //$Email->send($message);
				*/
                echo json_encode(array('type' => 'success', 'text' => 'Merci pour votre intérêt, nous vous contacterons bientôt'));
            } else {

                echo json_encode(array('type' => 'error', 'text' => 'Une erreur est survenue, réessayez plus tard'));
            }
        }
    }
}
