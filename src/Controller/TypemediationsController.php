<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Typemediations Controller
 *
 * @property \App\Model\Table\TypemediationsTable $Typemediations
 *
 * @method \App\Model\Entity\Typemediation[] paginate($object = null, array $settings = [])
 */
class TypemediationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $typemediations = $this->paginate($this->Typemediations);

        $this->set(compact('typemediations'));
        $this->set('_serialize', ['typemediations']);
    }

    /**
     * View method
     *
     * @param string|null $id Typemediation id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $typemediation = $this->Typemediations->get($id, [
            'contain' => []
        ]);

        $this->set('typemediation', $typemediation);
        $this->set('_serialize', ['typemediation']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $typemediation = $this->Typemediations->newEntity();
        if ($this->request->is('post')) {
            $typemediation = $this->Typemediations->patchEntity($typemediation, $this->request->getData());
            if ($this->Typemediations->save($typemediation)) {
                $this->Flash->success(__('The typemediation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The typemediation could not be saved. Please, try again.'));
        }
        $this->set(compact('typemediation'));
        $this->set('_serialize', ['typemediation']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Typemediation id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $typemediation = $this->Typemediations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $typemediation = $this->Typemediations->patchEntity($typemediation, $this->request->getData());
            if ($this->Typemediations->save($typemediation)) {
                $this->Flash->success(__('The typemediation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The typemediation could not be saved. Please, try again.'));
        }
        $this->set(compact('typemediation'));
        $this->set('_serialize', ['typemediation']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Typemediation id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $typemediation = $this->Typemediations->get($id);
        if ($this->Typemediations->delete($typemediation)) {
            $this->Flash->success(__('The typemediation has been deleted.'));
        } else {
            $this->Flash->error(__('The typemediation could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
