<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;

/**
 * Accounts Controller
 *
 * @property \App\Model\Table\AccountsTable $Accounts
 *
 * @method \App\Model\Entity\Account[] paginate($object = null, array $settings = [])
 */
class AccountsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    // payment detail of individual registered members
    public function index($id = null) {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 1 || $permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        if (!empty($_GET['id'])) {
            $id = $_GET['id'];
        }
        if ($id) {
            $this->paginate = [
                'contain' => ['Registerations', 'Paymenttypes', 'Users'],
                'conditions' => ['Accounts.Registeration_id' => $id],
            ];
            $us = $this->Accounts->find('all', array(
                        'contain' => ['Registerations', 'Paymenttypes', 'Users'],
                        'conditions' => ['Accounts.Registeration_id' => $id],
                        'order' => array('Accounts.id DESC')
                    ))->first();
        }

        $this->loadModel('Registerations');

        if ($id) {
            $user = $this->Registerations->find('all', array(
                        'contain' => 'Users',
                        'conditions' => ['Registerations.id' => $id],
                    ))->first();
        }


        if ($user != "") {
            $this->set(compact('user'));
        }
        $accounts = $this->paginate($this->Accounts);
        //debug($accounts);

        $this->set(compact('accounts', 'us'));
        $this->set('_serialize', ['accounts']);

        $this->viewBuilder()->setLayout('backend');
    }
// report can be generated here
    public function report() {
        $conn = ConnectionManager::get('default');
        $fields;
        $joins;
        $condition = 'u.role_id=6';

        $permission = $this->viewVars['actionPermission'];
        $this->set(compact('permission'));

        if ($this->request->is('post')) {
            if (!empty($this->request->data['search'])) {
                $search = str_replace(" ", "", $this->request->data['search']);
                $condition .= ' AND CONCAT(first_name, last_name) like "%' . $search . '%"';
                //$condition .= ' AND accounts.paymenttype_id=' . $this->request->data['pay'];
            }
            if (!empty($this->request->data['pay'])) {
                $condition .= ' AND a.paymenttype_id=' . $this->request->data['pay'];
            }

            if (isset($this->request->data['status'])) {

                if ($this->request->data['status'] == 0) {
                    $condition .= ' AND u.status = 2 AND r.status = 0';
                }
                if ($this->request->data['status'] == 2) {
                    $condition .= ' AND u.status <= 1 AND r.status = 0';
                }
                if ($this->request->data['status'] == 1) {
                    $condition .= ' AND u.status = 2  AND r.status = 1';
                }
            }
            if (!empty($this->request->data['amount_paid'])) {
                $condition .= ' AND a.amount_paid =' . $this->request->data['amount_paid'];
            }
            if (!empty($this->request->data['amount_due'])) {
                $condition .= ' AND a.amount_due=' . $this->request->data['amount_due'];
            }

            if (!empty($this->request->data['membertypes'])) {
                $condition .= ' AND r.membertype_id=' . $this->request->data['membertypes'];
            }

            if (!empty($this->request->data['to_amount_date']) && !empty($this->request->data['from_amount_date'])) {
                $condition .= ' AND DATE(a.amout_date) between "' . $this->request->data['from_amount_date'] . '" AND "' . $this->request->data['to_amount_date'] . '"';
            } else
            if (!empty($this->request->data['from_amount_date'])) {
                // $condition .= ' AND event_types.amount between ' . $minprice . ' AND ' . $maxprice
                $condition .= ' AND DATE(a.amout_date)=  "' . $this->request->data['from_amount_date'] . '"';
            }

            if (!empty($this->request->data['to_due_date']) && !empty($this->request->data['from_due_date'])) {
                $condition .= ' AND DATE(a.due_date) between "' . $this->request->data['from_due_date'] . '" AND "' . $this->request->data['to_due_date'] . '"';
            } else
            if (!empty($this->request->data['from_due_date'])) {
                // $condition .= ' AND event_types.amount between ' . $minprice . ' AND ' . $maxprice
                $condition .= ' AND DATE(a.due_date)= "' . $this->request->data['from_due_date'] . '"';
            }
            //            if (!empty($this->request->data['amount_due'])) {
            //                $condition .= 'accounts.amount_due=' . $this->request->data['amount_due'];
            //            }
        }
        if (isset($_GET['showdetails']) and $_GET['showdetails'] == 1) {
            $query = $conn->execute(
                    'SELECT  a.`registeration_id`, a.`amount_due` , u.`first_name` , u.`last_name` , u.`first_name` , p.`payment_name`, m.`description`, m.`name` , a.`amout_date`, a.`due_date`,a.`amount_paid`,m.`amount`'
                    //. $fields
                    . ' FROM users u  '
                    . 'inner join registerations r ON u.id= r.user_id '
                    . 'inner join accounts a ON r.id= a.registeration_id '
                    //. 'inner join comments ON accounts.id= comments.account_id '
                    . 'inner join paymenttypes p ON p.id = a.paymenttype_id '
                    . 'inner join membertypes m ON m.id= r.membertype_id '
                    //. $joins
                    . 'where ' . $condition . ' order by u.first_name ASC,u.last_name ASC,a.amount_due DESC  ');
        } else {
            $query = $conn->execute(
                    'SELECT  a.`registeration_id`, a.`amount_due` , u.`first_name` , u.`last_name` , u.`first_name` , p.`payment_name`, m.`description`, m.`name` , a.`amout_date`, a.`due_date`,a.`amount_paid`,m.`amount`'
                    //. $fields
                    . ' FROM accounts a  '
                    . 'inner join registerations r  ON r.id= a.registeration_id '
                    . 'inner join users u ON u.id= r.user_id '
                    //. 'inner join comments ON accounts.id= comments.account_id '
                    . 'inner join paymenttypes p ON p.id = a.paymenttype_id '
                    . 'inner join membertypes m ON m.id= r.membertype_id '
                    //. $joins
                    . 'where  a.id IN (
                SELECT MAX(id)
                FROM accounts 
                GROUP BY registeration_id
            ) AND ' . $condition . ' order by a.amount_due DESC ');
        }




        $results = $query->fetchAll('assoc');

        $this->loadModel('Membertypes');
        $membertypes = $this->Membertypes->find('list', ['limit' => 200]);

        $paymenttypes = $this->Accounts->Paymenttypes->find('list', ['limit' => 200, ['keyField' => 'id', 'valueField' => 'payment_name']]);
        $this->set(compact('results', 'paymenttypes', 'membertypes'));
        $this->viewBuilder()->setLayout('backend');
    }

    public function export($id = null) {
        if (!empty($_GET['id'])) {
            echo $id = $_GET['id'];
        }
        //        $this->paginate = [
        //            'contain' => ['Registerations', 'Paymenttypes', 'Users'],
        //            'conditions' => ['Accounts.registeration_id' => $id]
        //        ];
        //        $this->loadModel('Registerations');
        //         if ($id) {
        //            $user = $this->Registerations->find('all',array(
        //                'contain'=> 'Users',
        //    'conditions' => ['registerations.id' => $id],
        //    
        //))->first();
        //         }
        //           
        //           
        //        if ($user != "") {
        //            $this->set(compact('user'));
        //           
        //        }
        //        $accounts = $this->paginate($this->Accounts);
        $conn = ConnectionManager::get('default');
        $sql1 = "select * from accounts";

        $query = $conn->execute($sql1);

        $accounts = $query->fetchAll('assoc');

        $this->set(compact('accounts'));
        $this->set('_serialize', ['accounts']);

        $this->viewBuilder()->setLayout('backend');
    }

    public function changeStatus($id = null, $status = null) {
        $id = $_GET['id'];
        $accont_id = $_GET['accont_id'];
        $status = $_GET['status'];
        $regs = TableRegistry::get('Accounts');
        $reg = $regs->get($id);
        $reg->status = $status;
        if ($regs->save($reg)) {

            if ($reg->status == 1) {
                //                $email = new Email();
                //                $email->template('default', 'default')
                //                        ->emailFormat('html')
                //                        ->to($user->email)
                //                        ->from(['info@cyberclouds.com' => 'welvett.com'])
                //                        ->subject('Welvet Account Verified')
                //                        ->viewVars(['content' => $user])
                //                        ->send();
            }
            $this->Flash->success('status changed');
        } else {
            $this->Flash->error('status not changed');
        }
        //        if ($reg->role_id == 3) {
        //            return $this->redirect('/Users/corporate');
        //        } else {
        return $this->redirect(['action' => 'index', $accont_id]);
    }

    /**
     * View method
     *
     * @param string|null $id Account id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $account = $this->Accounts->get($id, [
            'contain' => ['Registerations', 'Paymenttypes', 'Users', 'Comments']
        ]);

        $this->set('account', $account);
        $this->set('_serialize', ['account']);
    }

    public function email() {
        $conn = ConnectionManager::get('default');
        $fields;
        $joins;
        $condition = 'users.role_id=6';
        $condition .= '  AND DATE(accounts.due_date) = DATE((NOW()) + INTERVAL 1 DAY) + INTERVAL 0 SECOND';

        $query = $conn->execute(
                'SELECT *, DATE(amout_date) '
                //. $fields
                . ' FROM users  '
                . 'inner join registerations ON users.id= registerations.user_id '
                . 'inner join accounts ON registerations.id= accounts.registeration_id '
                . 'inner join comments ON accounts.id= comments.account_id '
                . 'inner join paymenttypes ON paymenttypes.id= accounts.paymenttype_id '
                . 'inner join membertypes ON membertypes.id=registerations.membertype_id '
                //. $joins
                . 'where ' . $condition);

        $results1 = $query->fetchAll('assoc');
        debug($results1);
        exit;
        $this->loadModel('Emails');
        $user = 'khannking';
        $email = $this->Emails->find('all', array(
                    'conditions' => ['Emailtypes.id' => '1'],
                    'contain' => ['Emailtypes']
                ))->first();
        // debug($email['title']);
        $title = $email['title'];
        $from_person = $email['from_person'];
        $to_person = $email['to_person'];
        $subject = $email['subject'];
        $content = $email['message'];
        $message = str_replace("{first_name,last_name}", $message, $content);



        $Email = new Email();
        $Email->from(array($from_person => $title));
        $Email->emailFormat('both');
        $Email->to($to_person);
        $Email->subject($subject);
        // $message = $content;
        //$message .=$content;
        $Email->send($message);

        exit;
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
//    add new payment for each individuals
    public function add($id = null) {

        $permission = $this->viewVars['actionPermission'];

        if ($permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $this->loadModel('Registerations');
        if ($id) {
            $user = $this->Registerations->find('all', array(
                        'contain' => ['Users', 'Membertypes'],
                        //'contain'=> 'Membertypes',
                        'conditions' => ['Registerations.id' => $id],
                    ))->first();
        }
        $this->loadModel('MemberTypeAmounts');
        $typeAmount = $this->MemberTypeAmounts->find('all')
                        ->where(['membertype_id' => $user->membertype_id, 'year' => date('Y')])->first();
        if (empty($typeAmount)) {
            $typeAmount = $this->MemberTypeAmounts->find('all')
                            ->where(['membertype_id' => $user->membertype_id, 'year' => date('Y') - 1])->first();
        }
        if ($user != "") {
            $this->set(compact('user'));
        }
        $userId = $this->request->session()->read('Auth.User.id');

        $this->loadModel('Users');
        $roleId = $this->request->session()->read('Auth.User.role_id');

        $status = $this->Accounts->find('all', array(
                    'contain' => 'Registerations',
                    'conditions' => ['Registerations.id' => $id],
                ))->first();
        $due = $this->Accounts->find('all', array(
                    'contain' => 'Registerations',
                    'conditions' => ['Registerations.id' => $id],
                    'order' => ['Accounts.id' => 'DESC']
                ))->first();

        $currentYear = $this->Accounts->find('all', array(
                    'contain' => 'Registerations',
                    'conditions' => ['Registerations.id' => $id, 'Accounts.amout_date LIKE' => '%' . date('Y') . '%'],
                    'order' => ['Accounts.id' => 'DESC']
                ))->first();
        if ($currentYear == null) {
            $amount = $due['amount_due'] + $typeAmount['amount'];
        } else {
            $amount = $due['amount_due'];
        }

//        if (empty($due)) {
//
//            //debug($user['membertype']['amount']);
//            $amount = $user['membertype']['amount'];
//            $due_date = '';
//            $this->set(compact('amount', 'due_date'));
//        } else {
//
//            $amount = $due['amount_due'];
//
//            $due_date = date("Y-m-d", strtotime($due['due_date']));
//            $this->set(compact('amount', 'due_date'));
//            if ($due['amount_due'] == 0) {
//                $amount = $user['membertype']['amount'];
//            } else {
//                $amount = $due['amount_due'];
//            }
//        }
        $this->set(compact('amount'));
        //exit;
        $account = $this->Accounts->newEntity();
        if ($this->request->is('post')) {

            $year = $this->request->data['due_date'];

            $this->loadModel('NextRenewalDate');

            $findYear = $this->NextRenewalDate->find('all', [
                        'conditions' => ['NextRenewalDate.year' => $year]
                    ])->toArray();

            if (count($findYear) > 0) {
                if ($findYear[0]->month < 10) {
                    $month = '0' . $findYear[0]->month;
                } else {
                    $month = $findYear[0]->month;
                }
                if ($findYear[0]->day < 10) {
                    $day = '0' . $findYear[0]->day;
                } else {
                    $day = $findYear[0]->day;
                }
                $this->request->data['due_date'] = $year . '-' . $month . '-' . $day;
            } else {
                $this->request->data['due_date'] = $year . '-12-31';
            }

            //            if(empty($status)){
            //                $this->request->data['amount_due']=$this->request->data['amount_paid'];
            //            }
            //            else{
            $this->request->data['amount_due'] = $amount - $this->request->data['amount_paid'];
//            debug($this->request->data['amount_due']);
//            exit;
            //            }
            //            if(strtotime($this->request->data['due_date']) < strtotime(date('Y-m-d '))){
            //                $this->Flash->error(__('The due date is less than current date'));
            //                return $this->redirect(['action' => 'add', $id]);
            //
            //            }
            //$this->request->data['amout_date']=date('Y-m-d H:i:s', strtotime($this->request->data['amount_date']));
            $this->request->data['status'] = 1;
            $this->request->data['amout_date'] = date('Y-m-d H:i:s');

            $this->request->data['due_date'] = date('Y-m-d H:i:s', strtotime($this->request->data['due_date']));

            $account = $this->Accounts->patchEntity($account, $this->request->getData());
            //            debug($account);
            //            exit;
            if ($result = $this->Accounts->save($account)) {
                $this->loadModel('Comments');
                $acc = $result->id;
                $comments = $this->Comments->newEntity();
                $this->request->data['Comments']['user_id'] = $userId;
                $this->request->data['Comments']['account_id'] = $acc;
                $this->request->data['Comments']['comment'] = $this->request->data['comment'];


                $comments = $this->Comments->patchEntity($comments, $this->request->data['Comments']);


                if ($this->Comments->save($comments)) {
                    
                }

                //STORE LOGS
                $this->loadModel('Logs');

                $findRole = $this->Users->get($userId, [
                    'contain' => ['Roles']
                ]);

                $roleName = $findRole->role->name;

                $activity = 'New Account Added';
                $cityName = $user['user']['first_name'];

                $note = $roleName . ' added a account (' . $cityName . ') the comment is ' . $this->request->data['comment'];

                $logs = $this->Logs->newEntity();
                $this->request->data['Log']['user_id'] = $userId;
                $this->request->data['Log']['activity'] = $activity;
                $this->request->data['Log']['note'] = $note;

                $logs = $this->Logs->patchEntity($logs, $this->request->data['Log']);

                if ($this->Logs->save($logs)) {
                    $this->Flash->success(__('Le compte a été enregistré.'));
                    return $this->redirect(['action' => 'index', $id]);
                }
                $this->Flash->success(__('Le compte a été enregistré.'));

                return $this->redirect(['action' => 'index', $id]);
            }
            $this->Flash->error(__('Le compte n\'a pas pu être enregistré. Veuillez réessayer.'));
        }
        $registerations = $this->Accounts->Registerations->find('list', ['limit' => 200]);
        $paymenttypes = $this->Accounts->Paymenttypes->find('list', ['limit' => 200, ['keyField' => 'id', 'valueField' => 'payment_name']]);
        $users = $this->Accounts->Users->find('list', ['limit' => 200]);
        $this->set(compact('account', 'registerations', 'paymenttypes', 'users'));
        $this->set('_serialize', ['account']);
        $this->viewBuilder()->setLayout('backend');
    }

    /**
     * Edit method
     *
     * @param string|null $id Account id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
//    payment can be edited here
    public function edit($id = null, $accont_id = null) {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $id = $_GET['id'];
        $accont_id = $_GET['accont_id'];
        $this->loadModel('Registerations');




        $account = $this->Accounts->get($id, [
            'contain' => ['Registerations' => 'Users']
        ]);


        $amount = $account['amount_paid'];
        $amount_due = $account['amount_due'];
        $conn = ConnectionManager::get('default');
        $query = $conn->execute('SELECT  * '
                . ' FROM accounts '
                . 'where accounts.id> ' . $id . ' OR accounts.id < ' . $id
        );
        $search = $query->fetchAll('assoc');
        //debug($search);exit;
        if ($this->request->is(['patch', 'post', 'put'])) {

            $year = $this->request->data['due_date'];

            $this->loadModel('NextRenewalDate');

            $findYear = $this->NextRenewalDate->find('all', [
                        'conditions' => ['NextRenewalDate.year' => $year]
                    ])->toArray();

            if (count($findYear) > 0) {
                if ($findYear[0]->month < 10) {
                    $month = '0' . $findYear[0]->month;
                } else {
                    $month = $findYear[0]->month;
                }
                if ($findYear[0]->day < 10) {
                    $day = '0' . $findYear[0]->day;
                } else {
                    $day = $findYear[0]->day;
                }
                $this->request->data['due_date'] = $year . '-' . $month . '-' . $day;
            } else {
                $this->request->data['due_date'] = $year . '-12-31';
            }

            $userId = $this->request->session()->read('Auth.User.id');

            $this->loadModel('Users');
            $roleId = $this->request->session()->read('Auth.User.role_id');
            $this->request->data['due_date'] = date('Y-m-d H:i:s', strtotime($this->request->data['due_date']));


            if ($this->request->data['amount_paid'] < $amount) {
                $amount = $amount - $this->request->data['amount_paid'];
                $this->request->data['amount_due'] = $amount_due + $amount;
            } else
            if ($this->request->data['amount_paid'] > $amount) {
                $amount = $this->request->data['amount_paid'] - $amount;
                $this->request->data['amount_due'] = $amount_due - $amount;
            }
            //exit;
            $account = $this->Accounts->patchEntity($account, $this->request->getData());

            //debug($account);exit;
            if ($result = $this->Accounts->save($account)) {

                $this->loadModel('Comments');
                $acc = $result->id;
                $comments = $this->Comments->newEntity();
                $this->request->data['Comments']['user_id'] = $userId;
                $this->request->data['Comments']['account_id'] = $acc;
                $this->request->data['Comments']['comment'] = $this->request->data['comment'];



                $comments = $this->Comments->patchEntity($comments, $this->request->data['Comments']);


                if ($this->Comments->save($comments)) {
                    
                }

                //STORE LOGS
                $this->loadModel('Logs');

                $findRole = $this->Users->get($userId, [
                    'contain' => ['Roles']
                ]);

                $roleName = $findRole->role->name;

                $activity = 'Account Updated';
                $cityName = 'store by';

                $note = $roleName . ' Updated a account (' . $id . ')';

                $logs = $this->Logs->newEntity();
                $this->request->data['Log']['user_id'] = $userId;
                $this->request->data['Log']['activity'] = $activity;
                $this->request->data['Log']['note'] = $note;

                $logs = $this->Logs->patchEntity($logs, $this->request->data['Log']);

                if ($this->Logs->save($logs)) {
                    $this->Flash->success(__('Le compte a été enregistré.'));
                    return $this->redirect(['action' => 'index', $accont_id]);
                }
                $this->Flash->success(__('Le compte a été enregistré.'));

                return $this->redirect(['action' => 'index', $accont_id]);
            }
            $this->Flash->error(__('Le compte n\'a pas pu être enregistré. Veuillez réessayer.'));
        }
        $registerations = $this->Accounts->Registerations->find('list', ['limit' => 200]);
        $paymenttypes = $this->Accounts->Paymenttypes->find('list', ['limit' => 200, ['keyField' => 'id', 'valueField' => 'payment_name']]);
        $users = $this->Accounts->Users->find('list', ['limit' => 200]);
        $this->set(compact('account', 'registerations', 'paymenttypes', 'users', 'id', 'accont_id'));
        $this->set('_serialize', ['account']);
        $this->viewBuilder()->setLayout('backend');
    }

    /**
     * Delete method
     *
     * @param string|null $id Account id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
//    delition payment is done here
    public function delete($id = null, $accont_id = null) {


        $permission = $this->viewVars['actionPermission'];

        if ($permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));

        $id = $_GET['id'];
        $accont_id = $_GET['accont_id'];
        $this->loadModel('Registerations');
        $this->request->allowMethod(['post', 'delete', 'get']);
        $account = $this->Accounts->get($id);
        if ($this->Accounts->delete($account)) {
            $userId = $this->request->session()->read('Auth.User.id');

            $this->loadModel('Users');
            $roleId = $this->request->session()->read('Auth.User.role_id');
            //STORE LOGS
            $this->loadModel('Logs');

            $findRole = $this->Users->get($userId, [
                'contain' => ['Roles']
            ]);

            $roleName = $findRole->role->name;

            $activity = 'New Account Added';
            $cityName = 'deleted by';

            $note = $roleName . ' deleted a account (' . $id . ')';

            $logs = $this->Logs->newEntity();
            $this->request->data['Log']['user_id'] = $userId;
            $this->request->data['Log']['activity'] = $activity;
            $this->request->data['Log']['note'] = $note;

            $logs = $this->Logs->patchEntity($logs, $this->request->data['Log']);

            if ($this->Logs->save($logs)) {
                $this->Flash->success(__('Le compte a été supprimé'));
                return $this->redirect(['action' => 'index', $accont_id]);
            }
            $this->Flash->success(__('Le compte a été supprimé.'));

            return $this->redirect(['action' => 'index', $accont_id]);
            $this->Flash->success(__('Le compte a été supprimé.'));
        } else {
            $this->Flash->error(__('Le compte n\'a pas pu être supprimé. Veuillez réessayer.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /* CRON JOBS FUNCTIONS */

    public function feeRequest() {

        $this->loadModel('Emails');
        $email = $this->Emails->find('all', array(
                    'conditions' => ['Emailtypes.id' => '1'],
                    'contain' => ['Emailtypes']
                ))->first();

        $conn = ConnectionManager::get('default');
        $fields;
        $joins;
        $condition = 'u.role_id=6 AND r.status=1 ';
        $condition .= '  AND DATE(a.due_date) -  DATE(NOW()) =  ' . $email->time_interval;

        $query = $conn->execute(
                'SELECT  CONCAT(u.first_name, u.last_name) as user,u.email as emails, u.first_name, u.last_name,r.id as reid, a.id'
                //. $fields
                . ' FROM users u '
                . 'inner join registerations r ON u.id= r.user_id '
                . 'inner join accounts a ON r.id= a.registeration_id '
                // . 'inner join comments ON accounts.id= comments.account_id '
                //. 'inner join paymenttypes ON paymenttypes.id= accounts.paymenttype_id '
                . 'inner join membertypes m ON m.id=r.membertype_id '
                //. $joins
                . 'where  a.id IN (
                SELECT MAX(id)
                FROM accounts 
                GROUP BY registeration_id
            ) AND ' . $condition . ' order by a.amount_due DESC');

        $results1 = $query->fetchAll('assoc');



        //debug($email['interval']);
        // if($email['interval']==1){
        foreach ($results1 as $result):
            $user = $result['first_name'] . " " . $result['last_name'];

            //                  $query2 = $conn->execute(
            //                'SELECT  max(amount_due) from accounts ' 
            //                
            //                . 'where accounts.due_date='.$result['due_date']. 'and registerations.id=' .$result['reid']);
            //                  $results2 = $query2->fetchAll('assoc');
            //                         
            // debug($email['title']);
            $title = $email['title'];
            $from_person = 'info@cyberclouds.com';
            $to_person = $result['emails'];

            $subject = $email['subject'];
            $content = $email['message'];
            //$message = str_replace("{first_name,last_name}",$user, $content);
            $message = "";
            $message .= "<span style='background:#161616;display:block;padding:10px;'><img src='http://needanecommercesite.com/fgem/img/logo-light.png' style='display:block;margin:auto;max-width: 130px;'/></span>";

            $trans = array("{first_name,last_name}" => $user, "{first_name}" => $result['first_name'], "{last_name}" => $result['last_name'], "{first_name} {last_name}" => $user);
            $message .= strtr($content, $trans);


            //debug($message);exit;
            $Email = new Email();
            $Email->from(array($from_person => $title));
            $Email->emailFormat('both');
            $Email->to($to_person);
            $Email->subject($subject);
            // $message = "Dear ".$user.",</br></br>" ;
            //$message .=$content;
            $Email->send($message);

        endforeach;

        //}

        exit;
    }

    public function feeReminder1() {

        $this->loadModel('Emails');
        $email = $this->Emails->find('all', array(
                    'conditions' => ['Emailtypes.id' => '2'],
                    'contain' => ['Emailtypes']
                ))->first();

        $conn = ConnectionManager::get('default');
        $fields;
        $joins;
        $condition = 'u.role_id=6 AND r.status=1 ';
        $condition .= '  AND DATE(a.due_date) -  DATE(NOW()) =  ' . $email->time_interval;

        $query = $conn->execute(
                'SELECT  CONCAT(u.first_name, u.last_name) as user,u.email as emails, u.first_name, u.last_name,r.id as reid, a.id'
                //. $fields
                . ' FROM users u '
                . 'inner join registerations r ON u.id= r.user_id '
                . 'inner join accounts a ON r.id= a.registeration_id '
                // . 'inner join comments ON accounts.id= comments.account_id '
                //. 'inner join paymenttypes ON paymenttypes.id= accounts.paymenttype_id '
                . 'inner join membertypes m ON m.id=r.membertype_id '
                //. $joins
                . 'where  a.id IN (
                SELECT MAX(id)
                FROM accounts 
                GROUP BY registeration_id
            ) AND ' . $condition . ' order by a.amount_due DESC');

        $results1 = $query->fetchAll('assoc');


        //debug($email['interval']);
        // if($email['interval']==1){
        foreach ($results1 as $result):
            $user = $result['first_name'] . " " . $result['last_name'];
            $title = $email['title'];
            $from_person = 'info@cyberclouds.com';
            $to_person = $result['emails'];

            $subject = $email['subject'];
            $content = $email['message'];
            //$message = str_replace("{first_name,last_name}",$user, $content);
            $message = "";
            $message .= "<span style='background:#161616;display:block;padding:10px;'><img src='http://needanecommercesite.com/fgem/img/logo-light.png' style='display:block;margin:auto;max-width: 130px;'/></span>";

            $trans = array("{first_name,last_name}" => $user, "{first_name}" => $result['first_name'], "{last_name}" => $result['last_name'], "{first_name} {last_name}" => $user);
            $message .= strtr($content, $trans);
            //echo $message; 
            //debug($message);exit;
            $Email = new Email();
            $Email->from(array($from_person => $title));
            $Email->emailFormat('both');
            $Email->to($to_person);
            $Email->subject($subject);
            // $message = "Dear ".$user.",</br></br>" ;
            //$message .=$content;
            $Email->send($message);

        endforeach;

        //}

        exit;
    }

    public function feeReminder2() {

        $this->loadModel('Emails');
        $email = $this->Emails->find('all', array(
                    'conditions' => ['Emailtypes.id' => '3'],
                    'contain' => ['Emailtypes']
                ))->first();

        $conn = ConnectionManager::get('default');
        $fields;
        $joins;
        $condition = 'u.role_id=6 AND r.status=1 ';
        $condition .= '  AND DATE(a.due_date) -  DATE(NOW()) =  ' . $email->time_interval;

        $query = $conn->execute(
                'SELECT  CONCAT(u.first_name, u.last_name) as user,u.email as emails, u.first_name, u.last_name,r.id as reid, a.id'
                //. $fields
                . ' FROM users u '
                . 'inner join registerations r ON u.id= r.user_id '
                . 'inner join accounts a ON r.id= a.registeration_id '
                // . 'inner join comments ON accounts.id= comments.account_id '
                //. 'inner join paymenttypes ON paymenttypes.id= accounts.paymenttype_id '
                . 'inner join membertypes m ON m.id=r.membertype_id '
                //. $joins
                . 'where  a.id IN (
                SELECT MAX(id)
                FROM accounts 
                GROUP BY registeration_id
            ) AND ' . $condition . ' order by a.amount_due DESC');

        $results1 = $query->fetchAll('assoc');


        //debug($email['interval']);
        // if($email['interval']==1){
        foreach ($results1 as $result):
            $user = $result['first_name'] . " " . $result['last_name'];
            $title = $email['title'];
            $from_person = 'info@cyberclouds.com';
            $to_person = $result['emails'];

            $subject = $email['subject'];
            $content = $email['message'];
            //$message = str_replace("{first_name,last_name}",$user, $content);
            $message = "";
            $message .= "<span style='background:#161616;display:block;padding:10px;'><img src='http://needanecommercesite.com/fgem/img/logo-light.png' style='display:block;margin:auto;max-width: 130px;'/></span>";
            $trans = array("{first_name,last_name}" => $user, "{first_name}" => $result['first_name'], "{last_name}" => $result['last_name'], "{first_name} {last_name}" => $user);
            $message .= strtr($content, $trans);
            //echo $message; 
            //debug($message);exit;
            $Email = new Email();
            $Email->from(array($from_person => $title));
            $Email->emailFormat('both');
            $Email->to($to_person);
            $Email->subject($subject);
            // $message = "Dear ".$user.",</br></br>" ;
            //$message .=$content;
            $Email->send($message);

        endforeach;

        //}

        exit;
    }

    public function accountSuspend() {

        $this->loadModel('Emails');
        $email = $this->Emails->find('all', array(
                    'conditions' => ['Emailtypes.id' => '4'],
                    'contain' => ['Emailtypes']
                ))->first();

        $conn = ConnectionManager::get('default');
        $fields;
        $joins;

        $condition = 'u.role_id=6 AND r.status=1 ';
        $condition .= '  AND  DATE(a.due_date) + INTERVAL ' . $email->time_interval . ' DAY = DATE((NOW())) ';

        $query = $conn->execute(
                'SELECT  CONCAT(u.first_name, u.last_name) as user,u.email as emails, u.first_name, u.last_name,r.id as reid, a.id, a.due_date as due_date'
                //. $fields
                . ' FROM users u '
                . 'inner join registerations r ON u.id= r.user_id '
                . 'inner join accounts a ON r.id= a.registeration_id '
                // . 'inner join comments ON accounts.id= comments.account_id '
                //. 'inner join paymenttypes ON paymenttypes.id= accounts.paymenttype_id '
                . 'inner join membertypes m ON m.id=r.membertype_id '
                //. $joins
                . 'where  a.id IN (
                SELECT MAX(id)
                FROM accounts 
                GROUP BY registeration_id
            ) AND ' . $condition . ' order by a.amount_due DESC');

        $results1 = $query->fetchAll('assoc');

        // if($email['interval']==1){
        foreach ($results1 as $result):

            $query2 = $conn->execute(
                    'SELECT  min(amount_due) as am from accounts '
                    . 'where accounts.due_date="' . $result['due_date'] . '" and registeration_id=' . $result['reid']);
            $results2 = $query2->fetchAll('assoc');
            //debug($result);exit;
            if ($results2[0]['am'] > 0) {

                //UPDATE `registerations` SET `user_id` = '60' WHERE `registerations`.`id` = 16;
                $query = $conn->execute(
                        'UPDATE `registerations` SET `status` =0 WHERE `registerations`.`id` =' . $result['reid']);
            }
            $user = $result['first_name'] . " " . $result['last_name'];
            $title = $email['title'];
            $from_person = 'info@cyberclouds.com';
            $to_person = $result['emails'];

            $subject = $email['subject'];
            $content = $email['message'];
            //$message = str_replace("{first_name,last_name}",$user, $content);
            $message = "";
            $message .= "<span style='background:#161616;display:block;padding:10px;'><img src='http://needanecommercesite.com/fgem/img/logo-light.png' style='display:block;margin:auto;max-width: 130px;'/></span>";
            $trans = array("{first_name,last_name}" => $user, "{first_name}" => $result['first_name'], "{last_name}" => $result['last_name'], "{first_name} {last_name}" => $user);
            $message = strtr($content, $trans);
            //echo $message; 
            //debug($message);exit;
            $Email = new Email();
            $Email->from(array($from_person => $title));
            $Email->emailFormat('both');
            $Email->to($to_person);
            $Email->subject($subject);
            // $message = "Dear ".$user.",</br></br>" ;
            //$message .=$content;
            $Email->send($message);

        endforeach;

        //}

        exit;
    }

    public function lateFee1() {

        $this->loadModel('Emails');
        $email = $this->Emails->find('all', array(
                    'conditions' => ['Emailtypes.id' => '6'],
                    'contain' => ['Emailtypes']
                ))->first();

        $conn = ConnectionManager::get('default');
        $fields;
        $joins;

        $condition = 'u.role_id=6 AND r.status=0 ';
        $condition .= '  AND  DATE(a.due_date) + INTERVAL ' . $email->time_interval . ' DAY = DATE((NOW())) ';

        $query = $conn->execute(
                'SELECT  CONCAT(u.first_name, u.last_name) as user,u.email as emails, u.first_name, u.last_name,r.id as reid, a.id, a.due_date as due_date'
                //. $fields
                . ' FROM users u '
                . 'inner join registerations r ON u.id= r.user_id '
                . 'inner join accounts a ON r.id= a.registeration_id '
                // . 'inner join comments ON accounts.id= comments.account_id '
                //. 'inner join paymenttypes ON paymenttypes.id= accounts.paymenttype_id '
                . 'inner join membertypes m ON m.id=r.membertype_id '
                //. $joins
                . 'where  a.id IN (
                SELECT MAX(id)
                FROM accounts 
                GROUP BY registeration_id
            ) AND ' . $condition . ' order by a.amount_due DESC');

        $results1 = $query->fetchAll('assoc');
        //debug($results1);
        // if($email['interval']==1){
        foreach ($results1 as $result):

            $query2 = $conn->execute(
                    'INSERT INTO `accounts` SET registeration_id =' . $result['reid'] . ', paymenttype_id = 3, amount_paid = 0, amount_due =  ' . $email->late_fee . ', amout_date = DATE(NOW()), due_date = "' . $result['due_date'] . '", user_id = 0, status = 1, modified = DATE(NOW())');

            //$results2 = $query2->fetchAll('assoc');
            //debug($result);exit;

            $user = $result['first_name'] . " " . $result['last_name'];
            $title = $email['title'];
            $from_person = 'info@cyberclouds.com';
            $to_person = $result['emails'];

            $subject = $email['subject'];
            $content = $email['message'];
            //$message = str_replace("{first_name,last_name}",$user, $content);
            $message = "";
            $message .= "<span style='background:#161616;display:block;padding:10px;'><img src='http://needanecommercesite.com/fgem/img/logo-light.png' style='display:block;margin:auto;max-width: 130px;'/></span>";
            $trans = array("{first_name,last_name}" => $user, "{first_name}" => $result['first_name'], "{last_name}" => $result['last_name'], "{first_name} {last_name}" => $user);
            $message .= strtr($content, $trans);
            //debug($message);exit;
            $Email = new Email();
            $Email->from(array($from_person => $title));
            $Email->emailFormat('both');
            $Email->to($to_person);
            $Email->subject($subject);
            // $message = "Dear ".$user.",</br></br>" ;
            //$message .=$content;
            $Email->send($message);

        endforeach;

        //}

        exit;
    }

    public function lateFee2() {

        $this->loadModel('Emails');
        $email = $this->Emails->find('all', array(
                    'conditions' => ['Emailtypes.id' => '7'],
                    'contain' => ['Emailtypes']
                ))->first();

        $conn = ConnectionManager::get('default');
        $fields;
        $joins;

        $condition = 'u.role_id=6 AND r.status=0 ';
        $condition .= '  AND  DATE(a.due_date) + INTERVAL ' . $email->time_interval . ' DAY = DATE((NOW())) ';

        $query = $conn->execute(
                'SELECT  CONCAT(u.first_name, u.last_name) as user,u.email as emails, u.first_name, u.last_name,r.id as reid, a.id, a.due_date as due_date'
                //. $fields
                . ' FROM users u '
                . 'inner join registerations r ON u.id= r.user_id '
                . 'inner join accounts a ON r.id= a.registeration_id '
                // . 'inner join comments ON accounts.id= comments.account_id '
                //. 'inner join paymenttypes ON paymenttypes.id= accounts.paymenttype_id '
                . 'inner join membertypes m ON m.id=r.membertype_id '
                //. $joins
                . 'where  a.id IN (
                SELECT MAX(id)
                FROM accounts 
                GROUP BY registeration_id
            ) AND ' . $condition . ' order by a.amount_due DESC');

        $results1 = $query->fetchAll('assoc');
        //debug($results1);
        // if($email['interval']==1){
        foreach ($results1 as $result):

            $query2 = $conn->execute(
                    'INSERT INTO `accounts` SET registeration_id =' . $result['reid'] . ', paymenttype_id = 3, amount_paid = 0, amount_due =  ' . $email->late_fee . ', amout_date = DATE(NOW()), due_date = "' . $result['due_date'] . '", user_id = 0, status = 1, modified = DATE(NOW())');

            //$results2 = $query2->fetchAll('assoc');
            //debug($result);exit;

            $user = $result['first_name'] . " " . $result['last_name'];
            $title = $email['title'];
            $from_person = 'info@cyberclouds.com';
            $to_person = $result['emails'];

            $subject = $email['subject'];
            $content = $email['message'];
            //$message = str_replace("{first_name,last_name}",$user, $content);
            $message = "";
            $message .= "<span style='background:#161616;display:block;padding:10px;'><img src='http://needanecommercesite.com/fgem/img/logo-light.png' style='display:block;margin:auto;max-width: 130px;'/></span>";

            $trans = array("{first_name,last_name}" => $user, "{first_name}" => $result['first_name'], "{last_name}" => $result['last_name'], "{first_name} {last_name}" => $user);
            $message .= strtr($content, $trans);
            //debug($message);exit;
            $Email = new Email();
            $Email->from(array($from_person => $title));
            $Email->emailFormat('both');
            $Email->to($to_person);
            $Email->subject($subject);
            // $message = "Dear ".$user.",</br></br>" ;
            //$message .=$content;
            $Email->send($message);

        endforeach;

        //}

        exit;
    }

}
