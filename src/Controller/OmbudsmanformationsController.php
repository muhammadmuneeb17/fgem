<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Ombudsmanformations Controller
 *
 * @property \App\Model\Table\OmbudsmanformationsTable $Ombudsmanformations
 *
 * @method \App\Model\Entity\Ombudsmanformation[] paginate($object = null, array $settings = [])
 */
class OmbudsmanformationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Ombudsmen']
        ];
        $ombudsmanformations = $this->paginate($this->Ombudsmanformations);

        $this->set(compact('ombudsmanformations'));
        $this->set('_serialize', ['ombudsmanformations']);
    }

    /**
     * View method
     *
     * @param string|null $id Ombudsmanformation id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ombudsmanformation = $this->Ombudsmanformations->get($id, [
            'contain' => ['Ombudsmen']
        ]);

        $this->set('ombudsmanformation', $ombudsmanformation);
        $this->set('_serialize', ['ombudsmanformation']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ombudsmanformation = $this->Ombudsmanformations->newEntity();
        if ($this->request->is('post')) {
            $ombudsmanformation = $this->Ombudsmanformations->patchEntity($ombudsmanformation, $this->request->getData());
            if ($this->Ombudsmanformations->save($ombudsmanformation)) {
                $this->Flash->success(__('The ombudsmanformation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ombudsmanformation could not be saved. Please, try again.'));
        }
        $ombudsmen = $this->Ombudsmanformations->Ombudsmen->find('list', ['limit' => 200]);
        $this->set(compact('ombudsmanformation', 'ombudsmen'));
        $this->set('_serialize', ['ombudsmanformation']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ombudsmanformation id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ombudsmanformation = $this->Ombudsmanformations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ombudsmanformation = $this->Ombudsmanformations->patchEntity($ombudsmanformation, $this->request->getData());
            if ($this->Ombudsmanformations->save($ombudsmanformation)) {
                $this->Flash->success(__('The ombudsmanformation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ombudsmanformation could not be saved. Please, try again.'));
        }
        $ombudsmen = $this->Ombudsmanformations->Ombudsmen->find('list', ['limit' => 200]);
        $this->set(compact('ombudsmanformation', 'ombudsmen'));
        $this->set('_serialize', ['ombudsmanformation']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ombudsmanformation id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ombudsmanformation = $this->Ombudsmanformations->get($id);
        if ($this->Ombudsmanformations->delete($ombudsmanformation)) {
            $this->Flash->success(__('The ombudsmanformation has been deleted.'));
        } else {
            $this->Flash->error(__('The ombudsmanformation could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
