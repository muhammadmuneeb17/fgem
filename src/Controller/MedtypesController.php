<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Medtypes Controller
 *
 * @property \App\Model\Table\MedtypesTable $Medtypes
 *
 * @method \App\Model\Entity\Medtype[] paginate($object = null, array $settings = [])
 */
class MedtypesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $medtypes = $this->paginate($this->Medtypes);

        $this->set(compact('medtypes'));
        $this->set('_serialize', ['medtypes']);
    }

    /**
     * View method
     *
     * @param string|null $id Medtype id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $medtype = $this->Medtypes->get($id, [
            'contain' => []
        ]);

        $this->set('medtype', $medtype);
        $this->set('_serialize', ['medtype']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $medtype = $this->Medtypes->newEntity();
        if ($this->request->is('post')) {
            $medtype = $this->Medtypes->patchEntity($medtype, $this->request->getData());
            if ($this->Medtypes->save($medtype)) {
                $this->Flash->success(__('The medtype has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The medtype could not be saved. Please, try again.'));
        }
        $this->set(compact('medtype'));
        $this->set('_serialize', ['medtype']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Medtype id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $medtype = $this->Medtypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $medtype = $this->Medtypes->patchEntity($medtype, $this->request->getData());
            if ($this->Medtypes->save($medtype)) {
                $this->Flash->success(__('The medtype has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The medtype could not be saved. Please, try again.'));
        }
        $this->set(compact('medtype'));
        $this->set('_serialize', ['medtype']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Medtype id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $medtype = $this->Medtypes->get($id);
        if ($this->Medtypes->delete($medtype)) {
            $this->Flash->success(__('The medtype has been deleted.'));
        } else {
            $this->Flash->error(__('The medtype could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
