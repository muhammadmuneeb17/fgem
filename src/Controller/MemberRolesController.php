<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MemberRoles Controller
 *
 * @property \App\Model\Table\MemberRolesTable $MemberRoles
 *
 * @method \App\Model\Entity\MemberRole[] paginate($object = null, array $settings = [])
 */
class MemberRolesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Roles']
        ];
        $memberRoles = $this->paginate($this->MemberRoles);

        $this->set(compact('memberRoles'));
        $this->set('_serialize', ['memberRoles']);
    }

    /**
     * View method
     *
     * @param string|null $id Member Role id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $memberRole = $this->MemberRoles->get($id, [
            'contain' => ['Users', 'Roles']
        ]);

        $this->set('memberRole', $memberRole);
        $this->set('_serialize', ['memberRole']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $memberRole = $this->MemberRoles->newEntity();
        if ($this->request->is('post')) {
            $memberRole = $this->MemberRoles->patchEntity($memberRole, $this->request->getData());
            if ($this->MemberRoles->save($memberRole)) {
                $this->Flash->success(__('The member role has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The member role could not be saved. Please, try again.'));
        }
        $users = $this->MemberRoles->Users->find('list', ['limit' => 200]);
        $roles = $this->MemberRoles->Roles->find('list', ['limit' => 200]);
        $this->set(compact('memberRole', 'users', 'roles'));
        $this->set('_serialize', ['memberRole']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Member Role id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $memberRole = $this->MemberRoles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $memberRole = $this->MemberRoles->patchEntity($memberRole, $this->request->getData());
            if ($this->MemberRoles->save($memberRole)) {
                $this->Flash->success(__('The member role has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The member role could not be saved. Please, try again.'));
        }
        $users = $this->MemberRoles->Users->find('list', ['limit' => 200]);
        $roles = $this->MemberRoles->Roles->find('list', ['limit' => 200]);
        $this->set(compact('memberRole', 'users', 'roles'));
        $this->set('_serialize', ['memberRole']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Member Role id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $memberRole = $this->MemberRoles->get($id);
        if ($this->MemberRoles->delete($memberRole)) {
            $this->Flash->success(__('The member role has been deleted.'));
        } else {
            $this->Flash->error(__('The member role could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
