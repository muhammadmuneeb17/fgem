<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Contents Controller
 *
 *
 * @method \App\Model\Entity\Content[] paginate($object = null, array $settings = [])
 */
class ContentsController extends AppController {

    public function isAuthorized($user) {
        if (in_array($this->request->action, ['edit'])) {
            $id = (int) $this->request->params['pass'][0];
            if ($id == $user['id']) {
                return true;
            }
        }
        if (in_array($this->request->action, ['logout'])) {
            return true;
        }
        // Admin can access every action

        if ($this->viewVars['actionPermission'] != "") {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 1 || $permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $contents = $this->paginate($this->Contents);

        $this->set(compact('contents'));
        $this->set('_serialize', ['contents']);
    }

// sub pages are listed here
    public function listSubPages($id = null) {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 1 || $permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));

        $contentType = $this->Contents->ContentTypes->get($id, [
                    'contain' => [],
                    'conditions' => [
                    ]
                ])->toArray();
        if (!$contentType) {
            return $this->redirect(['controller' => 'ContentTypes', 'action' => 'listPages']);
        }

        if ($contentType['is_contact'] == 1) {
            return $this->redirect(['controller' => 'Contents', 'action' => 'contactPage', $id]);
        }

        $this->paginate = ['conditions' => [
                'content_type_id' => $id
            ],
            'order' => [
                'ordinal' => 'ASC'
            ]
        ];

        $subpages = $this->paginate($this->Contents);
        $this->set(compact('contentType'));
        $this->set('_serialize', ['contentType']);
        $this->set(compact('subpages'));
        $this->set('_serialize', ['subpages']);
        $this->viewBuilder()->setLayout('backend');
    }

    //addition of sub pages for front
    public function addSubpage($id = null) {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        if (!$id) {
            return $this->redirect(['controller' => 'ContentTypes', 'action' => 'listPages']);
        }
        $contentType = $this->Contents->ContentTypes->get($id, [
                    'contain' => [],
                    'conditions' => [
                    ]
                ])->toArray();
        if (!$contentType) {
            return $this->redirect(['controller' => 'ContentTypes', 'action' => 'listPages']);
        }

        $content = $this->Contents->newEntity();
        if ($this->request->is('post')) {
            $maxorder_no = $this->Contents->find('all', [
                'fields' => array('ordinal' => 'MAX(Contents.ordinal)'),
                'conditions' => array('Contents.content_type_id' => $id)
            ]);
            if (!$maxorder_no) {
                $content_order = 1;
            }
            $content_array = $maxorder_no->toArray();
            $content_order = $content_array[0]['ordinal'] + 1;
            if ($this->request->data['image']['name'] != "") {
                $ext = strrchr($this->request->data['image']['name'], '.');
                $content_image = time() . $ext;
                $source = $this->request->data['image']['tmp_name'];
                $destination = WWW_ROOT . 'img' . DS . 'content' . DS . $content_image;
                $thm_destination = WWW_ROOT . 'img' . DS . 'content' . DS . 'thumbnail' . DS . $content_image;
                $this->request->data['Content']['image'] = $content_image;
            }
            $this->request->data['Content']['title'] = $this->request->data['title'];
            $this->request->data['Content']['content_type_id'] = $id;
            $this->request->data['Content']['description'] = $this->request->data['description'];
            $this->request->data['Content']['template'] = $this->request->data['template'];
            $this->request->data['Content']['status'] = 1;
            $this->request->data['Content']['ordinal'] = $content_order;
            $this->request->data['Content']['user_id'] = $this->request->session()->read('Auth.User.id');
            $content = $this->Contents->patchEntity($content, $this->request->data['Content']);
            if ($this->Contents->save($content)) {
                if ($this->request->data['image']['name'] != "") {
                    $this->generate_thumb($source, $thm_destination, 140, 110);
                    $this->generate_thumb($source, $destination, 1600, 1600);
                }
                $this->Flash->success(__('Le contenu a été enregistré.'));
                return $this->redirect(['action' => 'listSubPages', $id]);
            }
            $this->Flash->error(__('Le contenu n\'a pas pu être enregistré. Veuillez réessayer.'));
        }
        $this->set(compact('content'));
        $this->set('_serialize', ['content']);
        $this->set(compact('contentType'));
        $this->set('_serialize', ['contentType']);
        $this->viewBuilder()->setLayout('backend');
    }

    // view sub pages of conetnt in backend
    public function viewSubpage($id = null) {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 1 || $permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $content = $this->Contents->get($id, [
            'contain' => ['ContentTypes'],
        ]);
        $this->set('content', $content);
        $this->set('_serialize', ['content']);
        $this->viewBuilder()->setLayout('backend');
    }

    // sub pages can be editted here
    public function editSubpage($id = null) {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $content = $this->Contents->get($id, [
            'contain' => ['ContentTypes']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            if ($this->request->data['image']['name'] != "") {
                $ext = strrchr($this->request->data['image']['name'], '.');
                $content_image = time() . $ext;
                $source = $this->request->data['image']['tmp_name'];
                $destination = WWW_ROOT . 'img' . DS . 'content' . DS . $content_image;
                $thm_destination = WWW_ROOT . 'img' . DS . 'content' . DS . 'thumbnail' . DS . $content_image;
                $this->request->data['Content']['image'] = $content_image;
            } else if (isset($this->request->data['remove_image']) && $this->request->data['remove_image'] == 1 && $this->request->data['template'] == "DEFAULT") {
                $this->request->data['Content']['image'] = "";
            } else {
                unset($this->request->data['Content']['image']);
            }

            $this->request->data['Content']['title'] = $this->request->data['title'];
            $this->request->data['Content']['description'] = $this->request->data['description'];
            $this->request->data['Content']['template'] = $this->request->data['template'];
            $this->request->data['Content']['status'] = $this->request->data['status'];
            $this->request->data['Content']['content_type_id'] = $this->request->data['content_type_id'];
            $this->request->data['Content']['user_id'] = $this->request->session()->read('Auth.User.id');
            $content = $this->Contents->patchEntity($content, $this->request->data['Content']);

            if ($this->Contents->save($content)) {
                if ($this->request->data['image']['name'] != "") {
                    $this->generate_thumb($source, $thm_destination, 140, 110);
                    $this->generate_thumb($source, $destination, 1600, 1600);
                }
                $this->Flash->success(__('La sous-page a été enregistrée.'));
                return $this->redirect(['action' => 'listSubPages', $this->request->data['content_type_id']]);
            }
            $this->Flash->error(__('La sous-page n\'a pas pu être enregistrée. Veuillez réessayer.'));
        }
        $this->set(compact('content'));
        $this->set('_serialize', ['content']);
        $this->viewBuilder()->setLayout('backend');
    }

    public function contactPage($id = null) {
        $content = $this->Contents->find('all', ['conditions' => ['content_type_id' => $id, 'is_contact' => '1'],
                    'contain' => ['ContentTypes']
                ])->first();
        if ($this->request->is(['patch', 'post', 'put'])) {
            if (!$content) {
                $content = $this->Contents->newEntity();
            }

//			if ($this->request->data['image']['name'] != "") {
//                $ext = strrchr($this->request->data['image']['name'], '.');
//                $content_image = $this->request->data['title'] . time() . $ext;
//                $source = $this->request->data['image']['tmp_name'];
//                $destination = WWW_ROOT . 'img' . DS . 'content' . DS . $content_image;
//                $thm_destination = WWW_ROOT . 'img' . DS . 'content' . DS . 'thumbnail' . DS . $content_image;
//                $this->request->data['Content']['image'] = $content_image;
//            }
//            else if(isset($this->request->data['remove_image']) && $this->request->data['remove_image'] == 1 &&  $this->request->data['template'] == "DEFAULT") {
//				$this->request->data['Content']['image'] = "";
//			}
//			else {
//                unset($this->request->data['Content']['image']);
//            }
            unset($this->request->data['Content']['image']);
            $this->request->data['Content']['title'] = $this->request->data['title'];
            $this->request->data['Content']['description'] = $this->request->data['description'];
            $this->request->data['Content']['lat'] = $this->request->data['lat'];
            $this->request->data['Content']['lng'] = $this->request->data['lng'];
            $this->request->data['Content']['phone'] = $this->request->data['phone'];
            if ($this->request->data['mobile']) {
                $this->request->data['Content']['mobile'] = $this->request->data['mobile'];
            } else {
                $this->request->data['Content']['mobile'] = "";
            }
            $this->request->data['Content']['email'] = $this->request->data['email'];
            if (!isset($this->request->data['show_form']) || $this->request->data['show_form'] != "1") {
                $this->request->data['show_form'] = 0;
            }
            $this->request->data['Content']['show_form'] = $this->request->data['show_form'];
            $this->request->data['Content']['template'] = $this->request->data['template'];
            $this->request->data['Content']['status'] = $this->request->data['status'];
            $this->request->data['Content']['content_type_id'] = $this->request->data['content_type_id'];
            $this->request->data['Content']['user_id'] = $this->request->session()->read('Auth.User.id');

            $content = $this->Contents->patchEntity($content, $this->request->data['Content']);
            if ($this->Contents->save($content)) {
//                 if ($this->request->data['image']['name'] != "") {
//                $this->generate_thumb($source, $thm_destination, 140, 110);
//                $this->generate_thumb($source, $destination, 1200, 497);
//                 }
                $this->Flash->success(__('The sub page has been saved.'));
                return $this->redirect(['controller' => 'contentTypes', 'action' => 'listPages']);
            }
            $this->Flash->error(__('The sub page could not be saved. Please, try again.'));
        }
        if (!$content) {
            $content = "";
        }
        $this->set(compact('content'));
        $this->set('_serialize', ['content']);
        $this->set(compact('id'));
        $this->set('_serialize', ['id']);
        $this->viewBuilder()->setLayout('backend');
    }

    /**
     * View method
     *
     * @param string|null $id Content id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $content = $this->Contents->get($id, [
            'contain' => []
        ]);

        $this->set('content', $content);
        $this->set('_serialize', ['content']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $content = $this->Contents->newEntity();
        if ($this->request->is('post')) {
            $content = $this->Contents->patchEntity($content, $this->request->getData());
            if ($this->Contents->save($content)) {
                $this->Flash->success(__('The content has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The content could not be saved. Please, try again.'));
        }
        $this->set(compact('content'));
        $this->set('_serialize', ['content']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Content id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $content = $this->Contents->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $content = $this->Contents->patchEntity($content, $this->request->getData());
            if ($this->Contents->save($content)) {
                $this->Flash->success(__('The content has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The content could not be saved. Please, try again.'));
        }
        $this->set(compact('content'));
        $this->set('_serialize', ['content']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Content id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    // deletion of the content of sub pages
    public function delete($id = null) {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $this->request->allowMethod(['post', 'delete', 'get']);
        $content = $this->Contents->get($id);
        //debug($content);
        //exit;
        if ($this->Contents->delete($content)) {
            $this->Flash->success(__('Le contenu a été supprimé.'));
        } else {
            $this->Flash->error(__('Le contenu n\'a pas pu être supprimé. Veuillez réessayer.'));
        }

        return $this->redirect(['action' => 'listSubPages', $content->content_type_id]);
    }

    /*
     * the page function is handling three pages of front end
     * 1: the landing or La Federation page
     * 2: the la mediation page
     * 3: the contact page
     * 
     * this function contains all the dynamic contents that are managed by admin in the backend
     */

    public function page($title = NULL) {
        if ($title == 'la-mediation') {
            $pageTitle='Mediation';
        } elseif ($title == 'contactez') {
            $pageTitle='Contact';
        } else {
            $pageTitle='Home';
        }
        $this->set('myTitle', $pageTitle);
        $this->loadModel('ContentTypes');

        if ($title == NULL) {
            $page = $this->ContentTypes->find('all', [
                'contain' => ['Contents' => ['strategy' => 'select',
                        'queryBuilder' => function ($q) {
                            return $q->where(['Contents.status' => '1'])->order(['Contents.ordinal' => 'ASC']);
                        }]],
                'conditions' => [
                    'ContentTypes.is_front' => 1,
                    'ContentTypes.status' => 1,
                ],
                'order' => [
                    'ContentTypes.ordinal' => 'ASC',
                ]
            ]);
        } elseif ($title != NULL) {
            //$slug = str_replace("-"," ",$title);
            $page_exists = $this->ContentTypes->exists(['slug' => $title, 'status' => 1]);
            if ($page_exists) {
                $page = $this->ContentTypes->find('all', [
                    'contain' => ['Contents' => ['strategy' => 'select',
                            'queryBuilder' => function ($q) {
                                return $q->where(['Contents.status' => '1'])->order(['Contents.ordinal' => 'ASC']);
                            }]],
                    'conditions' => [
                        'ContentTypes.slug' => $title,
                        'ContentTypes.status' => 1,
                    ],
                    'order' => [
                        'ContentTypes.ordinal' => 'ASC',
                    ]
                ]);
            } else {
                return $this->redirect(['controller' => 'contents', 'action' => 'page']);
            }
        }
        $page = $page->first();

        if ($page->show_slider == 1) {
            $this->loadModel('Sliders');
            $sliders = $this->Sliders->find('all', [
                        'conditions' => [
                            'status' => '1'
                        ],
                        'order' => ['ordinal' => 'ASC']
                    ])->toArray();
        }

        $this->set(compact('sliders'));
        $this->set('_serialize', ['sliders']);

        $this->set(compact('page'));
        $this->set('_serialize', ['page']);

        $this->viewBuilder()->setLayout('internal');
    }

    public function udapteorder() {
        if ($this->request->is('post')) {
            $success = 0;
            foreach ($this->request->data['data']['content_id'] as $key => $data) {
                $page = $this->Contents->newEntity();
                $this->request->data['Contents']['ordinal'] = $this->request->data['data']['ordinal'][$key];
                $page->id = $this->request->data['data']['content_id'][$key];
                $page = $this->Contents->patchEntity($page, $this->request->data['Contents'], [
                    'validate' => 'OnlyCheck'
                ]);
                if ($this->Contents->save($page)) {
                    $success = 1;
                } else {
                    $success = 0;
                }
            }
            $content_type_id = $this->request->data['data']['content_type_id'];
            if ($success == 1) {
                $this->Flash->success(__('Ordre de sous-page mis à jour.'));
                return $this->redirect(array('action' => 'listSubPages', $content_type_id));
            } else {

                $this->Flash->error(__('Ordre des sous-pages non mis à jour, réessayez plus tard'));
                return $this->redirect(array('action' => 'listSubPages', $content_type_id));
            }
        }
        $this->viewBuilder()->setLayout();
        $this->autoRender = false;
    }

}
