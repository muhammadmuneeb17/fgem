<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'user_name',
                        'password' => 'password'
                    ],
                    'contain' => ['Registerations' => 'Ombudsmans', 'MemberRoles']
                ]
            ],
            'authorize' => ['Controller'],
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login'
            ]
        ]);

        /*
         * Enable the following components for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Http\Response|null|void
     */
    public function beforeRender(Event $event) {
        // Note: These defaults are just to get started quickly with development
        // and should not be used in production. You should instead set "_serialize"
        // in each action as required.
        if (!array_key_exists('_serialize', $this->viewVars) &&
                in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    public function beforeFilter(Event $event) {
        $this->loadModel('Registerations');
        $this->loadModel('MemberRoles');
        $ticketConditions = $this->Registerations->find('all')
                        ->contain(['Ombudsmans'])
                        ->where(['Registerations.user_id' => $this->Auth->user('id'),
                            'Ombudsmans.accept_info' => true,
                            'Ombudsmans.mediator_question' => 'Oui',
                            'Ombudsmans.assermentation' => 1,
                            'Registerations.status' => true,
                            'Registerations.suspended' => 0
                        ])->first();
        $superviser = '';
        if ($this->Auth->user('member_roles')) {
            foreach ($this->Auth->user('member_roles') as $role):
                if ($role['role_id'] == 11) {
                    $superviser = 1;
                }
            endforeach;
        }
        if ($this->Auth->user('role_id') == 1) {
            $superviser = 1;
        }

//debug($registeration);exit;
        $this->set(compact('ticketConditions', 'superviser'));
//        exit;
        // allow only login, forgotpassword
        $this->Auth->allow(['page', 'forgotPassword', 'formation', 'resetPassword', 'login', 'logout', 'submitform', 'customerLogin', 'registerationform', 'search', 'dashboard1', 'email_request', 'verified', 'viewMembers', 'showFormationImage']);
        $this->loadModel('Contents');
        $contacts = $this->Contents->find('all', [
                    'contain' => ['ContentTypes' => ['conditions' => ['ContentTypes.status' => '1', 'ContentTypes.is_contact' => '1']]],
                    'conditions' => [
                        'Contents.status' => 1,
                    ],
                    'order' => [
                        'Contents.id' => 'DESC',
                    ]
                ])->first();
        /* Get Menu Names */
        $this->loadModel('ContentTypes');
        $menus = $this->ContentTypes->find('all', [
                    //'contain' => ['ContentTypes' => ['conditions'=>['ContentTypes.status' => '1','ContentTypes.is_contact' => '1']]],
                    'conditions' => [
                        'ContentTypes.status' => 1,
                    ],
                    'order' => [
                        'ContentTypes.ordinal' => 'ASC',
                    ]
                ])->toArray();

        $this->loadModel('Modules');
        $modulesList = $this->Modules->find('all', [
                    'order' => ['Modules.id' => 'ASC']
                ])->toArray();
//        debug($modulesList);exit;
        // role id and permission controller

        $roleId = $this->request->session()->read('Auth.User.role_id');
        $url = $_SERVER['REQUEST_URI'];

        $urlArray = explode('/', $url);
        $controller = strtoupper($this->request->params['controller']);


        $actionPermission = $this->permissionsList($roleId, $controller);

        if ($this->Auth->user('registeration.membertype_id')) {
            $this->loadModel('Membertypes');
            $checkMemberType = $this->Membertypes->get($this->Auth->user('registeration.membertype_id'));
//           debug($membertype);exit;
            $this->set(compact('checkMemberType'));
        }

        $this->set(compact('contacts'));
        $this->set('_serialize', ['contacts']);
        $this->set(compact('menus'));
        $this->set('_serialize', ['menus']);
        $this->set(compact('modulesList', 'unread_notifications', 'getMessages', 'messageCount', 'roleId', 'latest_msg', 'notifications'));
        $this->set(compact('actionPermission'));
    }

    public function isAuthorized($user) {

        if ($this->Auth->user('id') != '') {
            return true;
        }
        if ($this->viewVars['actionPermission'] != "") {
            return true;
        } else {
            return false;
        }
    }

    public function permissionsList($roleId, $controller) {
        $this->loadModel('Roles');

        $this->loadModel('Modules');
        // module => rights , => roles
        $module = $this->Modules->find('all', [
                    'contain' => ['Rights' => [
                            'conditions' => [
                                'Rights.role_id' => $roleId
                            ]
                        ]],
                    'conditions' => ['Modules.controller' => $controller]
                ])->first();
        //$moduleId = $module->id;
        if (isset($module['rights'][0]['per_type']) && $module['rights'][0]['per_type'] != "") {
            return $module['rights'][0]['per_type'];
        } else {

            return false;
        }
    }

    /* ------ Image Resize Function ------- */

    public function generate_thumb($source_image_path, $thumbnail_image_path, $max_width, $max_height) {
        list($source_image_width, $source_image_height, $source_image_type) = getimagesize($source_image_path);

        switch ($source_image_type) {

            case IMAGETYPE_GIF:
                $source_gd_image = imagecreatefromgif($source_image_path);

                break;

            case IMAGETYPE_JPEG:

                $source_gd_image = imagecreatefromjpeg($source_image_path);

                break;

            case IMAGETYPE_PNG:

                $source_gd_image = imagecreatefrompng($source_image_path);

                break;
        }

        $source_aspect_ratio = $source_image_width / $source_image_height;

        $thumbnail_aspect_ratio = $max_width / $max_height;

        if ($source_image_width <= $max_width && $source_image_height <= $max_height) {

            $thumbnail_image_width = $source_image_width;

            $thumbnail_image_height = $source_image_height;
        } elseif ($thumbnail_aspect_ratio > $source_aspect_ratio) {

            $thumbnail_image_width = (int) ($max_height * $source_aspect_ratio);

            $thumbnail_image_height = $max_height;
        } else {

            $thumbnail_image_width = $max_width;

            $thumbnail_image_height = (int) ($max_width / $source_aspect_ratio);
        }

        $newImg = imagecreatetruecolor($thumbnail_image_width, $thumbnail_image_height);
        imagealphablending($newImg, false);
        imagesavealpha($newImg, true);
        $transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
        imagefilledrectangle($newImg, 0, 0, $thumbnail_image_width, $thumbnail_image_height, $transparent);
        imagecopyresampled($newImg, $source_gd_image, 0, 0, 0, 0, $thumbnail_image_width, $thumbnail_image_height, $source_image_width, $source_image_height);

        imagepng($newImg, $thumbnail_image_path);
        return true;
    }

    public function generate_thumb2($source_image_path, $thumbnail_image_path, $max_width, $max_height) {

        $this->viewBuilder()->setLayout('');
        $this->autoRender = false;
        list($source_image_width, $source_image_height, $source_image_type) = getimagesize($source_image_path);


        switch ($source_image_type) {

            case IMAGETYPE_GIF:
                $source_gd_image = imagecreatefromgif($source_image_path);

                break;

            case IMAGETYPE_JPEG:

                $source_gd_image = imagecreatefromjpeg($source_image_path);

                break;

            case IMAGETYPE_PNG:

                $source_gd_image = imagecreatefrompng($source_image_path);

                break;
        }


        if ($source_gd_image === false) {

            return false;
        }

        $source_aspect_ratio = $source_image_width / $source_image_height;

        $thumbnail_aspect_ratio = $max_width / $max_height;

        if ($source_image_width <= $max_width && $source_image_height <= $max_height) {

            $thumbnail_image_width = $source_image_width;

            $thumbnail_image_height = $source_image_height;
        } elseif ($thumbnail_aspect_ratio > $source_aspect_ratio) {

            $thumbnail_image_width = (int) ($max_height * $source_aspect_ratio);

            $thumbnail_image_height = $max_height;
        } else {

            $thumbnail_image_width = $max_width;

            $thumbnail_image_height = (int) ($max_width / $source_aspect_ratio);
        }

        $thumbnail_gd_image = imagecreatetruecolor($thumbnail_image_width, $thumbnail_image_height);

        $white = imagecolorallocate($thumbnail_gd_image, 255, 255, 255);

        imagefill($thumbnail_gd_image, 0, 0, $white);

        imagecopyresampled($thumbnail_gd_image, $source_gd_image, 0, 0, 0, 0, $thumbnail_image_width, $thumbnail_image_height, $source_image_width, $source_image_height);

        imagejpeg($thumbnail_gd_image, $thumbnail_image_path, 90);

        imagedestroy($source_gd_image);

        imagedestroy($thumbnail_gd_image);

        return true;
    }

    public function sentence_case($str) {
        $cap = true;
        $ret = '';
        for ($x = 0; $x < strlen($str); $x++) {
            $letter = substr($str, $x, 1);
            if ($letter == "." || $letter == "!" || $letter == "?") {
                $cap = true;
            } elseif ($letter != " " && $cap == true) {
                $letter = strtoupper($letter);
                $cap = false;
            }
            $ret .= $letter;
        }
        return $ret;
    }

    function stringToSlug($str) {
        // turn into slug
        $str = Inflector::slug($str, "");
        // to lowercase
        $str = strtolower($str);
        return $str;
    }

}
