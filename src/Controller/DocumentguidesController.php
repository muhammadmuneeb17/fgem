<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Documentguides Controller
 *
 * @property \App\Model\Table\DocumentguidesTable $documentguides
 *
 * @method \App\Model\Entity\documentguide[] paginate($object = null, array $settings = [])
 */
class DocumentguidesController extends AppController
{

    public function isAuthorized($user) {
        if (in_array($this->request->action, ['edit'])) {
            $id = (int) $this->request->params['pass'][0];
            if ($id == $user['id']) {
                return true;
            }
        }
        if (in_array($this->request->action, ['logout'])) {
            return true;
        }
        // Admin can access every action
        if ($this->viewVars['actionPermission'] != "" || $this->request->params['action'] == "view") {
            return true;
        } else {
            return false;
        }
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function doclist()
    {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 1 || $permission == 2) {

        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $this->paginate = ['conditions' => [
            'Documentguides.type' => '1'
        ],'order' => ['Documentguides.ordinal' => 'ASC']];
        $documentguides = $this->paginate($this->Documentguides);

        $this->set(compact('documentguides'));
        $this->set('_serialize', ['documentguides']);
        $this->viewBuilder()->setLayout('backend');
    }

    /**
     * View method
     *
     * @param string|null $id documentguide id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
//    the document guide is shown here to all the members after loging in tio there accounts
    public function view($id = null)
    {

        $permission = $this->viewVars['actionPermission'];

        if ($permission == 1 || $permission == 2 || $this->request->session()->read('Auth.User.role_id') == 6) {

        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        if($id == "") {
            $contentID = $this->Documentguides->find('all',['conditions'=>['type'=>'0'],'order' =>['id'=>'DESC']])->first();       

            $id = $contentID['id'];
        }  

        $documentguide = $this->Documentguides->get($id, [
            'contain' => []
        ]);

        $this->set('documentguide', $documentguide);
        $this->set('_serialize', ['documentguide']);
        $this->viewBuilder()->layout('memberlayout');
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 2) {

        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $documentguide = $this->Documentguides->newEntity();
        if ($this->request->is('post')) {
            $documentguide_order = $this->Documentguides->find('all', [
                'fields' => array('order' => 'MAX(Documentguides.ordinal)')
            ]);
            if (!$documentguide_order) {
                $inc_documentguide_order = 1;
            }
            $documentguide_order_array = $documentguide_order->toArray();
            $inc_documentguide_order = $documentguide_order_array[0]['order'] + 1;

            $this->request->data['documentguide']['title'] = $this->request->data['title'];
            if ($this->request->data['doc']['name'] != "") {
                $ext = strrchr($this->request->data['doc']['name'], '.');
                $documentguide_image = $this->request->data['title'] . time() . $ext;
                $source = $this->request->data['doc']['tmp_name'];
                $destination = WWW_ROOT . 'img' . DS . 'documentguide' . DS . $documentguide_image;

                $this->request->data['documentguide']['file'] = $documentguide_image;
            }
            $this->request->data['documentguide']['type'] = 1;
            $this->request->data['documentguide']['status'] = 1;
            $this->request->data['documentguide']['ordinal'] = $inc_documentguide_order;
            $this->request->data['documentguide']['user_id'] = $this->request->session()->read('Auth.User.id');

            $documentguide = $this->Documentguides->patchEntity($documentguide,$this->request->data['documentguide']);
            if ($this->Documentguides->save($documentguide)) {
                move_uploaded_file($source, $destination);
                $this->Flash->success(__('The document has been saved.'));

                return $this->redirect(['action' => 'doclist']);
            }
            $this->Flash->error(__('The document could not be saved. Please, try again.'));
        }
        $this->set(compact('documentguide'));
        $this->set('_serialize', ['documentguide']);
        $this->viewBuilder()->setLayout('backend');
    }

    /**
     * Edit method
     *
     * @param string|null $id documentguide id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 2) {

        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $documentguide = $this->Documentguides->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            if ($this->request->data['doc']['name'] != "") {
                $ext = strrchr($this->request->data['doc']['name'], '.');
                $documentguide_image = $this->request->data['title'] . time() . $ext;
                $source = $this->request->data['doc']['tmp_name'];
                $destination = WWW_ROOT . 'img' . DS . 'documentguide' . DS . $documentguide_image;
                $this->request->data['documentguide']['file'] = $documentguide_image;
            }
            else {
                unset($this->request->data['documentguide']['file']); 
            }
            $this->request->data['documentguide']['title'] = $this->request->data['title'];
            $this->request->data['documentguide']['type'] = 1;
            $this->request->data['documentguide']['status'] = $this->request->data['status'];
            $this->request->data['documentguide']['user_id'] = $this->request->session()->read('Auth.User.id');

            $documentguide = $this->Documentguides->patchEntity($documentguide, $this->request->data['documentguide']);
            if ($this->Documentguides->save($documentguide)) {
                if ($this->request->data['doc']['name'] != "") {
                    move_uploaded_file($source, $destination);
                }        
                $this->Flash->success(__('The document has been saved.'));

                return $this->redirect(['action' => 'doclist']);
            }
            $this->Flash->error(__('The document could not be saved. Please, try again.'));
        }
        $this->set(compact('documentguide'));
        $this->set('_serialize', ['documentguide']);
        $this->viewBuilder()->setLayout('backend');
    }

    public function addContent()
    {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 2) {

        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $documentguide = $this->Documentguides->newEntity();
        if ($this->request->is('post')) {
            $documentguide_order = $this->Documentguides->find('all', [
                'fields' => array('order' => 'MAX(Documentguides.ordinal)')
            ]);
            if (!$documentguide_order) {
                $inc_documentguide_order = 1;
            }
            $documentguide_order_array = $documentguide_order->toArray();
            $inc_documentguide_order = $documentguide_order_array[0]['order'] + 1;

            $this->request->data['documentguide']['title'] = $this->request->data['title'];
            $this->request->data['documentguide']['description'] = $this->request->data['description'];

            $this->request->data['documentguide']['type'] = 0;
            $this->request->data['documentguide']['status'] = 1;
            $this->request->data['documentguide']['ordinal'] = $inc_documentguide_order;
            $this->request->data['documentguide']['user_id'] = $this->request->session()->read('Auth.User.id');

            $documentguide = $this->Documentguides->patchEntity($documentguide,$this->request->data['documentguide']);
            if ($this->Documentguides->save($documentguide)) {
                $this->Flash->success(__('The Guide content has been saved.'));

                return $this->redirect(['action' => 'guideContent',$documentguide->id]);
            }
            $this->Flash->error(__('The Guide content could not be saved. Please, try again.'));
        }
        $this->set(compact('documentguide'));
        $this->set('_serialize', ['documentguide']);
        $this->viewBuilder()->setLayout('backend');
    }
// guide content that can be shown to members are added here: this is a backend function and only admin can do that
    public function guideContent($id = null)
    {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 2) {

        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));

        if($id == "") {
            $contentID = $this->Documentguides->find('all',['conditions'=>['type'=>'0'],'order' =>['id'=>'DESC']])->first();       

            $id = $contentID['id'];
        }   

        $documentguide = $this->Documentguides->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $this->request->data['documentguide']['title'] = $this->request->data['title'];
            $this->request->data['documentguide']['description'] = $this->request->data['description'];
            $this->request->data['documentguide']['type'] = 0;
            $this->request->data['documentguide']['status'] = $this->request->data['status'];
            $this->request->data['documentguide']['ordinal'] = 0;
            $this->request->data['documentguide']['user_id'] = $this->request->session()->read('Auth.User.id');

            $documentguide = $this->Documentguides->patchEntity($documentguide, $this->request->data['documentguide']);
            if ($this->Documentguides->save($documentguide)) {

                $this->Flash->success(__('The guide content has been saved.'));

                return $this->redirect(['action' => 'guideContent',$id]);
            }
            $this->Flash->error(__('The guide content could not be saved. Please, try again.'));
        }
        $this->set(compact('documentguide'));
        $this->set('_serialize', ['documentguide']);
        $this->viewBuilder()->setLayout('backend');
    }
    /**
     * Delete method
     *
     * @param string|null $id documentguide id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 2) {

        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $this->request->allowMethod(['post', 'delete','get']);
        $documentguide = $this->Documentguides->get($id);
        if ($this->Documentguides->delete($documentguide)) {
            $this->Flash->success(__('Le document a été supprimé.'));
        } else {
            $this->Flash->error(__('Le document n\'a pas pu être supprimé. Veuillez réessayer.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    public function udapteorder() {
        if ($this->request->is('post')) {
            $success = 0;
            foreach ($this->request->data['data']['documentguide_id'] as $key => $data) {
                $documentguide = $this->Documentguides->newEntity();
                $this->request->data['documentguide']['ordinal'] = $this->request->data['data']['ordinal'][$key];

                $documentguide->id = $this->request->data['data']['documentguide_id'][$key];
                $documentguide = $this->Documentguides->patchEntity($documentguide, $this->request->data['documentguide'], [
                    'validate' => 'OnlyCheck'
                ]);

                if ($this->Documentguides->save($documentguide)) {
                    $success = 1;
                } else {
                    $success = 0;
                }
            }   
            if ($success == 1) {
                $this->Flash->success(__('Document order has been saved.'));
                return $this->redirect(array('action' => 'doclist'));
            } else {

                $this->Flash->error(__('Document order not saved, try again later'));
                return $this->redirect(array('action' => 'documentguidelist'));
            }
        }
        $this->viewBuilder()->setLayout();
        $this->autoRender = false;
    }
}
