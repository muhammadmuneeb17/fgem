<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Ombudsmans Controller
 *
 * @property \App\Model\Table\OmbudsmansTable $Ombudsmans
 *
 * @method \App\Model\Entity\Ombudsman[] paginate($object = null, array $settings = [])
 */
class OmbudsmansController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $ombudsmans = $this->paginate($this->Ombudsmans);

        $this->set(compact('ombudsmans'));
        $this->set('_serialize', ['ombudsmans']);
    }

    /**
     * View method
     *
     * @param string|null $id Ombudsman id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ombudsman = $this->Ombudsmans->get($id, [
            'contain' => ['Ombudsmanaccreds', 'Ombudsmanformations', 'Ombudsmanlevels']
        ]);

        $this->set('ombudsman', $ombudsman);
        $this->set('_serialize', ['ombudsman']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ombudsman = $this->Ombudsmans->newEntity();
        if ($this->request->is('post')) {
            $ombudsman = $this->Ombudsmans->patchEntity($ombudsman, $this->request->getData());
            if ($this->Ombudsmans->save($ombudsman)) {
                $this->Flash->success(__('The ombudsman has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ombudsman could not be saved. Please, try again.'));
        }
        $this->set(compact('ombudsman'));
        $this->set('_serialize', ['ombudsman']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ombudsman id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ombudsman = $this->Ombudsmans->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ombudsman = $this->Ombudsmans->patchEntity($ombudsman, $this->request->getData());
            if ($this->Ombudsmans->save($ombudsman)) {
                $this->Flash->success(__('The ombudsman has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ombudsman could not be saved. Please, try again.'));
        }
        $this->set(compact('ombudsman'));
        $this->set('_serialize', ['ombudsman']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ombudsman id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ombudsman = $this->Ombudsmans->get($id);
        if ($this->Ombudsmans->delete($ombudsman)) {
            $this->Flash->success(__('The ombudsman has been deleted.'));
        } else {
            $this->Flash->error(__('The ombudsman could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
