<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Comments Controller
 *
 * @property \App\Model\Table\CommentsTable $Comments
 *
 * @method \App\Model\Entity\Comment[] paginate($object = null, array $settings = [])
 */
class CommentsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    // comments of a payment can be added here
    public function commentlist($id = null,$accont_id = null)
    {
         $permission = $this->viewVars['actionPermission'];
        
       

        if ($permission == 1 || $permission == 2) {
            
        } else {
                return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $id=$_GET['id'];$accont_id=$_GET['accont_id'];
        $this->paginate = [
            'contain' => ['Accounts' => ['Registerations' => 'Users'], 'Users'],
             'conditions' => ['Accounts.id' => $id],
        ];
        $comments = $this->paginate($this->Comments);
        
        

        $this->set(compact('comments','id','accont_id'));
        $this->set('_serialize', ['comments']);
        $this->viewBuilder()->setLayout('backend');
    }

    /**
     * View method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $comment = $this->Comments->get($id, [
            'contain' => ['Accounts', 'Users'],
            
        ]);

        $this->set('comment', $comment);
        $this->set('_serialize', ['comment']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    // new comment can be added here
    public function add($id = null,$accont_id = null)
    {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 2) {

        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $id=$_GET['id'];$accont_id=$_GET['accont_id'];
        $comment = $this->Comments->newEntity();
        $userId = $this->request->session()->read('Auth.User.id');

            $this->loadModel('Users');
            $roleId = $this->request->session()->read('Auth.User.role_id');

        if ($this->request->is('post')) {
            $comment = $this->Comments->patchEntity($comment, $this->request->getData());
            $this->Comments->save($comment);
            //debug($comment);exit;
            if ($this->Comments->save($comment)) {
                
                //STORE LOGS
                    $this->loadModel('Logs');

                    $findRole = $this->Users->get($userId, [
                        'contain' => ['Roles']
                    ]);

                    $roleName = $findRole->role->name;

                    $activity = 'New Comment Added';
                    //$cityName = $user['user']['first_name'];

                    $note = $roleName . ' added a comment for account id (' . $id . ')';

                    $logs = $this->Logs->newEntity();
                    $this->request->data['Log']['user_id'] = $userId;
                    $this->request->data['Log']['activity'] = $activity;
                    $this->request->data['Log']['note'] = $note;

                    $logs = $this->Logs->patchEntity($logs, $this->request->data['Log']);

                    if ($this->Logs->save($logs)) {
                        $this->Flash->success(__('The comment has been saved.'));
                        return $this->redirect(['action' => 'index', 'id' =>$id,'accont_id'=>$accont_id]);
                    }
                $this->Flash->success(__('The comment has been saved.'));

                return $this->redirect(['action' => 'index','id' =>$id,'accont_id'=>$accont_id]);
            }
            $this->Flash->error(__('The comment could not be saved. Please, try again.'));
        }
        $accounts = $this->Comments->Accounts->find('list', ['limit' => 200]);
        $users = $this->Comments->Users->find('list', ['limit' => 200]);
        $this->set(compact('comment', 'accounts', 'users','accont_id','id','com_id'));
        $this->set('_serialize', ['comment']);
        $this->viewBuilder()->setLayout('backend');
    }

    /**
     * Edit method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
//    a comment can be edited here
    public function edit($id = null,$accont_id = null,$com_id = null)
    {
        $permission = $this->viewVars['actionPermission'];
        

        if ($permission == 2) {

        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
         $id=$_GET['id'];$accont_id=$_GET['accont_id'];$com_id=$_GET['com_id'];
        $comment = $this->Comments->get($com_id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userId = $this->request->session()->read('Auth.User.id');

            $this->loadModel('Users');
            $roleId = $this->request->session()->read('Auth.User.role_id');
            $comment = $this->Comments->patchEntity($comment, $this->request->getData());
            if ($this->Comments->save($comment)) {
                //STORE LOGS
                    $this->loadModel('Logs');

                    $findRole = $this->Users->get($userId, [
                        'contain' => ['Roles']
                    ]);

                    $roleName = $findRole->role->name;

                    $activity = 'Comment Updated';
                    $cityName = 'store by';

                    $note = $roleName . ' Updated a comment for account id (' . $id . ')';

                    $logs = $this->Logs->newEntity();
                    $this->request->data['Log']['user_id'] = $userId;
                    $this->request->data['Log']['activity'] = $activity;
                    $this->request->data['Log']['note'] = $note;

                    $logs = $this->Logs->patchEntity($logs, $this->request->data['Log']);

                    if ($this->Logs->save($logs)) {
                        $this->Flash->success(__('The comment has been saved.'));
                        return $this->redirect(['action' => 'index','id' =>$id,'accont_id'=>$accont_id]);
                    }
                $this->Flash->success(__('The comment has been saved.'));

                return $this->redirect(['action' => 'index','id' =>$id,'accont_id'=>$accont_id]);
            }
            $this->Flash->error(__('The comment could not be saved. Please, try again.'));
        }
        $accounts = $this->Comments->Accounts->find('list', ['limit' => 200]);
        $users = $this->Comments->Users->find('list', ['limit' => 200]);
        $this->set(compact('comment', 'accounts', 'users','id','accont_id','com_id'));
        $this->set('_serialize', ['comment']);
        $this->viewBuilder()->setLayout('backend');
    }

    /**
     * Delete method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    // deletion of a comment is done here
    public function delete($id = null)
    {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 2) {

        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
           $id=$_GET['id'];$accont_id=$_GET['accont_id'];$com_id=$_GET['com_id'];
        $this->request->allowMethod(['post', 'delete']);
        $comment = $this->Comments->get($com_id);
        if ($this->Comments->delete($comment)) {
            $userId = $this->request->session()->read('Auth.User.id');

            $this->loadModel('Users');
            $roleId = $this->request->session()->read('Auth.User.role_id');
            //STORE LOGS
                    $this->loadModel('Logs');

                    $findRole = $this->Users->get($userId, [
                        'contain' => ['Roles']
                    ]);

                    $roleName = $findRole->role->name;

                    $activity = ' Account Comment Deleted';
                    $cityName = 'deleted by';

                    $note = $roleName . ' deleted a Comment (' . $id . ')';

                    $logs = $this->Logs->newEntity();
                    $this->request->data['Log']['user_id'] = $userId;
                    $this->request->data['Log']['activity'] = $activity;
                    $this->request->data['Log']['note'] = $note;

                    $logs = $this->Logs->patchEntity($logs, $this->request->data['Log']);

                    if ($this->Logs->save($logs)) {
                        $this->Flash->success(__('The comment has been deleted'));
                        return $this->redirect(['action' => 'index' ,'id' =>$id,'accont_id'=>$accont_id]);
                    }
            $this->Flash->success(__('The comment has been deleted.'));
        } else {
            $this->Flash->error(__('The comment could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index','id' =>$id,'accont_id'=>$accont_id]);
        $this->viewBuilder()->setLayout('backend');
    }
}
