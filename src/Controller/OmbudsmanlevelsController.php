<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Ombudsmanlevels Controller
 *
 * @property \App\Model\Table\OmbudsmanlevelsTable $Ombudsmanlevels
 *
 * @method \App\Model\Entity\Ombudsmanlevel[] paginate($object = null, array $settings = [])
 */
class OmbudsmanlevelsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Ombudsmen', 'Languages']
        ];
        $ombudsmanlevels = $this->paginate($this->Ombudsmanlevels);

        $this->set(compact('ombudsmanlevels'));
        $this->set('_serialize', ['ombudsmanlevels']);
    }

    /**
     * View method
     *
     * @param string|null $id Ombudsmanlevel id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ombudsmanlevel = $this->Ombudsmanlevels->get($id, [
            'contain' => ['Ombudsmen', 'Languages']
        ]);

        $this->set('ombudsmanlevel', $ombudsmanlevel);
        $this->set('_serialize', ['ombudsmanlevel']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ombudsmanlevel = $this->Ombudsmanlevels->newEntity();
        if ($this->request->is('post')) {
            $ombudsmanlevel = $this->Ombudsmanlevels->patchEntity($ombudsmanlevel, $this->request->getData());
            if ($this->Ombudsmanlevels->save($ombudsmanlevel)) {
                $this->Flash->success(__('The ombudsmanlevel has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ombudsmanlevel could not be saved. Please, try again.'));
        }
        $ombudsmen = $this->Ombudsmanlevels->Ombudsmen->find('list', ['limit' => 200]);
        $languages = $this->Ombudsmanlevels->Languages->find('list', ['limit' => 200]);
        $this->set(compact('ombudsmanlevel', 'ombudsmen', 'languages'));
        $this->set('_serialize', ['ombudsmanlevel']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ombudsmanlevel id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ombudsmanlevel = $this->Ombudsmanlevels->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ombudsmanlevel = $this->Ombudsmanlevels->patchEntity($ombudsmanlevel, $this->request->getData());
            if ($this->Ombudsmanlevels->save($ombudsmanlevel)) {
                $this->Flash->success(__('The ombudsmanlevel has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ombudsmanlevel could not be saved. Please, try again.'));
        }
        $ombudsmen = $this->Ombudsmanlevels->Ombudsmen->find('list', ['limit' => 200]);
        $languages = $this->Ombudsmanlevels->Languages->find('list', ['limit' => 200]);
        $this->set(compact('ombudsmanlevel', 'ombudsmen', 'languages'));
        $this->set('_serialize', ['ombudsmanlevel']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ombudsmanlevel id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ombudsmanlevel = $this->Ombudsmanlevels->get($id);
        if ($this->Ombudsmanlevels->delete($ombudsmanlevel)) {
            $this->Flash->success(__('The ombudsmanlevel has been deleted.'));
        } else {
            $this->Flash->error(__('The ombudsmanlevel could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
