<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;
use Cake\Routing\Router;
use Cake\Datasource\ConnectionManager;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

    var $helpers = array('Html');

    public function isAuthorized($user) {
        // Admin can access every action
        $allowFrontEndUsers = array('customerProfile');
        $allowUsersarray = array('dashboard', 'getDates', 'memberdashboard', 'news', 'ticketCalendar', 'ticketCalendar2', 'mySchedule');
        if (in_array($this->request->params['action'], $allowFrontEndUsers) && $user['role_id'] == 10) {
            return true;
        }
        if (in_array($this->request->params['action'], $allowUsersarray)) {
            return true;
        }

        // Default deny
        return false;
    }

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['committee', 'scheduleStarts', 'checkStatus']);
    }

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function home() {
        $this->viewBuilder()->layout('front');
    }

    public function mySchedule() {
        $this->set('contentTitle', 'Mon emploi du temps');
        if ($this->Auth->user('role_id') == 1) {
            $this->viewBuilder()->layout('backend');
        } else {
            $this->viewBuilder()->layout('memberlayout');
        }
        $userId = $this->Auth->user('id');
        $this->loadModel('TicketCalendars');

        if ($this->request->is('post')) {
            $calendarId = $this->request->data['ticketId'];
            $myCals = $this->TicketCalendars->find('all')
                    ->where(['id' => $calendarId])
                    ->first();
            $startDate = new \DateTime($myCals->from_date->i18nFormat('YYY-MM-dd'));
            $endDate = new \DateTime($myCals->to_date->i18nFormat('YYY-MM-dd'));

            $holidays = array();
            $i = 0;
            while ($startDate <= $endDate) {
                if (in_array($startDate->format('w'), $this->request->data['off_day'])) {
                    $holidays[$i]['user_id'] = $this->Auth->user('id');
                    $holidays[$i]['ticket_calendar_id'] = $calendarId;
                    $holidays[$i]['date'] = $startDate->format('Y-m-d');
                    $i++;
                }
                $startDate->modify('+1 day');
            }
//             debug($holidays);
//            exit;
            if ($holidays) {
                $this->loadModel('WeekHolidays');
                $WeekHolidays = $this->WeekHolidays->newEntities($holidays);
                if ($this->WeekHolidays->saveMany($WeekHolidays)) {
                    $this->Flash->success('Holidays has been added.');
                }
            } else {
                $this->Flash->error('No date to add.');
            }
            return $this->redirect(['action' => 'my_schedule']);
        }

        $myCals = $this->TicketCalendars->find('all')
                ->contain(['WeekHolidays'])
                ->where(['user_id' => $userId])
                ->toArray();
        $this->set(compact('myCals'));
    }

//customer profile function and the form in this function is submitted to Registerations/customerprofile
    public function customerProfile() {
        $this->viewBuilder()->layout('customer_layout');
    }

    public function getDates() {
        $this->loadModel('TicketCalendars');
        $myCals = $this->TicketCalendars->find('all')
                ->where(['id' => $_GET['calenderId']])
                ->first();
        $this->loadModel('WeekHolidays');
        $holidays = $this->WeekHolidays->find('list', ['keyField' => 'id', 'valueField' => 'date'])
                ->where(['ticket_calendar_id' => $_GET['calenderId']])
                ->toArray();
        $hArray = array();
        foreach ($holidays as $k => $h):
            $hArray[$k] = $h->i18nFormat('YYY-MM-dd');
        endforeach;
//        $from_date = $myCals->from_date->i18nFormat('YYY-MM-dd');
//        $to_date = $myCals->to_date->i18nFormat('YYY-MM-dd');
//        $period = new \DatePeriod(
//                new \DateTime($from_date), new \DateInterval('P1D'), new \DateTime($to_date)
//        );
        $from_date = $myCals->from_date;
        $to_date = $myCals->to_date;
        $period = new \DatePeriod(
                $from_date, new \DateInterval('P1D'), $to_date
        );
//        debug($from_date);
        $html = '<table class="table table-bordered table-striped"><tr><th style="font-weight:bold;">Date</th>'
//                . '<th style="font-weight:bold;">Is Off</th>'
                . '<th style="font-weight:bold;">Off Days</th></tr>';
        foreach ($period as $key => $value) {
            $isOff = 'No';
            $isChecked = '';
            if (in_array($value->format('Y-m-d'), $hArray)):
                $isOff = "<span style='color:red'>Yes</span>";
                $isChecked = 'checked';
            endif;
            $html .= '<tr>';
            $html .= '<td>' . $value->format('Y-m-d') . '</td>';
//            $html .= '<td>' . $isOff . '</td>';
            $html .= '<td style="margin-bottom:10px;"><input type="checkbox" id="' . strtolower($value->format('l')) . '" name="off_day[]" value="' . $value->format('w') . '" ' . $isChecked . ' disabled>&nbsp; <label for="' . strtolower($value->format('l')) . '">' . $value->format('l') . '</label></td>';

            $html .= '</tr>';
        }
        $isOff = 'No';
        $isChecked = '';
        if (in_array($to_date->format('Y-m-d'), $hArray)):
            $isOff = "<span style='color:red'>Yes</span>";
            $isChecked = 'checked';
        endif;
        $html .= '<tr>';
        $html .= '<td>' . $to_date->format('Y-m-d') . '</td>';
//        $html .= '<td>' . $isOff . '</td>';
        $html .= '<td style="margin-bottom:10px;"><input type="checkbox" id="' . strtolower($to_date->format('l')) . '" name="off_day[]" value="' . $to_date->format('w') . '" ' . $isChecked . ' disabled>&nbsp; <label for="' . strtolower($to_date->format('l')) . '">' . $to_date->format('l') . '</label></td>';

        $html .= '</tr>';
        $html .= '</table>';
        echo $html;
        exit;
    }

    public function committee() {

        $this->loadModel('Emails');
        $email = $this->Emails->get(8);


        $predefinedYear = date('Y');
        $predefinedWeeks = date('W') + $email->time_interval;

// find first mоnday of the year
        $firstMon = strtotime("mon jan {$predefinedYear}");

// calculate how much weeks to add
        $weeksOffset = $predefinedWeeks - date('W', $firstMon);

// calculate searched monday
        $newDate = date('Y-m-d', strtotime("+{$weeksOffset} week " . date('Y-m-d', $firstMon)));


//        $newDate = '2009-12-18';
        $this->loadModel('TicketCalendars');
        $tcals = $this->TicketCalendars->find('all')
                ->contain(['WeekHolidays' => function($q) use($newDate) {
                        $q->where(['WeekHolidays.date' => $newDate]);
                        return $q;
                    }])
                ->where(['TicketCalendars.from_date <=' => $newDate, 'TicketCalendars.to_date >=' => $newDate])
                ->order(['TicketCalendars.type'])
                ->toArray();

        $checkUser = "";
        if (count($tcals) != 2) {
            if (count($tcals) == 0) {
                $checkUser = 'primary user missing';
            }
            if (count($tcals) == 1) {
                $checkUser = 'secondary user missing';
            }
        }
        if (!empty($checkUser)) {
            $this->loadModel('MemberRoles');
            $roles = $this->MemberRoles->find('all')
                    ->contain(['Users'])
                    ->where(['MemberRoles.role_id' => 11])
                    ->toArray();
//            $users = $roles->users;

            foreach ($roles as $role):
                $user = $role->user;

                $findString = ["{first_name}", "{last_name}"];
                $newString = [$user->first_name, $user->last_name];
                $email->message = str_replace($findString, $newString, $email->message);

                $emailToTalent = new Email();
                $emailToTalent->from(['no-reply@cyberclouds.com' => $email->title])
                        ->emailFormat('both')
                        ->to([$user->email])
                        ->subject($email->subject)
                        ->send($email->message);

//            
//                $Email = new Email();
//                $Email->from(array('no-reply@cyberclouds.com' => 'FGEM'));
//                $Email->emailFormat('both');
//                $Email->to($user->email);
//                $Email->subject(ucwords($checkUser) . ' | FGEM');
//                $message = "<style>strong,p {font-family: arial; font-size: 14px;} div {background:#f8f8f8;padding:15px;paddint-bottom:5px;} </style>
//                        <span style='background:#161616;display:block;padding:10px;'><img src='http://needanecommercesite.com/fgem/img/logo-light.png' style='display:block;margin:auto;max-width: 130px;'/></span>
//                        <div>
//                        <strong>Bonjour " . $user->first_name . " " . $user->last_name . ",</strong><p>" . $checkUser . ' for the current week.';
//
//                $message .= "<br /><a href=''></a></p><br/><br/>Merci, <br/>FGEM</div>";
//
//                $Email->send($message);
            endforeach;
        }
        exit;
    }

    public function checkStatus() {

        $this->loadModel('Emails');
        $email = $this->Emails->get(10);

        $days_ago = date('Y-m-d 00:00:00', strtotime('-' . $email->time_interval . ' days'));
//debug($days_ago);
//        $newDate = '2009-12-18';
        $this->loadModel('Tickets');
        $tcals = $this->Tickets->find('all')
//                ->contain()
                ->where(['Tickets.modified <=' => $days_ago, 'Tickets.status' => 1])
                ->toArray();

        if (!empty($tcals)) {
            $this->loadModel('MemberRoles');
            $roles = $this->MemberRoles->find('all')
                    ->contain(['Users'])
                    ->where(['MemberRoles.role_id' => 11])
                    ->toArray();
//            $users = $roles->users;

            foreach ($roles as $role):
                $user = $role->user;

                $findString = ["{first_name}", "{last_name}"];
                $newString = [$user->first_name, $user->last_name];
                $email->message = str_replace($findString, $newString, $email->message);

                $emailToTalent = new Email();
                $emailToTalent->from(['no-reply@cyberclouds.com' => $email->title])
                        ->emailFormat('both')
                        ->to([$user->email])
                        ->subject($email->subject)
                        ->send($email->message);
            endforeach;
        }
        exit;
    }

    public function scheduleStarts() {

        $this->loadModel('Emails');
        $email = $this->Emails->get(9);

        $newDate = date('Y-m-d', strtotime("next monday"));

        $this->loadModel('TicketCalendars');
        $tcals = $this->TicketCalendars->find('all')
                ->contain(['Users'])
                ->where(['TicketCalendars.from_date <=' => $newDate, 'TicketCalendars.to_date >=' => $newDate])
                ->toArray();

        if (!empty($tcals)) {
            $roles = $tcals;

            foreach ($roles as $role):
                $user = $role->user;

                $findString = ["{first_name}", "{last_name}"];
                $newString = [$user->first_name, $user->last_name];
                $email->message = str_replace($findString, $newString, $email->message);
                $emailToTalent = new Email();
                $emailToTalent->from(['no-reply@cyberclouds.com' => $email->title])
                        ->emailFormat('both')
                        ->to([$user->email])
                        ->subject($email->subject)
                        ->send($email->message);
            endforeach;
        }
        exit;
    }

// the calendar that are shown in backend as well as in backend are here
    public function ticketCalendar() {
        $html = 'Mon Calendrier &nbsp;<small><span class="dot dot-primary"></span>Permanent</small>&nbsp;<small><span class="dot dot-secondary"></span>Suppléant</small>';
        $this->set('contentTitle', $html);
        if ($this->Auth->user('role_id') == 1) {
            $this->viewBuilder()->layout('backend');
        } else {
            $this->viewBuilder()->layout('memberlayout');
        }
        $this->loadModel('Users');
        $c = ($this->Auth->user('role_id') == 1) ? array('6', '1') : '6';
        $users = $this->Users->find('list', ['keyField' => 'id', 'valueField' => function($row) {
                        return $row['first_name'] . ' ' . $row['last_name'];
                    }])
                ->where(['role_id IN' => $c])
                ->toArray();
        $this->set(compact('users'));
        if ($this->request->is('post')) {
            $fromDate = date('Y-m-d', strtotime($this->request->data['from_date']));
            $currentDate = date('Y-m-d', strtotime($this->request->data['current_date']));
            $this->loadModel('TicketCalendars');
            $this->loadModel('WeekHolidays');
            if ($this->request->data['action'] == 'delete holiday') {

                if (!empty($this->request->data['user_id'])) {
                    $userId = $this->request->data['user_id'];
//                            $this->request->data['assigned_by'] = $this->Auth->user('id');
                } else {
                    $userId = $this->Auth->user('id');
                }
                $check = $this->WeekHolidays->find('all')
                                ->where(['user_id' => $userId, 'date' => $currentDate])->first();
                if ($check) {
                    if ($this->WeekHolidays->delete($check)) {
                        $this->Flash->success('Vacances supprimées avec succès');
                    }
                } else {
                    $this->Flash->error('Aucun Enregistrement Trouvé');
                }
                return $this->redirect(['action' => 'ticket_calendar']);
            }
            if ($this->request->data['action'] == 'delete week') {
                if (!empty($this->request->data['user_id'])) {
                    $userId = $this->request->data['user_id'];
//                            $this->request->data['assigned_by'] = $this->Auth->user('id');
                } else {
                    $userId = $this->Auth->user('id');
                }
                $checkWeek = $this->TicketCalendars->find('all')
                        ->contain(['WeekHolidays'])
                        ->where(['user_id' => $userId, 'from_date' => $fromDate])
                        ->first();
                if ($checkWeek == null) {
                    $this->Flash->error('Aucune semaine trouvée');
                    return $this->redirect(['action' => 'ticket_calendar']);
                }
                if ($this->TicketCalendars->delete($checkWeek)) {
                    $checkSecondary = $this->TicketCalendars->find('all')
                            ->contain(['WeekHolidays'])
                            ->where(['type' => 2, 'from_date' => $fromDate])
                            ->first();
                    if ($checkSecondary) {
                        $checkSecondary->type = 1;
                        $this->TicketCalendars->save($checkSecondary);
                    }
                    $this->Flash->success('La semaine a été supprimée avec succès');
                } else {
                    $this->Flash->error('La semaine ne peut pas être supprimée');
                }
                return $this->redirect(['action' => 'ticket_calendar']);
            }
            if ($this->request->data['action'] == 'holiday') {
                if (!empty($this->request->data['user_id'])) {
                    $userId = $this->request->data['user_id'];
                } else {
                    $userId = $this->Auth->user('id');
                }

                $checkWeek = $this->TicketCalendars->find('all')
                        ->where(['user_id' => $userId, 'from_date' => $fromDate])
                        ->first();

                // check that if primary or secondary has already on leave in the current date
                $checkPrimaryHoliday = $this->WeekHolidays->find('all')
                        ->where(['user_id !=' => $userId, 'date' => $currentDate])
                        ->first();
                if ($checkPrimaryHoliday) {
                    $this->Flash->error('primaire et secondaire ne peuvent pas être en congé le même jour');
                    return $this->redirect(['action' => 'ticket_calendar']);
                }
                if ($checkWeek) {
//                    debug($checkWeek->id);
                    $check = $this->WeekHolidays->find('all')
                                    ->where(['user_id' => $userId, 'ticket_calendar_id' => $checkWeek->id, 'date' => $currentDate])->toArray();
                    if (empty($check)) {
                        $saveHoliday = array();
                        $saveHoliday['user_id'] = $userId;
                        $saveHoliday['ticket_calendar_id'] = $checkWeek->id;
                        $saveHoliday['date'] = $currentDate;
//                        debug($saveHoliday);
                        $week = $this->WeekHolidays->newEntity($saveHoliday);
                        if ($this->WeekHolidays->save($week)) {
                            $this->Flash->success('Vacances a été ajouté à votre horaire');
                            return $this->redirect(['action' => 'ticket_calendar']);
                        }
                    } else {
                        $this->Flash->error('Ceci est déjà ajouté comme vacances');
                        return $this->redirect(['action' => 'ticket_calendar']);
                    }
                } else {
                    $this->Flash->error("Cette semaine ne vous est pas assignée, veuillez l'ajouter à votre emploi du temps.");
                    return $this->redirect(['action' => 'ticket_calendar']);
                }
            }
            if ($this->request->data['action'] == 'week') {
                $from_date = date('Y-m-d', strtotime($this->request->data['from_date']));

                $checkweek = $this->TicketCalendars->find('all')
                                ->where(['from_date' => $from_date])->toArray();


                if (!empty($this->request->data['user_id'])) {
                    $userId = $this->request->data['user_id'];
                    $this->request->data['assigned_by'] = $this->Auth->user('id');
                } else {
                    $userId = $this->Auth->user('id');
                }
                // if user have scheduled already this week then just delete it 
                $userWeek = $this->TicketCalendars->find('all')
                                ->where(['from_date' => $from_date, 'user_id' => $userId])->first();

                if ($userWeek) {
                    $this->TicketCalendars->delete($userWeek);
                    $this->Flash->success("Votre nom a été retiré du calendrier pour la date choisie.");
                    return $this->redirect('/Pages/ticket_calendar');
                }
                if (count($checkweek) == 2) {
                    $this->Flash->error(__('Désolé, vous ne pouvez pas réserver cette semaine.'));
                    return $this->redirect(['action' => 'ticket_calendar']);
                }



                $this->request->data['user_id'] = $userId;
                $this->request->data['to_date'] = date('Y-m-d', strtotime('+6 day', strtotime($from_date)));
                if (count($checkweek) == 1) {

                    if ($checkweek[0]->user_id == $userId) {
                        $this->Flash->error('Vous êtes déjà ajouté en tant que primaire pour cette semaine.');
                        return $this->redirect(['action' => 'ticket_calendar']);
                    }
                    $this->request->data['type'] = 2;
                    $this->Flash->success(__('Vous avez été ajouté en tant que secondaire à la semaine sélectionnée'));
                } else {
                    $this->request->data['type'] = 1;
                    $this->Flash->success(__('Vous avez été ajouté comme primaire à la semaine sélectionnée'));
                }
                $TicketCalendars = $this->TicketCalendars->newEntity();
                $TicketCalendars = $this->TicketCalendars->patchEntity($TicketCalendars, $this->request->data);

                if (!$this->TicketCalendars->save($TicketCalendars)) {
                    $this->Flash->error('Votre demande ne peut pas encore être traitée. Veuillez réessayer.');
                }
            }
            return $this->redirect(['action' => 'ticket_calendar']);
        }
    }

//data for the calender are designed here
    public function ticketCalendar2() {
        $this->loadModel('TicketCalendars');
        $this->loadModel('WeekHolidays');
        $holidays = $this->WeekHolidays->find('all')
                ->contain(['Users'])
                ->toArray();
//        $holidaysList = $this->WeekHolidays->find('list', ['keyField' => 'id', 'valueField' => function($row) {
//                        return $row['date']->format('Y-m-d');
//                    }])
//                ->toArray();
        $cals = $this->TicketCalendars->find('all')
                ->contain(['Users', 'WeekHolidays'])
                ->order(['type'])
//                ->where(['MONTH(from_date)' => date('n')])
                ->toArray();

        $current_month = array();
        $key = 0;

        foreach ($cals as $cal):

            if (empty($cal->week_holidays)) {
                $primaryTitle = 'Primary';
                $primaryColor = '#99CCCC';
                $class = 'primary';
                if ($cal->type == 2) {
                    $primaryTitle = 'Secondary';
                    $primaryColor = '#FF5D4C';
                    $class = 'secondary';
                }
                $current_month[$key]['name'] = ucwords($cal->user->first_name . ' ' . $cal->user->last_name) . ' (' . $primaryTitle . ')';
                $current_month[$key]['startdate'] = $cal->from_date->i18nFormat('YYY-MM-dd');
                $current_month[$key]['enddate'] = $cal->to_date->i18nFormat('YYY-MM-dd');
                $current_month[$key]['color'] = $primaryColor;
                $current_month[$key]['url'] = '';
                $current_month[$key]['class'] = $class;
//                $individual->format('Y-m-d');
                $key++;
            } else {
                $primaryTitle = 'Primary';
                $primaryColor = '#99CCCC';
                $class = 'primary';
                if ($cal->type == 2) {
                    $primaryTitle = 'Secondary';
                    $primaryColor = '#FF5D4C';
                    $class = 'secondary';
                }

                $period = new \DatePeriod(
                        $cal->from_date, new \DateInterval('P1D'), $cal->to_date
                );
                $weekHolidaysArray = array();
                foreach ($cal->week_holidays as $weekHoliday):
                    array_push($weekHolidaysArray, $weekHoliday->date->format('Y-m-d'));
                endforeach;
                foreach ($period as $individual):

//                    debug($individual->format('Y-m-d'));
                    if (!in_array($individual->format('Y-m-d'), $weekHolidaysArray)) {
                        $current_month[$key]['name'] = ucwords($cal->user->first_name . ' ' . $cal->user->last_name) . ' (' . $primaryTitle . ')';
                        $current_month[$key]['startdate'] = $individual->format('Y-m-d');
//                $current_month[$key]['enddate'] = $cal->to_date->i18nFormat('YYY-MM-dd');
                        $current_month[$key]['color'] = $primaryColor;
                        $current_month[$key]['url'] = '';
                        $current_month[$key]['class'] = $class;
                        $individual->format('Y-m-d');
                        $key++;
                    } else {
                        $current_month[$key]['name'] = ucwords($cal->user->first_name . ' ' . $cal->user->last_name) . ' (Off day)';
                        $current_month[$key]['startdate'] = $individual->format('Y-m-d');
//                $current_month[$key]['enddate'] = $cal->to_date->i18nFormat('YYY-MM-dd');
                        $current_month[$key]['color'] = '#f40035';
                        $current_month[$key]['url'] = '';
                        $current_month[$key]['class'] = $class;
                        $individual->format('Y-m-d');
                        $key++;
                    }
                endforeach;
                if (!in_array($cal->to_date->format('Y-m-d'), $weekHolidaysArray)) {
                    $current_month[$key]['name'] = ucwords($cal->user->first_name . ' ' . $cal->user->last_name) . ' (' . $primaryTitle . ')';
                    $current_month[$key]['startdate'] = $cal->to_date->format('Y-m-d');
//                $current_month[$key]['enddate'] = $cal->to_date->i18nFormat('YYY-MM-dd');
                    $current_month[$key]['color'] = $primaryColor;
                    $current_month[$key]['url'] = '';
                    $current_month[$key]['class'] = $class;
                    $individual->format('Y-m-d');
                    $key++;
                }
            }

        endforeach;
//        debug($current_month);
//        exit;
//        foreach ($holidays as $cal):
////                    debug($cal->ticket_calendar_id);
////            $primaryTitle = 'Primary';
//            $primaryTitle = 'Off day';
//            $primaryColor = '#f40035';
//            $class = 'primaryHoliday';
//            if ($cal->type == 2) {
////                $primaryTitle = 'Secondary';
//                $primaryTitle = 'Off day';
//                $primaryColor = '#FF5D4C';
//                $class = 'secondaryHoliday';
//            }
//            $current_month[$key]['name'] = ucwords($cal->user->first_name . ' ' . $cal->user->last_name) . ' (' . $primaryTitle . ')';
//            $current_month[$key]['startdate'] = $cal->date->i18nFormat('YYY-MM-dd');
//            $current_month[$key]['enddate'] = '';
//            $current_month[$key]['color'] = $primaryColor;
//            $current_month[$key]['url'] = '';
//            $current_month[$key]['class'] = $class;
//            $key++;
//        endforeach;
//        debug($current_month);
        echo '{"monthly":' . json_encode($current_month) . '}';
        exit;
    }

    public function ticketCalendar2_backup() {
        $this->loadModel('TicketCalendars');
        $cals = $this->TicketCalendars->find('all')
                ->contain(['Users'])
                ->where(['MONTH(from_date)' => date('n')])
                ->toArray();
        $current_month = array();
        foreach ($cals as $key => $cal):
            $current_month[$key]['start'] = $cal->from_date->i18nFormat('YYY/MM/dd');
            $current_month[$key]['end'] = $cal->to_date->i18nFormat('YYY/MM/dd');
            $current_month[$key]['summary'] = $cal->user->first_name;
            $current_month[$key]['mask'] = true;
        endforeach;
        $cals = $this->TicketCalendars->find('all')
                ->where(['MONTH(from_date)' => date('n', strtotime('+1 month'))])
                ->toArray();
        $next_month = array();
        foreach ($cals as $key => $cal):
            $next_month[$key]['start'] = $cal->from_date->i18nFormat('YYY/MM/dd');
            $next_month[$key]['end'] = $cal->to_date->i18nFormat('YYY/MM/dd');
            $next_month[$key]['summary'] = 'naqeeb';
            $next_month[$key]['mask'] = true;
        endforeach;
        $responce = array('current_month' => $current_month, 'next_month' => $next_month);
        echo json_encode($responce);
        exit;
    }

    public function about() {
        $this->viewBuilder()->layout('internal');
    }

    public function search() {

        $conn = ConnectionManager::get('default');


        if ($this->request->is(['POST', 'GET'])) {
            if ($_GET['advance_search']) {
                $this->request->data['advance_search'] = $_GET['advance_search'];
            }
            if ($_GET['key_word']) {
                $this->request->data['key_word'] = $_GET['key_word'];
            }
            if ($_GET['mediation']) {
                $this->request->data['mediation'] = $_GET['mediation'];
            }
            if ($_GET['accreditation']) {
                $this->request->data['accreditation'] = $_GET['accreditation'];
            }
            if ($_GET['languages']) {
                $this->request->data['languages'] = $_GET['languages'];
            }
            $lang_join = '';
            $medi_join = '';
            $accre_join = '';
            if (isset($this->request->data['advance_search']) && $this->request->data['advance_search'] == 1) {
                $condition = "";
                $condition .= '  (publicprofile = 1 AND users.status = 2 AND registerations.status = 1 )';
                if ($this->request->data['key_word']) {
                    $user = $this->request->data['key_word'];
                    $search = str_replace(" ", "", $user);

                    $condition .= ' AND (tel1="' . $user . '" OR users.first_name LIKE "%' . $user . '%" OR tel2="' . $user . '" OR users.email= "' . $user . '" OR CONCAT(users.first_name, users.last_name) like "%' . $search . '%" OR users.last_name LIKE "%' . $user . '%")';
                }
                if (isset($this->request->data['languages'])) {
                    $languages = $this->request->data['languages'];
                    $condition .= " AND (";
                    foreach ($languages as $lang) {
                        $condition .= " ombudsmanlevels.language_id = $lang OR ";
                    }
                    $condition = rtrim($condition, 'OR ');
                    $condition .= ")";
                    $lang_join = ' join ombudsmanlevels on ombudsmanlevels.ombudsman_id = ombudsmans.id ';
                }
                if (isset($this->request->data['mediation'])) {
                    $mediations = $this->request->data['mediation'];
                    $condition .= " AND (";
                    foreach ($mediations as $medi) {
                        $condition .= " ombudsmanmeds.medtype_id = $medi OR ";
                    }
                    $condition = rtrim($condition, 'OR ');
                    $condition .= ")";
                    $medi_join = ' join ombudsmanmeds on ombudsmanmeds.ombudsman_id = ombudsmans.id ';
                }
                if (isset($this->request->data['accreditation'])) {
                    $accreditations = $this->request->data['accreditation'];
                    $condition .= " AND (";
                    foreach ($accreditations as $accre) {
                        $condition .= " ombudsmanaccreds.accreditation_id = $accre OR ";
                    }
                    $condition = rtrim($condition, 'OR ');
                    $condition .= ")";
                    $accre_join = ' join ombudsmanaccreds on ombudsmanaccreds.ombudsman_id = ombudsmans.id ';
                }
                /* echo $condition;
                  exit; */
//$languages = ;
                $condition = ltrim($condition, ' AND ');
                $query = $conn->execute('SELECT users.id as user_id,CONCAT(first_name, last_name) as names,registerations.image,tel1,registerations.status,tel1public,tel2public,emailpublic,tel2,users.email as mail,first_name,last_name,address1,enterprise,profession,postalcode, ombudsmans.mediator_question as mediator_question , ombudsmans.accept_info as  accept_info '
                        . ' FROM registerations '
                        . 'join users ON registerations.user_id=users.id '
                        . 'join ombudsmans ON ombudsmans.registeration_id = registerations.id '
                        . 'join accounts ON accounts.registeration_id = registerations.id '
                        . $lang_join
                        . $medi_join
                        . $accre_join
                        . ' where ombudsmans.mediator_question = "Oui" AND accounts.amount_paid > 0 AND ' . $condition . 'group by registerations.user_id');

                $search = $query->fetchAll('assoc');
            } else {
                if ($_GET['keyword']) {
                    $user = $_GET['keyword'];

                    $search = str_replace(" ", "", $user);
                    //exit;
                    $query = $conn->execute('SELECT users.id as user_id,CONCAT(first_name, last_name) as names,registerations.image,tel1,registerations.status,tel1public,tel2public,emailpublic,tel2,users.email as mail,first_name,last_name,address1,enterprise,profession,postalcode, ombudsmans.mediator_question as mediator_question , ombudsmans.accept_info as  accept_info '
                            . ' FROM registerations '
                            . 'join users ON registerations.user_id=users.id join accounts ON accounts.registeration_id = registerations.id join ombudsmans ON ombudsmans.registeration_id = registerations.id'
                            . ' where ombudsmans.mediator_question = "Oui" AND accounts.amount_paid > 0 AND publicprofile=1 AND users.status = 2 AND registerations.status=1 AND (users.first_name LIKE "%' . $user . '%"  OR tel1="' . $user . '" OR tel2="' . $user . '" OR users.email= "' . $user . '" OR CONCAT(first_name, last_name) like "' . $search . '" OR users.last_name LIKE "%' . $user . '%")'
                    );
                    $search = $query->fetchAll('assoc');
                }
            }


//         debug($search);exit;
        }

//MEDIATION TYPE
        $this->loadModel('Medtypes');
        $mediations = $this->Medtypes->find('all', [
                ])->toArray();

//ACCREDITATIONS
        $this->loadModel('Accreditations');
        $accreds = $this->Accreditations->find('all', [
                ])->toArray();

        $this->loadModel('Languages');
        $languages = $this->Languages->find('list')->toArray();

        $this->set(compact('search', 'mediations', 'accreds', 'languages'));

//$this->set(compact('search'));
// exit;
        $this->viewBuilder()->layout('internal');
    }

    public function welcome() {
        $this->set('content_title', 'Dashboard');
        $this->viewBuilder()->setLayout('backend');
    }

    // code for backend dashboard is done in this function
    public function dashboard() {
        $this->loadModel('Registerations');
        $user = $this->Registerations->find('all', array(
            'contain' => ['Users'],
            'conditions' => ['Users.role_id' => 6, 'Registerations.status' => 0, 'Users.status <=' => 1 , 'Users.suspend =' => 0]
        ));

        $user = $user->count();
        $this->loadModel('Accounts');
        $user_pend = $this->Accounts->find('all', array(
                    'contain' => ['Registerations' => ['Users' => ['conditions' => ['Users.role_id' => 6, 'Users.status' => 2, 'Users.suspend' => 0]]]],
                    'conditions' => ['Registerations.status' => 0],
                    'group' => ['Accounts.registeration_id']
                ))->count();
       
                $user_app = $this->Registerations->find( 'all', array(
                    'contain' => ['Users'],
                    'conditions' => ['Registerations.status' => 1, 'Users.role_id' => 6,'Users.suspend' => 0],
                ))->count();
                
                $reg_pend = $this->Registerations->find( 'all', array(
                    'limit' => 10,
                    'contain' => ['Users', 'Membertypes', 'Accounts'],
                    'conditions' => ['Registerations.status' => 0, 'Users.status' => 2, 'Users.suspend' => 0]
                ))->toArray();


                $reg_app = $this->Registerations->find( 'all', array(
                    'limit' => 10,
                    'contain' => ['Users', 'Membertypes'],
                    'conditions' => ['Registerations.status' => 1,'Users.suspend' => 0]
                ))->toArray();


        /* $member_owed = $this->Accounts->find('all',array(
          'limit' => 10,
          'contain' => ['Registerations'=>['Membertypes','Users' => ['conditions'=>['Users.status ' => 2]]]],
          'conditions' => ['Accounts.amount_due > '=> '0'],
          'group' => ['Accounts.registeration_id'],
          'order' => ['Accounts.id' => 'DESC']
          ))->toArray(); */

                $conn = ConnectionManager::get('default');

                $query = $conn->execute('SELECT ac.id as accid, ac.amount_due , r.* , m.* , u.* , r.id as rid'
                . ' FROM accounts ac '
                . 'inner join registerations r ON ac.registeration_id=r.id '
                . 'inner join membertypes m ON m.id = r.membertype_id '
                . 'inner join users u ON u.id = r.user_id '
                .  ' where ac.id IN (
                                            SELECT MAX(accounts.id)
                                            FROM accounts
                                            GROUP BY accounts.registeration_id
                                        ) and  ac.amount_due > 0 and u.status = 2');

                $member_owed = $query->fetchAll('assoc');


                $suspended = $this->Accounts->find( 'all', array(
                    'limit' => 10,
                    'contain' => ['Registerations' => ['Membertypes', 'Users' => ['conditions' => ['Users.status ' => 2,'Users.suspend ' => 1]]]],
                    'conditions' => ['Accounts.amount_paid > ' => '0', 'Registerations.status' => '0'],
                    'group' => ['Accounts.registeration_id']
                ))->toArray();

        $this->set(compact('registerations'));
        $this->set( '_serialize', ['registerations']);
        $this->set(compact( 'user', 'user_pend', 'user_app', 'reg_pend', 'reg_app', 'member_owed', 'suspended'));
        $this->viewBuilder()->setLayout('backend');
        }

//dashboard of members 
        public function memberdashboard() {

// Member Profile Status
        $this->loadModel('Users');
                $user = $this->Users->find( 'all', array(
                    'contain' => ['Registerations' => ['Membertypes']],
                    'conditions' => ['Users.role_id' => 6, 'Users.id' => $this->request->session()->read('Auth.User.id')]
                ))->first();
        if ($user['status'] < 2) {
                $profile_status = "PROFILE_INCOMPLETE";
        } else if ($user['registeration']['status'] < 1) {
                $profile_status = "IN_REVIEW";
        } else {
                $profile_status = "APPROVED";
        }
                $member_type = $user['registeration']['membertype']['description'];
                $member_fee = $user['registeration']['membertype']['amount'];

                $reg_id = $user['registeration']['id'];

        $this->loadModel('Accounts');
                $next_pay = $this->Accounts->find( 'all', array(
                    'contain' => ['Registerations'],
                    'fields' => ['next_date' => 'MAX(Accounts.due_date)'],
                    'conditions' => ['Registerations.id' => $reg_id]
                ))->first()['next_date'];


                $amount_paid = $this->Accounts->find( 'all', array(
                    'contain' => ['Registerations'],
                    'fields' => ['amount_paid' => 'MAX(Accounts.amount_paid)'],
                    'conditions' => ['Registerations.id' => $reg_id]
                ))->first()['amount_paid'];

                $amount_owed = $this->Accounts->find( 'all', array(
                    'contain' => ['Registerations'],
                    //'fields' => ['Accounts.amount_paid'], 
                    'conditions' => ['Registerations.id' => $reg_id],
                    'order' => ['Accounts.due_date' => 'DESC']
                ))->first()['amount_due'];
        $this->loadModel('Tickets');
                $tickets = $this->Tickets->find('all')->where(['status !=' => 3])->count();

        $this->set(compact( 'tickets', 'profile_status', 'member_type', 'member_fee', 'next_pay', 'amount_paid', 'amount_owed'));
        $this->viewBuilder()->setLayout('memberlayout');
        }

        public function dashboard1() {
                $this->paginate = [
            'contain' => ['Membertypes', 'Users']
        ];
                $registerations = $this->paginate($this->Registerations);
// debug($registerations);exit;

        $this->set(compact('registerations'));
        $this->set( '_serialize', ['registerations']);
        $this->viewBuilder()->setLayout('backend');
        }

// dynamic news are shown here to members and the news can be added by admin
        public function news() {
        $this->viewBuilder()->setLayout('memberlayout');
        }

        public function contact() {
//I18n::locale('fr');
        if ($this->request->is('post')) {
                $themeColor = "#F2802A";
                $fontColor = "#46B652";
                $primaryColor = "#FFF";
                $subjectColor = "#ffffff";
                $Email = new Email();
        $Email->from(array('inquiry@shristi.com' => 'Srishti Dance Acadmey'));
        $Email->emailFormat('both');
        $Email->bcc('raj@cyberclouds.com');
        $Email->to('contact@srishti.com');
        $Email->subject('Customer Inquiry');

                $message = "<html><head>
			<style>
				h2 {
					background-color:$themeColor;
					color:$fontColor;
					text-align:center;
				}
				table {
					border:1px solid $themeColor;
                                        background-color: #f8f8f8; 
				}
				tr , td , th{
					border:1px solid $themeColor;
				}
                                .subject {
                                background-color: $primaryColor;
                                padding:10px 5px;
                                }
                                h3 {
                                color: $subjectColor;
                                text-align:center;
                                }
                                img {
                                display:block;
                                margin:auto;
                                width:80px;
                                }
                                                        </style>	
                                                        </head><body>
                                                        <div class='subject'><img src='http://cyberclouds.com/srishti/images/logo.png' width='80px'/>
                                <h3>Customer Inquiry</h3></div>
                                 <table cellpadding='10' align='center' width='100%' cellspacing='0'>
				<tr>
					<th>Full Name</th>
					<td>" . $this->request->data['name'] . "</td>	
				</tr>
				<tr>
					<th>Email</th>
					<td>" . $this->request->data['email'] . "</td>	
				</tr>
                                <tr>
					<th>Phone</th>
					<td>" . $this->request->data['phone'] . "</td>	
				</tr>

				<tr>
					<th>Message</th>
					<td>" . $this->request->data['message'] . "</td>	
				</tr>
			</table>
<br />
</body></html>";
        if ($Email->send($message)) {
        $this->Flash->success(__('Thanks for your interest, we will contact you soon'));

        return $this->redirect(['action' => 'contact']);
        }
        $this->Flash->error(__('Some error occured. Please, try again.'));
        }
        $this->viewBuilder()->layout('internal');
    }

    }
