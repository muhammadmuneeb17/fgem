<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * NextRenewalDate Controller
 *
 *
 * @method \App\Model\Entity\NextRenewalDate[] paginate($object = null, array $settings = [])
 */
class NextRenewalDateController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    // next renewal date are listed here
    public function index() {


        $this->paginate = [
            'order' => ['NextRenewalDate.year' => 'ASC']
        ];
        $nextRenewalDate = $this->paginate($this->NextRenewalDate);

        $this->set(compact('nextRenewalDate'));
        $this->set('_serialize', ['nextRenewalDate']);

        $this->viewBuilder()->setLayout('backend');
    }

    /**
     * View method
     *
     * @param string|null $id Next Renewal Date id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $nextRenewalDate = $this->NextRenewalDate->get($id, [
            'contain' => []
        ]);

        $this->set('nextRenewalDate', $nextRenewalDate);
        $this->set('_serialize', ['nextRenewalDate']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    // next renewal date can be added here
    public function add() {
        $nextRenewalDate = $this->NextRenewalDate->newEntity();
        if ($this->request->is('post')) {
            $year = $this->request->data['year'];
            $month = $this->request->data['month'];
            $day = $this->request->data['day'];

            $findYear = $this->NextRenewalDate->find('all', [
                        'conditions' => ['NextRenewalDate.year' => $year]
                    ])->count();

            if ($findYear > 0) {
                $this->Flash->error(__('The selected year already exist. Please choose a new one.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->request->data['NextRenewalDate']['year'] = $year;
            $this->request->data['NextRenewalDate']['month'] = $month;
            $this->request->data['NextRenewalDate']['day'] = $day;
            $nextRenewalDate = $this->NextRenewalDate->patchEntity($nextRenewalDate, $this->request->data['NextRenewalDate']);
            if ($this->NextRenewalDate->save($nextRenewalDate)) {
                $this->loadModel('Accounts');
                $accountDates = TableRegistry::get("Accounts");

                $query = $accountDates->query();
                $result = $query->update()
                        ->set(['due_date' => $year . '-' . $month . '-' . $day])
                        ->where(['year(due_date)' => $year])
                        ->execute();
                $this->Flash->success(__('The next renewal date has been saved and updated for members.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The next renewal date could not be saved. Please, try again.'));
        }
        $this->set(compact('nextRenewalDate'));
        $this->set('_serialize', ['nextRenewalDate']);
        $this->viewBuilder()->setLayout('backend');
    }

    public function addMemberType() {
        $this->loadModel('Membertypes');
        $nextRenewalDate = $this->Membertypes->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['amount'] = 0;

            $nextRenewalDate = $this->Membertypes->patchEntity($nextRenewalDate, $this->request->data);
            if ($this->Membertypes->save($nextRenewalDate)) {

                $this->Flash->success(__('La date du type de membre a été enregistrée.'));
                return $this->redirect('/Registerations/membertypes');
            }
            $this->Flash->error(__("Le type de membre n'a pas pu être enregistré. Veuillez réessayer."));
        }
        $this->loadModel('NextRenewalDate');
        $years = $this->NextRenewalDate->find('list', ['keyField' => 'year', 'valueField' => 'year'])->toArray();

        $this->set(compact('nextRenewalDate', 'years'));
        $this->set('_serialize', ['nextRenewalDate']);
        $this->viewBuilder()->setLayout('backend');
    }
//annual amount for each member type can be added here
    public function addAnnualAmount($id = null) {
        $this->loadModel('MemberTypeAmounts');
        $nextRenewalDate = $this->MemberTypeAmounts->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['membertype_id'] = $id;
            $nextRenewalDate = $this->MemberTypeAmounts->patchEntity($nextRenewalDate, $this->request->data);

            if ($this->MemberTypeAmounts->save($nextRenewalDate)) {
                $this->Flash->success(__('La date du type de membre a été enregistrée.'));
                return $this->redirect('/Registerations/annualAmount/' . $id);
            }
            $this->Flash->error(__("Le type de membre n'a pas pu être enregistré. Veuillez réessayer."));
        }
        $this->loadModel('NextRenewalDate');
        $years = $this->NextRenewalDate->find('list', ['keyField' => 'year', 'valueField' => 'year'])->toArray();

        $this->set(compact('nextRenewalDate', 'years'));
        $this->set('_serialize', ['nextRenewalDate']);
        $this->viewBuilder()->setLayout('backend');
    }
// annual amount edition
    public function editAnnualAmount($id = null, $redirect = null) {
        $this->loadModel('MemberTypeAmounts');
        $nextRenewalDate = $this->MemberTypeAmounts->get($id);
        if ($this->request->is(['post', 'put', 'patch'])) {

            $nextRenewalDate = $this->MemberTypeAmounts->patchEntity($nextRenewalDate, $this->request->data);
//            debug($nextRenewalDate);
//            exit;
            if ($this->MemberTypeAmounts->save($nextRenewalDate)) {
                $this->Flash->success(__('La date du type de membre a été enregistrée.'));
                return $this->redirect('/Registerations/annualAmount/' . $redirect);
            }
            $this->Flash->error(__("Le type de membre n'a pas pu être enregistré. Veuillez réessayer."));
        }
        $this->loadModel('NextRenewalDate');
        $years = $this->NextRenewalDate->find('list', ['keyField' => 'year', 'valueField' => 'year'])->toArray();

        $this->set(compact('nextRenewalDate', 'years'));
        $this->set('_serialize', ['nextRenewalDate']);
        $this->viewBuilder()->setLayout('backend');
    }
// membertype can be eited here
    public function editMemberType($id = null) {
        $this->loadModel('Membertypes');
        $nextRenewalDate = $this->Membertypes->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $nextRenewalDate = $this->Membertypes->patchEntity($nextRenewalDate, $this->request->data);

            if ($this->Membertypes->save($nextRenewalDate)) {

                $this->Flash->success(__('La date du type de membre a été mise à jour.'));
                return $this->redirect('/Registerations/membertypes');
            }
            $this->Flash->error(__("Le type de membre n'a pas pu être mis à jour. Veuillez réessayer."));
        }
        $this->loadModel('NextRenewalDate');
        $years = $this->NextRenewalDate->find('list', ['keyField' => 'year', 'valueField' => 'year'])->toArray();

        $this->set(compact('nextRenewalDate', 'years'));
        $this->set('_serialize', ['nextRenewalDate']);
        $this->viewBuilder()->setLayout('backend');
    }
// member type can be deleted here

    public function deleteMemberType($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $this->loadModel('Membertypes');
        $nextRenewalDate = $this->Membertypes->get($id);
        if ($this->Membertypes->delete($nextRenewalDate)) {
            $this->Flash->success(__("Le type de membre a été supprimé."));
        } else {
            $this->Flash->error(__("Le type de membre n'a pas pu être supprimé. Veuillez réessayer."));
        }

        return $this->redirect('/Registerations/membertypes');
    }
// annual amount of each member type can be deleted here
    public function deleteAnnualAmount($id = null, $r = null) {
        $this->request->allowMethod(['post', 'delete']);
        $this->loadModel('MemberTypeAmounts');
        $nextRenewalDate = $this->MemberTypeAmounts->get($id);
        if ($this->MemberTypeAmounts->delete($nextRenewalDate)) {
            $this->Flash->success(__("Le type de membre a été supprimé."));
        } else {
            $this->Flash->error(__("Le type de membre n'a pas pu être supprimé. Veuillez réessayer."));
        }

        return $this->redirect('/Registerations/annualAmount/' . $r);
    }

    /**
     * Edit method
     *
     * @param string|null $id Next Renewal Date id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    // next renewal date can be editted here
    public function edit($id = null) {
        $nextRenewalDate = $this->NextRenewalDate->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $year = $this->request->data['year'];
            $month = $this->request->data['month'];
            $day = $this->request->data['day'];
            $this->request->data['NextRenewalDate']['year'] = $year;
            $this->request->data['NextRenewalDate']['month'] = $month;
            $this->request->data['NextRenewalDate']['day'] = $day;
            $nextRenewalDate = $this->NextRenewalDate->patchEntity($nextRenewalDate, $this->request->data['NextRenewalDate']);
            if ($this->NextRenewalDate->save($nextRenewalDate)) {
                $this->loadModel('Accounts');
                $accountDates = TableRegistry::get("Accounts");

                $query = $accountDates->query();
                $result = $query->update()
                        ->set(['due_date' => $year . '-' . $month . '-' . $day])
                        ->where(['year(due_date)' => $year])
                        ->execute();
                $this->Flash->error(__('The next renewal date has been saved and updated for members.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The next renewal date could not be saved. Please, try again.'));
        }
        $this->set(compact('nextRenewalDate'));
        $this->set('_serialize', ['nextRenewalDate']);

        $this->viewBuilder()->setLayout('backend');
    }

    /**
     * Delete method
     *
     * @param string|null $id Next Renewal Date id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    // next renewal dates can be deleted here
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $nextRenewalDate = $this->NextRenewalDate->get($id);
        $year = $nextRenewalDate->year;
        $month = $nextRenewalDate->month;
        $day = $nextRenewalDate->day;

        if ($this->NextRenewalDate->delete($nextRenewalDate)) {

            $this->loadModel('Accounts');
            $accountDates = TableRegistry::get("Accounts");

            $query = $accountDates->query();
            $result = $query->update()
                    ->set(['due_date' => $year . '-12-31'])
                    ->where(['year(due_date)' => $year])
                    ->execute();
            $this->Flash->success(__('The next renewal date has been deleted.'));
            return $this->redirect(['action' => 'index']);
        } else {
            $this->Flash->error(__('The next renewal date could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
