<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Datasource\ConnectionManager;

//use Cake\I18n\I18n;
//I18n::setLocale('fr_FR');

/**
 * Registerations Controller
 *
 * @property \App\Model\Table\RegisterationsTable $Registerations
 *
 * @method \App\Model\Entity\Registeration[] paginate($object = null, array $settings = [])
 */
class RegisterationsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['getDetails']);
    }

    public function index() {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 1 || $permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $this->paginate = [
            'contain' => ['Membertypes', 'Users'],
            'conditions' => [],
        ];
        $registerations = $this->paginate($this->Registerations);
        // debug($registerations);exit;


        $this->set(compact('registerations'));
        $this->set('_serialize', ['registerations']);
        $this->viewBuilder()->setLayout('backend');
    }

    public function pending() {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 1 || $permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $this->paginate = [
            'contain' => ['Membertypes', 'Users'],
            'conditions' => ['Registerations.status' => 0]
        ];
        $registerations = $this->paginate($this->Registerations);
        // debug($registerations);exit;

        $this->set(compact('registerations'));
        $this->set('_serialize', ['registerations']);
        $this->viewBuilder()->setLayout('backend');
    }

    public function approved() {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 1 || $permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $this->paginate = [
            'contain' => ['Membertypes', 'Users'],
            'conditions' => ['Registerations.status' => 1, 'Users.role_id' => 6]
        ];
        $registerations = $this->paginate($this->Registerations);
        // debug($registerations);exit;

        $this->set(compact('registerations'));
        $this->set('_serialize', ['registerations']);
        $this->viewBuilder()->setLayout('backend');
    }

    /**
     * View method
     *
     * @param string|null $id Registeration id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $registeration = $this->Registerations->get($id, [
            'contain' => ['Membertypes']
        ]);

        $this->set('registeration', $registeration);
        $this->set('_serialize', ['registeration']);
    }

    public function getDetails() {
        $this->loadModel('Users');
        $user = $this->Users->get($_GET['user_id'], [
            'contain' => [
                'Registerations' => ['Ombudsmans' => ['Ombudsmanlevels' => ['Languages'], 'Ombudsmanformations', 'Ombudsmanaccreds' => ['accreditations'], 'Ombudsmanswears', 'Ombudsmanmeds' => ['Medtypes']], 'Membertypes']]
        ]);


        if ($user) {

            if ($user->registeration->image && file_exists(WWW_ROOT . 'img' . DS . 'slider' . DS . $user->registeration->image)) {
                $p = $this->request->webroot . 'img/slider/' . $user->registeration->image;
            } else {
                $p = $this->request->webroot . 'img/Users/cyber1550500683.png';
            }

            $html = '
<div class="row">
    <div class="col-md-2">
<img src="' . $p . '" style="height: 80px;">
    </div>
    <div class="col-md-10">
        <div>
            <h1 class="margin_0 modal_profile text-capitalize">' . ucwords($user->first_name . ' ' . $user->last_name) . '</h1>';
            if ($user->registeration->profession != '') {
                $html .= '<p class="font_12 margin_0 normal_text_color"><i class="fa fa-building icon_color"></i> ' . $user->registeration->profession . '</p>';
            }
            if ($user->registeration->site_web != '') {
                $html .= '<p class="font_12 margin_0 normal_text_color"><i class="fa fa-globe icon_color"></i>  http://www.astural.ch</p>';
            }
            $html .= '<p class="font_12 margin_0 normal_text_color">';
            if ($user->registeration->email != '' && $user->registeration->emailpublic != 0) {
                $html .= '<i class="fa fa-envelope icon_color"></i>  ' . $user->registeration->email . ' &nbsp;&nbsp;';
            }

            $html .= '<p class="font_12 margin_0 normal_text_color">';
            if ($user->registeration->tel1 != '' && $user->registeration->tel1public != 0) {
                $html .= '<i class="fa fa-phone icon_color"></i>  ' . $user->registeration->tel1 . ' &nbsp;&nbsp;';
            }

            if ($user->registeration->tel2 != '' && $user->registeration->tel2public != 0) {
                $html .= '<i class="fa fa-phone icon_color"></i>  ' . $user->registeration->tel2;
            }
            $html .= '</p>';
            $html .= '<p class="font_12 margin_0 normal_text_color"><i class="fa fa-map-marker icon_color"></i> ' . $user->registeration->address1 . ', ' . $user->registeration->address2 . ', ' . $user->registeration->city . ', ' . $user->registeration->pay . '.</p>
        </div>';


            if ($user->registeration->ombudsmans) {
                $languages = $user->registeration->ombudsmans[0]->ombudsmanlevels;

                if ($languages) {

                    $html .= '<div class="clearfix">&nbsp;</div>
        <div>
            <strong class="custom_labels">Langues</strong><br>';
                    foreach ($languages as $language) {
                        $html .= '<span class="languages"><i class="fa fa-language icon_color"></i> ' . $language->language->name . '</span>';
                    }
                    $html .= '</div>';
                }
            }


            if ($user->registeration->ombudsmans) {
                $formations = $user->registeration->ombudsmans[0]->ombudsmanformations;
                if ($formations) {
                    $html .= '<div class="clearfix">&nbsp;</div>
        <div class="col-md-6 padding_0">
            <strong class="custom_labels">Formations</strong><br>';
                    foreach ($formations as $formation) {
                        $html .= '<span class="languages">' . $formation->formation . ' ( <a class="profile_link" href="">' . date('d.m.Y', strtotime($formation->certificate_date)) . '</a> )</span>';
                    }
                    $html .= '</div>';
                }
            }



            if ($user->registeration->ombudsmans && $user->registeration->ombudsmans[0]->ombudsmanaccreds) {
                $accreds = $user->registeration->ombudsmans[0]->ombudsmanaccreds;
                if ($accreds) {

                    $html .= '<div class="col-md-6 padding_0">
            <strong class="custom_labels">Accréditation</strong><br>';
                    foreach ($accreds as $accred) {
                        $html .= ' <span class="languages"><i class="fa fa-check icon_color"></i> ' . $accred['Accreditations']['name'] . '</span>';
                    }
                    $html .= ' </div>';
                }
            }

            if ($user->registeration->ombudsmans && $user->registeration->ombudsmans[0]->ombudsmanswears) {
                $swears = $user->registeration->ombudsmans[0]->ombudsmanswears;
                if ($swears) {


                    $html .= '<div class="clearfix">&nbsp;</div>
        <div class="col-md-6 padding_0">
            <strong class="custom_labels">Accréditation</strong><br>';
                    foreach ($swears as $swear) {
                        $html .= '<span class="languages"><i class="fa fa-location-arrow icon_color"></i> ' . $swear->place . ' (' . date('d.m.Y', strtotime($swear->date_of_auth)) . ')</span>';
                    }
                    $html .= ' </div>';
                }
            }


            if ($user->registeration->ombudsmans && $user->registeration->ombudsmans[0]->ombudsmanmeds) {
                $mediaTypes = $user->registeration->ombudsmans[0]->ombudsmanmeds;
                if ($mediaTypes) {
                    $html .= '<div class="col-md-6 padding_0">
            <strong class="custom_labels">Types de médiation</strong><br>';
                    foreach ($mediaTypes as $mediaType) {
                        $html .= '<span class="languages"><i class="fa fa-handshake-o icon_color"></i> ' . $mediaType['medtype']['names'] . '</span>';
                    }
                    $html .= ' </div>';
                }
            }
            $html .= ' <div class="clearfix"></div>
        <hr>
        <div class="clearfix"></div>
        <div>
            <p class="description">' . $user->registeration->ombudsmans[0]->presentation_text . '</p>
        </div>
    </div>
    <div class="clearfix"></div>
    <hr>
    <div class="clearfix"></div>
    <div class="col-md-12 text-center">
        <button type="button" data-dismiss="modal" class="btn custom_btn">Ok</button>
    </div>
</div>';


            $responce = array('flag' => 'success', 'html' => $html);
        } else {
            $responce = array('flag' => 'error', 'message' => 'Record not found');
        }
        echo json_encode($responce);
        exit;
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function customerprofile() {
        $this->viewBuilder()->layout('internal');
        $roleid = $this->request->session()->read('Auth.User.role_id');


        if ($roleid == 6) {

            $id = $this->request->session()->read('Auth.User.registeration.id');
            $userid = $this->request->session()->read('Auth.User.id');
            $registeration = $this->Registerations->get($id, [
                'contain' => ['Users']
            ]);
            $this->loadModel('Users');
            $user = $this->Users->get($userid, [
            ]);

            //$registeration = $this->Registerations->newEntity();
            if ($this->request->is(['patch', 'post', 'put'])) {


                if ($this->request->data['image']['name'] != "") {

                    $ext = strrchr($this->request->data['image']['name'], '.');
                    $slider_image = $this->request->data['city'] . time() . $ext;
                    $source = $this->request->data['image']['tmp_name'];
                    $destination = WWW_ROOT . 'img' . DS . 'slider' . DS . $slider_image;
                    $thm_destination = WWW_ROOT . 'img' . DS . 'slider' . DS . 'thumbnail' . DS . $slider_image;
                    $this->request->data['image'] = $slider_image;
                } else {
                    unset($this->request->data['image']);
                }
                if (isset($this->request->data['tel1public']) == 'on') {
                    $tel1 = 1;
                } else {
                    $tel1 = 0;
                }
                if (isset($this->request->data['tel2public']) == 'on') {
                    $tel2 = 1;
                } else {
                    $tel2 = 0;
                }

                if (isset($this->request->data['emailpublic']) == 'on') {
                    $email = 1;
                } else {
                    $email = 0;
                }

                if (isset($this->request->data['publicprofile']) && $this->request->data['publicprofile'] == 'public') {
                    $profilepublic = 1;

                    //debug(1);
                } else {
                    $profilepublic = 0;
                    //debug(0);
                }

                $this->request->data['tel1public'] = $tel1;
                $this->request->data['tel2public'] = $tel2;
                $this->request->data['emailpublic'] = $email;
                $this->request->data['publicprofile'] = $profilepublic;

                $registeration = $this->Registerations->patchEntity($registeration, $this->request->getData(), [
                    'validate' => 'RegCheck'
                ]);

                $this->request->data['User']['first_name'] = $this->request->data['first_name'];
                $this->request->data['User']['last_name'] = $this->request->data['lastname'];
                //$this->request->data['User']['role_id'] = $this->request->data['membertype_id'];
                $this->request->data['User']['password'] = $this->request->data['pwd'];
                $this->request->data['User']['status'] = 1;
                if ($this->request->data['pwd'] == "") {
                    unset($this->request->data['User']['password']);
                }
                unset($this->request->data['User']['email']);
                $user = $this->Users->patchEntity($user, $this->request->data['User'], [
                ]);
                $this->Users->save($user);

                if ($this->Registerations->save($registeration)) {
                    if (!empty($this->request->data['image'])) {
                        $this->generate_thumb($source, $thm_destination, 400, 300);
                        $this->generate_thumb($source, $destination, 1200, 497);
                    }

                    $superAdmins = $this->Users->find('all', [
                                'conditions' => ['Users.role_id' => 1, 'Users.status' => 1]
                            ])->toArray();

                    //                    foreach($superAdmins as $admin){
                    //                        $email = $admin->email;
                    //                        $Email = new Email();
                    //                        $Email->from(array('info@cyberclouds.com' => 'FGEM'));
                    //                        $Email->emailFormat('both');
                    //                        $Email->to($email);
                    //                        $Email->subject('Member Profile | FGEM');
                    //
                    //                        
                    //                        $reset_token_link_admin = Router::url(['controller' => 'Users', 'action' => 'login'], TRUE);
                    //                        $message = 'Bonjour Admin,<br />Le membre obtenir le nom du membre ici a terminé le profil avec succès. S\'il vous plaît cliquez ici pour activer son compte.<br/><a href='.$reset_token_link_admin.'> '.$reset_token_link_admin.'</a><br/>Merci <br/>FGEM';
                    //
                    //                        $Email->send($message);
                    //
                    //                    }
                    //$this->Flash->success(__('Les informations de profil ont été enregistrées.'));

                    return $this->redirect(['controller' => 'Registerations', 'action' => 'business']);
                }
                //$this->Flash->error(__('The profile could not be saved. Please, try again.'));
            }
            $membertypes = $this->Registerations->Membertypes->find('list', ['limit' => 200]);
            $this->set(compact('registeration', 'membertypes'));
            $this->set('_serialize', ['registeration']);
            //   $this->viewBuilder()->setLayout('');
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);
        }
    }

    public function registerationform() {
        $this->viewBuilder()->layout('internal');

        $this->loadModel('Users');
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {

            $this->request->data['User']['first_name'] = $this->request->data['first_name'];
            $this->request->data['User']['last_name'] = $this->request->data['lastname'];
            //$this->request->data['User']['user_name'] = $this->request->data['user_name'];
            $this->request->data['User']['email'] = $this->request->data['email'];
            $this->request->data['User']['password'] = $this->request->data['password'];
            $this->request->data['User']['role_id'] = 6;
            $rand1 = str_pad(rand(1, 9999999), 5, STR_PAD_LEFT);
            $this->request->data['User']['status'] = 0;
            $this->request->data['User']['verified'] = $rand1;

            //debug($registeration);exit;
            $user = $this->Users->patchEntity($user, $this->request->data['User'], [
                'validate' => 'UserCheck'
            ]);
            //            debug($user);exit;


            if ($result = $this->Users->save($user)) {
                $userId = $result->id;
                $registerations = $this->Registerations->newEntity();
                $this->request->data['Registerations']['user_id'] = $userId;
                $this->request->data['Registerations']['membertype_id'] = $this->request->data['membertype_id'];

                $this->request->data['Registerations']['status'] = 0;


                $registerations = $this->Registerations->patchEntity($registerations, $this->request->data['Registerations'], [
                    'validate' => 'RegremoveCheck'
                ]);



                if ($result2 = $this->Registerations->save($registerations)) {
                    $reId = $result2->id;

                    $this->loadModel('Ombudsmans');
                    $ombudsman = $this->Ombudsmans->newEntity();
                    $this->request->data['Ombudsmans']['registeration_id'] = $reId;
                    $ombudsman = $this->Ombudsmans->patchEntity($ombudsman, $this->request->data['Ombudsmans']);

                    $this->Ombudsmans->save($ombudsman);

                    $number = count($this->request->data['Registerations']);

                    // $passkey = uniqid();
                    // $url = Router::Url(['controller' => 'users', 'action' => 'reset'], true) . '/' . $passkey;


                    $reset_token_link = Router::url(['controller' => 'Users', 'action' => 'verified'], TRUE) . '/' . $rand1;
                    //$emaildata = [$user->email, $reset_token_link];
                    $Email = new Email();
                    $Email->from(array('info@cyberclouds.com' => 'FGEM'));
                    $Email->emailFormat('both');
                    $Email->to($this->request->data['email']);
                    $Email->subject('Verification | FGEM');
                    $user = $this->request->data['first_name'] . " " . $this->request->data['lastname'];
                    //$message = "Bonjour " . $user . ",<br />Merci pour votre inscription à FGEM. S'il vous plaît <a href='$reset_token_link'> cliquez ici</a> pour vérifier votre compte.<br/><br/>Merci <br/>FGEM";
                    $message = "Bonjour " . $user . ",<br />Merci d'être membre. Veuillez vérifier votre courrier électronique en cliquant sur le lien ci-dessous et remplir la page de profil pour terminer votre processus d'inscription.<br/><a href='$reset_token_link'> $reset_token_link</a><br/>Merci <br/>FGEM";
                    echo $message;
                    exit;
                    // $message .= "<br />Verification Link";
                    //$Email->send($message);


                    $superAdmins = $this->Users->find('all', [
                                'conditions' => ['Users.role_id' => 1, 'Users.status' => 1]
                            ])->toArray();

                    foreach ($superAdmins as $admin) {
                        $email = $admin->email;
                        $Email = new Email();
                        $Email->from(array('info@cyberclouds.com' => 'FGEM'));
                        $Email->emailFormat('both');
                        $Email->to($email);
                        $Email->subject('Member Registeration | FGEM');


                        $reset_token_link_admin = Router::url(['controller' => 'Users', 'action' => 'login'], TRUE);
                        $message = "Bonjour Admin,<br />Un nouveau membre a été enregistré. S'il vous plaît cliquez ici pour voir les détails.<br/><a href='$reset_token_link_admin'> $reset_token_link_admin</a><br/>Merci <br/>FGEM";

                        // $Email->send($message);
                    }

                    $this->Flash->success("Une confirmation a été envoyée à votre adresse email. Merci de bien vouloir suivre les indications se trouvant dans le
email pour valider la création de votre compte.");


                    return $this->redirect(['controller' => 'Users', 'action' => 'customerLogin']);
                }
            }
            $this->Flash->error(__("L'enregistrement n'a pas pu être effectué. L'adresse email existe déjà."));
        }
        $membertypes = $this->Registerations->Membertypes->find('all')->toArray();
        $this->set(compact('registeration', 'user', 'membertypes'));
        $this->set('_serialize', ['registeration']);
        $this->viewBuilder()->layout('internal');
    }

    public function business() {
        $this->viewBuilder()->layout('internal');
        $id = $this->request->session()->read('Auth.User.registeration.id');

        $userId = $this->request->session()->read('Auth.User.id');
        $ombId = $this->request->session()->read('Auth.User.registeration');
        //debug($ombId['ombudsmans'][0]['id']);exit;
        $ombId = $ombId['ombudsmans'][0]['id'];
        $this->loadModel('Users');
        $user = $this->Users->get($userId);
        $this->loadModel('Registerations');
        $registrations = $this->Registerations->find('all')
                ->contain([
                    'Ombudsmans' => [
                        'Ombudsmanswears',
                        'Ombudsmanaccreds',
                        'Ombudsmanformations',
                        'Ombudsmanlevels',
                        'Ombudsmanmeds'
            ]])
                ->where(['user_id' => $user->id])
                ->first()
                ->toArray();

        $this->loadModel('Ombudsmans');
        $ombudsman = $this->Ombudsmans->get($ombId);

        if ($this->request->is(['patch', 'post', 'put'])) {
//            debug($this->request->data);
            //            exit;
            // ombudsman entry
            $this->loadModel('Ombudsmans');
            $this->request->data['Ombudsmans']['registeration_id'] = $id;
            $this->request->data['Ombudsmans']['mediator_question'] = $this->request->data['mediator_question'];
            $this->request->data['Ombudsmans']['presentation_text'] = $this->request->data['presentation_text'];
            $this->request->data['Ombudsmans']['assermentation'] = $this->request->data['assermentation'];
            
            $ombudsman = $this->Ombudsmans->patchEntity($ombudsman, $this->request->data['Ombudsmans']);

            if ($this->Ombudsmans->save($ombudsman)) {

                //save mediations
                $this->loadModel('Ombudsmanmeds');
                $saveOmbudsmanmeds = array();
                foreach ($this->request->data['ombudsmanmeds']['mediat'] as $k => $om)://change
                    $saveOmbudsmanmeds[$k]['ombudsman_id'] = $ombudsman->id;
                    $saveOmbudsmanmeds[$k]['medtype_id'] = $om;
                endforeach;
                $ombudsmanmeds = $this->Ombudsmanmeds->newEntities($saveOmbudsmanmeds);
                $this->Ombudsmanmeds->saveMany($ombudsmanmeds);


                //save languages
                $this->loadModel('Ombudsmanlevels');
                $saveLanguage = array();
                foreach ($this->request->data['ombudsmanlevels_mediation'] as $n => $o)://change
                    $saveLanguage[$n]['ombudsman_id'] = $ombudsman->id;
                    $saveLanguage[$n]['language_id'] = $o;
                endforeach;
                $saveLanguage = $this->Ombudsmanlevels->newEntities($saveLanguage);
                $this->Ombudsmanlevels->saveMany($saveLanguage);


                //save ombudsmanswears
                $this->loadModel('Ombudsmanswears');
                $saveOmbudsmanswears = array();
                //                debug($this->request->data['ombudsmanswears']['place']);
                foreach ($this->request->data['place'] as $p => $q)://change
                    $saveOmbudsmanswears[$p]['ombudsman_id'] = $ombudsman->id;
                    $saveOmbudsmanswears[$p]['place'] = $q;
                    $saveOmbudsmanswears[$p]['date_of_auth'] = date('Y-m-d', strtotime($this->request->data['date_of_auth'][$p]));
                endforeach;
                $saveOmbudsmanswears = $this->Ombudsmanswears->newEntities($saveOmbudsmanswears);

                $this->Ombudsmanswears->saveMany($saveOmbudsmanswears);

                //save ombudsmanaccreds
                $this->loadModel('Ombudsmanaccreds');
                $saveOmbudsmanaccreds = array();
                //                debug($this->request->data['ombudsmanswears']['place']);
                foreach ($this->request->data['accre'] as $t => $u)://change
                    $saveOmbudsmanaccreds[$t]['ombudsman_id'] = $ombudsman->id;
                    $saveOmbudsmanaccreds[$t]['accreditation_id'] = $u;
                endforeach;
                $saveOmbudsmanaccreds = $this->Ombudsmanaccreds->newEntities($saveOmbudsmanaccreds);
                //                debug($saveOmbudsmanaccreds);
                $this->Ombudsmanaccreds->saveMany($saveOmbudsmanaccreds);

                //save ombudsmanformations
                $this->loadModel('Ombudsmanformations');
                $saveOmbudsmanformations = array();
                //                debug($this->request->data['ombudsmanswears']['place']);
                foreach ($this->request->data['image'] as $r => $s):
                    $get_ext = pathinfo($s['name'], PATHINFO_EXTENSION);
                    $new_name = $r . '-' . date('ymdhis') . '.' . $get_ext;
                    $path = WWW_ROOT . 'img' . DS . 'formations' . DS . $new_name;
                    move_uploaded_file($s['tmp_name'], $path);
                    $saveOmbudsmanformations[$r]['ombudsman_id'] = $ombudsman->id;
                    $saveOmbudsmanformations[$r]['formation'] = $this->request->data['ombudsmanformations_formation'][$r];
                    $saveOmbudsmanformations[$r]['certificate_date'] = date('Y-m-d', strtotime($this->request->data['ombudsmanformations_date'][$r]));
                    $saveOmbudsmanformations[$r]['copy_upload'] = $new_name;
                endforeach;

                $saveOmbudsmanformations = $this->Ombudsmanformations->newEntities($saveOmbudsmanformations);

                $this->Ombudsmanformations->saveMany($saveOmbudsmanformations);
            }


            //debug($number);exit;
            $this->request->data['User']['status'] = 2;
            $this->request->data['User']['role_id'] = $this->request->session()->read('Auth.User.role_id');
            $user = $this->Users->patchEntity($user, $this->request->data['User'], [
                'validate' => 'UserCheck'
            ]);

            if ($result = $this->Users->save($user)) {
                $this->Flash->success("Les informations ont été enregistrées.");
                return $this->redirect(['controller' => 'Registerations', 'action' => 'customerprofile']);
            }
            $this->Flash->error(__("Les informations sur la société n'ont pas pu être enregistrées. Veuillez réessayer."));
        }

        $this->loadModel('Medtypes');
        $medtype = $this->Medtypes->find('all')->toArray();

        $this->loadModel('Accreditations');
        $acc = $this->Accreditations->find('all')->toArray();
        //debug($medtype);exit;
        $this->loadModel('Languages');
        $languages = $this->Languages->find('list')->toArray();
        //        debug($languages);exit;
        $this->set('user', $user);
//        debug($registrations);exit;
        $this->set(compact('languages', 'registrations'));
        $this->set('acc', $acc);
        $this->set('medtype', $medtype);
        $this->viewBuilder()->layout('internal');
    }

    public function changeStatus($id = null, $status = null) {
        $this->loadModel('Accounts');
        $user = $this->Accounts->find('all', array(
                    'contain' => 'Registerations',
                    'conditions' => ['Registerations.id' => $id],
                ))->first();
        $this->loadModel('Registerations');
        $reg = $this->Registerations->find('all', array(
                    'conditions' => ['id' => $id],
                ))->first();
        //debug($reg['status']);exit;


        if (empty($user) && $reg['status'] != 1) {
            $this->Flash->error('Please enter the account information first');
            return $this->redirect(['controller' => 'Accounts', 'action' => 'index', $id]);
        }

        $regs = TableRegistry::get('Registerations');
        $reg = $regs->get($id);
        $reg->status = $status;
        if ($regs->save($reg)) {

            if ($reg->status == 1) {
                //                $email = new Email();
                //                $email->template('default', 'default')
                //                        ->emailFormat('html')
                //                        ->to($user->email)
                //                        ->from(['info@cyberclouds.com' => 'welvett.com'])
                //                        ->subject('Welvet Account Verified')
                //                        ->viewVars(['content' => $user])
                //                        ->send();
            }
            $this->Flash->success('status changed');
        } else {
            $this->Flash->error('status not changed');
        }
        //        if ($reg->role_id == 3) {
        //            return $this->redirect('/Users/corporate');
        //        } else {
        return $this->redirect(['controller' => 'Accounts', 'action' => 'index', $id]);
        //return $this->redirect('/Registerations/index');
        //}
    }

    /**
     * Edit method
     *
     * @param string|null $id Registeration id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $registeration = $this->Registerations->get($id, [
            'contain' => ['Users']
        ]);
        //debug($registeration['user_id']);exit;
        $userid = $registeration['user_id'];
        $this->loadModel('Users');
        $user = $this->Users->get($userid, [
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            //echo $this->request->data['image']['name'];exit;
            if ($this->request->data['image']['name'] != "") {
                $ext = strrchr($this->request->data['image']['name'], '.');
                $slider_image = $this->request->data['city'] . time() . $ext;
                $source = $this->request->data['image']['tmp_name'];
                $destination = WWW_ROOT . 'img' . DS . 'slider' . DS . $slider_image;
                $thm_destination = WWW_ROOT . 'img' . DS . 'slider' . DS . 'thumbnail' . DS . $slider_image;
                $this->request->data['image'] = $slider_image;
            } else {
                unset($this->request->data['image']);
            }
            if ($this->request->data['password'] == "") {
                unset($this->request->data['password']);
            } else {
                $this->request->data['User']['password'] = $this->request->data['password'];
            }
            $this->request->data['User']['first_name'] = $this->request->data['firstname'];
            $this->request->data['User']['last_name'] = $this->request->data['lastname'];
            $this->request->data['User']['email'] = $this->request->data['email'];

            $user = $this->Users->patchEntity($user, $this->request->data['User'], [
            ]);
            $this->Users->save($user);

            $registeration = $this->Registerations->patchEntity($registeration, $this->request->getData());

            if ($this->Registerations->save($registeration)) {
                //echo $slider_image;exit;
                if ($slider_image != "") {
                    $this->generate_thumb($source, $thm_destination, 400, 300);
                    $this->generate_thumb($source, $destination, 1200, 497);
                }
                $this->Flash->success(__('The registeration has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The registeration could not be saved. Please, try again.'));
        }
        $membertypes = $this->Registerations->Membertypes->find('list', ['limit' => 200]);
        $this->set(compact('registeration', 'membertypes', 'user'));
        $this->set('_serialize', ['registeration']);
        $this->viewBuilder()->setLayout('backend');
    }

    /**
     * Delete method
     *
     * @param string|null $id Registeration id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $this->request->allowMethod(['post', 'delete']);
        $registeration = $this->Registerations->get($id);
        if ($this->Registerations->delete($registeration)) {
            $this->Flash->success(__('The registeration has been deleted.'));
        } else {
            $this->Flash->error(__('The registeration could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function viewMembers() {


        $members = $this->Registerations->find('all', [
                    'contain' => ['Users' => ['conditions' => ['Users.status' => 2]], 'Ombudsmans' => ['conditions' => ['Ombudsmans.mediator_question' => 'Oui']]],
                    'conditions' => ['Registerations.status' => 1, 'Registerations.publicprofile' => 1]
                ])->toArray();


        $this->set(compact('members'));

        $this->viewBuilder()->layout('internal');
    }

}
