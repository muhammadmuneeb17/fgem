<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Accreditations Controller
 *
 * @property \App\Model\Table\AccreditationsTable $Accreditations
 *
 * @method \App\Model\Entity\Accreditation[] paginate($object = null, array $settings = [])
 */
class AccreditationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $accreditations = $this->paginate($this->Accreditations);

        $this->set(compact('accreditations'));
        $this->set('_serialize', ['accreditations']);
    }

    /**
     * View method
     *
     * @param string|null $id Accreditation id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $accreditation = $this->Accreditations->get($id, [
            'contain' => ['Ombudsmanaccreds']
        ]);

        $this->set('accreditation', $accreditation);
        $this->set('_serialize', ['accreditation']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $accreditation = $this->Accreditations->newEntity();
        if ($this->request->is('post')) {
            $accreditation = $this->Accreditations->patchEntity($accreditation, $this->request->getData());
            if ($this->Accreditations->save($accreditation)) {
                $this->Flash->success(__('The accreditation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The accreditation could not be saved. Please, try again.'));
        }
        $this->set(compact('accreditation'));
        $this->set('_serialize', ['accreditation']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Accreditation id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $accreditation = $this->Accreditations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $accreditation = $this->Accreditations->patchEntity($accreditation, $this->request->getData());
            if ($this->Accreditations->save($accreditation)) {
                $this->Flash->success(__('The accreditation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The accreditation could not be saved. Please, try again.'));
        }
        $this->set(compact('accreditation'));
        $this->set('_serialize', ['accreditation']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Accreditation id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $accreditation = $this->Accreditations->get($id);
        if ($this->Accreditations->delete($accreditation)) {
            $this->Flash->success(__('The accreditation has been deleted.'));
        } else {
            $this->Flash->error(__('The accreditation could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
