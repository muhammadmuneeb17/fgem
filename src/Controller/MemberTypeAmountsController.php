<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MemberTypeAmounts Controller
 *
 * @property \App\Model\Table\MemberTypeAmountsTable $MemberTypeAmounts
 *
 * @method \App\Model\Entity\MemberTypeAmount[] paginate($object = null, array $settings = [])
 */
class MemberTypeAmountsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Membertypes']
        ];
        $memberTypeAmounts = $this->paginate($this->MemberTypeAmounts);

        $this->set(compact('memberTypeAmounts'));
        $this->set('_serialize', ['memberTypeAmounts']);
    }

    /**
     * View method
     *
     * @param string|null $id Member Type Amount id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $memberTypeAmount = $this->MemberTypeAmounts->get($id, [
            'contain' => ['Membertypes']
        ]);

        $this->set('memberTypeAmount', $memberTypeAmount);
        $this->set('_serialize', ['memberTypeAmount']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $memberTypeAmount = $this->MemberTypeAmounts->newEntity();
        if ($this->request->is('post')) {
            $memberTypeAmount = $this->MemberTypeAmounts->patchEntity($memberTypeAmount, $this->request->getData());
            if ($this->MemberTypeAmounts->save($memberTypeAmount)) {
                $this->Flash->success(__('The member type amount has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The member type amount could not be saved. Please, try again.'));
        }
        $membertypes = $this->MemberTypeAmounts->Membertypes->find('list', ['limit' => 200]);
        $this->set(compact('memberTypeAmount', 'membertypes'));
        $this->set('_serialize', ['memberTypeAmount']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Member Type Amount id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $memberTypeAmount = $this->MemberTypeAmounts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $memberTypeAmount = $this->MemberTypeAmounts->patchEntity($memberTypeAmount, $this->request->getData());
            if ($this->MemberTypeAmounts->save($memberTypeAmount)) {
                $this->Flash->success(__('The member type amount has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The member type amount could not be saved. Please, try again.'));
        }
        $membertypes = $this->MemberTypeAmounts->Membertypes->find('list', ['limit' => 200]);
        $this->set(compact('memberTypeAmount', 'membertypes'));
        $this->set('_serialize', ['memberTypeAmount']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Member Type Amount id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $memberTypeAmount = $this->MemberTypeAmounts->get($id);
        if ($this->MemberTypeAmounts->delete($memberTypeAmount)) {
            $this->Flash->success(__('The member type amount has been deleted.'));
        } else {
            $this->Flash->error(__('The member type amount could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
