<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Ombudsmanswears Controller
 *
 * @property \App\Model\Table\OmbudsmanswearsTable $Ombudsmanswears
 *
 * @method \App\Model\Entity\Ombudsmanswear[] paginate($object = null, array $settings = [])
 */
class OmbudsmanswearsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $ombudsmanswears = $this->paginate($this->Ombudsmanswears);

        $this->set(compact('ombudsmanswears'));
        $this->set('_serialize', ['ombudsmanswears']);
    }

    /**
     * View method
     *
     * @param string|null $id Ombudsmanswear id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ombudsmanswear = $this->Ombudsmanswears->get($id, [
            'contain' => []
        ]);

        $this->set('ombudsmanswear', $ombudsmanswear);
        $this->set('_serialize', ['ombudsmanswear']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ombudsmanswear = $this->Ombudsmanswears->newEntity();
        if ($this->request->is('post')) {
            $ombudsmanswear = $this->Ombudsmanswears->patchEntity($ombudsmanswear, $this->request->getData());
            if ($this->Ombudsmanswears->save($ombudsmanswear)) {
                $this->Flash->success(__('The ombudsmanswear has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ombudsmanswear could not be saved. Please, try again.'));
        }
        $this->set(compact('ombudsmanswear'));
        $this->set('_serialize', ['ombudsmanswear']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ombudsmanswear id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ombudsmanswear = $this->Ombudsmanswears->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ombudsmanswear = $this->Ombudsmanswears->patchEntity($ombudsmanswear, $this->request->getData());
            if ($this->Ombudsmanswears->save($ombudsmanswear)) {
                $this->Flash->success(__('The ombudsmanswear has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ombudsmanswear could not be saved. Please, try again.'));
        }
        $this->set(compact('ombudsmanswear'));
        $this->set('_serialize', ['ombudsmanswear']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ombudsmanswear id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ombudsmanswear = $this->Ombudsmanswears->get($id);
        if ($this->Ombudsmanswears->delete($ombudsmanswear)) {
            $this->Flash->success(__('The ombudsmanswear has been deleted.'));
        } else {
            $this->Flash->error(__('The ombudsmanswear could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
