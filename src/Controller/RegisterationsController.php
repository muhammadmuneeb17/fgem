<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Datasource\ConnectionManager;

//use Cake\I18n\I18n;
//I18n::setLocale('fr_FR');

/**
 * Registerations Controller
 *
 * @property \App\Model\Table\RegisterationsTable $Registerations
 *
 * @method \App\Model\Entity\Registeration[] paginate($object = null, array $settings = [])
 */
class RegisterationsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['getDetails', 'customerRegistration']);
    }

// all members registration listings
    public function index() {

        $permission = $this->viewVars['actionPermission'];

        if ($permission == 1 || $permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $this->paginate = [
            'contain' => ['Membertypes', 'Users', 'Users.MemberRoles'],
            'conditions' => [],
        ];
        $registerations = $this->paginate($this->Registerations);
//         debug($registerations);exit;


        $this->set(compact('registerations'));
        $this->set('_serialize', ['registerations']);
        $this->viewBuilder()->setLayout('backend');
    }

    public function supervisor($id = null, $action = null) {

        $permission = $this->viewVars['actionPermission'];

        if ($permission == 1 || $permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->loadModel('MemberRoles');

        if ($action == 1) {
            $getRole = $this->MemberRoles->newEntity();
            $getRole->user_id = $id;
            $getRole->role_id = 11;
            if ($this->MemberRoles->save($getRole)) {
                $this->Flash->success("Le rôle a été attribué à l'utilisateur");
            }
        } else {
            $getRole = $this->MemberRoles->find('all')
                    ->where(['user_id' => $id, 'role_id' => 11])
                    ->first();
            if ($this->MemberRoles->delete($getRole)) {
                $this->Flash->success("Le rôle a été supprimé de l'utilisateur");
            }
        }
        return $this->redirect('/Registerations');
    }

    public function membersOwed() {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 1 || $permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));

        $this->loadModel('Accounts');


        /* $this->paginate = ['limit' => 10,
          'contain' => ['Registerations'=>['Membertypes','Users' => ['conditions'=>['Users.status ' => 2]]]],
          'conditions' => ['Accounts.amount_due > '=> '0'],
          'group' => ['Accounts.registeration_id'],
          'order' => ['Accounts.id' => 'DESC']
          ];
          $member_owed = $this->paginate($this->Accounts); */

        $conn = ConnectionManager::get('default');

        $query = $conn->execute('SELECT ac.id as accid, ac.amount_due , r.* , m.* , u.* , r.id as rid'
                . ' FROM accounts ac '
                . 'inner join registerations r ON ac.registeration_id=r.id '
                . 'inner join membertypes m ON m.id = r.membertype_id '
                . 'inner join users u ON u.id = r.user_id '
                . ' where ac.id IN (
                                            SELECT MAX(accounts.id)
                                            FROM accounts
                                            GROUP BY accounts.registeration_id
                                        ) and  ac.amount_due > 0 and u.status = 2');

        $member_owed = $query->fetchAll('assoc');



        //debug($member_owed);exit;
        // debug($registerations);exit;

        $this->set(compact('member_owed'));
        $this->set('_serialize', ['member_owed']);
        $this->viewBuilder()->setLayout('backend');
    }

    public function suspendedMembers() {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 1 || $permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));

        $this->loadModel('Accounts');


        $this->paginate = ['limit' => 10,
            'contain' => ['Registerations' => ['Membertypes', 'Users' => ['conditions' => ['Users.status ' => 2]]]],
            'conditions' => ['Accounts.amount_paid > ' => '0', 'Registerations.status' => '0'],
            'group' => ['Accounts.registeration_id']
        ];
        $member_owed = $this->paginate($this->Accounts);
        // debug($registerations);exit;

        $this->set(compact('member_owed'));
        $this->set('_serialize', ['member_owed']);
        $this->viewBuilder()->setLayout('backend');
    }

    public function pending() {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 1 || $permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $this->paginate = [
            'contain' => ['Membertypes', 'Users' => ['conditions' => ['Users.status' => 2,'Users.suspend' => 0]]],
            'conditions' => ['Registerations.status' => 0]
        ];
        $registerations = $this->paginate($this->Registerations);
        // debug($registerations);exit;

        $this->set(compact('registerations'));
        $this->set('_serialize', ['registerations']);
        $this->viewBuilder()->setLayout('backend');
    }

    public function approved() {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 1 || $permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $this->paginate = [
            'contain' => ['Membertypes', 'Users'],
            'conditions' => ['Registerations.status' => 1, 'Users.role_id' => 6,'Users.suspend' => 0]
        ];
        $registerations = $this->paginate($this->Registerations);
        // debug($registerations);exit;

        $this->set(compact('registerations'));
        $this->set('_serialize', ['registerations']);
        $this->viewBuilder()->setLayout('backend');
    }

    public function suspend($user_id = NULL) {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->request->allowMethod(['post', 'delete']);

        $tablename = TableRegistry::get("Users");
        $query = $tablename->query();
        
        $result = $query->update()
                    ->set(['suspend' => '1'])
                    ->where(['id' => $user_id])
                    ->execute();

        $tablename2 = TableRegistry::get("Registerations");
        $query2 = $tablename2->query();
        
        $result2 = $query2->update()
                    ->set(['status' => '0'])
                    ->where(['user_id' => $user_id])
                    ->execute();
         if($result && $result2) {
            $this->Flash->success(__('Compte suspendu avec succès'));
         }   else {
            $this->Flash->error(__('Une erreur est survenue, veuillez réessayer plus tard'));
         }        

        return $this->redirect(['action' => 'index']);


        $this->viewBuilder()->setLayout('');
    }

    public function activate($user_id = NULL) {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->request->allowMethod(['post', 'delete']);

        $tablename = TableRegistry::get("Users");
        $query = $tablename->query();
        
        $result = $query->update()
                    ->set(['suspend' => '0'])
                    ->where(['id' => $user_id])
                    ->execute();

        $tablename2 = TableRegistry::get("Registerations");
        $query2 = $tablename2->query();
        
        $result2 = $query2->update()
                    ->set(['status' => '1'])
                    ->where(['user_id' => $user_id])
                    ->execute();
         if($result && $result2) {
            $this->Flash->success(__('Le compte a été activé'));
         }   else {
            $this->Flash->error(__('Une erreur est survenue, veuillez réessayer plus tard'));
         }        

        return $this->redirect(['action' => 'index']);


        $this->viewBuilder()->setLayout('');
    }


    /**
     * View method
     *
     * @param string|null $id Registeration id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $registeration = $this->Registerations->get($id, [
            'contain' => ['Membertypes']
        ]);

        $this->set('registeration', $registeration);
        $this->set('_serialize', ['registeration']);
    }

    public function languagaeExperience() {
        $values = explode(',', $_GET['values']);
        //        debug($values);
        $this->loadModel('Languages');
        $languages = $this->Languages->find('list')->toArray();

        $ombId = $this->request->session()->read('Auth.User.registeration');
        $ombId = $ombId['ombudsman']['id'];


        $this->loadModel('Ombudsmanlevels');
        $myLang = $this->Ombudsmanlevels->find('list', ['keyField' => 'language_id', 'valueField' => 'language_level'])
                        ->where(['ombudsman_id' => $ombId])->toArray();

        $html = "";
        foreach ($values as $v):
            $checkedValue = '';
            if (array_key_exists($v, $myLang)) {
                $checkedValue = $myLang[$v];
            }

            $html .= '<div class="form-group"><div class="col-xs-12 col-md-4" style="padding:0 50px;">'
                    . '<label><h4>Niveau ' . $languages[$v] . '<small class="text-danger">*</small></h4></label>'
                    . ' <select name="ombudsmanlevels.language_level[]" class="form-control">'
                    . '  <option value="basic" ';
            if ($checkedValue == "basic") {
                $html .= 'selected';
            }
            $html .= '>Basique</option>'
                    . ' <option value="conversational" ';
            if ($checkedValue == "conversational") {
                $html .= 'selected';
            }
            $html .= '>Moyen</option>'
                    . '<option value="fluent" ';
            if ($checkedValue == "fluent") {
                $html .= 'selected';
            }
            $html .= '>Courant</option>'
                    . '</select>'
                    . '</div></div>';
        endforeach;
        echo json_encode($html);

        exit;
    }

    public function getDetails() {
        $this->loadModel('Users');
        $user = $this->Users->get($_GET['user_id'], [
            'contain' => [
                'Registerations' => ['Ombudsmans' => ['Ombudsmanlevels' => ['Languages'], 'Ombudsmanformations', 'Ombudsmanaccreds' => ['accreditations'], 'Ombudsmanswears', 'Ombudsmanmeds' => ['Medtypes']], 'Membertypes']]
        ]);


        if ($user) {

            if ($user->registeration->image && file_exists(WWW_ROOT . 'img' . DS . 'slider' . DS . $user->registeration->image)) {
                $p = $this->request->webroot . 'img/slider/' . $user->registeration->image;
            } else {
                $p = $this->request->webroot . 'img/Users/cyber1550500683.png';
            }

            $html = '
<div class="row">

    <div class="col-md-12">
            <img src="' . $p . '" style="height: 120px;" class="pull-left">
        <div class="pull-left" style="margin-left: 10px;">
            <h1 class="margin_0 modal_profile " style="font-variant: small-caps;font-size: 24px;">' . ucwords($user->first_name . ' ' . $user->last_name) . '</h1><div class="clearfix"></div>';
            if ($user->registeration->profession != '') {
                $html .= '<p class="font_12 margin_0 normal_text_color"><i class="fa fa-building icon_color"></i> ' . $user->registeration->profession . '</p>';
            }
            if ($user->registeration->site_web != '') {
                $html .= '<p class="font_12 margin_0 normal_text_color"><i class="fa fa-globe icon_color"></i>  http://www.astural.ch</p>';
            }
            $html .= '<p class="font_12 margin_0 normal_text_color">';
            if ($user->registeration->email != '' && $user->registeration->emailpublic != 0) {
                $html .= '<i class="fa fa-envelope icon_color"></i>  ' . $user->registeration->email . ' &nbsp;&nbsp;';
            }

            $html .= '<p class="font_12 margin_0 normal_text_color">';
            if ($user->registeration->tel1 != '' && $user->registeration->tel1public != 0) {
                $html .= '<i class="fa fa-phone icon_color"></i>  ' . $user->registeration->tel1 . ' &nbsp;&nbsp;';
            }

            if ($user->registeration->tel2 != '' && $user->registeration->tel2public != 0) {
                $html .= '<i class="fa fa-phone icon_color"></i>  ' . $user->registeration->tel2;
            }
            $html .= '</p>';
            if ($user->registeration->address2) {
                $html .= '<p class="font_12 margin_0 normal_text_color"><i class="fa fa-map-marker icon_color"></i> ' . $user->registeration->address1 . ', ' . $user->registeration->address2 . ', ' . $user->registeration->city . ', ' . $user->registeration->pay . '.</p>
            </div>';
            } else {
                $html .= '<p class="font_12 margin_0 normal_text_color"><i class="fa fa-map-marker icon_color"></i> ' . $user->registeration->address1 . ', ' . $user->registeration->city . ', ' . $user->registeration->pay . '.</p>
            </div>';
            }

            if ($user->registeration->ombudsman) {
                $languages = $user->registeration->ombudsman->ombudsmanlevels;

                if ($languages) {

                    $html .= '<div class="clearfix">&nbsp;</div>
        <div class="col-lg-12 " style="margin-bottom:10px">
            <strong class="custom_labels">Langues</strong>';
                    foreach ($languages as $language) {
                        $html .= '<span class="languages"><i class="fa fa-language icon_color"></i> ' . $language->language->name . '</span>';
                    }
                    $html .= '</div>';
                }
            }


            if ($user->registeration->ombudsman) {
                $formations = $user->registeration->ombudsman->ombudsmanformations;
                if ($formations) {
                    $html .= '
        <div class="col-md-6 " style="margin-bottom:10px">
            <strong class="custom_labels">Formations</strong>';
                    foreach ($formations as $formation) {
                        if ($formation->copy_upload != "") {

                            $html .= '<span class="languages">' . $formation->formation . ' (<a class="profile_link show_image" id="' . $formation->id . '">' . date('d.m.y', strtotime($formation->certificate_date)) . '</a>)</span> <br/>';
                        } else {
                            $html .= '<span class="languages">' . $formation->formation . ' ( ' . date('d.m.y', strtotime($formation->certificate_date)) . ' )</span> <br/>';
                        }
                    }
                    $html .= '</div>';
                }
            }



            if ($user->registeration->ombudsman && $user->registeration->ombudsman->ombudsmanaccreds) {
                $accreds = $user->registeration->ombudsman->ombudsmanaccreds;
                if ($accreds) {
                    if ($formations) {
                        $col = "6";
                    } else {
                        $col = "12";
                    }
                    $html .= '<div class="col-md-' . $col . ' " style="margin-bottom:10px">
            <strong class="custom_labels">Accréditation</strong>';
                    foreach ($accreds as $accred) {
                        $html .= ' <span class="languages"><i class="fa fa-check icon_color"></i> ' . $accred['Accreditations']['name'] . '</span>';
                    }
                    $html .= ' </div>';
                }
            }
            $swears = '';
            if ($user->registeration->ombudsman && $user->registeration->ombudsman->ombudsmanswears) {
                $swears = $user->registeration->ombudsman->ombudsmanswears;
                if ($swears) {


                    $html .= '
        <div class="clearfix"></div><div class="col-md-6 " style="margin-bottom:10px">
            <strong class="custom_labels">Accréditation</strong>';
                    foreach ($swears as $swear) {
                        $html .= '<span class="languages"><i class="fa fa-location-arrow icon_color"></i> ' . $swear->place . ' (' . date('d.m.y', strtotime($swear->date_of_auth)) . ')</span> <br/>';
                    }
                    $html .= ' </div>';
                }
            }


            if ($user->registeration->ombudsman && $user->registeration->ombudsman->ombudsmanmeds) {
                $mediaTypes = $user->registeration->ombudsman->ombudsmanmeds;
                if ($mediaTypes) {
                    if ($swears) {
                        $col = "6";
                    } else {
                        $col = "12";
                    }
                    $html .= '<div class="col-md-' . $col . ' " style="margin-bottom:10px">
            <strong class="custom_labels">Types de médiation</strong>';
                    foreach ($mediaTypes as $mediaType) {
                        $html .= '<span class="languages"><i class="fa fa-handshake-o icon_color"></i> ' . $mediaType['medtype']['names'] . '</span>';
                    }
                    $html .= ' </div>';
                }
            }
            $html .= ' <div class="clearfix"></div>';
            if ($user->registeration->ombudsman->presentation_text) {
                $html .= '     <hr>
        <div class="clearfix"></div>
        <div class="col-lg-12 description" style="max-height:150px;overflow:auto">
           ' . nl2br($user->registeration->ombudsman->presentation_text) . '
        </div>';
            }
            $html .= '</div>
    <div class="clearfix"></div>
    <hr>
    <div class="clearfix"></div>
    <div class="col-md-12 text-center">
        <button type="button" data-dismiss="modal" class="btn custom_btn">Fermer</button>
    </div>
</div>';


            $responce = array('flag' => 'success', 'html' => $html);
        } else {
            $responce = array('flag' => 'error', 'message' => 'Record not found');
        }
        echo json_encode($responce);
        exit;
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    // the profile form of registeration from member dashboard is submitted to this function
    public function customerprofile() {
        $this->viewBuilder()->layout('memberlayout');
        $roleid = $this->request->session()->read('Auth.User.role_id');
        if ($roleid == 6) {
            $id = $this->request->session()->read('Auth.User.registeration.id');
            $userid = $this->request->session()->read('Auth.User.id');
            $registeration = $this->Registerations->get($id, [
                'contain' => ['Users', 'Membertypes']
            ]);
            $this->loadModel('Users');
            $user = $this->Users->get($userid, [
            ]);
            $status = $user->status;
            $this->set(compact('status'));
            //$registeration = $this->Registerations->newEntity();
            if ($this->request->is(['patch', 'post', 'put'])) {
                if ($this->request->data['image']['name'] != "") {
                    $ext = strrchr($this->request->data['image']['name'], '.');
                    $slider_image = $this->request->data['city'] . time() . $ext;
                    $source = $this->request->data['image']['tmp_name'];
                    $destination = WWW_ROOT . 'img' . DS . 'slider' . DS . $slider_image;
                    $thm_destination = WWW_ROOT . 'img' . DS . 'slider' . DS . 'thumbnail' . DS . $slider_image;
                    $this->request->data['image'] = $slider_image;
                } else {
                    unset($this->request->data['image']);
                }
                if (isset($this->request->data['tel1public']) == 'on') {
                    $tel1 = 1;
                } else {
                    $tel1 = 0;
                }
                if (isset($this->request->data['tel2public']) == 'on') {
                    $tel2 = 1;
                } else {
                    $tel2 = 0;
                }

                if (isset($this->request->data['emailpublic']) == 'on') {
                    $email = 1;
                } else {
                    $email = 0;
                }

                if (isset($this->request->data['publicprofile']) && $this->request->data['publicprofile'] == 'public') {
                    $profilepublic = 1;

                    //debug(1);
                } else {
                    $profilepublic = 0;
                    //debug(0);
                }

                $this->request->data['tel1public'] = $tel1;
                $this->request->data['tel2public'] = $tel2;
                $this->request->data['emailpublic'] = $email;
                $this->request->data['publicprofile'] = $profilepublic;

                $registeration = $this->Registerations->patchEntity($registeration, $this->request->getData(), [
                    'validate' => 'RegCheck'
                ]);

                if (isset($this->request->data['first_name'])) {
                    $this->request->data['User']['first_name'] = $this->request->data['first_name'];
                }
                if (isset($this->request->data['last_name'])) {
                    $this->request->data['User']['last_name'] = $this->request->data['lastname'];
                }
                //$this->request->data['User']['role_id'] = $this->request->data['membertype_id'];
                $this->request->data['User']['password'] = $this->request->data['pwd'];
                //                $this->request->data['User']['status'] = 1;
                if ($this->request->data['pwd'] == "") {
                    unset($this->request->data['User']['password']);
                }
                unset($this->request->data['User']['email']);
                if ($registeration->membertype->type == 2) {
                    $this->request->data['User']['status'] = 2;
                }
//                debug($this->request->data['User']['status']);
//                exit;
                $user = $this->Users->patchEntity($user, $this->request->data['User'], [
                ]);
                $this->Users->save($user);

                if ($this->Registerations->save($registeration)) {
                    if (!empty($this->request->data['image'])) {
                        $this->generate_thumb($source, $thm_destination, 400, 300);
                        $this->generate_thumb($source, $destination, 1200, 497);
                    }

                    $superAdmins = $this->Users->find('all', [
                                'conditions' => ['Users.role_id' => 1, 'Users.status' => 1]
                            ])->toArray();

                    //                    foreach($superAdmins as $admin){
                    //                        $email = $admin->email;
                    //                        $Email = new Email();
                    //                        $Email->from(array('info@cyberclouds.com' => 'FGEM'));
                    //                        $Email->emailFormat('both');
                    //                        $Email->to($email);
                    //                        $Email->subject('Member Profile | FGEM');
                    //
                    //                        
                    //                        $reset_token_link_admin = Router::url(['controller' => 'Users', 'action' => 'login'], TRUE);
                    //                        $message = 'Bonjour Admin,<br />Le membre obtenir le nom du membre ici a terminé le profil avec succès. S\'il vous plaît cliquez ici pour activer son compte.<br/><a href='.$reset_token_link_admin.'> '.$reset_token_link_admin.'</a><br/>Merci <br/>FGEM';
                    //
                    //                        $Email->send($message);
                    //
                    //                    }
                    //$this->Flash->success(__('Les informations de profil ont été enregistrées.'));
                    if ($status == 2) {
                        return $this->redirect(['controller' => 'Pages', 'action' => 'memberdashboard']);
                    } else {
                        if ($registeration->membertype->type == 2) {
                            return $this->redirect(['controller' => 'Pages', 'action' => 'memberdashboard']);
                        } else {
                            return $this->redirect(['controller' => 'Registerations', 'action' => 'business']);
                        }
                    }
                }
                //$this->Flash->error(__('The profile could not be saved. Please, try again.'));
            }
            $membertypes = $this->Registerations->Membertypes->find('list', ['limit' => 200]);
            $this->set(compact('registeration', 'membertypes'));
            $this->set('_serialize', ['registeration']);
            //   $this->viewBuilder()->setLayout('');
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);
        }
    }

// a new member type can be added here
    public function membertypes() {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->viewBuilder()->layout('backend');
        $this->loadModel('Membertypes');
        $membertypes = $this->paginate($this->Membertypes);
        $this->set(compact('membertypes'));
    }

// annual amount of each member types are listed here
    public function annualAmount($memberTypeId = null) {
        $permission = $this->viewVars['actionPermission'];
        if ($permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->viewBuilder()->layout('backend');
        $this->loadModel('Membertypes');
        $this->loadModel('MemberTypeAmounts');
        $this->paginate = [
            'conditions' => ['MemberTypeAmounts.membertype_id' => $memberTypeId]
        ];
        $membertypes = $this->paginate($this->MemberTypeAmounts);
        $type = $this->Membertypes->get($memberTypeId);
//        debug($membertypes);
//        exit;
        $this->set(compact('membertypes', 'type'));
    }

    public function registerationform() {
        $this->viewBuilder()->layout('internal');

        $this->loadModel('Users');
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['User']['first_name'] = $this->request->data['first_name'];
            $this->request->data['User']['last_name'] = $this->request->data['lastname'];
            //$this->request->data['User']['user_name'] = $this->request->data['user_name'];
            $this->request->data['User']['email'] = $this->request->data['email'];
            $this->request->data['User']['password'] = $this->request->data['password'];
            $this->request->data['User']['role_id'] = 6;
            $rand1 = str_pad(rand(1, 9999999), 5, STR_PAD_LEFT);
            $this->request->data['User']['status'] = 0;
            $this->request->data['User']['verified'] = $rand1;

            $user = $this->Users->patchEntity($user, $this->request->data['User'], [
                'validate' => 'UserCheck'
            ]);
//                        debug($user);exit;

            if ($result = $this->Users->save($user)) {
//            if (true) {
                $userId = $result->id;
                $registerations = $this->Registerations->newEntity();
                $this->request->data['Registerations']['user_id'] = $userId;
                $this->request->data['Registerations']['membertype_id'] = $this->request->data['membertype_id'];

                $this->request->data['Registerations']['status'] = 0;
                $this->request->data['Registerations']['org_name'] = $this->request->data['org_name'];


                $registerations = $this->Registerations->patchEntity($registerations, $this->request->data['Registerations'], [
                    'validate' => 'RegremoveCheck'
                ]);



//                if ($result2 = $this->Registerations->save($registerations)) {
                if ($result2 = $this->Registerations->save($registerations)) {
                    $reId = $result2->id;
                    $memberTypeId = $result2->membertype_id;

                    $this->loadModel('Accounts');
                    $this->loadModel('Membertypes');
                    $this->loadModel('NextRenewalDate');

                    $membership = $this->Membertypes->get($memberTypeId, ['contain'=>['MemberTypeAmounts'=>['conditions'=>['year'=>date('Y')]]]]);
//                    debug();exit;
//                    $membershipAmount = $membership->amount;
                    $membershipAmount = $membership->member_type_amounts[0]->amount;

                    $currentYear = date('Y');
                    $year = $currentYear + 1;

                    $findYear = $this->NextRenewalDate->find('all', [
                                'conditions' => ['NextRenewalDate.year' => $year]
                            ])->toArray();

                    if (count($findYear) > 0) {
                        if ($findYear[0]->month < 10) {
                            $month = '0' . $findYear[0]->month;
                        } else {
                            $month = $findYear[0]->month;
                        }
                        if ($findYear[0]->day < 10) {
                            $day = '0' . $findYear[0]->day;
                        } else {
                            $day = $findYear[0]->day;
                        }
                        $dueDate = $year . '-' . $month . '-' . $day . ' 00:00:00';
                    } else {
                        $dueDate = $year . '-12-31 00:00:00';
                    }
                    $amountDate = date('Y-m-d H:i:s');

                    $account = $this->Accounts->newEntity();

                    $this->request->data['Accounts']['registeration_id'] = $reId;
                    $this->request->data['Accounts']['paymenttype_id'] = 2;
                    $this->request->data['Accounts']['amount_paid'] = 0;
                    $this->request->data['Accounts']['amount_due'] = $membershipAmount;
                    $this->request->data['Accounts']['amout_date'] = $amountDate;
                    $this->request->data['Accounts']['due_date'] = $dueDate;

                    $this->request->data['Accounts']['user_id'] = 1;
                    $this->request->data['Accounts']['status'] = 0;

                    $account = $this->Accounts->patchEntity($account, $this->request->data['Accounts']);

                    $this->Accounts->save($account);


                    $this->loadModel('Ombudsmans');
                    $ombudsman = $this->Ombudsmans->newEntity();
                    $this->request->data['Ombudsmans']['registeration_id'] = $reId;
                    $ombudsman = $this->Ombudsmans->patchEntity($ombudsman, $this->request->data['Ombudsmans']);

                    $this->Ombudsmans->save($ombudsman);

                    $number = count($this->request->data['Registerations']);

                    // $passkey = uniqid();
                    // $url = Router::Url(['controller' => 'users', 'action' => 'reset'], true) . '/' . $passkey;


                    $reset_token_link = Router::url(['controller' => 'Users', 'action' => 'verified'], TRUE) . '/' . $rand1;
                    $this->loadModel('Emails');
                    $email = $this->Emails->find('all', array(
                                'conditions' => ['Emails.id' => '11'],
                                'contain' => ['Emailtypes']
                            ))->first();
                    $adminEmail = $this->Emails->find('all', array(
                                'conditions' => ['Emails.id' => '12'],
                                'contain' => ['Emailtypes']
                            ))->first();

                    $user = $this->request->data['first_name'] . " " . $this->request->data['lastname'];
                    $title = $email['title'];
                    $from_person = 'info@cyberclouds.com';
                    $to_person = $this->request->data['email'];

                    $subject = $email['subject'];
                    $content = $email['message'];
                    //$message = str_replace("{first_name,last_name}",$user, $content);
                    $message = "";
                    $message .= "<span style='background:#161616;display:block;padding:10px;'><img src='http://needanecommercesite.com/fgem/img/logo-light.png' style='display:block;margin:auto;max-width: 130px;'/></span>";
                    $trans = array("{first_name,last_name}" => $user, "{first_name}" => $this->request->data['first_name'], "{last_name}" => $this->request->data['lastname'], "{first_name} {last_name}" => $user, "{link}" => $reset_token_link);
                    $message .= strtr($content, $trans);
                    $Email = new Email();
                    $Email->from(array($from_person => $title));
                    $Email->emailFormat('both');
                    $Email->to($to_person);
                    $Email->subject($subject);
                    // $message = "Dear ".$user.",</br></br>" ;
                    //$message .=$content;
                    $Email->send($message);
                    $committee = $this->Users->find('all', [
                                'conditions' => ['Users.role_id' => 9, 'Users.status' => 1]
                            ])->toArray();

                    foreach ($committee as $admin) {
                        $user = $admin->first_name . " " . $admin->last_name;
                        $member_name = $this->request->data['first_name'] . " " . $this->request->data['lastname'];
                        $title = $adminEmail['title'];
                        $from_person = 'info@cyberclouds.com';
                        $to_person = $admin->email;
                        $subject = $adminEmail['subject'];
                        $content = $adminEmail['message'];
                        //$message = str_replace("{first_name,last_name}",$user, $content);
                        $link = Router::url(['controller' => 'Registerations', 'action' => 'index'], TRUE);
                        $message = "";
                        $message .= "<span style='background:#161616;display:block;padding:10px;'><img src='http://needanecommercesite.com/fgem/img/logo-light.png' style='display:block;margin:auto;max-width: 130px;'/></span>";
                        $trans = array("{first_name,last_name}" => $user, "{first_name}" => $admin->first_name, "{last_name}" => $admin->lastname, "{first_name} {last_name}" => $user, "{link}" => $link, "{user_name}" => $member_name);
                        $message .= strtr($content, $trans);
                        $Email = new Email();
                        $Email->from(array($from_person => $title));
                        $Email->emailFormat('both');
                        $Email->to($to_person);
                        $Email->subject($subject);
                        // $message = "Dear ".$user.",</br></br>" ;
                        //$message .=$content;
                        $Email->send($message);
                    }

                    $this->Flash->success("Une confirmation a été envoyée à votre adresse email. Merci de bien vouloir suivre les indications se trouvant dans le email pour valider la création de votre compte.Si vous ne trouvez pas la demande de validation dans votre boîte de réception il est possible que le message ait été placé dans votre boîte spam.");


                    return $this->redirect(['controller' => 'Users', 'action' => 'customerLogin']);
                }
            }
            $this->Flash->error(__("L'enregistrement n'a pas pu être effectué. L'adresse email existe déjà."));
        }
        $membertypes = $this->Registerations->Membertypes->find('all')
                ->contain(['MemberTypeAmounts'=>['conditions'=>['year'=>date('Y')]]])
                ->order('type')->toArray();
        $this->loadModel('Contents');
        $termsAndConditions = $this->Contents->get(76);

        $this->set(compact('registeration', 'user', 'membertypes', 'termsAndConditions'));
        $this->set('_serialize', ['registeration']);
        $this->viewBuilder()->layout('internal');
    }

    public function customerRegistration() {
        $this->viewBuilder()->layout('internal');

        $this->loadModel('Users');
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
//            debug($this->request->data);
//            exit;
            $this->request->data['User']['first_name'] = $this->request->data['first_name'];
            $this->request->data['User']['last_name'] = $this->request->data['lastname'];
//$this->request->data['User']['user_name'] = $this->request->data['user_name'];
            $this->request->data['User']['email'] = $this->request->data['email'];
            $this->request->data['User']['password'] = $this->request->data['password'];
            $this->request->data['User']['role_id'] = 10;
//            $rand1 = str_pad(rand(1, 9999999), 5, STR_PAD_LEFT);
            $this->request->data['User']['status'] = 1;
//            $this->request->data['User']['verified'] = $rand1;
//debug($registeration);exit;
            $user = $this->Users->patchEntity($user, $this->request->data['User'], [
                'validate' => 'UserCheck'
            ]);
//                        debug($user);exit;


            if ($result = $this->Users->save($user)) {
                $this->Flash->success("You have successfully registered.");
                return $this->redirect(['controller' => 'Users', 'action' => 'customerLogin']);
            }
            $this->Flash->error(__("L'enregistrement n'a pas pu être effectué. L'adresse email existe déjà."));
        }
        $membertypes = $this->Registerations->Membertypes->find('all')->toArray();
        $this->set(compact('registeration', 'user', 'membertypes'));
        $this->set('_serialize', ['registeration']);
        $this->viewBuilder()->layout('internal');
    }

    public function deleteFormation() {
        if (isset($_GET['id'])) {
            $this->loadModel('Ombudsmanformations');
            $formation = $this->Ombudsmanformations->get($_GET['id']);
            if ($formation) {
                if ($this->Ombudsmanformations->delete($formation)) {
                    $responce = array('flag' => 1, 'message' => 'Record deleted successfully');
                }
            } else {
                $responce = array('flag' => 0, 'message' => 'Record not found');
            }
        } else {
            $responce = array('flag' => 0, 'message' => 'Can not access');
        }
        echo json_encode($responce);
        exit;
    }

//business  details are loaded and the form can be added and updated to this function 
    public function business() {
        $this->viewBuilder()->layout('memberlayout');
        $id = $this->request->session()->read('Auth.User.registeration.id');

        $userId = $this->request->session()->read('Auth.User.id');
//$status = $this->request->session()->read('Auth.User.status');
        $this->loadModel('Users');
        $user = $this->Users->get($userId, [
        ]);
        $status = $user->status;
        $this->set(compact('status'));
//        if($status !== 2){
//            return $this->redirect(['controller' => 'Registerations', 'action' => 'customerprofile']);
//        }
        $ombId = $this->request->session()->read('Auth.User.registeration');
        $ombId = $ombId['ombudsman']['id'];
        $this->loadModel('Users');
        $user = $this->Users->get($userId);
        $userStatus = $user->status;

        $this->loadModel('Registerations');
        $registrations = $this->Registerations->find('all')->contain([
                    'Ombudsmans' => [
                        'Ombudsmanswears',
                        'Ombudsmanaccreds',
                        'Ombudsmanformations',
                        'Ombudsmanlevels',
                        'Ombudsmanmeds'
            ]])->where(['user_id' => $user->id])->first()->toArray();

        $this->loadModel('Ombudsmans');
        $ombudsman = $this->Ombudsmans->get($ombId);


        if ($this->request->is(['patch', 'post', 'put'])) {

// ombudsman entry
            $this->loadModel('Ombudsmans');
            $this->request->data['Ombudsmans']['registeration_id'] = $id;
            $this->request->data['Ombudsmans']['mediator_question'] = $this->request->data['mediator_question'];
            $this->request->data['Ombudsmans']['presentation_text'] = $this->request->data['presentation_text'];
            $this->request->data['Ombudsmans']['assermentation'] = $this->request->data['assermentation'];

            $this->request->data['Ombudsmans']['accept_info'] = isset($this->request->data['accept_info']) ? $this->request->data['accept_info'] : 0;

            if (!isset($this->request->data['accept_info'])) {

                $this->request->data['Ombudsmans']['accept_info'] = "0";
            }
            if (isset($this->request->data['accept_info']) && $this->request->data['accept_info'] == 1) {
                if ($ombudsman->email_on_accept_info == 0) {

                    $this->request->data['Ombudsmans']['email_on_accept_info'] = 1;

                    $email = $registrations['email'];
                    $this->loadModel('Emails');
                    $emailTemplete = $this->Emails->find('all', array(
                                'conditions' => ['Emails.id' => '13'],
                                'contain' => ['Emailtypes']
                            ))->first();

                    $userName = $user->first_name . " " . $user->last_name;
                    $title = $emailTemplete['title'];
                    $from_person = 'info@cyberclouds.com';
                    $to_person = $user->email;

                    $subject = $emailTemplete['subject'];
                    $content = $emailTemplete['message'];
                    //$message = str_replace("{first_name,last_name}",$user, $content);
                    $message = "";
                    $message .= "<span style='background:#161616;display:block;padding:10px;'><img src='http://needanecommercesite.com/fgem/img/logo-light.png' style='display:block;margin:auto;max-width: 130px;'/></span>";
                    $trans = array("{first_name,last_name}" => $userName, "{first_name}" => $user->first_name, "{last_name}" => $user->last_name, "{first_name} {last_name}" => $userName);
                    $message .= strtr($content, $trans);
                    $Email = new Email();
                    $Email->from(array($from_person => $title));
                    $Email->emailFormat('both');
                    $Email->to($to_person);
                    $Email->subject($subject);
                    $Email->send($message);
                }
            }

            $ombudsman = $this->Ombudsmans->patchEntity($ombudsman, $this->request->data['Ombudsmans']);
//debug($ombudsman);exit;
            if ($this->Ombudsmans->save($ombudsman)) {
                if ($this->request->data['mediator_question'] == 'Non') {
                    $this->loadModel('Ombudsmanlevels');
                    $this->Ombudsmanlevels->deleteAll(['Ombudsmanlevels.ombudsman_id' => $ombudsman->id]);

                    $this->loadModel('Ombudsmanswears');
                    $this->Ombudsmanswears->deleteAll(['Ombudsmanswears.ombudsman_id' => $ombudsman->id]);
                    $this->loadModel('Ombudsmanaccreds');
                    $this->Ombudsmanaccreds->deleteAll(['Ombudsmanaccreds.ombudsman_id' => $ombudsman->id]);
                    $this->loadModel('Ombudsmanformations');
                    $this->Ombudsmanformations->deleteAll(['Ombudsmanformations.ombudsman_id' => $ombudsman->id]);
                } else {
                    //Type de mediation
                    if (!empty($this->request->data['ombudsmanmeds']['mediat'])) {
                        $this->loadModel('Ombudsmanmeds');
                        $this->Ombudsmanmeds->deleteAll(['Ombudsmanmeds.ombudsman_id' => $ombudsman->id]);

//save mediations
                        $saveOmbudsmanmeds = array();
                        foreach ($this->request->data['ombudsmanmeds']['mediat'] as $k => $om)://change
                            $saveOmbudsmanmeds[$k]['ombudsman_id'] = $ombudsman->id;
                            $saveOmbudsmanmeds[$k]['medtype_id'] = $om;
                        endforeach;
                        $ombudsmanmeds = $this->Ombudsmanmeds->newEntities($saveOmbudsmanmeds);
                        $this->Ombudsmanmeds->saveMany($ombudsmanmeds);
                    }else {
                        $this->loadModel('Ombudsmanmeds');
                        $this->Ombudsmanmeds->deleteAll(['Ombudsmanmeds.ombudsman_id' => $ombudsman->id]);
                    }
                    if (!empty($this->request->data['ombudsmanlevels_mediation'])) {
//save languages
                        $this->loadModel('Ombudsmanlevels');
                        $this->Ombudsmanlevels->deleteAll(['Ombudsmanlevels.ombudsman_id' => $ombudsman->id]);

                        $saveLanguage = array();
                        foreach ($this->request->data['ombudsmanlevels_mediation'] as $n => $o)://change
                            $saveLanguage[$n]['ombudsman_id'] = $ombudsman->id;
                            $saveLanguage[$n]['language_id'] = $o;
                            $saveLanguage[$n]['language_level'] = $this->request->data['ombudsmanlevels_language_level'][$n];
                        endforeach;
//                    debug($saveLanguage);exit;
                        $saveLanguage = $this->Ombudsmanlevels->newEntities($saveLanguage);
                        $this->Ombudsmanlevels->saveMany($saveLanguage);
                    }
                    if (!empty($this->request->data['place'])) {
//save ombudsmanswears
                        $this->loadModel('Ombudsmanswears');
                        $this->Ombudsmanswears->deleteAll(['Ombudsmanswears.ombudsman_id' => $ombudsman->id]);

                        $saveOmbudsmanswears = array();
//                debug($this->request->data['ombudsmanswears']['place']);
                        foreach ($this->request->data['place'] as $p => $q)://change
                            $saveOmbudsmanswears[$p]['ombudsman_id'] = $ombudsman->id;
                            $saveOmbudsmanswears[$p]['place'] = $q;
                            $saveOmbudsmanswears[$p]['date_of_auth'] = ($this->request->data['date_of_auth'][$p]) ? date('Y-m-d', strtotime($this->request->data['date_of_auth'][$p])) : null;
                        endforeach;
                        $saveOmbudsmanswears = $this->Ombudsmanswears->newEntities($saveOmbudsmanswears);
//                    debug($saveOmbudsmanswears);
//                    exit;
                        $this->Ombudsmanswears->saveMany($saveOmbudsmanswears);
                    }
                    if (!empty($this->request->data['accre'])) {
//save ombudsmanaccreds
                        $this->loadModel('Ombudsmanaccreds');
                        $this->Ombudsmanaccreds->deleteAll(['Ombudsmanaccreds.ombudsman_id' => $ombudsman->id]);

                        $saveOmbudsmanaccreds = array();
//                debug($this->request->data['ombudsmanswears']['place']);
                        foreach ($this->request->data['accre'] as $t => $u)://change
                            $saveOmbudsmanaccreds[$t]['ombudsman_id'] = $ombudsman->id;
                            $saveOmbudsmanaccreds[$t]['accreditation_id'] = $u;
                        endforeach;
                        $saveOmbudsmanaccreds = $this->Ombudsmanaccreds->newEntities($saveOmbudsmanaccreds);
//                debug($saveOmbudsmanaccreds);
                        $this->Ombudsmanaccreds->saveMany($saveOmbudsmanaccreds);
                    }
//save ombudsmanformations
                    if (!empty($this->request->data['ombudsmanformations_formation'][0])) {
                        $this->loadModel('Ombudsmanformations');
                        $saveOmbudsmanformations = array();
                        $updateImages = array();

                        foreach ($this->request->data['image'] as $r => $s):
                            if ($s['name'] != "") {
                                $get_ext = pathinfo($s['name'], PATHINFO_EXTENSION);
                                $new_name = $r . '-' . date('ymdhis') . '.' . $get_ext;
                                $path = WWW_ROOT . 'img' . DS . 'formations' . DS . $new_name;
                                move_uploaded_file($s['tmp_name'], $path);
                            }

                            if (isset($this->request->data['ombudsmanformations_image_id'][$r])) {
                                $updateImages[$this->request->data['ombudsmanformations_image_id'][$r]]['ombudsman_id'] = $ombudsman->id;
                                $updateImages[$this->request->data['ombudsmanformations_image_id'][$r]]['formation'] = $this->request->data['ombudsmanformations_formation'][$r];
                                $updateImages[$this->request->data['ombudsmanformations_image_id'][$r]]['certificate_date'] = date('Y-m-d', strtotime($this->request->data['ombudsmanformations_date'][$r]));

                                if (empty($s['name'])) {
                                    $updateImages[$this->request->data['ombudsmanformations_image_id'][$r]]['copy_upload'] = $this->request->data['ombudsmanformations_image_name'][$r];
                                } else {
                                    $updateImages[$this->request->data['ombudsmanformations_image_id'][$r]]['copy_upload'] = $new_name;
                                }
                            } else {
                                $saveOmbudsmanformations[$r]['ombudsman_id'] = $ombudsman->id;
                                $saveOmbudsmanformations[$r]['formation'] = $this->request->data['ombudsmanformations_formation'][$r];
                                $saveOmbudsmanformations[$r]['certificate_date'] = date('Y-m-d', strtotime($this->request->data['ombudsmanformations_date'][$r]));
                                $saveOmbudsmanformations[$r]['copy_upload'] = $new_name;
                            }

                        endforeach;

//update records
                        if ($updateImages) {
                            $allRec = TableRegistry::get('Ombudsmanformations');
                            foreach ($updateImages as $oK => $uImage) {
//debug($oK);
                                $getRec = $allRec->get($oK);
                                $getRec = $this->Ombudsmanformations->patchEntity($getRec, $uImage);
                                $this->Ombudsmanformations->save($getRec);
                            }
                        }
//add records
                        if ($saveOmbudsmanformations) {
                            $saveOmbudsmanformations = $this->Ombudsmanformations->newEntities($saveOmbudsmanformations);
                            $this->Ombudsmanformations->saveMany($saveOmbudsmanformations);
                        }
                    }
                }
            }
//debug($number);exit;
            $this->request->data['User']['status'] = 2;
            $this->request->data['User']['role_id'] = $this->request->session()->read('Auth.User.role_id');
            $user = $this->Users->patchEntity($user, $this->request->data['User'], [
                'validate' => 'UserCheck'
            ]);

            if ($result = $this->Users->save($user)) {

                $committee = $this->Users->find('all', [
                            'conditions' => ['Users.role_id' => 9, 'Users.status' => 1]
                        ])->toArray();
                if ($userStatus == 1) {
                    $this->loadModel('Emails');
                    $emailTemplete = $this->Emails->find('all', array(
                                'conditions' => ['Emails.id' => '14'],
                                'contain' => ['Emailtypes']
                            ))->first();
                    foreach ($committee as $admin) {
                        $userName = $admin->first_name . " " . $admin->last_name;
                        $title = $emailTemplete['title'];
                        $from_person = 'info@cyberclouds.com';
                        $to_person = $admin->email;
                        $memberLink = Router::url(['controller' => 'Registerations', 'action' => 'index'], TRUE);

                        $subject = $emailTemplete['subject'];
                        $content = $emailTemplete['message'];
                        $message = "";
                        $message .= "<span style='background:#161616;display:block;padding:10px;'><img src='http://needanecommercesite.com/fgem/img/logo-light.png' style='display:block;margin:auto;max-width: 130px;'/></span>";
                        $trans = array("{first_name,last_name}" => $userName, "{first_name}" => $admin->first_name, "{last_name}" => $admin->last_name, "{first_name} {last_name}" => $userName, '{user_name}' => $user->first_name . ' ' . $user->last_name, '{link}' => $memberLink);
                        $message .= strtr($content, $trans);
                        $Email = new Email();
                        $Email->from(array($from_person => $title));
                        $Email->emailFormat('both');
                        $Email->to($to_person);
                        $Email->subject($subject);
                        $Email->send($message);
                    }
                }
                $this->Flash->success("Les informations ont été enregistrées.");
                return $this->redirect(['controller' => 'pages', 'action' => 'memberdashboard']);
            }
            $this->Flash->error(__("Les informations sur la société n'ont pas pu être enregistrées. Veuillez réessayer."));
        }

        $this->loadModel('Medtypes');
        $medtype = $this->Medtypes->find('all', ['order' => 'ordinal ASC'])->toArray();

        $this->loadModel('Accreditations');
        $acc = $this->Accreditations->find('all', ['order' => 'ordinal ASC'])->toArray();
//debug($medtype);exit;
        $this->loadModel('Languages');
        $languages = $this->Languages->find('list')->order(['ordinal', 'name'])->toArray();
//        debug($languages);exit;
        $this->set('user', $user);
//        debug($registrations);exit;
        $this->loadModel('Contents');
        $termsAndConditions = $this->Contents->get(77);
        $this->set(compact('languages', 'registrations', 'termsAndConditions'));
        $this->set('acc', $acc);
        $this->set('medtype', $medtype);
        $this->viewBuilder()->layout('memberlayout');
    }

    public function changeStatus($id = null, $status = null) {
        $this->loadModel('Accounts');
        $user = $this->Accounts->find('all', array(
                    'contain' => 'Registerations',
                    'conditions' => ['Registerations.id' => $id],
                ))->first();
        $this->loadModel('Registerations');
        $reg = $this->Registerations->find('all', array(
                    'conditions' => ['id' => $id],
                ))->first();
//debug($reg['status']);exit;


        if (empty($user) && $reg['status'] != 1) {
            $this->Flash->error('S\'il vous plaît entrer les informations de compte d\'abord');
            return $this->redirect(['controller' => 'Accounts', 'action' => 'index', $id]);
        }

        $regs = TableRegistry::get('Registerations');
        $reg = $regs->get($id);
        $reg->status = $status;
$users = TableRegistry::get('Users');
 $user = $users->get($reg['user_id']);
        if($status == 1) {

        $user->suspend = 0;

        } else if($status == 0) {
            $user->suspend = 1;
        }

        if ($regs->save($reg) && $users->save($user) ) {

            if ($reg->status == 1) {

            }
            $this->Flash->success('status changed');
        } else {
            $this->Flash->error('status not changed');
        }
//        if ($reg->role_id == 3) {
//            return $this->redirect('/Users/corporate');
//        } else {
        return $this->redirect(['controller' => 'Accounts', 'action' => 'index', $id]);
//return $this->redirect('/Registerations/index');
//}
    }

    /**
     * Edit method
     *
     * @param string|null $id Registeration id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
// A registeration is edited here
    public function edit($id = null, $business = null) {
        $permission = $this->viewVars['actionPermission'];
        if ($permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $registeration = $this->Registerations->get($id, [
            'contain' => ['Users.MemberRoles', 'ombudsmans', 'Membertypes']
        ]);
        $userid = $registeration['user_id'];
        $this->loadModel('Users');
        $user = $this->Users->get($userid, [
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            if (isset($this->request->data['is_superviser'])) {
                $action = $this->request->data['is_superviser'];
                if ($action == 1) {
                    $getRole = $this->MemberRoles->find('all')->where(['user_id' => $userid, 'role_id' => 11])->first();
                    if (empty($getRole)) {
                        $getRole = $this->MemberRoles->newEntity();
                        $getRole->user_id = $userid;
                        $getRole->role_id = 11;
                        $this->MemberRoles->save($getRole);
                    }
                } else {
                    $getRole = $this->MemberRoles->find('all')->where(['user_id' => $userid, 'role_id' => 11])->first();
                    if (!empty($getRole)) {
                        $this->MemberRoles->delete($getRole);
                    }
                }
            }

//echo $this->request->data['image']['name'];exit;
            $slider_image = "";
            if ($this->request->data['image']['name'] != "") {
                $ext = strrchr($this->request->data['image']['name'], '.');
                $slider_image = $this->request->data['city'] . time() . $ext;
                $source = $this->request->data['image']['tmp_name'];
                $destination = WWW_ROOT . 'img' . DS . 'slider' . DS . $slider_image;
                $thm_destination = WWW_ROOT . 'img' . DS . 'slider' . DS . 'thumbnail' . DS . $slider_image;
                $this->request->data['image'] = $slider_image;
            } else {
                unset($this->request->data['image']);
            }
            if ($this->request->data['password'] == "") {
                unset($this->request->data['password']);
            } else {
                $this->request->data['User']['password'] = $this->request->data['password'];
            }
            $this->request->data['User']['first_name'] = $this->request->data['firstname'];
            $this->request->data['User']['last_name'] = $this->request->data['lastname'];
            $this->request->data['User']['email'] = $this->request->data['email'];

            if ($registeration->membertype->type == 2) {
                $this->request->data['User']['status'] = 2;
            }
            $user = $this->Users->patchEntity($user, $this->request->data['User'], [
            ]);
            $this->Users->save($user);

            $this->request->data['city'] = strtolower($this->request->data['city']);
//            debug($this->request->data['city']);
//            exit;
            $registeration = $this->Registerations->patchEntity($registeration, $this->request->getData());
            if ($this->Registerations->save($registeration)) {
//echo $slider_image;exit;
                if ($slider_image != "") {
                    $this->generate_thumb($source, $thm_destination, 400, 300);
                    $this->generate_thumb($source, $destination, 1200, 497);
                }
                $this->Flash->success(__('L\'inscription a été sauvegardée.'));
                if ($registeration->membertype->type == 1) {
                    return $this->redirect(['action' => 'edit', $id, 'business']);
                } else {
                    return $this->redirect(['action' => 'index']);
                }
            }
            $this->Flash->error(__('L\'enregistrement n\'a pas pu être enregistré. Veuillez réessayer.'));
        }
        $membertypes = $this->Registerations->Membertypes->find('list', ['limit' => 200]);
        $this->set(compact('registeration', 'membertypes', 'user'));
        $this->set('_serialize', ['registeration']);
        $this->viewBuilder()->setLayout('backend');

// business info

        $userId = $userid;
//$status = $this->request->session()->read('Auth.User.status');
        $this->loadModel('Users');
        $user = $this->Users->get($userId, [
        ]);
        $status = $user->status;
        $this->set(compact('status'));
//        if($status !== 2){
//            return $this->redirect(['controller' => 'Registerations', 'action' => 'customerprofile']);
//        }
        $ombId = $this->request->session()->read('Auth.User.registeration');
        $ombId = $ombId['ombudsman']['id'];
        $this->loadModel('Users');
        $user = $this->Users->get($userId);
        $userStatus = $user->status;

        $this->loadModel('Registerations');
        $registrations = $this->Registerations->find('all')->contain([
                    'Ombudsmans' => [
                        'Ombudsmanswears',
                        'Ombudsmanaccreds',
                        'Ombudsmanformations',
                        'Ombudsmanlevels',
                        'Ombudsmanmeds'
            ]])->where(['user_id' => $user->id])->first()->toArray();

        $this->loadModel('Ombudsmans');
        $this->loadModel('Accreditations');
        $this->loadModel('Languages');

        $ombudsman = $registeration->Ombudsmans;
        $this->loadModel('Medtypes');
        $medtype = $this->Medtypes->find('all', ['order' => 'ordinal ASC'])->toArray();

        $acc = $this->Accreditations->find('all', ['order' => 'ordinal ASC'])->toArray();
        $languages = $this->Languages->find('list', ['order' => ['ordinal', 'name']])->toArray();
        $this->set('user', $user);
        $this->set(compact('languages', 'registrations'));
        $this->set('acc', $acc);
        $this->set('medtype', $medtype);
    }

    public function saveBusinessInfo($id = null) {
        if ($this->request->is(['patch', 'post', 'put'])) {
//            debug($this->request->data);exit;
            $this->loadModel('Ombudsmans');
            $this->request->data['Ombudsmans']['registeration_id'] = $id;
            $this->request->data['Ombudsmans']['mediator_question'] = $this->request->data['mediator_question'];
            $this->request->data['Ombudsmans']['presentation_text'] = $this->request->data['presentation_text'];
            $this->request->data['Ombudsmans']['assermentation'] = $this->request->data['assermentation'];

            $this->request->data['Ombudsmans']['accept_info'] = isset($this->request->data['accept_info']) ? $this->request->data['accept_info'] : 0;
//            debug($this->request->data['accept_info']);exit;

            if (!isset($this->request->data['accept_info'])) {

                $this->request->data['Ombudsmans']['accept_info'] = "0";
            }
            $ombudsman = $this->Ombudsmans->find('all')
                            ->contain(['Registerations.Users'])
                            ->where(['registeration_id' => $id])->first();
            if (isset($this->request->data['accept_info']) && $this->request->data['accept_info'] == 1) {
                if ($ombudsman->email_on_accept_info == 0) {

                    $this->request->data['Ombudsmans']['email_on_accept_info'] = 1;
                    $user = $ombudsman->registeration->user;
                    $email = $user->email;
                    $this->loadModel('Emails');
                    $emailTemplete = $this->Emails->find('all', array(
                                'conditions' => ['Emails.id' => '13'],
                                'contain' => ['Emailtypes']
                            ))->first();

                    $userName = $user->first_name . " " . $user->last_name;
                    $title = $emailTemplete['title'];
                    $from_person = 'info@cyberclouds.com';
                    $to_person = $user->email;

                    $subject = $emailTemplete['subject'];
                    $content = $emailTemplete['message'];
                    //$message = str_replace("{first_name,last_name}",$user, $content);
                    $message = "";
                    $message .= "<span style='background:#161616;display:block;padding:10px;'><img src='http://needanecommercesite.com/fgem/img/logo-light.png' style='display:block;margin:auto;max-width: 130px;'/></span>";
                    $trans = array("{first_name,last_name}" => $userName, "{first_name}" => $user->first_name, "{last_name}" => $user->last_name, "{first_name} {last_name}" => $userName);
                    $message .= strtr($content, $trans);
                    $Email = new Email();
                    $Email->from(array($from_person => $title));
                    $Email->emailFormat('both');
                    $Email->to($to_person);
                    $Email->subject($subject);
                    $Email->send($message);
                }
            }
            $ombudsman = $this->Ombudsmans->patchEntity($ombudsman, $this->request->data['Ombudsmans']);

            if ($this->Ombudsmans->save($ombudsman)) {

                if ($this->request->data['mediator_question'] == 'Non') {
                    $this->loadModel('Ombudsmanlevels');
                    $this->Ombudsmanlevels->deleteAll(['Ombudsmanlevels.ombudsman_id' => $ombudsman->id]);

                    $this->loadModel('Ombudsmanswears');
                    $this->Ombudsmanswears->deleteAll(['Ombudsmanswears.ombudsman_id' => $ombudsman->id]);
                    $this->loadModel('Ombudsmanaccreds');
                    $this->Ombudsmanaccreds->deleteAll(['Ombudsmanaccreds.ombudsman_id' => $ombudsman->id]);
                    $this->loadModel('Ombudsmanformations');
                    $this->Ombudsmanformations->deleteAll(['Ombudsmanformations.ombudsman_id' => $ombudsman->id]);
                } else {
                    //Type de mediation
                    if (!empty($this->request->data['ombudsmanmeds']['mediat'])) {
                        $this->loadModel('Ombudsmanmeds');
                        $this->Ombudsmanmeds->deleteAll(['Ombudsmanmeds.ombudsman_id' => $ombudsman->id]);

//save mediations
                        $saveOmbudsmanmeds = array();
                        foreach ($this->request->data['ombudsmanmeds']['mediat'] as $k => $om)://change
                            $saveOmbudsmanmeds[$k]['ombudsman_id'] = $ombudsman->id;
                            $saveOmbudsmanmeds[$k]['medtype_id'] = $om;
                        endforeach;
                        $ombudsmanmeds = $this->Ombudsmanmeds->newEntities($saveOmbudsmanmeds);
                        $this->Ombudsmanmeds->saveMany($ombudsmanmeds);
                    }else {
                        $this->loadModel('Ombudsmanmeds');
                        $this->Ombudsmanmeds->deleteAll(['Ombudsmanmeds.ombudsman_id' => $ombudsman->id]);
                    }
                    if (!empty($this->request->data['ombudsmanlevels_mediation'])) {
//save languages
                        $this->loadModel('Ombudsmanlevels');
                        $this->Ombudsmanlevels->deleteAll(['Ombudsmanlevels.ombudsman_id' => $ombudsman->id]);

                        $saveLanguage = array();
                        foreach ($this->request->data['ombudsmanlevels_mediation'] as $n => $o)://change
                            $saveLanguage[$n]['ombudsman_id'] = $ombudsman->id;
                            $saveLanguage[$n]['language_id'] = $o;
                            $saveLanguage[$n]['language_level'] = $this->request->data['ombudsmanlevels_language_level'][$n];
                        endforeach;
//                    debug($saveLanguage);exit;
                        $saveLanguage = $this->Ombudsmanlevels->newEntities($saveLanguage);
                        $this->Ombudsmanlevels->saveMany($saveLanguage);
                    }
                    if (!empty($this->request->data['place'])) {
//save ombudsmanswears
                        $this->loadModel('Ombudsmanswears');
                        $this->Ombudsmanswears->deleteAll(['Ombudsmanswears.ombudsman_id' => $ombudsman->id]);

                        $saveOmbudsmanswears = array();
//                debug($this->request->data['ombudsmanswears']['place']);
                        foreach ($this->request->data['place'] as $p => $q)://change
                            $saveOmbudsmanswears[$p]['ombudsman_id'] = $ombudsman->id;
                            $saveOmbudsmanswears[$p]['place'] = $q;
                            $saveOmbudsmanswears[$p]['date_of_auth'] = ($this->request->data['date_of_auth'][$p]) ? date('Y-m-d', strtotime($this->request->data['date_of_auth'][$p])) : null;
                        endforeach;
                        $saveOmbudsmanswears = $this->Ombudsmanswears->newEntities($saveOmbudsmanswears);
//                    debug($saveOmbudsmanswears);
//                    exit;
                        $this->Ombudsmanswears->saveMany($saveOmbudsmanswears);
                    }
                    if (!empty($this->request->data['accre'])) {
//save ombudsmanaccreds
                        $this->loadModel('Ombudsmanaccreds');
                        $this->Ombudsmanaccreds->deleteAll(['Ombudsmanaccreds.ombudsman_id' => $ombudsman->id]);

                        $saveOmbudsmanaccreds = array();
//                debug($this->request->data['ombudsmanswears']['place']);
                        foreach ($this->request->data['accre'] as $t => $u)://change
                            $saveOmbudsmanaccreds[$t]['ombudsman_id'] = $ombudsman->id;
                            $saveOmbudsmanaccreds[$t]['accreditation_id'] = $u;
                        endforeach;
                        $saveOmbudsmanaccreds = $this->Ombudsmanaccreds->newEntities($saveOmbudsmanaccreds);
//                debug($saveOmbudsmanaccreds);
                        $this->Ombudsmanaccreds->saveMany($saveOmbudsmanaccreds);
                    }
//save ombudsmanformations
                    if (!empty($this->request->data['ombudsmanformations_formation'][0])) {
                        $this->loadModel('Ombudsmanformations');
                        $saveOmbudsmanformations = array();
                        $updateImages = array();

                        foreach ($this->request->data['image'] as $r => $s):
                            if ($s['name'] != "") {
                                $get_ext = pathinfo($s['name'], PATHINFO_EXTENSION);
                                $new_name = $r . '-' . date('ymdhis') . '.' . $get_ext;
                                $path = WWW_ROOT . 'img' . DS . 'formations' . DS . $new_name;
                                move_uploaded_file($s['tmp_name'], $path);
                            }

                            if (isset($this->request->data['ombudsmanformations_image_id'][$r])) {
                                $updateImages[$this->request->data['ombudsmanformations_image_id'][$r]]['ombudsman_id'] = $ombudsman->id;
                                $updateImages[$this->request->data['ombudsmanformations_image_id'][$r]]['formation'] = $this->request->data['ombudsmanformations_formation'][$r];
                                $updateImages[$this->request->data['ombudsmanformations_image_id'][$r]]['certificate_date'] = date('Y-m-d', strtotime($this->request->data['ombudsmanformations_date'][$r]));

                                if (empty($s['name'])) {
                                    $updateImages[$this->request->data['ombudsmanformations_image_id'][$r]]['copy_upload'] = $this->request->data['ombudsmanformations_image_name'][$r];
                                } else {
                                    $updateImages[$this->request->data['ombudsmanformations_image_id'][$r]]['copy_upload'] = $new_name;
                                }
                            } else {
                                $saveOmbudsmanformations[$r]['ombudsman_id'] = $ombudsman->id;
                                $saveOmbudsmanformations[$r]['formation'] = $this->request->data['ombudsmanformations_formation'][$r];
                                $saveOmbudsmanformations[$r]['certificate_date'] = date('Y-m-d', strtotime($this->request->data['ombudsmanformations_date'][$r]));
                                $saveOmbudsmanformations[$r]['copy_upload'] = $new_name;
                            }

                        endforeach;

//update records
                        if ($updateImages) {
                            $allRec = TableRegistry::get('Ombudsmanformations');
                            foreach ($updateImages as $oK => $uImage) {
//debug($oK);
                                $getRec = $allRec->get($oK);
                                $getRec = $this->Ombudsmanformations->patchEntity($getRec, $uImage);
                                $this->Ombudsmanformations->save($getRec);
                            }
                        }
//add records
                        if ($saveOmbudsmanformations) {
                            $saveOmbudsmanformations = $this->Ombudsmanformations->newEntities($saveOmbudsmanformations);
                            $this->Ombudsmanformations->saveMany($saveOmbudsmanformations);
                        }
                    }
                }
            }
//debug($number);exit;
            $this->request->data['User']['status'] = 2;
            $this->request->data['User']['role_id'] = 6;
            $this->loadModel('Registerations');
            $this->loadModel('Users');
            $registeration = $this->Registerations->get($id, ['contain' => ['Users']]);
            $user = $registeration->user;
            $user = $this->Users->patchEntity($user, $this->request->data['User'], [
                'validate' => 'UserCheck'
            ]);

            if ($result = $this->Users->save($user)) {
                $userStatus = $user->status;
                $committee = $this->Users->find('all', [
                            'conditions' => ['Users.role_id' => 9, 'Users.status' => 1]
                        ])->toArray();
                $this->loadModel('Emails');
                $emailTemplete = $this->Emails->find('all', array(
                            'conditions' => ['Emails.id' => '14'],
                            'contain' => ['Emailtypes']
                        ))->first();
                if ($userStatus == 1) {
                    foreach ($committee as $admin) {
                        $userName = $admin->first_name . " " . $admin->last_name;
                        $title = $emailTemplete['title'];
                        $from_person = 'info@cyberclouds.com';
                        $to_person = $admin->email;
                        $memberLink = Router::url(['controller' => 'Registerations', 'action' => 'index'], TRUE);

                        $subject = $emailTemplete['subject'];
                        $content = $emailTemplete['message'];
                        $message = "";
                        $message .= "<span style='background:#161616;display:block;padding:10px;'><img src='http://needanecommercesite.com/fgem/img/logo-light.png' style='display:block;margin:auto;max-width: 130px;'/></span>";
                        $trans = array("{first_name,last_name}" => $userName, "{first_name}" => $admin->first_name, "{last_name}" => $admin->last_name, "{first_name} {last_name}" => $userName, '{user_name}' => $user->first_name . ' ' . $user->last_name, '{link}' => $memberLink);
                        $message .= strtr($content, $trans);
                        $Email = new Email();
                        $Email->from(array($from_person => $title));
                        $Email->emailFormat('both');
                        $Email->to($to_person);
                        $Email->subject($subject);
                        $Email->send($message);
                    }
                }
                $this->Flash->success("Les informations ont été enregistrées.");
                return $this->redirect(['controller' => 'Registerations', 'action' => 'index']);
            }
            $this->Flash->error(__("Les informations sur la société n'ont pas pu être enregistrées. Veuillez réessayer."));
            return $this->redirect('/Registerations/saveBusinessInfo/' . $id);
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Registeration id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
//    regiseration of member can be deleted here
    public function delete($id = null) {
        $permission = $this->viewVars['actionPermission'];
        if ($permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }

        $this->set(compact('permission'));
        $this->request->allowMethod(['post', 'delete']);
        $registeration = $this->Registerations->get($id, ['contain' => ['Users']]);
        $user = $registeration->user;

        $this->loadModel('Users');
        $this->loadModel('Ombudsmans');
        $this->Ombudsmans->deleteAll(['Ombudsmans.registeration_id' => $id]);
        if ($this->Registerations->delete($registeration)) {
            $this->Users->delete($user);
            $this->Flash->success(__('L\'inscription a été supprimée.'));
        } else {
            $this->Flash->error(__('L\'enregistrement n\'a pas pu être supprimé. Veuillez réessayer.'));
        }

        return $this->redirect(['action' => 'index']);
    }

//this function contains a the members that are shown on front end and can be viewed by anyone
    public function viewMembers() {
        $this->set('myTitle', ' Annuaire');

        $members = $this->Registerations->find('all', [
                    'contain' => ['Accounts' => ['conditions' => ['Accounts.amount_paid >' => 0]], 'Users' => ['conditions' => ['Users.status' => 2]], 'Ombudsmans' => ['conditions' => ['Ombudsmans.mediator_question' => 'Oui']]],
//                    'conditions' => ['Users.status' => 2, 'Registerations.publicprofile' => 1]
                    'conditions' => ['Registerations.status' => 1, 'Registerations.publicprofile' => 1]
                ])->toArray();
//        debug(count($members));
//        exit;
//MEDIATION TYPE
        $this->loadModel('Medtypes');
        $mediations = $this->Medtypes->find('all', [
                ])->toArray();

//ACCREDITATIONS
        $this->loadModel('Accreditations');
        $accreds = $this->Accreditations->find('all', [
                ])->toArray();

//LANGUAGUES
        $this->loadModel('Languages');
        $languages = $this->Languages->find('list')->toArray();


        $this->set(compact('members', 'mediations', 'accreds', 'languages'));

        $this->viewBuilder()->layout('internal');
    }

    public function showFormationImage() {
        $this->autoRender = false;
        $id = $_GET['id'];

        $this->loadModel('Ombudsmanformations');

        $formation = $this->Ombudsmanformations->get($id);
        $image = $formation->copy_upload;
        $title = $formation->formation;

        $html = '';

        $html .= '<div class="modal_titles" style="">
                            <button type="button" class="close" data-dismiss="modal" style="">&times;</button>
                            <h5 class="modal-title">' . $title . '</h5>
                        </div>
                        <div class="modal-body ">
                            <div class="formation_image_div">
                                <img class="formation_image" src="' . $this->request->webroot . 'img/formations/' . $image . '" alt="" />
                            </div>
                        </div>';

        $responce = array('flag' => 'success', 'html' => $html);

        echo json_encode($responce);
        exit;
    }

    public function memberProfile() {
        $roleid = $this->request->session()->read('Auth.User.role_id');


        if ($roleid == 6) {

            $id = $this->request->session()->read('Auth.User.registeration.id');
            $userid = $this->request->session()->read('Auth.User.id');
            $registeration = $this->Registerations->get($id, [
                'contain' => ['Users']
            ]);
            $this->loadModel('Users');
            $user = $this->Users->get($userid, [
            ]);

//$registeration = $this->Registerations->newEntity();
            if ($this->request->is(['patch', 'post', 'put'])) {


                if ($this->request->data['image']['name'] != "") {

                    $ext = strrchr($this->request->data['image']['name'], '.');
                    $slider_image = $this->request->data['city'] . time() . $ext;
                    $source = $this->request->data['image']['tmp_name'];
                    $destination = WWW_ROOT . 'img' . DS . 'slider' . DS . $slider_image;
                    $thm_destination = WWW_ROOT . 'img' . DS . 'slider' . DS . 'thumbnail' . DS . $slider_image;
                    $this->request->data['image'] = $slider_image;
                } else {
                    unset($this->request->data['image']);
                }
                if (isset($this->request->data['tel1public']) == 'on') {
                    $tel1 = 1;
                } else {
                    $tel1 = 0;
                }
                if (isset($this->request->data['tel2public']) == 'on') {
                    $tel2 = 1;
                } else {
                    $tel2 = 0;
                }

                if (isset($this->request->data['emailpublic']) == 'on') {
                    $email = 1;
                } else {
                    $email = 0;
                }

                if (isset($this->request->data['publicprofile']) && $this->request->data['publicprofile'] == 'public') {
                    $profilepublic = 1;

//debug(1);
                } else {
                    $profilepublic = 0;
//debug(0);
                }

                $this->request->data['tel1public'] = $tel1;
                $this->request->data['tel2public'] = $tel2;
                $this->request->data['emailpublic'] = $email;
                $this->request->data['publicprofile'] = $profilepublic;

                $registeration = $this->Registerations->patchEntity($registeration, $this->request->getData(), [
                    'validate' => 'RegCheck'
                ]);

                $this->request->data['User']['first_name'] = $this->request->data['first_name'];
                $this->request->data['User']['last_name'] = $this->request->data['lastname'];
//$this->request->data['User']['role_id'] = $this->request->data['membertype_id'];
                $this->request->data['User']['password'] = $this->request->data['pwd'];
//                $this->request->data['User']['status'] = 1;
                if ($this->request->data['pwd'] == "") {
                    unset($this->request->data['User']['password']);
                }
                unset($this->request->data['User']['email']);
                $user = $this->Users->patchEntity($user, $this->request->data['User'], [
                ]);
                $this->Users->save($user);

                if ($this->Registerations->save($registeration)) {
                    if (!empty($this->request->data['image'])) {
                        $this->generate_thumb($source, $thm_destination, 400, 300);
                        $this->generate_thumb($source, $destination, 1200, 497);
                    }

                    $superAdmins = $this->Users->find('all', [
                                'conditions' => ['Users.role_id' => 1, 'Users.status' => 1]
                            ])->toArray();

//                    foreach($superAdmins as $admin){
//                        $email = $admin->email;
//                        $Email = new Email();
//                        $Email->from(array('info@cyberclouds.com' => 'FGEM'));
//                        $Email->emailFormat('both');
//                        $Email->to($email);
//                        $Email->subject('Member Profile | FGEM');
//
//                        
//                        $reset_token_link_admin = Router::url(['controller' => 'Users', 'action' => 'login'], TRUE);
//                        $message = 'Bonjour Admin,<br />Le membre obtenir le nom du membre ici a terminé le profil avec succès. S\'il vous plaît cliquez ici pour activer son compte.<br/><a href='.$reset_token_link_admin.'> '.$reset_token_link_admin.'</a><br/>Merci <br/>FGEM';
//
//                        $Email->send($message);
//
//                    }
//$this->Flash->success(__('Les informations de profil ont été enregistrées.'));

                    return $this->redirect(['controller' => 'Pages', 'action' => 'memberdashboard']);
                }
//$this->Flash->error(__('The profile could not be saved. Please, try again.'));
            }
            $membertypes = $this->Registerations->Membertypes->find('list', ['limit' => 200]);
            $this->set(compact('registeration', 'membertypes'));
            $this->set('_serialize', ['registeration']);
//   $this->viewBuilder()->setLayout('');
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);
        }

        $this->viewBuilder()->layout('memberlayout');
    }

    public function memberBusiness() {
//        $this->viewBuilder()->layout('internal');
        $id = $this->request->session()->read('Auth.User.registeration.id');

        $userId = $this->request->session()->read('Auth.User.id');
        $ombId = $this->request->session()->read('Auth.User.registeration');
        $ombId = $ombId['ombudsman']['id'];
        $this->loadModel('Users');
        $user = $this->Users->get($userId);
        $this->loadModel('Registerations');
        $registrations = $this->Registerations->find('all')->contain([
                    'Ombudsmans' => [
                        'Ombudsmanswears',
                        'Ombudsmanaccreds',
                        'Ombudsmanformations',
                        'Ombudsmanlevels',
                        'Ombudsmanmeds'
            ]])->where(['user_id' => $user->id])->first()->toArray();

        $this->loadModel('Ombudsmans');
        $ombudsman = $this->Ombudsmans->get($ombId);

        if ($this->request->is(['patch', 'post', 'put'])) {
//            debug($this->request->data);
//            exit;
// ombudsman entry
            $this->loadModel('Ombudsmans');
            $this->request->data['Ombudsmans']['registeration_id'] = $id;
            $this->request->data['Ombudsmans']['mediator_question'] = $this->request->data['mediator_question'];
            $this->request->data['Ombudsmans']['presentation_text'] = $this->request->data['presentation_text'];
            $this->request->data['Ombudsmans']['assermentation'] = $this->request->data['assermentation'];

            $ombudsman = $this->Ombudsmans->patchEntity($ombudsman, $this->request->data['Ombudsmans']);

            if ($this->Ombudsmans->save($ombudsman)) {

                if (!empty($this->request->data['ombudsmanmeds']['mediat'])) {
                    $this->loadModel('Ombudsmanmeds');
                    $this->Ombudsmanmeds->deleteAll(['Ombudsmanmeds.ombudsman_id' => $ombudsman->id]);

//save mediations
                    $saveOmbudsmanmeds = array();
                    foreach ($this->request->data['ombudsmanmeds']['mediat'] as $k => $om)://change
                        $saveOmbudsmanmeds[$k]['ombudsman_id'] = $ombudsman->id;
                        $saveOmbudsmanmeds[$k]['medtype_id'] = $om;
                    endforeach;
                    $ombudsmanmeds = $this->Ombudsmanmeds->newEntities($saveOmbudsmanmeds);
                    $this->Ombudsmanmeds->saveMany($ombudsmanmeds);
                }

                if (!empty($this->request->data['ombudsmanlevels_mediation'])) {
//save languages
                    $this->loadModel('Ombudsmanlevels');
                    $this->Ombudsmanlevels->deleteAll(['Ombudsmanlevels.ombudsman_id' => $ombudsman->id]);

                    $saveLanguage = array();
                    foreach ($this->request->data['ombudsmanlevels_mediation'] as $n => $o)://change
                        $saveLanguage[$n]['ombudsman_id'] = $ombudsman->id;
                        $saveLanguage[$n]['language_id'] = $o;
                        $saveLanguage[$n]['language_level'] = $this->request->data['ombudsmanlevels_language_level'][$n];
                    endforeach;
//                    debug($saveLanguage);exit;
                    $saveLanguage = $this->Ombudsmanlevels->newEntities($saveLanguage);
                    $this->Ombudsmanlevels->saveMany($saveLanguage);
                }



                if (!empty($this->request->data['place'])) {
//save ombudsmanswears
                    $this->loadModel('Ombudsmanswears');
                    $this->Ombudsmanswears->deleteAll(['Ombudsmanswears.ombudsman_id' => $ombudsman->id]);

                    $saveOmbudsmanswears = array();
//                debug($this->request->data['ombudsmanswears']['place']);
                    foreach ($this->request->data['place'] as $p => $q)://change
                        $saveOmbudsmanswears[$p]['ombudsman_id'] = $ombudsman->id;
                        $saveOmbudsmanswears[$p]['place'] = $q;
                        $saveOmbudsmanswears[$p]['date_of_auth'] = date('Y-m-d', strtotime($this->request->data['date_of_auth'][$p]));
                    endforeach;
                    $saveOmbudsmanswears = $this->Ombudsmanswears->newEntities($saveOmbudsmanswears);

                    $this->Ombudsmanswears->saveMany($saveOmbudsmanswears);
                }
                if (!empty($this->request->data['accre'])) {
//save ombudsmanaccreds
                    $this->loadModel('Ombudsmanaccreds');
                    $this->Ombudsmanaccreds->deleteAll(['Ombudsmanaccreds.ombudsman_id' => $ombudsman->id]);

                    $saveOmbudsmanaccreds = array();
//                debug($this->request->data['ombudsmanswears']['place']);
                    foreach ($this->request->data['accre'] as $t => $u)://change
                        $saveOmbudsmanaccreds[$t]['ombudsman_id'] = $ombudsman->id;
                        $saveOmbudsmanaccreds[$t]['accreditation_id'] = $u;
                    endforeach;
                    $saveOmbudsmanaccreds = $this->Ombudsmanaccreds->newEntities($saveOmbudsmanaccreds);
//                debug($saveOmbudsmanaccreds);
                    $this->Ombudsmanaccreds->saveMany($saveOmbudsmanaccreds);
                }
//save ombudsmanformations

                $this->loadModel('Ombudsmanformations');
                $saveOmbudsmanformations = array();
                $updateImages = array();
                foreach ($this->request->data['image'] as $r => $s):

                    $get_ext = pathinfo($s['name'], PATHINFO_EXTENSION);
                    $new_name = $r . '-' . date('ymdhis') . '.' . $get_ext;
                    $path = WWW_ROOT . 'img' . DS . 'formations' . DS . $new_name;
                    move_uploaded_file($s['tmp_name'], $path);

                    if (isset($this->request->data['ombudsmanformations_image_id'][$r])) {
                        $updateImages[$this->request->data['ombudsmanformations_image_id'][$r]]['ombudsman_id'] = $ombudsman->id;
                        $updateImages[$this->request->data['ombudsmanformations_image_id'][$r]]['formation'] = $this->request->data['ombudsmanformations_formation'][$r];
                        $updateImages[$this->request->data['ombudsmanformations_image_id'][$r]]['certificate_date'] = date('Y-m-d', strtotime($this->request->data['ombudsmanformations_date'][$r]));

                        if (empty($s['name'])) {
                            $updateImages[$this->request->data['ombudsmanformations_image_id'][$r]]['copy_upload'] = $this->request->data['ombudsmanformations_image_name'][$r];
                        } else {
                            $updateImages[$this->request->data['ombudsmanformations_image_id'][$r]]['copy_upload'] = $new_name;
                        }
                    } else {
                        $saveOmbudsmanformations[$r]['ombudsman_id'] = $ombudsman->id;
                        $saveOmbudsmanformations[$r]['formation'] = $this->request->data['ombudsmanformations_formation'][$r];
                        $saveOmbudsmanformations[$r]['certificate_date'] = date('Y-m-d', strtotime($this->request->data['ombudsmanformations_date'][$r]));
                        $saveOmbudsmanformations[$r]['copy_upload'] = $new_name;
                    }

                endforeach;
//update records
                if ($updateImages) {
                    $allRec = TableRegistry::get('Ombudsmanformations');
                    foreach ($updateImages as $oK => $uImage) {
//debug($oK);
                        $getRec = $allRec->get($oK);
                        $getRec = $this->Ombudsmanformations->patchEntity($getRec, $uImage);
                        $this->Ombudsmanformations->save($getRec);
                    }
                }
//add records
                if ($saveOmbudsmanformations) {
                    $saveOmbudsmanformations = $this->Ombudsmanformations->newEntities($saveOmbudsmanformations);
                    $this->Ombudsmanformations->saveMany($saveOmbudsmanformations);
                }
            }
//debug($number);exit;
            $this->request->data['User']['status'] = 2;
            $this->request->data['User']['role_id'] = $this->request->session()->read('Auth.User.role_id');
            $user = $this->Users->patchEntity($user, $this->request->data['User'], [
                'validate' => 'UserCheck'
            ]);

            if ($result = $this->Users->save($user)) {

                $this->Flash->success("Les informations ont été enregistrées.");
                return $this->redirect(['controller' => 'pages', 'action' => 'memberdashboard']);
            }
            $this->Flash->error(__("Les informations sur la société n'ont pas pu être enregistrées. Veuillez réessayer."));
        }

        $this->loadModel('Medtypes');
        $medtype = $this->Medtypes->find('all')->toArray();

        $this->loadModel('Accreditations');
        $acc = $this->Accreditations->find('all')->toArray();
//debug($medtype);exit;
        $this->loadModel('Languages');
        $languages = $this->Languages->find('list')->toArray();
//        debug($languages);exit;
        $this->set('user', $user);
//        debug($registrations);exit;
        $this->set(compact('languages', 'registrations'));
        $this->set('acc', $acc);
        $this->set('medtype', $medtype);
        $this->viewBuilder()->layout('memberlayout');
    }

}
