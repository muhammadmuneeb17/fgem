<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TicketCalendars Controller
 *
 * @property \App\Model\Table\TicketCalendarsTable $TicketCalendars
 *
 * @method \App\Model\Entity\TicketCalendar[] paginate($object = null, array $settings = [])
 */
class TicketCalendarsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $ticketCalendars = $this->paginate($this->TicketCalendars);

        $this->set(compact('ticketCalendars'));
        $this->set('_serialize', ['ticketCalendars']);
    }

    /**
     * View method
     *
     * @param string|null $id Ticket Calendar id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ticketCalendar = $this->TicketCalendars->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('ticketCalendar', $ticketCalendar);
        $this->set('_serialize', ['ticketCalendar']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ticketCalendar = $this->TicketCalendars->newEntity();
        if ($this->request->is('post')) {
            $ticketCalendar = $this->TicketCalendars->patchEntity($ticketCalendar, $this->request->getData());
            if ($this->TicketCalendars->save($ticketCalendar)) {
                $this->Flash->success(__('The ticket calendar has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ticket calendar could not be saved. Please, try again.'));
        }
        $users = $this->TicketCalendars->Users->find('list', ['limit' => 200]);
        $this->set(compact('ticketCalendar', 'users'));
        $this->set('_serialize', ['ticketCalendar']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ticket Calendar id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ticketCalendar = $this->TicketCalendars->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ticketCalendar = $this->TicketCalendars->patchEntity($ticketCalendar, $this->request->getData());
            if ($this->TicketCalendars->save($ticketCalendar)) {
                $this->Flash->success(__('The ticket calendar has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ticket calendar could not be saved. Please, try again.'));
        }
        $users = $this->TicketCalendars->Users->find('list', ['limit' => 200]);
        $this->set(compact('ticketCalendar', 'users'));
        $this->set('_serialize', ['ticketCalendar']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ticket Calendar id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ticketCalendar = $this->TicketCalendars->get($id);
        if ($this->TicketCalendars->delete($ticketCalendar)) {
            $this->Flash->success(__('The ticket calendar has been deleted.'));
        } else {
            $this->Flash->error(__('The ticket calendar could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
