<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;
use Cake\Routing\Router;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController {

    public function isAuthorized($user) {
        if (in_array($this->request->action, ['edit'])) {
            $id = (int) $this->request->params['pass'][0];
            if ($id == $user['id']) {
                return true;
            }
        }
        if (in_array($this->request->action, ['logout'])) {
            return true;
        }
        // Admin can access every action
        if ($this->viewVars['actionPermission'] != "") {
            return true;
        } else {
            return false;
        }
    }

    public function formation() {
        $city = $_GET['field'];
        $this->loadModel('Formations');
        $cities = $this->Formations->find('list', ['keyField' => 'id', 'valueField' => 'name', 'conditions' => ['name LIKE' => '%' . $city . '%'], 'order' => 'name'])
                ->group('name')
                ->toArray();
        echo json_encode($cities);
        exit;
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    // the listing of all admin users are done here
    public function index() {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 1 || $permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $this->paginate = [
            'contain' => ['Roles']];
        $users = $this->paginate($this->Users);
        //debug($users);exit;

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
        $this->viewBuilder()->setLayout('backend');
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 1 || $permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    // this function is for adding users from backend
    public function add() {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $this->loadModel('Roles');
        $roleList = $this->Roles->find('list', [
                    'keyField' => 'id',
                    'valueField' => 'name',
                    'conditions' => [
                    ]
                ])->toArray();

        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {

            $this->request->data['User']['first_name'] = $this->request->data['first_name'];
            $this->request->data['User']['last_name'] = $this->request->data['last_name'];
            $this->request->data['User']['user_name'] = $this->request->data['user_name'];
            $this->request->data['User']['email'] = $this->request->data['email'];
            $this->request->data['User']['password'] = $this->request->data['password'];
            $this->request->data['User']['role_id'] = $this->request->data['role_id'];
            $this->request->data['User']['status'] = $this->request->data['status'];
            $user = $this->Users->patchEntity($user, $this->request->getData());
            //debug($user);exit;
            if ($this->Users->save($user)) {
                $this->Flash->success("L'utilisateur a été enregistré");

                return $this->redirect(['action' => 'index']);
            }
            if ($user->errors()) {

                $model_error = $user->errors();
                if ($model_error['user_name']['_isUnique'] != "") {

                    $this->Flash->error(__($model_error['user_name']['_isUnique']));
                } elseif ($model_error['email']['_isUnique'] != "") {
                    $this->Flash->error(__($model_error['email']['_isUnique']));
                }
            } else {
                $this->Flash->error(__('L\'utilisateur n\'a pas pu être enregistré. Veuillez réessayer.'));
            }
        }
        $this->set(compact('user'));
        $this->set(compact('roleList'));
        $this->set('_serialize', ['user']);
        $this->viewBuilder()->setLayout('backend');
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    // this method will edit our backend users
    public function edit($id = null) {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 2 || $id = $this->request->session()->read('Auth.User.id')) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));

        $roleList = $this->Roles->find('list', [
                    'keyField' => 'id',
                    'valueField' => 'name'
                ])->toArray();
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            if ($this->request->data['password'] == "") {
                unset($this->request->data['password']);
            }
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('L\'utilisateur a été enregistré.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('L\'utilisateur n\'a pas pu être enregistré. Veuillez réessayer.'));
        }
        $this->set(compact('user'));
        $this->set(compact('roleList'));
        $this->set('_serialize', ['user']);
        $this->viewBuilder()->setLayout('backend');
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 2) {
            
        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));

        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('L\'utilisateur a été supprimé.'));
        } else {
            $this->Flash->error(__('L\'utilisateur n\'a pas pu être supprimé. Veuillez réessayer.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /*
     * Admin Login 
     */

    public function login() {
        if ($this->request->is('Post')) {
            $user = $this->Auth->identify();
//            debug($user);
//            exit;
            if ($user) {
                if ($user['status'] == 0) {
                    $this->Flash->error('Your account has been deactivated, contact web admin');
                    return $this->redirect($this->Auth->logout());
                }
                $this->Auth->setUser($user);
                $session = $this->request->session();
                $session->write('KCFINDER.disabled', false);
                //$session = $this->request->session();
                //$session->write('User.id', $user['id']);
                $this->Flash->success(__('Connecté avec succès'));

                if ($user['role_id'] == 9) {
                    return $this->redirect(['controller' => 'Registerations', 'action' => 'index']);
                } else {
                    return $this->redirect(['controller' => 'Pages', 'action' => 'dashboard']);
                }
            }
            $this->Flash->error('Nom d\'utilisateur ou mot de passe invalide');
        }


        $this->viewBuilder()->setLayout('');
    }

// customer(member) can be login from this function
    public function customerLogin($check = null) {
        if ($check && $check == 'verified') {
            $message = "Félicitations ! Votre compte a été créé avec succès.<br />Nous vous prions maintenant de compléter votre profil afin que nous puissions traiter votre demande d’adhésion. Si elle est acceptée vous serez invité à procéder au paiement de votre cotisation. Dès que nous l’aurons reçue vous aurez accès à toute les prestations que nous offrons à nos membres et, si vous en avez fait le choix, votre profil sera publié dans l’annuaire des médiateurs de la Fédération.";
        }
        $this->set('myTitle', 'Members');
        $this->set(compact('message'));
        $this->viewBuilder()->setLayout('internal');
        if ($this->request->is('Post')) {

            //login by Email address
            $this->Auth->config('authenticate', [
                'Form' => [
                    'fields' => ['username' => 'email']
                ]
            ]);
            $this->Auth->constructAuthenticate();
            $user = $this->Auth->identify();


            if ($user) {
                $roleId = $user['role_id'];
                $this->loadModel('Roles');
                $userRole = $this->Roles->get($roleId);
                //                if ($user['status'] == 1 && $user['registeration']['status'] == 0) {
                //                    $this->Flash->error("Désolé, vous ne pouvez pas vous connecter maintenant, notre membre de l'équipe vous contactera sous peu pour activer votre compte.");
                //                    return $this->redirect('/');
                //                }
                ////                 else
                ////                if ($user['status'] == 0 && $user['registeration']['status'] == 0 &&  !empty($user['verified'])) {
                ////                    $this->Flash->error('A verification link has been sent on your email , Please check');
                ////                    return $this->redirect('/');
                ////                }
                //                else
                //                if ($user['status'] == 0 && $user['registeration']['status'] == 0 &&  empty($user['verified'])) {
                //                    $this->Flash->error('Sorry you cannot login right now , Please Registered with us first');
                //                    return $this->redirect('/');
                //                }
                //                else
                //                if ($user['status'] == 0 && $user['registeration']['status'] == 0) {
                //                    $this->Flash->error("Désolé, votre compte a été suspendu. Veuillez contacter le service clientèle.");
                //                    return $this->redirect('/');
                //                }
                //                else
                //                if ($user['registeration']['status'] == 0) {
                //                    $this->Flash->error('Sorry you cannot login right now , our team member will contact you shortly to active your account');
                //                    return $this->redirect('/');
                //                }
                //                 if ($user['status'] == 1 && $user['registeration']['status'] == 0) {
                //                    $this->Flash->error("Désolé, vous ne pouvez pas vous connecter maintenant, notre membre de l'équipe vous contactera sous peu pour activer votre compte.");
                //                    return $this->redirect('/');
                //                }
                //                 else
                //                if ($user['status'] == 0 && $user['registeration']['status'] == 0 &&  !empty($user['verified'])) {
                //                    $this->Flash->error('A verification link has been sent on your email , Please check');
                //                    return $this->redirect('/');
                //                }
                //else

                if (($user['status'] == 0 && $user['registeration']['status'] == 0) || $user['suspend'] == 1) {
                    $this->Flash->error("Désolé, votre compte a été suspendu. Veuillez contacter le Service Support.");
                    return $this->redirect('/');
                } else if ($user['status'] == 0 && $user['registeration']['status'] == 0 && !empty($user['verified'])) {
                    $this->Flash->error('Une confirmation a été envoyée à votre adresse email. Merci de bien vouloir suivre les indications se trouvant dans le email pour valider la création de votre compte.');
                    return $this->redirect('/');
                } else if ($user['status'] == 0 && empty($user['verified'])) {
                    $this->Flash->error('Désolé, vous ne pouvez pas vous connecter maintenant, s\'il vous plaît enregistré avec nous d\'abord');
                    return $this->redirect('/');
                }

                //                else
                //                if ($user['registeration']['status'] == 0) {
                //                    $this->Flash->error('Sorry you cannot login right now , our team member will contact you shortly to active your account');
                //                    return $this->redirect('/');
                //                }

                $this->Auth->setUser($user);
                $session = $this->request->session();

                //STORE LOGS
                $this->loadModel('Logs');

                $userId = $this->request->session()->read('Auth.User.id');
                $roleId = $this->request->session()->read('Auth.User.role_id');

                $findRole = $this->Users->get($userId, [
                    'contain' => ['Roles']
                ]);

                $roleName = $findRole->role->name;
                $activity = $roleName . ' Login';

                $note = 'User logged in with the role of ' . $roleName;

                $logs = $this->Logs->newEntity();
                $this->request->data['Log']['user_id'] = $userId;
                $this->request->data['Log']['activity'] = $activity;
                $this->request->data['Log']['note'] = $note;

                $logs = $this->Logs->patchEntity($logs, $this->request->data['Log']);

                if ($this->Logs->save($logs)) {
                    //debug($user);exit;
                    if ($user['status'] == 2) {
                        return $this->redirect('/pages/memberdashboard');
                    } else if ($user['status'] == 1) {
                        if ($user['role_id'] == 10) {
                            return $this->redirect('/Tickets');
                        } else {
                            return $this->redirect('/Registerations/customerprofile');
                        }
                    }
                }
            }
            $this->Flash->error('Identifiant ou mot de passe incorrect !');
            //            $this->viewBuilder()->setLayout('login');
        }
    }

    public function logout() {
        $session = $this->request->session();
        $session->write('KCFINDER.disabled', true);
        //STORE LOGS
        $this->loadModel('Logs');

        $userId = $this->request->session()->read('Auth.User.id');
        $roleId = $this->request->session()->read('Auth.User.role_id');

        $findRole = $this->Users->get($userId, [
            'contain' => ['Roles']
        ]);

        $roleName = $findRole->role->name;
        $activity = $roleName . ' Logout';

        $note = 'User logged out with the role of ' . $roleName;

        $logs = $this->Logs->newEntity();
        $this->request->data['Log']['user_id'] = $userId;
        $this->request->data['Log']['activity'] = $activity;
        $this->request->data['Log']['note'] = $note;

        $logs = $this->Logs->patchEntity($logs, $this->request->data['Log']);

        if ($this->Logs->save($logs)) {
            
        }
        $this->Flash->success(__('Déconnecté avec succès'));

        $this->Auth->logout();
        return $this->redirect('/');
    }

    public function forgotPassword() {
        if ($this->request->is('post')) {

            if (!empty($this->request->data)) {
                $email = $this->request->data['email'];

                $user = $this->Users->findByEmail($email)->first();
                //debug($user);exit;
                $isSuccess = '';
                if (!empty($user)) {
                    $passkey = uniqid();
                    $url = Router::Url(['controller' => 'users', 'action' => 'reset'], true) . '/' . $passkey;

                    $user->password_reset_token = $passkey;
                    $reset_token_link = Router::url(['controller' => 'Users', 'action' => 'resetPassword'], TRUE) . '/' . $passkey;

                    $this->loadModel('Emails');
                    $emailTemplete = $this->Emails->find('all', array(
                                'conditions' => ['Emails.id' => '16'],
                                'contain' => ['Emailtypes']
                            ))->first();
                    $userName = $user->first_name . " " . $user->last_name;
                    $title = $emailTemplete['title'];
                    $from_person = 'info@cyberclouds.com';
                    $to_person = $user->email;
                    $subject = $emailTemplete['subject'];
                    $content = $emailTemplete['message'];
                    $message = "";
                    $message .= "<span style='background:#161616;display:block;padding:10px;'><img src='http://needanecommercesite.com/fgem/img/logo-light.png' style='display:block;margin:auto;max-width: 130px;'/></span>";
                    $trans = array("{first_name,last_name}" => $userName, "{first_name}" => $user->first_name, "{last_name}" => $user->last_name, "{first_name} {last_name}" => $userName, '{link}' => $reset_token_link);
                    $message .= strtr($content, $trans);
                    $Email = new Email();
                    $Email->from(array($from_person => $title));
                    $Email->emailFormat('both');
                    $Email->to($to_person);
                    $Email->subject($subject);
                    $Email->send($message);
                    $this->Users->save($user);

                    $isSuccess = array('success' => 1, 'message' => "Pour réinitialiser votre mot de passe, nous vous invitons à suivre les instructions se trouvant dans le message que nous avons envoyé à l’adresse mail que vous nous aviez communiqué. <br />En cas de problème n’hésitez pas à nous contacter en nous écrivant à <a href='mailto:support@fgem.ch'>support@fgem.ch</a> et pensez à inclure vos coordonnées pour que nous puissions vous appeler.");
                } else {
                    $isSuccess = array('success' => 0, 'message' => " L'adresse email que vous avez saisie n'est pas enregistrée chez nous. Merci de la vérifier et de recommencer. En cas de problème, merci d'envoyer un message à <a href='mailto:support@fgem.ch'>support@fgem.ch</a>");
                }
            }
        }

        $this->set(compact('isSuccess'));
        $this->viewBuilder()->setLayout('internal');
    }

    public function resetPassword($token = null) {
        if (!empty($token)) {
            $user = $this->Users->findByPasswordResetToken($token)->first();

            if ($user) {

                if (!empty($this->request->data)) {
                    $user = $this->Users->patchEntity($user, [
                        'password' => $this->request->data['new_password'],
                        'new_password' => $this->request->data['new_password'],
                        'confirm_password' => $this->request->data['confirm_password']
                            ], ['validate' => 'password']
                    );


                    $user->password_reset_token = "";

                    if ($this->Users->save($user)) {
                        $this->Flash->success("Votre mot de passe a été changé avec succès");
                        $this->redirect(['action' => 'customerLogin']);
                    } elseif ($user->errors()) {
                        $model_error = $user->errors();
                        if ($model_error['new_password']['length'] != "") {
                            $this->Flash->error(__($model_error['new_password']['length']));
                        } elseif ($model_error['new_password']['match'] != "") {
                            $this->Flash->error(__($model_error['new_password']['match']));
                        } elseif ($model_error['confirm_password']['length'] != "") {
                            $this->Flash->error(__($model_error['confirm_password']['length']));
                        } elseif ($model_error['confirm_password']['match'] != "") {
                            $this->Flash->error(__($model_error['confirm_password']['match']));
                        }
                    } else {
                        $this->Flash->error("Erreur lors de la modification du mot de passe. Veuillez réessayer!");
                    }
                }
            } else {
                $this->Flash->error('Désolé, votre mot de passe a expiré.');
            }
        } else {
            $this->Flash->error('Erreur lors du chargement de la réinitialisation du mot de passe.');
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
        if ($user->role_id == 6) {
            $this->viewBuilder()->setLayout('internal');
        } else {
            $this->viewBuilder()->setLayout('login');
        }
    }

    public function verified($token = null) {
        if (!empty($token)) {

            $user = $this->Users->find('all', array(
                        'contain' => 'Registerations',
                        'conditions' => ['Users.verified' => $token],
                    ))->first();


            if ($user) {

                if (!empty($user['verified'])) {
                    $user = $this->Users->patchEntity($user, [
                        'status' => 1,
                    ]);

                    if ($this->Users->save($user)) {
                        $this->redirect(['controller' => 'Users', 'action' => 'customer_login', 'verified']);
                    } else {
                        $this->Flash->error('Error Please try again!');
                    }
                }
            } else {
                $this->Flash->error('Sorry your code has been expired.');
            }
        } else {
            $this->Flash->error('Error loading verification process.');
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
        $this->viewBuilder()->setLayout('login');
    }

}
