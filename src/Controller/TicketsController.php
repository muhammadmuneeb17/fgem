<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;
use Imap;
use Cake\Datasource\ConnectionManager;

/**
 * Tickets Controller
 *
 * @property \App\Model\Table\TicketsTable $Tickets
 *
 * @method \App\Model\Entity\Ticket[] paginate($object = null, array $settings = [])
 */
class TicketsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function isAuthorized($user) {

        // Admin can access every action
        $allowAdmin = array('deleteScedule', 'ticketHandlers', 'viewTicket', 'allTickets', 'editTicket', 'changeUser', 'deleteTicket', 'getMessage');
        $allowFrontEndUsers = array('add', 'index', 'viewMessages');
        if (in_array($this->request->params['action'], $allowFrontEndUsers) && $user['role_id'] == 10) {
            return true;
        }
        if (in_array($this->request->params['action'], $allowAdmin) && ($user['role_id'] == 6 || $user['role_id'] == 1)) {
            return true;
        }
        return false;
    }

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['create', 'newtickets', 'getMessage']);
    }

    public function index() {
        $this->set('contentTitle', 'Mes billets');

        $this->viewBuilder()->layout('customer_layout');
        $this->paginate = [
            'conditions' => ['user_id' => $this->Auth->user('id')]
        ];
        if ($this->request->is('post')) {
            $this->loadModel('TicketConversations');
            $ticket = $this->Tickets->get($this->request->data['ticketId']);
            $conversation = $this->TicketConversations->newEntity();
            $conversation->user_id = $this->Auth->user('id');
            $conversation->created_by = $this->Auth->user('id');
            $conversation->ticket_id = $this->request->data['ticketId'];
            $conversation->message = $this->request->data['message'];

            //assign ticket to user
            $newDate = date('Y-m-d');
            $this->loadModel('TicketCalendars');
            $tcals = $this->TicketCalendars->find('all')
                    ->contain(['WeekHolidays' => function($q) use($newDate) {
                            $q->where(['WeekHolidays.date' => $newDate]);
                            return $q;
                        }])
                    ->where(['TicketCalendars.from_date <=' => $newDate, 'TicketCalendars.to_date >=' => $newDate])
                    ->order(['TicketCalendars.type'])
                    ->toArray();
            if (count($tcals) == 0) {
                $userId = '';
            }
            if (count($tcals) == 1) {
                if (empty($tcals[0]->week_holidays)) {
                    $userId = $tcals[0]->user_id;
                } else {
                    $userId = '';
                }
            }
            if (count($tcals) == 2) {
                foreach ($tcals as $tcal) {
                    if ($tcal->type == 1) {
                        // if primary is not available or is on holiday
                        if (empty($tcal->week_holidays)) {
                            $userId = $tcal->user_id;
                            break;
                        }
                    } else {
                        // if secondary is not available then put condition here
                        $userId = $tcal->user_id;
                    }
                }
            }
            $conversation->assigned_to = $userId;

            if ($this->TicketConversations->save($conversation)) {
                $getMessages = $this->Tickets->get($ticket->id, ['contain' => 'TicketConversations']);
                $this->loadModel('Users');
                if ($userId) {
                    $user = $this->Users->get($userId);
                    $emailToTalent = new Email();
                    $emailToTalent->template('ticket_created')
                            ->emailFormat('html')->to($user['email'])
                            ->from(['no-reply@cyberclouds.com' => 'FGEM'])
                            ->subject('New Ticket created')
                            ->viewVars(['content' => $user, 'messages' => $getMessages])
                            ->send();
                } else {
                    $users = $this->Users->find('all')
                            ->where(['role_id' => 9])
                            ->toArray();
                    foreach ($users as $user):
                        $emailToTalent = new Email();
                        $emailToTalent->template('ticket_created')
                                ->emailFormat('html')->to($user['email'])
                                ->from(['no-reply@cyberclouds.com' => 'FGEM'])
                                ->subject('New Ticket created')
                                ->viewVars(['content' => $user, 'messages' => $getMessages])
                                ->send();
                    endforeach;
                }
            }
            $this->Flash->success("Nouveau message a été créé");
            return $this->redirect(['action' => 'index']);
        }

        $tickets = $this->paginate($this->Tickets);
        $this->loadModel('Users');
        $users = $this->Users->find('list', ['keyField' => 'id', 'valueField' => function ($row) {
                        return $row['first_name'] . ' ' . $row['last_name'];
                    }])
                ->toArray();
        $this->set(compact('tickets', 'users'));
        $this->set('_serialize', ['tickets']);
    }

    public function viewMessages() {
        $this->loadModel('TicketConversations');
        $conversations = $this->TicketConversations->find('all')
                ->where(['Ticket_id' => $_GET['ticketId']])
                ->order(['id DESC'])
                ->toArray();
        $html = '<table class="table table-responsive table-striped table-bordered">';
        foreach ($conversations as $conversation):
            $class = 'adminResponse';
            if ($conversation->created_by == $this->Auth->user('id')) {
                $class = 'customerResponse';
            }
            $html .= '<tr class="' . $class . '"><td>' . $conversation->message . '</td></tr>';
        endforeach;
        $html .= '</table>';
        echo $html;

        exit;
    }

    /**
     * View method
     *
     * @param string|null $id Ticket id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function viewTicket($id = null) {

        $this->viewBuilder()->layout('memberlayout');
        $ticket = $this->Tickets->get($id, [
            'contain' => ['Users', 'TicketConversations' => function($q) {
                    $q->order(['id DESC']);
                    return $q;
                }]
        ]);

        if ($this->request->is('post')) {

            $this->loadModel('TicketConversations');
            $ticket = $this->Tickets->get($this->request->data['ticketId']);
            $conversation = $this->TicketConversations->newEntity();
            $conversation->user_id = $ticket->user_id;
            $conversation->created_by = $this->Auth->user('id');
            $conversation->ticket_id = $this->request->data['ticketId'];
            $conversation->message = $this->request->data['message'];
            $conversation->assigned_to = $this->Auth->user('id');

            if ($this->TicketConversations->save($conversation)) {
                $getMessages = $this->Tickets->get($ticket->id, ['contain' => ['TicketConversations', 'Users']]);
                $this->loadModel('Users');
                $user = $getMessages->user;
                $emailToTalent = new Email();
                $emailToTalent->template('ticket_created')
                        ->emailFormat('html')->to($user['email'])
                        ->from(['no-reply@cyberclouds.com' => 'FGEM'])
                        ->subject('Ticket Responce')
                        ->viewVars(['content' => $user, 'messages' => $getMessages])
                        ->send();
            }
            $this->Flash->success('New Message has been created');
            return $this->redirect(['action' => 'viewTicket', $id]);
        }

        $this->loadModel('Users');
        $users = $this->Users->find('list', ['keyField' => 'id', 'valueField' => function ($row) {
                        return $row['first_name'] . ' ' . $row['last_name'];
                    }])
                ->toArray();
//        debug($users);
//        exit;
        $this->set(compact('ticket', 'users'));
        $this->set('_serialize', ['ticket']);
    }

    /* this function is for displaying tickets to
     * supervisor:
     * a supervisor can filter out tickets and can assign tickets to other members
     * members withe the active accounts etc
     */

    public function allTickets() {
        $this->set('contentTitle', 'Demandes reçues');
        if ($this->Auth->user('role_id') == 1) {
            $this->viewBuilder()->layout('backend');
        } else {
            $this->viewBuilder()->layout('memberlayout');
        }

        $this->loadModel('Tickets');

        $cond = array();
        if ($this->request->is('post')) {

            if (!empty($this->request->data['from_date']) && !empty($this->request->data['to_date'])) {
                $fromDate = date('Y-m-d', strtotime($this->request->data['from_date']));
                $toDate = date('Y-m-d', strtotime($this->request->data['to_date']));
                $cond = array('Tickets.created >=' => $fromDate, 'Tickets.created <=' => $toDate);
            }
            if (!empty($this->request->data['user'])) {
                $cond['CONCAT(Managers.first_name," ",Managers.last_name) LIKE'] = '%' . $this->request->data['user'] . '%';
            }
            if (!empty($this->request->data['type'])) {
                $cond['Tickets.status'] = $this->request->data['type'];
            }
            if (!empty($this->request->data['type_of_message'])) {
                $cond['Tickets.type_of_message'] = $this->request->data['type_of_message'];
            }
        }
        if (empty($this->viewVars['superviser'])) {
            $cond = array('Tickets.assigned_to' => $this->Auth->user('id'));
        }
        $this->loadModel('Users');
        $users = $this->Users->find('list', ['keyField' => 'id', 'valueField' => function($row) {
                        return $row['first_name'] . ' ' . $row['last_name'];
                    }])->where(['role_id' => 6])->toArray();

        $this->paginate = ['contain' => ['Managers'],
            'conditions' => $cond
        ];
        $tickets = $this->paginate($this->Tickets);
        $this->set(compact('tickets', 'users'));
//        debug($tickets);
//        exit;
    }

    public function ticketHandlers() {
        $this->set('contentTitle', 'Demandes reçues');
        if ($this->Auth->user('role_id') == 1) {
            $this->viewBuilder()->layout('backend');
        } else {
            $this->viewBuilder()->layout('memberlayout');
        }

        $this->loadModel('Tickets');
        $conn = ConnectionManager::get('default');
        $cond = array();
        if ($this->request->is('post')) {

            if (!empty($this->request->data['from_date']) && !empty($this->request->data['to_date'])) {
                $fromDate = date('Y-m-d', strtotime($this->request->data['from_date']));
                $toDate = date('Y-m-d', strtotime($this->request->data['to_date']));
                $cond = array('Tickets.created >=' => $fromDate, 'Tickets.created <=' => $toDate);
            }
            if (!empty($this->request->data['user'])) {
                $cond['CONCAT(Managers.first_name," ",Managers.last_name) LIKE'] = '%' . $this->request->data['user'] . '%';
            }
            if (!empty($this->request->data['type'])) {
                $cond['Tickets.status'] = $this->request->data['type'];
            }
            if (!empty($this->request->data['type_of_message'])) {
                $cond['Tickets.type_of_message'] = $this->request->data['type_of_message'];
            }
        }
        if (empty($this->viewVars['superviser'])) {
            $cond = array('Tickets.assigned_to' => $this->Auth->user('id'));
        }
        $this->loadModel('TicketCalendars');
        $query = $conn->execute('SELECT users.id as user_id,CONCAT(first_name, " ",last_name) as name,'
                . '(SELECT count(*) FROM ticket_calendars WHERE ticket_calendars.user_id=users.id AND type=1) as as_primary,'
                . '(SELECT count(*) FROM ticket_calendars WHERE ticket_calendars.user_id=users.id AND type=2) as as_secondary,'
                . '(SELECT ticket_calendars.from_date FROM ticket_calendars WHERE ticket_calendars.user_id=users.id ORDER BY ticket_calendars.from_date LIMIT 1) as available_from,'
                . '(SELECT ticket_calendars.to_date FROM ticket_calendars WHERE ticket_calendars.user_id=users.id AND ticket_calendars.to_date < "' . date("Y-m-d") . '" ORDER BY ticket_calendars.to_date DESC LIMIT 1) as last_schedule, '
                . '(SELECT ticket_calendars.from_date FROM ticket_calendars WHERE ticket_calendars.user_id=users.id AND ticket_calendars.from_date > "' . date("Y-m-d") . '" ORDER BY ticket_calendars.from_date LIMIT 1) as next_schedule '
                . ' FROM ticket_calendars '
                . 'join users ON ticket_calendars.user_id=users.id '
                . ' group by users.id');

        $tickets = $query->fetchAll('assoc');

        $this->set(compact('tickets', 'users'));
//        debug($tickets);
//        exit;
    }

    public function create() {
        $this->viewBuilder()->layout('internal');
        $ticket = $this->Tickets->newEntity();
        if ($this->request->is('post')) {
            if ($this->request->is('post')) {
                $newDate = date('Y-m-d');
                $this->loadModel('TicketCalendars');
                $tcals = $this->TicketCalendars->find('all')
                        ->contain(['WeekHolidays' => function($q) use($newDate) {
                                $q->where(['WeekHolidays.date' => $newDate]);
                                return $q;
                            }])
                        ->where(['TicketCalendars.from_date <=' => $newDate, 'TicketCalendars.to_date >=' => $newDate])
                        ->order(['TicketCalendars.type'])
                        ->toArray();
                if (count($tcals) == 0) {
                    $userId = '';
                }
                if (count($tcals) == 1) {
                    if (empty($tcals[0]->week_holidays)) {
                        $userId = $tcals[0]->user_id;
                    } else {
                        $userId = '';
                    }
                }
                if (count($tcals) == 2) {
                    foreach ($tcals as $tcal) {
                        if ($tcal->type == 1) {
                            // if primary is not available or is on holiday
                            if (empty($tcal->week_holidays)) {
                                $userId = $tcal->user_id;
                                break;
                            }
                        } else {
                            // if secondary is not available then put condition here
                            $userId = $tcal->user_id;
                        }
                    }
                }
                $this->request->data['assigned_to'] = $userId;
//                $this->request->data['user_id'] = $this->Auth->user('id');
                $this->request->data['type_of_message'] = 'web';
                $this->request->data['message'] = trim($this->request->data['message']);
                $this->request->data['status'] = 1;

                $ticket = $this->Tickets->patchEntity($ticket, $this->request->getData());
//                debug($ticket);
//            exit;
                if ($this->Tickets->save($ticket)) {
                    $digits = 8 - strlen($ticket->id);
                    $random_code = rand(pow(10, $digits - 1), pow(10, $digits) - 1);

                    $ticket_number = $ticket->id . $random_code;
                    $ticket->ticket_number = $ticket_number;
                    $this->Tickets->save($ticket);

                    $getMessages = $this->Tickets->get($ticket->id, ['contain' => 'TicketConversations']);
                    $this->loadModel('Users');
                    if ($userId) {
                        $user = $this->Users->get($userId);
                        $emailToTalent = new Email();
                        $emailToTalent->template('ticket_created')
                                ->emailFormat('html')->to($user['email'])
                                ->from(['no-reply@cyberclouds.com' => 'FGEM'])
                                ->subject('New Ticket created')
                                ->viewVars(['content' => $user, 'messages' => $getMessages])
                                ->send();
                    } else {
                        $users = $this->Users->find('all')
                                ->where(['role_id' => 9])
                                ->toArray();
                        foreach ($users as $user):
                            $emailToTalent = new Email();
                            $emailToTalent->template('ticket_created')
                                    ->emailFormat('html')->to($user['email'])
                                    ->from(['no-reply@cyberclouds.com' => 'FGEM'])
                                    ->subject('New Ticket created')
                                    ->viewVars(['content' => $user, 'messages' => $getMessages])
                                    ->send();
                        endforeach;
                    }
                    $this->Flash->success(__('Le billet a été enregistré.'));
                    return $this->redirect('/Tickets/create');
                }
                $this->Flash->error(__("Le billet n'a pas pu être enregistré. Veuillez réessayer."));
            }
        }
        $this->set(compact('ticket'));
    }

    public function newtickets() {


// include Imap.Class
        require_once(ROOT . DS . 'vendor' . DS . 'imap' . DS . 'lib' . DS . 'class.imap.php');

        $email = new Imap();
        $connect = $email->connect(
                '{mail.infomaniak.com:993/imap/ssl}INBOX', //host
//                'cyberuser2019@gmail.com', //username
//                'ITCyber6' //password
                'pim@fgem.ch', 'VedicionTeRa'
        );

//header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        if ($connect) {
            $_POST['inbox'] = 1;
            $inbox = "";
            if (isset($_POST['inbox'])) {


                $newDate = date('Y-m-d');
                $this->loadModel('TicketCalendars');
                $tcals = $this->TicketCalendars->find('all')
                        ->contain(['WeekHolidays' => function($q) use($newDate) {
                                $q->where(['WeekHolidays.date' => $newDate]);
                                return $q;
                            }])
                        ->where(['TicketCalendars.from_date <=' => $newDate, 'TicketCalendars.to_date >=' => $newDate])
                        ->order(['TicketCalendars.type'])
                        ->toArray();
                if (count($tcals) == 0) {
                    $userId = '';
                }
                if (count($tcals) == 1) {
                    if (empty($tcals[0]->week_holidays)) {
                        $userId = $tcals[0]->user_id;
                    } else {
                        $userId = '';
                    }
                }
                if (count($tcals) == 2) {
                    foreach ($tcals as $tcal) {
                        if ($tcal->type == 1) {
                            // if primary is not available or is on holiday
                            if (empty($tcal->week_holidays)) {
                                $userId = $tcal->user_id;
                                break;
                            }
                        } else {
                            // if secondary is not available then put condition here
                            $userId = $tcal->user_id;
                        }
                    }
                }

                // inbox array
                //$dt = "2019-05-27 02:39:21pm";
                $inbox = $email->getMessages('html');
                $arrayToSave = array();
                foreach ($inbox as $k => $inbx) {
                    $arrayToSave[$k]['assigned_to'] = $userId;
                    $arrayToSave[$k]['type_of_message'] = 'email';
                    $arrayToSave[$k]['status'] = 1;
                    $arrayToSave[$k]['subject'] = $inbx['subject'];
                    $arrayToSave[$k]['sender_email'] = $inbx['from']['address'];
                    $arrayToSave[$k]['message'] = $inbx['message'];

                    $uid = $inbx['uid'];
                    $attachments = array();
                    if (isset($inbx['attachments']) && count($inbx['attachments']) > 0) {

                        $attachments = $inbx['attachments'];
                        foreach ($attachments as $key => $attch) {

                            $attach_done = "";
                            $ext = "";
                            $part = $attch['part'];
                            $file = $attch['file'];
                            $encode = $attch['encoding'];
                            $final = array();
                            $final['uid'] = $uid;
                            $final['part'] = $part;
                            //$final['file'] = $file;
                            $final['encoding'] = $encode;
                            $ext = strrchr($file, '.');
                            if ($ext === ".wav") {
//                                $final['file'] = $this->request->webroot."email_attachments/My-file-" . $uid . "-" . $key . "" . $ext;
                                $final['file'] = "My-file-" . $uid . "-" . $key . "" . $ext;
                                $final['path'] = WWW_ROOT . 'email_attachments' . DS;
                                $attach_done = $email->getFiles($final);
                                $arrayToSave[$k]['wav_url'] = $final['file'];
                                $explodeSub = explode(' ', $inbx['subject']);
                                $arrayToSave[$k]['contact'] = end($explodeSub);
                                $arrayToSave[$k]['type_of_message'] = 'vm';




//                                debug($attach_done);
                            }
                        }
//var_dump(imap_msgno($connect, $uid));
                    }
                    imap_delete($connect, $inbx['emailnum']); // DELETE THE EMAIL AFTER READING
                    //echo $uid . "<br />";
                    //var_dump($attachments);
                }

                //echo json_encode($inbox, JSON_PRETTY_PRINT);
            }/* else if(!empty($_POST['uid']) && !empty($_POST['part']) && !empty($_POST['file']) && !empty($_POST['encoding'])){
              // attachments
              $inbox = $email->getFiles($_POST);
              echo json_encode($inbox, JSON_PRETTY_PRINT);
              }else {
              echo json_encode(array("status" => "error", "message" => "Not connect."), JSON_PRETTY_PRINT);
              } */
        } else {
            echo json_encode(array("status" => "error", "message" => "Not connect."), JSON_PRETTY_PRINT);
        }

//        debug();
//        exit;
//        $this->request->data['assigned_to'] = $userId;
////                $this->request->data['user_id'] = $this->Auth->user('id');
//        $this->request->data['type_of_message'] = 'web';
//        $this->request->data['message'] = trim($this->request->data['message']);
//        $this->request->data['status'] = 1;
        if (!empty($arrayToSave)) {
            $tickets = $this->Tickets->newEntities($arrayToSave);
            if ($this->Tickets->saveMany($tickets)) {
//            if (true) {
                $this->loadModel('Emails');
                $email = $this->Emails->get(7);


                $this->loadModel('Users');
                if ($userId) {
                    $user = $this->Users->get($userId);
                    $findString = ["{first_name}", "{last_name}"];
                    $newString = [$user->first_name, $user->last_name];
                    $email->message = str_replace($findString, $newString, $email->message);

                    $emailToTalent = new Email();
                    $emailToTalent->from(['no-reply@cyberclouds.com' => $email->title])
                            ->emailFormat('both')
                            ->to([$user->email])
                            ->subject($email->subject)
                            ->send($email->message);
                } else {
                    $this->loadModel('MemberRoles');
                    $roles = $this->MemberRoles->find('all')
                            ->contain(['Users'])
                            ->where(['MemberRoles.role_id' => 11])
                            ->toArray();
//                debug();
//                exit;
                    foreach ($roles as $role):
                        $user = $role->user;
                        $findString = ["{first_name}", "{last_name}"];
                        $newString = [$user->first_name, $user->last_name];
                        $email->message = str_replace($findString, $newString, $email->message);

                        $emailToTalent = new Email();
                        $emailToTalent->from(['no-reply@cyberclouds.com' => $email->title])
                                ->emailFormat('both')
                                ->to([$user->email])
                                ->subject($email->subject)
                                ->send($email->message);
                    endforeach;
                }
            }
        }
        exit;
    }

    public function getMessage() {
        $getTicket = $this->Tickets->get($_GET['ticketId']);
        echo $getTicket->message;
        exit;
    }

    public function changeUser() {

        $ticket = $this->Tickets->get($_GET['ticketId']);
        $ticket->assigned_to = $_GET['value'];
        $success = 0;
        if ($this->Tickets->save($ticket)) {
            $this->loadModel('Users');
            $this->loadModel('Emails');
            $emailTemplete = $this->Emails->find('all', array(
                        'conditions' => ['Emails.id' => '15'],
                        'contain' => ['Emailtypes']
                    ))->first();
            $user = $this->Users->get($_GET['value']);

            $userName = $user->first_name . " " . $user->last_name;
            $title = $emailTemplete['title'];
            $from_person = 'info@cyberclouds.com';
            $to_person = $user->email;
            $subject = $emailTemplete['subject'];
            $content = $emailTemplete['message'];
            $message = "";
            $message .= "<span style='background:#161616;display:block;padding:10px;'><img src='http://needanecommercesite.com/fgem/img/logo-light.png' style='display:block;margin:auto;max-width: 130px;'/></span>";
            $trans = array("{first_name,last_name}" => $userName, "{first_name}" => $user->first_name, "{last_name}" => $user->last_name, "{first_name} {last_name}" => $userName, '{admin_name}' => $this->Auth->user('first_name') . ' ' . $this->Auth->user('last_name'));
            $message .= strtr($content, $trans);
            $Email = new Email();
            $Email->from(array($from_person => $title));
            $Email->emailFormat('both');
            $Email->to($to_person);
            $Email->subject($subject);
            $Email->send($message);
            $success = 1;
        }
        echo $success;
        exit;
    }

// tickets are deleted here note: only supervisor can delete tickets
    public function deleteTicket($id = null, $r = null) {
        if (empty($this->viewVars['superviser'])) {
            $this->Flash->error('You are not authorize');
            return $this->redirect('/Tickets/allTickets');
        }
        $ticket = $this->Tickets->get($id);
        if ($this->Tickets->delete($ticket)) {
            $this->Flash->success('enregistrement supprimé avec succès');
            return $this->redirect('/Tickets/allTickets');
        }
    }

    public function deleteScedule($id = null) {
        $this->loadModel('TicketCalendars');
        $ticket = $this->TicketCalendars->get($id);
        if ($this->TicketCalendars->delete($ticket)) {
            $this->Flash->success('calendrier supprimé');
            return $this->redirect('/Pages/my_schedule');
        }
    }

    // When a ticket is closed or in progress or modified it is handled by this function
    public function editTicket($id = null) {
        if ($this->Auth->user('role_id') == 1) {
            $this->viewBuilder()->layout('backend');
        } else {
            $this->viewBuilder()->layout('memberlayout');
        }
        $ticket = $this->Tickets->get($id);

        $subject = $ticket->subject;
        if (strlen($ticket->subject) > 60) {
            $subject = substr($ticket->subject, 0, 60) . ' ...';
        }
        $this->set('contentTitle', 'Traitement de la demande - <small>' . $subject . '</small>');

        $this->loadModel('Users');
        $users = $this->Users->find('list', ['keyField' => 'id', 'valueField' => function($row) {
                        return $row['first_name'] . ' ' . $row['last_name'];
                    }])->where(['role_id' => 6]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            //if ticket is transfered to another user  && this field will be updated only one time
            if (empty($ticket->transfered_from) && !empty($this->request->data['assigned_to'])) {
                $this->request->data['transfered_from'] = $ticket->assigned_to;
            }
            if ($this->request->data['status'] == 2) {
                $this->request->data['first_accessed'] = date('Y-m-d h:i:s');
            }
            if ($this->request->data['status'] == 3) {
                $closingDate = new \DateTime();
                $createdDate = $ticket->created;
                $diff = $closingDate->diff($createdDate);
                $hours = $diff->h;
                $hours = $hours + ($diff->days * 24);

                $this->request->data['closing_date'] = $closingDate->format('Y-m-d h:i:s');
                $this->request->data['total_time'] = $hours;
            }
            if ($this->request->data['notify_supervisor'] == 1) {
                $name = ucwords($this->Auth->user('first_name') . ' ' . $this->Auth->user('last_name'));
                $this->loadModel('MemberRoles');
                $roles = $this->MemberRoles->find('all')
                        ->contain(['Users'])
                        ->where(['MemberRoles.role_id' => 11])
                        ->toArray();
//                debug();
//                exit;
                foreach ($roles as $role):
                    $user = $role->user;
                    $Email = new Email();
                    $Email->from(array('no-reply@cyberclouds.com' => 'FGEM'));
                    $Email->emailFormat('both');
                    $Email->to($user->email);
                    $Email->subject('Notification | FGEM');
                    $message = "<style>strong,p {font-family: arial; font-size: 14px;} div {background:#f8f8f8;padding:15px;paddint-bottom:5px;} </style>
                        <span style='background:#161616;display:block;padding:10px;'><img src='http://needanecommercesite.com/fgem/img/logo-light.png' style='display:block;margin:auto;max-width: 130px;'/></span>
                        <div>
                        <strong>Bonjour " . $user->first_name . " " . $user->last_name . ",</strong><p>Une action est effectuée sur le numéro de ticket " . $ticket->ticket_number . " par " . $name;

                    $message .= "<br /><a href=''></a></p><br/><br/>Merci, <br/>FGEM</div>";

                    $Email->send($message);
                endforeach;
            }
            $ticket = $this->Tickets->patchEntity($ticket, $this->request->data);
            if ($this->Tickets->save($ticket)) {
                $this->Flash->success('Enregistrement enregistré avec succès');
                return $this->redirect('/Tickets/allTickets');
            }
        }
        $this->loadModel('Medtypes');
        $Medtypes = $this->Medtypes->find('list', ['keyField' => 'names', 'valueField' => 'names'])
                ->order('ordinal')
                ->toArray();
        $Medtypes['others'] = 'Autres';
        $this->set(compact('ticket', 'users', 'Medtypes'));
    }

    public function add() {
        $this->set('contentTitle', 'Ajouter un billet');

        $this->viewBuilder()->layout('customer_layout');
        $ticket = $this->Tickets->newEntity();
        if ($this->request->is('post')) {
            $newDate = date('Y-m-d');
            $this->loadModel('TicketCalendars');
            $tcals = $this->TicketCalendars->find('all')
                    ->contain(['WeekHolidays' => function($q) use($newDate) {
                            $q->where(['WeekHolidays.date' => $newDate]);
                            return $q;
                        }])
                    ->where(['TicketCalendars.from_date <=' => $newDate, 'TicketCalendars.to_date >=' => $newDate])
                    ->order(['TicketCalendars.type'])
                    ->toArray();
            if (count($tcals) == 0) {
                $userId = '';
            }
            if (count($tcals) == 1) {
                if (empty($tcals[0]->week_holidays)) {
                    $userId = $tcals[0]->user_id;
                } else {
                    $userId = '';
                }
            }
            if (count($tcals) == 2) {
                foreach ($tcals as $tcal) {
                    if ($tcal->type == 1) {
                        // if primary is not available or is on holiday
                        if (empty($tcal->week_holidays)) {
                            $userId = $tcal->user_id;
                            break;
                        }
                    } else {
                        // if secondary is not available then put condition here
                        $userId = $tcal->user_id;
                    }
                }
            }
            $this->request->data['assigned_to'] = $userId;
            $this->request->data['user_id'] = $this->Auth->user('id');
            $this->request->data['type_of_message'] = 'web';
            $this->request->data['message'] = trim($this->request->data['message']);
            $this->request->data['sender_email'] = $this->Auth->user('email');
            $this->request->data['status'] = 1;
//            debug($this->request->data);
//            exit;
            $ticket = $this->Tickets->patchEntity($ticket, $this->request->getData());

            if ($this->Tickets->save($ticket)) {
                $digits = 8 - strlen($ticket->id);
                $random_code = rand(pow(10, $digits - 1), pow(10, $digits) - 1);

                $ticket_number = $ticket->id . $random_code;
                $ticket->ticket_number = $ticket_number;
                $this->Tickets->save($ticket);

                $getMessages = $this->Tickets->get($ticket->id, ['contain' => 'TicketConversations']);
                $this->loadModel('Users');
                if ($userId) {
                    $user = $this->Users->get($userId);
                    $emailToTalent = new Email();
                    $emailToTalent->template('ticket_created')
                            ->emailFormat('html')->to($user['email'])
                            ->from(['no-reply@cyberclouds.com' => 'FGEM'])
                            ->subject('New Ticket created')
                            ->viewVars(['content' => $user, 'messages' => $getMessages])
                            ->send();
                } else {
                    $users = $this->Users->find('all')
                            ->where(['role_id' => 9])
                            ->toArray();
                    foreach ($users as $user):
                        $emailToTalent = new Email();
                        $emailToTalent->template('ticket_created')
                                ->emailFormat('html')->to($user['email'])
                                ->from(['no-reply@cyberclouds.com' => 'FGEM'])
                                ->subject('New Ticket created')
                                ->viewVars(['content' => $user, 'messages' => $getMessages])
                                ->send();
                    endforeach;
                }

                $this->Flash->success(__('Le billet a été enregistré.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__("Le billet n'a pas pu être enregistré. Veuillez réessayer."));
        }
        $this->set(compact('ticket'));
        $this->set('_serialize', ['ticket']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ticket id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $ticket = $this->Tickets->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ticket = $this->Tickets->patchEntity($ticket, $this->request->getData());
            if ($this->Tickets->save($ticket)) {
                $this->Flash->success(__('The ticket has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ticket could not be saved. Please, try again.'));
        }
        $users = $this->Tickets->Users->find('list', ['limit' => 200]);
        $this->set(compact('ticket', 'users'));
        $this->set('_serialize', ['ticket']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ticket id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $ticket = $this->Tickets->get($id);
        if ($this->Tickets->delete($ticket)) {
            $this->Flash->success(__('The ticket has been deleted.'));
        } else {
            $this->Flash->error(__('The ticket could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
