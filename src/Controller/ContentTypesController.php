<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ContentTypes Controller
 *
 * @property \App\Model\Table\ContentTypesTable $ContentTypes
 *
 * @method \App\Model\Entity\ContentType[] paginate($object = null, array $settings = [])
 */
class ContentTypesController extends AppController
{

    public function isAuthorized($user) {
        if (in_array($this->request->action, ['edit'])) {
            $id = (int) $this->request->params['pass'][0];
            if ($id == $user['id']) {
                return true;
            }
        }
        if (in_array($this->request->action, ['logout'])) {
            return true;
        }
        // Admin can access every action
        if ($this->viewVars['actionPermission'] != "") {
            return true;
        } else {
            return false;
        }
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 1 || $permission == 2) {

        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $contentTypes = $this->paginate($this->ContentTypes);

        $this->set(compact('contentTypes'));
        $this->set('_serialize', ['contentTypes']);
    }
    // all the content are pages are displayed here
    public function listPages() {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 1 || $permission == 2) {

        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $this->paginate = ['contain' => ['Users'],'conditions' => [

        ],'order' => ['ContentTypes.ordinal' => 'ASC']];
        $pages = $this->paginate($this->ContentTypes);
        //debug($pages);
        $this->set(compact('pages'));
        $this->set('_serialize', ['pages']);
        $this->viewBuilder()->setLayout('backend');
    }
// new page can be added here
    public function addPage() {
        $permission = $this->viewVars['actionPermission'];
        //exit;

        if ($permission == 2) {

        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $page = $this->ContentTypes->newEntity();
        if ($this->request->is('post')) {
            $maxorder_no = $this->ContentTypes->find('all', [
                'fields' => array('ordinal' => 'MAX(ContentTypes.ordinal)')
            ]);
            if (!$maxorder_no) {
                $content_order = 1;
            }
            $content_array = $maxorder_no->toArray();
            $content_order = $content_array[0]['ordinal'] + 1;

            $this->request->data['ContentTypes']['title'] = $this->request->data['title'];
            $this->request->data['ContentTypes']['slug'] = $this->request->data['slug'];
            $this->request->data['ContentTypes']['status'] = $this->request->data['status'];
            $this->request->data['ContentTypes']['ordinal'] = $content_order;
            $this->request->data['ContentTypes']['user_id'] = $this->request->session()->read('Auth.User.id');
            $page = $this->ContentTypes->patchEntity($page, $this->request->data['ContentTypes']);
            if ($this->ContentTypes->save($page)) {
                $this->Flash->success(__('La page a été enregistrée.'));
                return $this->redirect(['controller' => 'ContentTypes','action' => 'listPages']);
            }
            $this->Flash->error(__('La page n\'a pas pu être enregistrée. Veuillez réessayer.'));
        }
        $this->set(compact('page'));
        $this->set('_serialize', ['page']);
        $this->viewBuilder()->setLayout('backend');
    }
    // edit pages that are displaying on front
    public function editPage($id = null) {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 2) {

        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $contentType = $this->ContentTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $contentType = $this->ContentTypes->patchEntity($contentType, $this->request->getData());
            if ($this->ContentTypes->save($contentType)) {
                $this->Flash->success(__('La page a été enregistrée.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('La page n\'a pas pu être enregistrée. Veuillez réessayer.'));
        }
        $this->set(compact('contentType'));
        $this->set('_serialize', ['contentType']);

        $this->viewBuilder()->setLayout('backend');
    }

    /**
     * View method
     *
     * @param string|null $id Content Type id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 1 || $permission == 2) {

        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $contentType = $this->ContentTypes->get($id, [
            'contain' => ['Contents']
        ]);

        $this->set('contentType', $contentType);
        $this->set('_serialize', ['contentType']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $permission = $this->viewVars['actionPermission'];


        if ($permission == 2) {

        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $contentType = $this->ContentTypes->newEntity();
        if ($this->request->is('post')) {
            $contentType = $this->ContentTypes->patchEntity($contentType, $this->request->getData());
            if ($this->ContentTypes->save($contentType)) {
                $this->Flash->success(__('The content type has been saved.'));

                return $this->redirect(['controller' => 'ContentTypes','action' => 'index']);
            }
            $this->Flash->error(__('The content type could not be saved. Please, try again.'));
        }
        $this->set(compact('contentType'));
        $this->set('_serialize', ['contentType']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Content Type id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $permission = $this->viewVars['actionPermission'];

        if ($permission == 2) {

        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $contentType = $this->ContentTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $contentType = $this->ContentTypes->patchEntity($contentType, $this->request->getData());
            if ($this->ContentTypes->save($contentType)) {
                $this->Flash->success(__('The content type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The content type could not be saved. Please, try again.'));
        }
        $this->set(compact('contentType'));
        $this->set('_serialize', ['contentType']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Content Type id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    //deletion of content/dynamic pages 
    public function delete($id = null)
    {

        $permission = $this->viewVars['actionPermission'];

        if ($permission == 2) {

        } else {
            return $this->redirect(['controller' => 'Pages', 'action' => 'welcome']);
        }
        $this->set(compact('permission'));
        $is_contact = $this->ContentTypes->find('all',['conditions' => ['id' => $id]])->first();
        $exists = $this->ContentTypes->Contents->exists(['content_type_id' => $id] );
        if($exists && $is_contact->is_contact == 0) {
            $this->Flash->error(__('Vous ne pouvez pas supprimer cette page car elle contient des sous-pages.'));
            return $this->redirect(['action' => 'index']);
        }
        $is_front = $this->ContentTypes->find('all',['conditions'=>['id'=>$id]]);
        $page = $is_front->first();
        if($page->is_front == 1) {
            $this->Flash->error(__('Vous ne pouvez pas supprimer cette page car il s\'agit de la page de couverture, définissez une autre page comme page de garde avant de supprimer cette page..'));
            return $this->redirect(['action' => 'index']);	
        }
        $this->request->allowMethod(['post', 'delete','get']);
        $contentType = $this->ContentTypes->get($id);
        if ($this->ContentTypes->delete($contentType)) {
            $this->Flash->success(__('Le type de contenu a été supprimé.'));
        } else {
            $this->Flash->error(__('Le type de contenu n\'a pas pu être supprimé. Veuillez réessayer.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function udapteorder() {
        if ($this->request->is('post')) {
            $success = 0;
            foreach ($this->request->data['data']['content_id'] as $key => $data) {
                $page = $this->ContentTypes->newEntity();
                $this->request->data['ContentTypes']['ordinal'] = $this->request->data['data']['ordinal'][$key];

                $this->request->data['ContentTypes']['is_front'] = $this->request->data['data']['is_front'][$key];
                $this->request->data['ContentTypes']['is_contact'] = $this->request->data['data']['is_contact'][$key];
                $this->request->data['ContentTypes']['show_slider'] = $this->request->data['data']['show_slider'][$key];
                $page->id = $this->request->data['data']['content_id'][$key];
                $page = $this->ContentTypes->patchEntity($page, $this->request->data['ContentTypes'], [
                    'validate' => 'OnlyCheck'
                ]);
                if ($this->ContentTypes->save($page)) {
                    $success = 1;
                } else {
                    $success = 0;
                }
            }

            if ($success == 1) {
                $this->Flash->success(__('La mise en page a été enregistrée.'));
                return $this->redirect(array('action' => 'listPages'));
            } else {

                $this->Flash->error(__('Mise en page non enregistrée, réessayez plus tard'));
                return $this->redirect(array('action' => 'listPages'));
            }
        }
        $this->viewBuilder()->setLayout();
        $this->autoRender = false;
    }
}
