<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TicketConversations Controller
 *
 * @property \App\Model\Table\TicketConversationsTable $TicketConversations
 *
 * @method \App\Model\Entity\TicketConversation[] paginate($object = null, array $settings = [])
 */
class TicketConversationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Tickets']
        ];
        $ticketConversations = $this->paginate($this->TicketConversations);

        $this->set(compact('ticketConversations'));
        $this->set('_serialize', ['ticketConversations']);
    }

    /**
     * View method
     *
     * @param string|null $id Ticket Conversation id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ticketConversation = $this->TicketConversations->get($id, [
            'contain' => ['Tickets']
        ]);

        $this->set('ticketConversation', $ticketConversation);
        $this->set('_serialize', ['ticketConversation']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ticketConversation = $this->TicketConversations->newEntity();
        if ($this->request->is('post')) {
            $ticketConversation = $this->TicketConversations->patchEntity($ticketConversation, $this->request->getData());
            if ($this->TicketConversations->save($ticketConversation)) {
                $this->Flash->success(__('The ticket conversation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ticket conversation could not be saved. Please, try again.'));
        }
        $tickets = $this->TicketConversations->Tickets->find('list', ['limit' => 200]);
        $this->set(compact('ticketConversation', 'tickets'));
        $this->set('_serialize', ['ticketConversation']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ticket Conversation id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ticketConversation = $this->TicketConversations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ticketConversation = $this->TicketConversations->patchEntity($ticketConversation, $this->request->getData());
            if ($this->TicketConversations->save($ticketConversation)) {
                $this->Flash->success(__('The ticket conversation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ticket conversation could not be saved. Please, try again.'));
        }
        $tickets = $this->TicketConversations->Tickets->find('list', ['limit' => 200]);
        $this->set(compact('ticketConversation', 'tickets'));
        $this->set('_serialize', ['ticketConversation']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ticket Conversation id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ticketConversation = $this->TicketConversations->get($id);
        if ($this->TicketConversations->delete($ticketConversation)) {
            $this->Flash->success(__('The ticket conversation has been deleted.'));
        } else {
            $this->Flash->error(__('The ticket conversation could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
