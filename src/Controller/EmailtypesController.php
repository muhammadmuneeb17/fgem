<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Emailtypes Controller
 *
 * @property \App\Model\Table\EmailtypesTable $Emailtypes
 *
 * @method \App\Model\Entity\Emailtype[] paginate($object = null, array $settings = [])
 */
class EmailtypesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $emailtypes = $this->paginate($this->Emailtypes);

        $this->set(compact('emailtypes'));
        $this->set('_serialize', ['emailtypes']);
    }

    /**
     * View method
     *
     * @param string|null $id Emailtype id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $emailtype = $this->Emailtypes->get($id, [
            'contain' => ['Emails']
        ]);

        $this->set('emailtype', $emailtype);
        $this->set('_serialize', ['emailtype']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $emailtype = $this->Emailtypes->newEntity();
        if ($this->request->is('post')) {
            $emailtype = $this->Emailtypes->patchEntity($emailtype, $this->request->getData());
            if ($this->Emailtypes->save($emailtype)) {
                $this->Flash->success(__('The emailtype has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The emailtype could not be saved. Please, try again.'));
        }
        $this->set(compact('emailtype'));
        $this->set('_serialize', ['emailtype']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Emailtype id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $emailtype = $this->Emailtypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $emailtype = $this->Emailtypes->patchEntity($emailtype, $this->request->getData());
            if ($this->Emailtypes->save($emailtype)) {
                $this->Flash->success(__('The emailtype has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The emailtype could not be saved. Please, try again.'));
        }
        $this->set(compact('emailtype'));
        $this->set('_serialize', ['emailtype']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Emailtype id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $emailtype = $this->Emailtypes->get($id);
        if ($this->Emailtypes->delete($emailtype)) {
            $this->Flash->success(__('The emailtype has been deleted.'));
        } else {
            $this->Flash->error(__('The emailtype could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
