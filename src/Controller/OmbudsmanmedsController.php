<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Ombudsmanmeds Controller
 *
 * @property \App\Model\Table\OmbudsmanmedsTable $Ombudsmanmeds
 *
 * @method \App\Model\Entity\Ombudsmanmed[] paginate($object = null, array $settings = [])
 */
class OmbudsmanmedsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Ombudsmen', 'Medtypes']
        ];
        $ombudsmanmeds = $this->paginate($this->Ombudsmanmeds);

        $this->set(compact('ombudsmanmeds'));
        $this->set('_serialize', ['ombudsmanmeds']);
    }

    /**
     * View method
     *
     * @param string|null $id Ombudsmanmed id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ombudsmanmed = $this->Ombudsmanmeds->get($id, [
            'contain' => ['Ombudsmen', 'Medtypes']
        ]);

        $this->set('ombudsmanmed', $ombudsmanmed);
        $this->set('_serialize', ['ombudsmanmed']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ombudsmanmed = $this->Ombudsmanmeds->newEntity();
        if ($this->request->is('post')) {
            $ombudsmanmed = $this->Ombudsmanmeds->patchEntity($ombudsmanmed, $this->request->getData());
            if ($this->Ombudsmanmeds->save($ombudsmanmed)) {
                $this->Flash->success(__('The ombudsmanmed has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ombudsmanmed could not be saved. Please, try again.'));
        }
        $ombudsmen = $this->Ombudsmanmeds->Ombudsmen->find('list', ['limit' => 200]);
        $medtypes = $this->Ombudsmanmeds->Medtypes->find('list', ['limit' => 200]);
        $this->set(compact('ombudsmanmed', 'ombudsmen', 'medtypes'));
        $this->set('_serialize', ['ombudsmanmed']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ombudsmanmed id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ombudsmanmed = $this->Ombudsmanmeds->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ombudsmanmed = $this->Ombudsmanmeds->patchEntity($ombudsmanmed, $this->request->getData());
            if ($this->Ombudsmanmeds->save($ombudsmanmed)) {
                $this->Flash->success(__('The ombudsmanmed has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ombudsmanmed could not be saved. Please, try again.'));
        }
        $ombudsmen = $this->Ombudsmanmeds->Ombudsmen->find('list', ['limit' => 200]);
        $medtypes = $this->Ombudsmanmeds->Medtypes->find('list', ['limit' => 200]);
        $this->set(compact('ombudsmanmed', 'ombudsmen', 'medtypes'));
        $this->set('_serialize', ['ombudsmanmed']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ombudsmanmed id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ombudsmanmed = $this->Ombudsmanmeds->get($id);
        if ($this->Ombudsmanmeds->delete($ombudsmanmed)) {
            $this->Flash->success(__('The ombudsmanmed has been deleted.'));
        } else {
            $this->Flash->error(__('The ombudsmanmed could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
