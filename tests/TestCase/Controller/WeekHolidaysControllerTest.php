<?php
namespace App\Test\TestCase\Controller;

use App\Controller\WeekHolidaysController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\WeekHolidaysController Test Case
 */
class WeekHolidaysControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.week_holidays',
        'app.users',
        'app.contents',
        'app.content_types',
        'app.sliders',
        'app.roles',
        'app.rights',
        'app.modules',
        'app.registerations',
        'app.ombudsmans',
        'app.ombudsmanaccreds',
        'app.ombudsmen',
        'app.accreditations',
        'app.ombudsmanformations',
        'app.ombudsmanlevels',
        'app.languages',
        'app.ombudsmanswears',
        'app.ombudsmanmeds',
        'app.medtypes',
        'app.accounts',
        'app.paymenttypes',
        'app.comments',
        'app.membertypes',
        'app.ticket_calendars'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
