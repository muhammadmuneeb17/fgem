<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TicketConversationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TicketConversationsTable Test Case
 */
class TicketConversationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TicketConversationsTable
     */
    public $TicketConversations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ticket_conversations',
        'app.tickets',
        'app.users',
        'app.contents',
        'app.content_types',
        'app.sliders',
        'app.roles',
        'app.rights',
        'app.modules',
        'app.registerations',
        'app.ombudsmans',
        'app.ombudsmanaccreds',
        'app.ombudsmen',
        'app.accreditations',
        'app.ombudsmanformations',
        'app.ombudsmanlevels',
        'app.languages',
        'app.ombudsmanswears',
        'app.ombudsmanmeds',
        'app.medtypes',
        'app.accounts',
        'app.paymenttypes',
        'app.comments',
        'app.membertypes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TicketConversations') ? [] : ['className' => TicketConversationsTable::class];
        $this->TicketConversations = TableRegistry::get('TicketConversations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TicketConversations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
