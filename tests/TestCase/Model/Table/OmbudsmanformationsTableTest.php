<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OmbudsmanformationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OmbudsmanformationsTable Test Case
 */
class OmbudsmanformationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OmbudsmanformationsTable
     */
    public $Ombudsmanformations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ombudsmanformations',
        'app.ombudsmen'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Ombudsmanformations') ? [] : ['className' => OmbudsmanformationsTable::class];
        $this->Ombudsmanformations = TableRegistry::get('Ombudsmanformations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Ombudsmanformations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
