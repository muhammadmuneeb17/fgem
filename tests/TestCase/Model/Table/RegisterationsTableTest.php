<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RegisterationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RegisterationsTable Test Case
 */
class RegisterationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RegisterationsTable
     */
    public $Registerations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.registerations',
        'app.membertypes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Registerations') ? [] : ['className' => RegisterationsTable::class];
        $this->Registerations = TableRegistry::get('Registerations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Registerations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
