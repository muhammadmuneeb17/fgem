<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TicketCalendarsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TicketCalendarsTable Test Case
 */
class TicketCalendarsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TicketCalendarsTable
     */
    public $TicketCalendars;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ticket_calendars',
        'app.users',
        'app.contents',
        'app.content_types',
        'app.sliders',
        'app.roles',
        'app.rights',
        'app.modules',
        'app.registerations',
        'app.ombudsmans',
        'app.ombudsmanaccreds',
        'app.ombudsmen',
        'app.accreditations',
        'app.ombudsmanformations',
        'app.ombudsmanlevels',
        'app.languages',
        'app.ombudsmanswears',
        'app.ombudsmanmeds',
        'app.medtypes',
        'app.accounts',
        'app.paymenttypes',
        'app.comments',
        'app.membertypes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TicketCalendars') ? [] : ['className' => TicketCalendarsTable::class];
        $this->TicketCalendars = TableRegistry::get('TicketCalendars', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TicketCalendars);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
