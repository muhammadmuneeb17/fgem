<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EmailtypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\EmailtypesTable Test Case
 */
class EmailtypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\EmailtypesTable
     */
    public $Emailtypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.emailtypes',
        'app.emails'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Emailtypes') ? [] : ['className' => EmailtypesTable::class];
        $this->Emailtypes = TableRegistry::get('Emailtypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Emailtypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
