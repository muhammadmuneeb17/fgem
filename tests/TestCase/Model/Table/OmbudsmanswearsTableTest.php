<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OmbudsmanswearsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OmbudsmanswearsTable Test Case
 */
class OmbudsmanswearsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OmbudsmanswearsTable
     */
    public $Ombudsmanswears;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ombudsmanswears'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Ombudsmanswears') ? [] : ['className' => OmbudsmanswearsTable::class];
        $this->Ombudsmanswears = TableRegistry::get('Ombudsmanswears', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Ombudsmanswears);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
