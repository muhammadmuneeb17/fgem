<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MedtypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MedtypesTable Test Case
 */
class MedtypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MedtypesTable
     */
    public $Medtypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.medtypes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Medtypes') ? [] : ['className' => MedtypesTable::class];
        $this->Medtypes = TableRegistry::get('Medtypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Medtypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
