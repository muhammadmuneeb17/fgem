<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DocumentguidesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DocumentguidesTable Test Case
 */
class DocumentguidesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DocumentguidesTable
     */
    public $Documentguides;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.documentguides',
        'app.users',
        'app.contents',
        'app.content_types',
        'app.sliders',
        'app.roles',
        'app.rights',
        'app.modules',
        'app.registerations',
        'app.ombudsmans',
        'app.ombudsmanaccreds',
        'app.ombudsmen',
        'app.accreditations',
        'app.ombudsmanformations',
        'app.ombudsmanlevels',
        'app.languages',
        'app.ombudsmanswears',
        'app.ombudsmanmeds',
        'app.medtypes',
        'app.accounts',
        'app.paymenttypes',
        'app.comments',
        'app.membertypes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Documentguides') ? [] : ['className' => DocumentguidesTable::class];
        $this->Documentguides = TableRegistry::get('Documentguides', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Documentguides);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
