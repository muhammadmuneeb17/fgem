<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AccreditationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AccreditationsTable Test Case
 */
class AccreditationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AccreditationsTable
     */
    public $Accreditations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.accreditations',
        'app.ombudsmanaccreds',
        'app.ombudsmen'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Accreditations') ? [] : ['className' => AccreditationsTable::class];
        $this->Accreditations = TableRegistry::get('Accreditations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Accreditations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
