<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OmbudsmansTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OmbudsmansTable Test Case
 */
class OmbudsmansTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OmbudsmansTable
     */
    public $Ombudsmans;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ombudsmans',
        'app.ombudsmanaccreds',
        'app.ombudsmen',
        'app.ombudsmanformations',
        'app.ombudsmanlevels',
        'app.languages'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Ombudsmans') ? [] : ['className' => OmbudsmansTable::class];
        $this->Ombudsmans = TableRegistry::get('Ombudsmans', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Ombudsmans);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationOmbremoveCheck method
     *
     * @return void
     */
    public function testValidationOmbremoveCheck()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
