<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NextRenewalDateTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NextRenewalDateTable Test Case
 */
class NextRenewalDateTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NextRenewalDateTable
     */
    public $NextRenewalDate;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.next_renewal_date'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('NextRenewalDate') ? [] : ['className' => NextRenewalDateTable::class];
        $this->NextRenewalDate = TableRegistry::get('NextRenewalDate', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->NextRenewalDate);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
