<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OmbudsmanaccredsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OmbudsmanaccredsTable Test Case
 */
class OmbudsmanaccredsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OmbudsmanaccredsTable
     */
    public $Ombudsmanaccreds;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ombudsmanaccreds',
        'app.ombudsmen'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Ombudsmanaccreds') ? [] : ['className' => OmbudsmanaccredsTable::class];
        $this->Ombudsmanaccreds = TableRegistry::get('Ombudsmanaccreds', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Ombudsmanaccreds);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
