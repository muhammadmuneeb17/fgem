<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MembertypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MembertypesTable Test Case
 */
class MembertypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MembertypesTable
     */
    public $Membertypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.membertypes',
        'app.registerations'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Membertypes') ? [] : ['className' => MembertypesTable::class];
        $this->Membertypes = TableRegistry::get('Membertypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Membertypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
