<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MemberTypeAmountsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MemberTypeAmountsTable Test Case
 */
class MemberTypeAmountsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MemberTypeAmountsTable
     */
    public $MemberTypeAmounts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.member_type_amounts',
        'app.membertypes',
        'app.registerations',
        'app.ombudsmans',
        'app.ombudsmanaccreds',
        'app.ombudsmen',
        'app.accreditations',
        'app.ombudsmanformations',
        'app.ombudsmanlevels',
        'app.languages',
        'app.ombudsmanswears',
        'app.ombudsmanmeds',
        'app.medtypes',
        'app.accounts',
        'app.paymenttypes',
        'app.users',
        'app.contents',
        'app.content_types',
        'app.sliders',
        'app.roles',
        'app.rights',
        'app.modules',
        'app.member_roles',
        'app.comments'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('MemberTypeAmounts') ? [] : ['className' => MemberTypeAmountsTable::class];
        $this->MemberTypeAmounts = TableRegistry::get('MemberTypeAmounts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MemberTypeAmounts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
