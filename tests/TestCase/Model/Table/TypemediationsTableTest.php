<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TypemediationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TypemediationsTable Test Case
 */
class TypemediationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TypemediationsTable
     */
    public $Typemediations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.typemediations'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Typemediations') ? [] : ['className' => TypemediationsTable::class];
        $this->Typemediations = TableRegistry::get('Typemediations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Typemediations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
