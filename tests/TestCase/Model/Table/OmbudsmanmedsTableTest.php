<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OmbudsmanmedsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OmbudsmanmedsTable Test Case
 */
class OmbudsmanmedsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OmbudsmanmedsTable
     */
    public $Ombudsmanmeds;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ombudsmanmeds',
        'app.ombudsmen',
        'app.medtypes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Ombudsmanmeds') ? [] : ['className' => OmbudsmanmedsTable::class];
        $this->Ombudsmanmeds = TableRegistry::get('Ombudsmanmeds', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Ombudsmanmeds);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
