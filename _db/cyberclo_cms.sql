-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 25, 2018 at 09:47 AM
-- Server version: 10.0.36-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cyberclo_cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `fname` varchar(80) NOT NULL,
  `lname` varchar(120) NOT NULL,
  `email` varchar(80) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `message` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contents`
--

CREATE TABLE `contents` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `image` varchar(120) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `content_type_id` int(11) NOT NULL,
  `template` varchar(80) NOT NULL DEFAULT 'DEFAULT',
  `lat` float DEFAULT NULL,
  `lng` float DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `show_form` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `ordinal` int(11) NOT NULL DEFAULT '0',
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contents`
--

INSERT INTO `contents` (`id`, `title`, `description`, `image`, `status`, `content_type_id`, `template`, `lat`, `lng`, `phone`, `mobile`, `email`, `show_form`, `user_id`, `ordinal`, `modified`) VALUES
(23, 'Who We Are', '<p style=\"text-align: center;\">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>\r\n', 'Who We Are1518967227.jpg', 1, 6, 'DEFAULT', 0, 0, NULL, NULL, NULL, 0, 46, 3, '2018-02-19 11:33:00'),
(24, 'Our Service', '<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>\r\n', 'Our Service1518967080.jpg', 1, 6, 'PARALLAX', 0, 0, NULL, NULL, NULL, 0, 46, 2, '2018-02-19 11:33:00'),
(25, 'Introduction', '<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>\r\n', '', 1, 6, 'DEFAULT', 0, 0, NULL, NULL, NULL, 0, 49, 1, '2018-02-19 16:41:46'),
(27, 'Our Company Information', '<p>Company Name Services,Inc&nbsp;<br />\r\nP.O.Box 89845<br />\r\nTampa, FL 33689</p>\r\n', 'Contact1518621051.jpg', 1, 10, 'DEFAULT', 27.9944, -81.7603, '+18855236', '+18138434477', 'info@companyname.com', 1, 46, 4, '2018-02-19 11:18:24'),
(28, 'Our Mission', '<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>\r\n', 'Our Mission1518967360.jpg', 1, 6, 'PARALLAX', NULL, NULL, NULL, NULL, NULL, 0, 46, 4, '2018-02-19 11:33:00'),
(29, 'Who We Are', '<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.<img alt=\"\" src=\"/cms/webroot/img/included/images/4.jpg\" style=\"border-width: 0px; border-style: solid; margin: 5px; float: right; width: 250px; height: 167px;\" />Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>\r\n', NULL, 1, 7, 'DEFAULT', NULL, NULL, NULL, NULL, NULL, 0, 46, 1, '2018-04-23 21:34:51');

-- --------------------------------------------------------

--
-- Table structure for table `content_types`
--

CREATE TABLE `content_types` (
  `id` int(11) NOT NULL,
  `title` varchar(80) NOT NULL,
  `is_front` int(11) NOT NULL DEFAULT '0',
  `show_slider` int(11) NOT NULL DEFAULT '0',
  `is_contact` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `modified` datetime NOT NULL,
  `ordinal` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content_types`
--

INSERT INTO `content_types` (`id`, `title`, `is_front`, `show_slider`, `is_contact`, `user_id`, `modified`, `ordinal`, `status`) VALUES
(6, 'Home', 1, 1, 0, 46, '2018-04-24 12:22:22', 1, 1),
(7, 'About', 0, 0, 0, 46, '2018-04-24 12:22:22', 2, 1),
(10, 'Contact', 0, 0, 1, 46, '2018-04-24 12:22:22', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image_alt` varchar(180) NOT NULL,
  `image` varchar(120) NOT NULL,
  `modified` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ordinal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `image_alt`, `image`, `modified`, `status`, `user_id`, `ordinal`) VALUES
(107, 'Slider One Title', 'Test', 'Slider One Title1511779331.jpg', '2018-04-24 12:20:14', 1, 49, 2),
(120, 'Slider Two Title', 'Slider Two Title', 'Slider Two Title1524573849.jpg', '2018-04-24 12:44:09', 1, 49, 3);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(120) NOT NULL,
  `modified` datetime NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `role` varchar(20) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `user_name`, `email`, `password`, `modified`, `password_reset_token`, `role`, `status`) VALUES
(46, 'Rajesh', 'Bahl', 'cyberclouds', 'raj@cyberclouds.com', '$2y$10$03sOacyvpBvbQfhIofcOVu6XKmPSUN.HdWeRq9Kw9p/DJKx98Vcu6', '2017-10-15 11:45:12', NULL, 'SUPERADMIN', 1),
(49, 'Backend', 'Admin', 'admin', 'admin@domain.com', '$2y$10$HcLYrnzdUm51l44wSs334eYs2KBAulIrWBIfJfSc8qH.2mmh774.K', '2018-02-19 16:24:42', NULL, 'SUPERADMIN', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contents`
--
ALTER TABLE `contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content_types`
--
ALTER TABLE `content_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contents`
--
ALTER TABLE `contents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `content_types`
--
ALTER TABLE `content_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
