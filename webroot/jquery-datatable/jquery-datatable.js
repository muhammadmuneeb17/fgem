$(function () {
    $('.js-basic-example').DataTable({
        responsive: true
    });

    //Exportable table
    $('.js-exportable').DataTable({
        dom: 'Bfrtip',
        responsive: true,
        search: 'Search In Results',
        buttons: [
            {
                    extend: 'copy',
                    title: 'Copy',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                }, {
                    extend: 'csv',
                    title: 'Export CSV',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                }, {
                    extend: 'excel',
                    title: 'Export Excel',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                }, {
                    extend: 'print',
                    title: 'Print',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                } ,
            
        ],
        "language": {
            "search": "Chercher dans la liste:"
          }
    });
});