/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
};
CKEDITOR.editorConfig = function( config )
{
   config.filebrowserBrowseUrl = 'http://localhost/fgem/js/kcfinder/browse.php?type=files';
   config.filebrowserImageBrowseUrl = 'http://localhost/fgem/js/kcfinder/browse.php?type=images';
   config.filebrowserFlashBrowseUrl = 'http://localhost/fgem/js/kcfinder/browse.php?type=flash';
   config.filebrowserUploadUrl = 'http://localhost/fgem/js/kcfinder/upload.php?type=files';
   config.filebrowserImageUploadUrl = 'http://localhost/fgem/js/kcfinder/upload.php?type=images';
   config.filebrowserFlashUploadUrl = 'http://localhost/fgem/js/kcfinder/upload.php?type=flash';
   config.font_names = 'Calibri;' + config.font_names;
   config.filebrowserUploadMethod = 'form';
   //config.extraPlugins = 'image2';
   //config.extraPlugins = 'uploadfile';   
   //config.image2_captionedClass = 'image-captioned';
   config.allowedContent = true;
};