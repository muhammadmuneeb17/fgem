function include(scriptUrl){document.write('<script src="'+ scriptUrl+'"></script>');}
function lazyInit(element,func){var $win=jQuery(window),wh=$win.height();$win.on('load scroll',function(){var st=$(this).scrollTop();if(!element.hasClass('lazy-loaded')){var et=element.offset().top,eb=element.offset().top+ element.outerHeight();if(st+ wh>et- 100&&st<eb+ 100){func.call();element.addClass('lazy-loaded');}}});}
function isIE(){var myNav=navigator.userAgent.toLowerCase(),msie=(myNav.indexOf('msie')!=-1)?parseInt(myNav.split('msie')[1]):false;if(!msie){return(myNav.indexOf('trident')!=-1)?11:((myNav.indexOf('edge')!=-1)?12:false);}
                return msie;};;(function($){var ieVersion=isIE();if(ieVersion===12){$('html').addClass('ie-edge');}
                                            if(ieVersion===11){$('html').addClass('ie-11');}
                                            if(ieVersion&&ieVersion<11){$('html').addClass('lt-ie11');$(document).ready(function(){PointerEventsPolyfill.initialize({});});}
                                            if(ieVersion&&ieVersion<10){$('html').addClass('lt-ie10');}})(jQuery);;(function($){$(document).ready(function(){$("#copyright-year").text((new Date).getFullYear());});})(jQuery);;(function($){var o=$('html');if(o.hasClass('desktop')&&o.hasClass("wow-animation")&&$(".wow").length){$(document).ready(function(){new WOW().init();});}})(jQuery);;(function($){var o=$('html');if(o.hasClass('desktop')){$(document).ready(function(){$().UItoTop({easingType:'easeOutQuart',containerClass:'ui-to-top fa fa-angle-up'});});}})(jQuery);;(function($){var o=$('.rd-mailform');if(o.length>0){$(document).ready(function(){var o=$('.rd-mailform');if(o.length){o.rdMailForm({validator:{'constraints':{'@LettersOnly':{message:'Please use only letters.'},'@NumbersOnly':{message:'Please use only numbers.'},'@NotEmpty':{message:'This field should not be empty.'},'@Email':{message:'Enter valid e-mail address.'},'@Phone':{message:'Enter valid phone number.'},'@Date':{message:'Use MM/DD/YYYY format.'},'@SelectRequired':{message:'Please choose an option.'}}}},{'MF000':'Sent','MF001':'Recipients are not set.','MF002':'Form will not work locally.','MF003':'Please define email field in your form.','MF004':'Please define the type of your form.','MF254':'Something went wrong with PHPMailer.','MF255':'There was an error submitting the form.'});}});}})(jQuery);;(function($){var o=$('#google-map');if(o.length){include('//maps.google.com/maps/api/js');$(document).ready(function(){var head=document.getElementsByTagName('head')[0],insertBefore=head.insertBefore;head.insertBefore=function(newElement,referenceElement){if(newElement.href&&newElement.href.indexOf('//fonts.googleapis.com/css?family=Roboto')!=-1||newElement.innerHTML.indexOf('gm-style')!=-1){return;}
insertBefore.call(head,newElement,referenceElement);};lazyInit(o,function(){o.googleMap({styles:[{"featureType":"landscape","stylers":[{"hue":"#FFBB00"},{"saturation":43.400000000000006},{"lightness":37.599999999999994},{"gamma":1}]},{"featureType":"road.highway","stylers":[{"hue":"#FFC200"},{"saturation":-61.8},{"lightness":45.599999999999994},{"gamma":1}]},{"featureType":"road.arterial","stylers":[{"hue":"#FF0300"},{"saturation":-100},{"lightness":51.19999999999999},{"gamma":1}]},{"featureType":"road.local","stylers":[{"hue":"#FF0300"},{"saturation":-100},{"lightness":52},{"gamma":1}]},{"featureType":"water","stylers":[{"hue":"#0078FF"},{"saturation":-13.200000000000003},{"lightness":2.4000000000000057},{"gamma":1}]},{"featureType":"poi","stylers":[{"hue":"#00FF6A"},{"saturation":-1.0989010989011234},{"lightness":11.200000000000017},{"gamma":1}]}]});});});}})(jQuery);;(function($){var o=$('.rd-navbar');if(o.length>0){$(document).ready(function(){o.RDNavbar({stuckWidth:768,stuckMorph:true,stuckLayout:"rd-navbar-static",responsive:{0:{layout:'rd-navbar-fixed',focusOnHover:false},768:{layout:'rd-navbar-fullwidth'},1200:{layout:o.attr("data-rd-navbar-lg").split(" ")[0],}},onepage:{enable:false,offset:0,speed:400}});});}})(jQuery);;(function($){var o=$('.owl-carousel');if(o.length){var isTouch="ontouchstart"in window;function preventScroll(e){e.preventDefault();}
$(document).ready(function(){o.each(function(){var c=$(this),responsive={};var aliaces=["-","-xs-","-sm-","-md-","-lg-"],values=[0,480,768,992,1200],i,j;for(i=0;i<values.length;i++){responsive[values[i]]={};for(j=i;j>=-1;j--){if(!responsive[values[i]]["items"]&&c.attr("data"+ aliaces[j]+"items")){responsive[values[i]]["items"]=j<0?1:parseInt(c.attr("data"+ aliaces[j]+"items"));}
if(!responsive[values[i]]["stagePadding"]&&responsive[values[i]]["stagePadding"]!==0&&c.attr("data"+ aliaces[j]+"stage-padding")){responsive[values[i]]["stagePadding"]=j<0?0:parseInt(c.attr("data"+ aliaces[j]+"stage-padding"));}
if(!responsive[values[i]]["margin"]&&responsive[values[i]]["margin"]!==0&&c.attr("data"+ aliaces[j]+"margin")){responsive[values[i]]["margin"]=j<0?30:parseInt(c.attr("data"+ aliaces[j]+"margin"));}}}
                                               c.owlCarousel({autoplay:c.attr("data-autoplay")==="true",loop:c.attr("data-loop")!=="false",items:1,mouseDrag:c.attr("data-mouse-drag")!=="false",nav:c.attr("data-nav")==="true",dots:c.attr("data-dots")==="true",dotsEach:c.attr("data-dots-each")?parseInt(c.attr("data-dots-each")):false,responsive:responsive,navText:[],onInitialized:function(){if($.fn.magnificPopup){var o=this.$element.attr('data-lightbox')!==undefined&&this.$element.attr("data-lightbox")!=="gallery",g=this.$element.attr('data-lightbox')==="gallery";if(o){this.$element.each(function(){var $this=$(this);$this.magnificPopup({type:$this.attr("data-lightbox"),callbacks:{open:function(){if(isTouch){$(document).on("touchmove",preventScroll);$(document).swipe({swipeDown:function(){$.magnificPopup.close();}});}},close:function(){if(isTouch){$(document).off("touchmove",preventScroll);$(document).swipe("destroy");}}}});})}
if(g){this.$element.each(function(){var $gallery=$(this);$gallery.find('[data-lightbox]').each(function(){var $item=$(this);$item.addClass("mfp-"+ $item.attr("data-lightbox"));}).end().magnificPopup({delegate:'.owl-item [data-lightbox]',type:"image",gallery:{enabled:true},callbacks:{open:function(){if(isTouch){$(document).on("touchmove",preventScroll);$(document).swipe({swipeDown:function(){$.magnificPopup.close();}});}},close:function(){if(isTouch){$(document).off("touchmove",preventScroll);$(document).swipe("destroy");}}}});})}}}});});});}})(jQuery);;(function($){var o=$('[data-lightbox]').not('[data-lightbox="gallery"] [data-lightbox]'),g=$('[data-lightbox^="gallery"]');if(o.length>0||g.length>0){$(document).ready(function(){if(o.length){o.each(function(){var $this=$(this);$this.magnificPopup({type:$this.attr("data-lightbox")});})}
if(g.length){g.each(function(){var $gallery=$(this);$gallery.find('[data-lightbox]').each(function(){var $item=$(this);$item.addClass("mfp-"+ $item.attr("data-lightbox"));}).end().magnificPopup({delegate:'[data-lightbox]',type:"image",gallery:{enabled:true}});})}});}})(jQuery);

$(document).on('ready',function(){

    if($(window).width() > 991) {
        if($(window).scrollTop() > 400) {
            $(".floating-form").fadeIn();
        }
        else {
            $(".floating-form").fadeOut();
        }
        $(window).on("scroll",function(){
            //alert($(window).height());
            if($(window).scrollTop() > 400) {
                $(".floating-form").fadeIn();
            }
            else {
                $(".floating-form").fadeOut();
            }
        });
    }
    if($(window).scrollTop() + $(window).height() < $(document).height()) {
        $(".ui-to-top").css("bottom",'51px !important');
    }
    else {
        $(".ui-to-top").css("bottom",'0px');
    }
    $(window).on('resize',function(){
        if($(window).width() > 991) {
            if($(window).scrollTop() > 400) {
                $(".floating-form").fadeIn();
            }
            else {
                $(".floating-form").fadeOut();
            }
            $(window).on("scroll",function(){
                //alert($(window).height());
                if($(window).scrollTop() > 400) {
                    $(".floating-form").fadeIn();
                }
                else {
                    $(".floating-form").fadeOut();
                }
            });
        }


    });

    if($(window).width() >= 481) {
        var _scroll = true, _timer = false, _floatbox = $("#contact_form"), _floatbox_opener = $(".contact-opener") ;
        _floatbox.css("right", "-400px"); //initial contact form position

        //Contact form Opener button
        _floatbox_opener.click(function(){
            if (_floatbox.hasClass('visiable')){
                _floatbox.animate({"right":"-400px"}, {duration: 300}).removeClass('visiable');
            }else{
                _floatbox.animate({"right":"-10px"},  {duration: 300}).addClass('visiable');
            }
        });
    }

    if($(window).width() <= 480) {
        var _scroll = true, _timer = false, _floatbox = $("#contact_form"), _floatbox_opener = $(".contact-opener") ;
        _floatbox.css("right", "-300px"); //initial contact form position

        //Contact form Opener button
        _floatbox_opener.click(function(){
            if (_floatbox.hasClass('visiable')){
                _floatbox.animate({"right":"-300px"}, {duration: 300}).removeClass('visiable');
            }else{
                _floatbox.animate({"right":"-10px"},  {duration: 300}).addClass('visiable');
            }
        });
    }
});

$(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url'],input[type='email'],input[type='password'],textarea"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
});

$(document).ready(function(){
    alert("hello");
});